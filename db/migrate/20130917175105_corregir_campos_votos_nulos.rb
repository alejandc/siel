class CorregirCamposVotosNulos < ActiveRecord::Migration
  
	def change
		change_table :resultados_elecciones do |t|
			t.remove :cantidad_votos_nulos
			t.remove :cantidad_votos_en_blanco
			t.boolean :nulos
			t.boolean :en_blanco
		end
	end

end
