class AddComentariosToObservaciones < ActiveRecord::Migration
  def self.up
    add_column :observaciones, :comentarios, :text
  end

  def self.down
    remove_column :observaciones, :comentarios
  end
end
