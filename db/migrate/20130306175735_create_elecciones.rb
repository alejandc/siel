class CreateElecciones < ActiveRecord::Migration
  def change
    create_table :elecciones do |t|
      t.string :descripcion
      t.date :fecha
      t.text :notas
      t.text :observaciones

      t.timestamps
    end
  end
end
