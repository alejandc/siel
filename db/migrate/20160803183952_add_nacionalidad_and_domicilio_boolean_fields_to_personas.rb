class AddNacionalidadAndDomicilioBooleanFieldsToPersonas < ActiveRecord::Migration
  def self.up
    add_column :personas, :nacionalidad_argentina, :boolean
    add_column :personas, :nativo_naturalizado, :boolean
    add_column :personas, :constancia_domicilio, :boolean
  end

  def self.down
    remove_column :personas, :nacionalidad_argentina
    remove_column :personas, :nativo_naturalizado
    remove_column :personas, :constancia_domicilio
  end
end