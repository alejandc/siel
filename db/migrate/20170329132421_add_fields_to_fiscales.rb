class AddFieldsToFiscales < ActiveRecord::Migration
  def change
    add_column :fiscales, :codigo_establecimiento, :integer
    add_column :fiscales, :comuna, :integer
  end
end
