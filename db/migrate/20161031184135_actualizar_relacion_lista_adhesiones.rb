class ActualizarRelacionListaAdhesiones < ActiveRecord::Migration
  def change
    add_column :adhesiones, :lista_cargo_id, :integer
    remove_column :adhesiones, :lista_id, :integer
  end
end
