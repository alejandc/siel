class CreateApoderados < ActiveRecord::Migration
  def change
    create_table :apoderados do |t|
      t.references :fuerza
      t.integer :dni
      t.string :nombre
      t.string :apellido
      t.string :email
      t.string :direccion
      t.string :telefono

      t.timestamps
    end
    add_index :apoderados, :fuerza_id
  end
end
