class AddCamposResultadosToEleccion < ActiveRecord::Migration
  
	def change
  		change_table :elecciones do |t|
  			t.decimal :porcentaje_exclusion
  			t.integer :cantidad_inscriptos
  			t.integer :cantidad_mesas
  		end
  	end

end
