class AddEstadoCandidaturasAndAdhesionesToListaCargos < ActiveRecord::Migration
  def self.up
    rename_column :lista_cargos, :estado_cd, :estado_candidaturas_cd
    add_column :lista_cargos, :estado_adhesiones_cd, :integer
  end

  def self.down
    rename_column :lista_cargos, :estado_candidaturas_cd, :estado_cd
    remove_column :lista_cargos, :estado_adhesiones_cd
  end
end
