class CreateDocumentosFuerzas < ActiveRecord::Migration
  def change
    create_table :documentos_fuerzas do |t|
      t.references :fuerza
      t.references :archivo
      t.string :descripcion

      t.timestamps
    end
    add_index :documentos_fuerzas, :fuerza_id
    add_index :documentos_fuerzas, :archivo_id
  end
end
