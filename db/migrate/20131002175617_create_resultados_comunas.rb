class CreateResultadosComunas < ActiveRecord::Migration
  def change
    create_table :resultados_comunas do |t|
      t.references :eleccion
      t.references :comuna
      t.integer :cantidad_mesas
      t.integer :cantidad_inscriptos

      t.timestamps
    end
    add_index :resultados_comunas, :eleccion_id
    add_index :resultados_comunas, :comuna_id
  end
end
