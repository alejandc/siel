class CreateFuerzaElecciones < ActiveRecord::Migration
  def change
    create_table :fuerza_elecciones do |t|
      t.float   :umbral
      t.integer :fuerza_id
      t.integer :eleccion_id

      t.timestamps null: false
    end
  end
end
