class AddCodigoPostalToDomicilios < ActiveRecord::Migration
	def change
  		change_table :domicilios do |t|
  			t.string :codigo_postal
  		end
  	end
end
