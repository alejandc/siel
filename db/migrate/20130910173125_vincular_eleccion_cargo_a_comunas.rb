class VincularEleccionCargoAComunas < ActiveRecord::Migration

	def change
  		change_table :eleccion_cargos do |t|
  			t.references :comuna
  			t.remove :cantidad_votos_blancos
  			t.remove :cantidad_votos_nulos
  		end
  	end

end
