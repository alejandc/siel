class AddComunasToResultados < ActiveRecord::Migration

	def change
		change_table :resultados_elecciones do |t|
			t.references :comuna
			t.integer :cantidad_votos_en_blanco
			t.integer :cantidad_votos_nulos
		end
	end

end
