class AddOrdenToCargos < ActiveRecord::Migration
	def change
		change_table :cargos do |t|
			t.integer :orden
		end
	end
end
