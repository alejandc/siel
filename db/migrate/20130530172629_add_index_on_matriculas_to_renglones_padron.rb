class AddIndexOnMatriculasToRenglonesPadron < ActiveRecord::Migration
  def change
  	add_index :renglones_padron, :matricula, name: 'renglones_padron_matricula_index'
  end
end
