class AddDescripcionToArchivosPersona < ActiveRecord::Migration
  def change
  	change_table :archivos_persona do |t|
  		t.string :descripcion
  	end
  end
end
