class AddResideEnComunaToCandidaturas < ActiveRecord::Migration
  def change
	change_table :candidaturas do |t|
		t.boolean :reside_en_comuna
	end  	
  end
end
