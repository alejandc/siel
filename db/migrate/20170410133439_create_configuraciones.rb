class CreateConfiguraciones < ActiveRecord::Migration
  def change
    create_table :configuraciones do |t|
      t.boolean :habilitar_renaper_api
      
      t.timestamps null: false
    end
  end
end
