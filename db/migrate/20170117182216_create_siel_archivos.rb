class CreateSielArchivos < ActiveRecord::Migration
  def change
    create_table :siel_archivos do |t|
      t.boolean :activo
      t.integer :archivo_type_cd

      t.references :archivo

      t.timestamps null: false
    end
  end
end
