class AddDescripcionFieldToIncidencia < ActiveRecord::Migration
  def change
    add_column :incidencias, :descripcion, :string
  end
end
