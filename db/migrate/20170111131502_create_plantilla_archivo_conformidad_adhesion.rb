class CreatePlantillaArchivoConformidadAdhesion < ActiveRecord::Migration
  def change
    create_table :plantilla_archivo_conformidad_adhesiones do |t|
      t.integer :archivo_id
      t.boolean :activo

      t.timestamps
    end
  end
end
