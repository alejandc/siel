class CreateAfiliaciones < ActiveRecord::Migration
  def change
    create_table :afiliaciones do |t|

      t.integer :numero_documento
      t.string  :sexo
      t.string  :tipo_documento

      t.references :fuerza
      t.references :eleccion

      t.timestamps null: false
    end

    add_index :afiliaciones, :numero_documento
    add_index :afiliaciones, :fuerza_id
    add_index :afiliaciones, :eleccion_id
  end
end
