class AddEleccionAndCargoReferencesToCandidatura < ActiveRecord::Migration
  def change
    add_reference :candidaturas, :eleccion, index: true
    add_reference :candidaturas, :cargo, index: true
  end
end
