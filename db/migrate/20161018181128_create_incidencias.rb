class CreateIncidencias < ActiveRecord::Migration
  def change
    create_table :incidencias do |t|
      t.string :tipo_documento
      t.integer :numero_documento
      t.string :sexo
      t.string :tipo_incidencia

      t.references :eleccion

      t.timestamps null: false
    end

    add_index :incidencias, :eleccion_id
    add_index :incidencias, :numero_documento
  end
end
