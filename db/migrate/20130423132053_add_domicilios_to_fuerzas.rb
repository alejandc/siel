class AddDomiciliosToFuerzas < ActiveRecord::Migration
  def change
  	change_table :fuerzas do |t|
  		t.integer :domicilio_constituido_id
  		t.integer :domicilio_real_id

  		t.index :domicilio_constituido_id
  		t.index :domicilio_real_id
  	end
  end
end
