class AddVariablePToVerificacionesFirmas < ActiveRecord::Migration
	def change
		change_table :verificaciones_firmas do |t|
			t.float :variable_p
		end
	end
end
