﻿class AddComunas < ActiveRecord::Migration
  def up
  	comunas = {'1' => 'Retiro, San Nicolás, Puerto Madero, San Telmo, Montserrat y Constitución.',
  				'2' => 'Recoleta.',
  				'3' => 'Balvanera y San Cristóbal.',
  				'4' => 'La Boca, Barracas, Parque Patricios, y Nueva Pompeya.',
  				'5' => 'Almagro y Boedo.',
  				'6' => 'Caballito.',
  				'7' => 'Flores y Parque Chacabuco.',
  				'8' => 'Villa Soldati, Villa Riachuelo y Villa Lugano.',
  				'9' => 'Liniers, Mataderos y Parque Avellaneda.',
  				'10' => 'Villa Real, Monte Castro, Versalles, Floresta, Vélez Sarsfield y Villa Luro.',
  				'11' => 'Villa General Mitre, Villa Devoto, Villa del Parque y Villa Santa Rita.',
  				'12' => 'Coghlan, Saavedra, Villa Urquiza y Villa Pueyrredón.',
  				'13' => 'Núñez, Belgrano y Colegiales.',
  				'14' => 'Palermo.',
  				'15' => 'Chacarita, Villa Crespo, La Paternal, Villa Ortúzar, Agronomía y Parque Chas.'}

  	comunas.keys.each do |numero|
  		Comuna.create(numero: numero.to_i, nombre: "Comuna #{numero.to_s}", descripcion: comunas[numero])
  	end
  end

  def down
  	Comuna.delete_all
  end
end
