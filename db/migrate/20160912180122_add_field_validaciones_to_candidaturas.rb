class AddFieldValidacionesToCandidaturas < ActiveRecord::Migration
  def self.up
    add_column :candidaturas, :validaciones, :text
  end

  def self.down
    remove_column :candidaturas, :validaciones
  end
end
