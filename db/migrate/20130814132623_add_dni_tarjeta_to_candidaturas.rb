class AddDniTarjetaToCandidaturas < ActiveRecord::Migration
  def change
  	change_table :candidaturas do |t|
  		t.boolean :dni_tarjeta
  	end
  end
end
