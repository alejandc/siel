class AddEstadoToFuerzas < ActiveRecord::Migration
  
	def change
		change_table :fuerzas do |t|
			t.string :estado
		end
	end
end
