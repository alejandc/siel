class AddNativoCabaToPersonas < ActiveRecord::Migration
  def change
	change_table :personas do |t|
		t.boolean :nativo_caba
	end  	
  end
end
