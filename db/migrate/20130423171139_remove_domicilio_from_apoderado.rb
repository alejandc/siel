class RemoveDomicilioFromApoderado < ActiveRecord::Migration
  def change
  	change_table :apoderados do |t|
  		t.remove :domicilio_id
  	end
  end
end
