class ActualizarRelacionListaCandidaturas < ActiveRecord::Migration
  def change
    add_column :candidaturas, :lista_cargo_id, :integer
    remove_column :candidaturas, :lista_id, :integer
  end
end
