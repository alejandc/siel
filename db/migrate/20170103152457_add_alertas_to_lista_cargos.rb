class AddAlertasToListaCargos < ActiveRecord::Migration
  def self.up
    add_column :lista_cargos, :alertas, :text
  end

  def self.down
    remove_column :lista_cargos, :alertas
  end
end
