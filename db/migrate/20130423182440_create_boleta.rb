class CreateBoleta < ActiveRecord::Migration
  def change
    create_table :boleta do |t|
      t.references :fuerza
      t.references :archivo
      t.references :eleccion

      t.timestamps
    end
    add_index :boleta, :fuerza_id
    add_index :boleta, :archivo_id
    add_index :boleta, :eleccion_id
  end
end
