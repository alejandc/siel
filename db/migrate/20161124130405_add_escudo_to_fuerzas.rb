class AddEscudoToFuerzas < ActiveRecord::Migration
  def self.up
    add_attachment :fuerzas, :escudo
  end

  def self.down
    remove_attachment :fuerzas, :escudo
  end
end
