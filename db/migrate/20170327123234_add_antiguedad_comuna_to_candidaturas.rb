class AddAntiguedadComunaToCandidaturas < ActiveRecord::Migration
  def self.up
    add_column :candidaturas, :antiguedad_comuna, :integer
  end

  def self.down
    remove_column :candidaturas, :antiguedad_comuna
  end
end
