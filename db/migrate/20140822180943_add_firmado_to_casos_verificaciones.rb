class AddFirmadoToCasosVerificaciones < ActiveRecord::Migration
	def change
		change_table :casos_verificacion do |t|
			t.boolean :firmado
		end
	end
end
