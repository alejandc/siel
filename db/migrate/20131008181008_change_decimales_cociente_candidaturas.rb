class ChangeDecimalesCocienteCandidaturas < ActiveRecord::Migration

	def up
	   change_column :candidaturas, :cociente, :decimal, precision: 12, scale: 4
	end

end
