class ChangeColumnaMatriculaFromRenglonesPadron < ActiveRecord::Migration
  def change
  	change_table :renglones_padron do |t|
  		t.remove :matricula
  		t.integer :matricula
  	end
  end
end
