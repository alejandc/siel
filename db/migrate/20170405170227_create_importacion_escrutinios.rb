class CreateImportacionEscrutinios < ActiveRecord::Migration
  def change
    create_table :importacion_escrutinios do |t|

      t.integer :cantidad_registros
      t.integer :eleccion_id
      t.text    :data_info
      t.timestamps null: false
    end
  end
end
