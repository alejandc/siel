class CreateAlianzaPartidos < ActiveRecord::Migration
	def change
		create_table :alianza_partidos do |t|
			t.integer :alianza_id
			t.integer :partido_id
			t.timestamps
		end
		add_index :alianza_partidos, :alianza_id
		add_index :alianza_partidos, :partido_id
	end
end
