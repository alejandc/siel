class AddDuplicadoSexoToCasos < ActiveRecord::Migration
	def change
		change_table :casos_verificacion do |t|
			t.string :sexo
			t.boolean :duplicado
		end
	end
end
