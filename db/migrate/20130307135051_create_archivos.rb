class CreateArchivos < ActiveRecord::Migration
  def change
    create_table :archivos do |t|
      t.string :nombre
      t.string :tipo_contenido

      t.timestamps
    end
  end
end
