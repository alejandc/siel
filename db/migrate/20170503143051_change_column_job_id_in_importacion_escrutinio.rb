class ChangeColumnJobIdInImportacionEscrutinio < ActiveRecord::Migration
  def change
    change_column :importacion_escrutinios, :job_id, :string
  end
end
