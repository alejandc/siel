class AddEnPadronToPersonas < ActiveRecord::Migration
  def self.up
    add_column :personas, :en_padron, :boolean, default: false
  end

  def self.down
    remove_column :personas, :en_padron
  end
end
