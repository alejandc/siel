class CreateEleccionCargos < ActiveRecord::Migration
  def change
    create_table :eleccion_cargos do |t|
      t.references :eleccion
      t.references :cargo
	  t.boolean :seleccionado

      t.timestamps
    end
    add_index :eleccion_cargos, :eleccion_id
    add_index :eleccion_cargos, :cargo_id
  end
end
