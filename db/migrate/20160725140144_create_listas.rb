class CreateListas < ActiveRecord::Migration
  def change
    create_table :listas do |t|
      t.string :nombre
      t.string :estado

      t.references :fuerza
      t.references :eleccion
      
      t.timestamps null: false
    end
  end
end
