class CreateResultadoEscrutinios < ActiveRecord::Migration
  def change
    create_table :resultado_escrutinios do |t|
      t.references :eleccion
      t.references :importacion_escrutinio
      t.integer    :numero_partido
      t.integer    :numero_lista
      t.integer    :total_votos

      t.timestamps null: false
    end
  end
end
