class RemoveDomiciliosFromFuerzas < ActiveRecord::Migration
  def change
  	change_table :fuerzas do |t|
  		t.remove :domicilio_constituido
  		t.remove :domicilio_real
  	end
  end
end
