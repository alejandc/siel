class CreateAdhesiones < ActiveRecord::Migration
  def change
    create_table :adhesiones do |t|
      t.references :lista
      t.references :cargo
      t.references :persona

      t.timestamps null: false
    end

    add_index :adhesiones, :lista_id
    add_index :adhesiones, :persona_id
    add_index :adhesiones, :cargo_id
  end
end
