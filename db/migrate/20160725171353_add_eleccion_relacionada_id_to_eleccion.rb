class AddEleccionRelacionadaIdToEleccion < ActiveRecord::Migration
  def self.up
    add_column :elecciones, :eleccion_relacionada_id, :integer
  end

  def self.down
    remove_column :elecciones, :eleccion_relacionada_id
  end
end
