class CreateCasosVerificacion < ActiveRecord::Migration
  def change
    create_table :casos_verificacion do |t|
      t.integer :hoja
      t.integer :fila
      t.integer :numero_documento
      t.string :nombre
      t.string :apellido
      t.boolean :figura_en_padron
      t.boolean :fila_vacia
      t.references :verificacion_firmas, index: true

      t.timestamps
    end
  end
end
