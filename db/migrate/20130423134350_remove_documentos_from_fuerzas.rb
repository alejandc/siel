class RemoveDocumentosFromFuerzas < ActiveRecord::Migration
  def change
  	change_table :fuerzas do |t|
  		t.remove :plataforma_id
  		t.remove :carta_organica_id
  		t.remove :acta_constitucion_id
  	end
  end
end
