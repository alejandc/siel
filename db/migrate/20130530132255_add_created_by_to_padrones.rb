class AddCreatedByToPadrones < ActiveRecord::Migration
  def change
  	change_table :padrones do |t|
  		t.string :created_by
  	end
  end
end
