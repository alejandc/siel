class AddOficializadaToCandidaturas < ActiveRecord::Migration
  def change
  	change_table :candidaturas do |t|
  		t.boolean :oficializada, default: false
  	end
  end
end
