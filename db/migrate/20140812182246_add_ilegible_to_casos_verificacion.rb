class AddIlegibleToCasosVerificacion < ActiveRecord::Migration
	def change
		change_table :casos_verificacion do |t|
			t.boolean :ilegible
		end
	end
end
