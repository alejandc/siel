class AddListaCargoPasoIdToCandidaturas < ActiveRecord::Migration
  def self.up
    add_column :candidaturas, :lista_cargo_paso_id, :integer
  end

  def self.down
    remove_column :candidaturas, :lista_cargo_paso_id
  end
end
