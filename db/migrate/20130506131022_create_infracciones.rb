class CreateInfracciones < ActiveRecord::Migration
  def change
    create_table :infracciones do |t|
    	t.references :fuerza
      	t.date :fecha
      	t.string :expediente
      	t.string :observaciones

      	t.timestamps
    end
  end
end
