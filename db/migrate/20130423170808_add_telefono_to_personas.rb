class AddTelefonoToPersonas < ActiveRecord::Migration
  def change
  	change_table :personas do |t|
  		t.string :telefono
  		t.string :email
  	end
  end
end
