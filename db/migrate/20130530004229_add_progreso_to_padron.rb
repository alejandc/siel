class AddProgresoToPadron < ActiveRecord::Migration
  def change
  	change_table :padrones do |t|
  		t.integer :cantidad_renglones
  		t.boolean :importacion_completa
  	end
  end
end
