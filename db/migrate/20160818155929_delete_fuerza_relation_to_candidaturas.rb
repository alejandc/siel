class DeleteFuerzaRelationToCandidaturas < ActiveRecord::Migration
  def self.up
    remove_column :candidaturas, :fuerza_id
  end

  def self.down
    add_column :candidaturas, :fuerza_id, :integer
  end
end
