class CreateCandidaturas < ActiveRecord::Migration
  def change
    create_table :candidaturas do |t|
      t.references :persona
      t.references :eleccion_cargo
      t.references :partido

      t.timestamps
    end
    add_index :candidaturas, :persona_id
    add_index :candidaturas, :eleccion_cargo_id
    add_index :candidaturas, :partido_id
  end
end
