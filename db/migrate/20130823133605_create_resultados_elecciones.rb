class CreateResultadosElecciones < ActiveRecord::Migration
  def change
    create_table :resultados_elecciones do |t|
      t.references :eleccion_cargo
      t.references :fuerza
      t.integer :cantidad_votos

      t.timestamps
    end
    add_index :resultados_elecciones, :eleccion_cargo_id
    add_index :resultados_elecciones, :fuerza_id
  end
end
