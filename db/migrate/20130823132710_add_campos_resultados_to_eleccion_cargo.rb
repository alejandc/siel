class AddCamposResultadosToEleccionCargo < ActiveRecord::Migration

	def change
		change_table :eleccion_cargos do |t|
			t.integer :cantidad_escanios
			t.integer :cantidad_votos_blancos
			t.integer :cantidad_votos_nulos
		end
	end

end
