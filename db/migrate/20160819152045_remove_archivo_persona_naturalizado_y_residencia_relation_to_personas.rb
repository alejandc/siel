class RemoveArchivoPersonaNaturalizadoYResidenciaRelationToPersonas < ActiveRecord::Migration
  def self.up
    remove_column :personas, :archivo_naturalizado_id
    remove_column :personas, :archivo_residencia_id
  end

  def self.down
    add_column :personas, :archivo_naturalizado_id, :integer
    add_column :personas, :archivo_residencia_id, :integer
  end
end
