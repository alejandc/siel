class CreateDomicilios < ActiveRecord::Migration
  def change
    create_table :domicilios do |t|
      t.string :calle
      t.string :numero
      t.string :piso
      t.string :depto
      t.string :comuna

      t.timestamps
    end
  end
end
