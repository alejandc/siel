class ChangeFechaFromPadrones < ActiveRecord::Migration
  def change
  	change_table :padrones do |t|
  		t.remove :fecha
  		t.datetime :fecha
  	end
  end
end
