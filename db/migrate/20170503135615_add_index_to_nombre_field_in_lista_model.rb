class AddIndexToNombreFieldInListaModel < ActiveRecord::Migration
  def change
    add_index :listas, :nombre
  end
end
