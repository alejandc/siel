class RemoveCargoReferenceFromAdhesion < ActiveRecord::Migration
  def change
    remove_column :adhesiones, :cargo_id
  end
end
