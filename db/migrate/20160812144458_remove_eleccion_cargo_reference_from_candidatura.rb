class RemoveEleccionCargoReferenceFromCandidatura < ActiveRecord::Migration
  def change
    remove_column :candidaturas, :eleccion_cargo_id, :integer
  end
end
