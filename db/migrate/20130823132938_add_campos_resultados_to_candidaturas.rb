class AddCamposResultadosToCandidaturas < ActiveRecord::Migration

	def change
		change_table :candidaturas do |t|
			t.integer :divisor
			t.decimal :cociente
			t.boolean :tiene_escanio
		end
	end

end
