class AddDenominacionNacionalidadArgentinaYResidenciaToPersonas < ActiveRecord::Migration
  def self.up
    remove_column :personas, :nativo_naturalizado
    remove_column :personas, :constancia_domicilio
    add_column :personas, :denominacion, :string
    change_column :personas, :nacionalidad_argentina, :string
    add_column :personas, :residencia, :string
    add_column :personas, :archivo_naturalizado_id, :integer
    add_column :personas, :archivo_residencia_id, :integer
  end

  def self.down
    add_column :personas, :nativo_naturalizado, :boolean
    add_column :personas, :constancia_domicilio, :boolean
    remove_column :personas, :denominacion
    change_column :personas, :nacionalidad_argentina, :boolean
    remove_column :personas, :nacionalidad_argentina
    remove_column :personas, :residencia
    remove_column :personas, :archivo_naturalizado_id
    remove_column :personas, :archivo_residencia_id
  end
end
