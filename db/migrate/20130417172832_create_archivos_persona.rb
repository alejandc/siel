class CreateArchivosPersona < ActiveRecord::Migration
  def change
    create_table :archivos_persona do |t|
      t.references :persona
      t.references :archivo

      t.timestamps
    end
    add_index :archivos_persona, :persona_id
    add_index :archivos_persona, :archivo_id
  end
end
