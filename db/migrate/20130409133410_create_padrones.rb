class CreatePadrones < ActiveRecord::Migration
  def change
    create_table :padrones do |t|
      t.date :fecha

      t.timestamps
    end
  end
end
