class AddFieldTypoArchivoToArchivosPersona < ActiveRecord::Migration
  def change
    add_column :archivos_persona, :tipo_archivo, :string
  end
end
