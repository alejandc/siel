class AddFieldSwitchToElecciones < ActiveRecord::Migration
  def change
    add_column :elecciones, :habilitar_carga_candidaturas, :boolean
  end
end
