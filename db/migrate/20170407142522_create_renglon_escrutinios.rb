class CreateRenglonEscrutinios < ActiveRecord::Migration
  def change
    create_table :renglon_escrutinios do |t|

      t.integer :cantidad_votos
      t.integer :codigo_cargo
      t.integer :codigo_partido
      t.string  :descripcion_partido
      t.string  :codigo_cargo
      t.string  :descripcion_cargo
      t.integer :codigo_seccion
      t.integer :codigo_circuito
      t.integer :numero_mesa
      t.string  :sexo_mesa
      t.integer :importacion_escrutinio_id
      t.integer :cargo_id
      t.string  :contable_type
      t.integer :contable_id
      t.timestamps null: false
    end
  end
end
