class CreateObservaciones < ActiveRecord::Migration
  def change
    create_table :observaciones do |t|
      t.references :lista_cargo
      t.integer    :observable_id
      t.string     :nombre_candidato
      t.string     :observable_type
      t.string     :referencia_observacion
      t.integer    :estado_cd
    end
  end
end
