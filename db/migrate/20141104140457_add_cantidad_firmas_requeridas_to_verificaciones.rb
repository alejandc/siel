class AddCantidadFirmasRequeridasToVerificaciones < ActiveRecord::Migration
	def change
		change_table :verificaciones_firmas do |t|
			t.integer :cantidad_firmas_requeridas
		end
	end
end
