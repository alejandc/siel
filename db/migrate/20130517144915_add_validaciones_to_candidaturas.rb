class AddValidacionesToCandidaturas < ActiveRecord::Migration
  def change
  	change_table :candidaturas do |t|
  		t.boolean :fotocopia_dni
  		t.boolean :constancia_domicilio
  		t.string :tipo_nacionalidad
  		t.boolean :cargo_dos_periodos
  		t.boolean :aceptacion_firmada
  		t.boolean :deudor_alimentario
  		t.boolean :inhabilitado
  	end
  end
end
