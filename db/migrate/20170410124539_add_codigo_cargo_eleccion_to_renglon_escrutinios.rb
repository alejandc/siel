class AddCodigoCargoEleccionToRenglonEscrutinios < ActiveRecord::Migration
  def self.up
    add_column :renglon_escrutinios, :codigo_cargo_eleccion, :integer
  end

  def self.down
    remove_column :renglon_escrutinios, :codigo_cargo_eleccion
  end
end
