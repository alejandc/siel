class AddFieldDataInfoToListaModel < ActiveRecord::Migration
  def change
    add_column :listas, :data_info, :text
  end
end
