class AddFechaOficializacionToCandidaturas < ActiveRecord::Migration
	def change
  		change_table :candidaturas do |t|
  			t.remove :oficializada
  			t.datetime :fecha_oficializacion
  		end
  	end
end
