class CreateArchivoAceptacionCandidatura < ActiveRecord::Migration
  def change
    create_table :archivo_aceptacion_candidaturas do |t|
      t.integer :archivo_id
      t.boolean :activo

      t.timestamps
    end
  end
end
