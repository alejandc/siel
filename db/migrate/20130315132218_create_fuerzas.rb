class CreateFuerzas < ActiveRecord::Migration
  def change
    create_table :fuerzas do |t|
      t.integer :numero
      t.string :nombre
      t.string :expediente
      t.string :tipo
      t.string :domicilio_constituido
      t.string :domicilio_real
      t.integer :plataforma_id
      t.integer :carta_organica_id
      t.integer :acta_constitucion_id
      t.text :observaciones

      t.timestamps
    end
  end
end
