class AddNumeroToListas < ActiveRecord::Migration
  def self.up
    add_column :listas, :numero, :integer
  end

  def self.down
    remove_column :listas, :numero
  end
end
