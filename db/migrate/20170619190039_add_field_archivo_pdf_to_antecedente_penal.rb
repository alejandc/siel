class AddFieldArchivoPdfToAntecedentePenal < ActiveRecord::Migration
  def change
    add_column :antecedente_penal, :archivo_pdf, :binary, :limit => 10.megabyte
  end
end
