class RemovePartidoAddFuerzaToCandidaturas < ActiveRecord::Migration
  def change
  	change_table :candidaturas do |t|
  		t.remove :partido_id
  		t.references :fuerza
  	end
  end
end
