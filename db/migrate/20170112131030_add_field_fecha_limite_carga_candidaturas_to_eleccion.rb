class AddFieldFechaLimiteCargaCandidaturasToEleccion < ActiveRecord::Migration
  def change
    add_column :elecciones, :fecha_limite_carga_candidaturas, :datetime
  end
end
