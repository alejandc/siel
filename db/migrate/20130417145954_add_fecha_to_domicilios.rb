class AddFechaToDomicilios < ActiveRecord::Migration
  def change
  	change_table :domicilios do |t|
  		t.date :fecha
  	end
  end
end
