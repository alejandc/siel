class AddTipoToElecciones < ActiveRecord::Migration
  def self.up
    add_column :elecciones, :tipo, :string
  end

  def self.down
    remove_column :elecciones, :tipo
  end
end
