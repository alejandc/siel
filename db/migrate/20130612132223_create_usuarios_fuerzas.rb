class CreateUsuariosFuerzas < ActiveRecord::Migration
  def change
    create_table :usuarios_fuerzas do |t|
      t.references :fuerza
      t.references :usuario

      t.timestamps
    end
    add_index :usuarios_fuerzas, :fuerza_id
    add_index :usuarios_fuerzas, :usuario_id
  end
end
