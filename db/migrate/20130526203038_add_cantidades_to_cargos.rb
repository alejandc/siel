class AddCantidadesToCargos < ActiveRecord::Migration
	def change
		change_table :cargos do |t|
			t.integer :cantidad_minima
			t.integer :cantidad_maxima
		end
	end
end
