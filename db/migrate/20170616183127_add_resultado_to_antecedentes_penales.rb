class AddResultadoToAntecedentesPenales < ActiveRecord::Migration
  def self.up
    add_column :antecedente_penal, :resultado, :text
  end

  def self.down
    remove_column :antecedente_penal, :resultado
  end
end
