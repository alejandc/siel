class CreateComunas < ActiveRecord::Migration
  def change
    create_table :comunas do |t|
      t.integer :numero
      t.string :nombre
      t.string :descripcion

      t.timestamps
    end
  end
end
