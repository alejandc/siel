class AddFechaRenunciaToCandidaturas < ActiveRecord::Migration
 def change
	change_table :candidaturas do |t|
		t.datetime :fecha_renuncia
  	end  	
  end
end
