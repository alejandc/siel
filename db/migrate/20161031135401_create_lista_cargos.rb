class CreateListaCargos < ActiveRecord::Migration
  def change
    create_table :lista_cargos do |t|

      t.integer :estado_cd
      t.text :data_info

      t.references :cargo
      t.references :lista

      t.timestamps null: false
    end

    add_index :lista_cargos, :cargo_id
    add_index :lista_cargos, :lista_id
  end
end
