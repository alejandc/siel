class AddWebToFuerzas < ActiveRecord::Migration
  def self.up
    add_column :fuerzas, :web, :string
  end

  def self.down
    remove_column :fuerzas, :web
  end
end
