class AddAdhesionesMultiplesToAdhesiones < ActiveRecord::Migration
  def change
    add_column :adhesiones, :adhesiones_multiples, :boolean, default: false
  end
end
