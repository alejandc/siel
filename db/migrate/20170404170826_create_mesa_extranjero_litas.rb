class CreateMesaExtranjeroLitas < ActiveRecord::Migration
  def change
    create_table :mesa_extranjero_litas do |t|
      t.integer :cantidad_votos_jefe
      t.integer :cantidad_votos_diputados
      t.integer :cantidad_votos_comuna
      
      t.references :lista
      t.references :mesa_extranjero
      
      t.timestamps null: false
    end
  end
end
