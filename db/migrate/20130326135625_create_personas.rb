class CreatePersonas < ActiveRecord::Migration
  def change
    create_table :personas do |t|
      t.string :tipo_documento
      t.integer :numero_documento
      t.string :nombres
      t.string :apellidos
      t.string :sexo
      t.date :fecha_nacimiento
      t.string :lugar_nacimiento
      t.string :nombre_padre
      t.string :nombre_madre
      t.string :estado

      t.timestamps
    end
  end
end
