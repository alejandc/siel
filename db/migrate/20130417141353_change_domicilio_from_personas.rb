class ChangeDomicilioFromPersonas < ActiveRecord::Migration
  def change
  	change_table :personas do |t|
  		t.references :domicilio
  	end
  end
end
