class ChangeColumnNumeroListaForIdListaInResultadoEscrutinios < ActiveRecord::Migration
  def change
    rename_column :resultado_escrutinios, :numero_lista, :lista_id
  end
end
