class AddCargoReferenceFieldToIncidencias < ActiveRecord::Migration
  def change
    add_reference :incidencias, :cargo, foreign_key: true
  end
end
