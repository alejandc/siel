class RemoveCamposFromApoderado < ActiveRecord::Migration
  def change
  	change_table :apoderados do |t|
  		t.remove :dni
  		t.remove :nombre
  		t.remove :apellido
  		t.remove :email
  		t.remove :direccion
  		t.remove :telefono

  		t.references :persona
  		t.references :domicilio
  	end
  end
end
