class AddDescripcionToArchivos < ActiveRecord::Migration
  def change
  	change_table :archivos do |t|
  		t.string :descripcion
  	end
  end
end
