class RemoveCantidadesFromElecciones < ActiveRecord::Migration

	def change
		change_table :elecciones do |t|
			t.remove :cantidad_mesas
			t.remove :cantidad_inscriptos
		end
	end

end
