class AddPersonaAndDomicilioInfoToApoderados < ActiveRecord::Migration
  def change
    add_column    :apoderados, :nombres, :string
    add_column    :apoderados, :apellidos, :string
    add_column    :apoderados, :telefono, :string
    add_column    :apoderados, :email, :string
    add_column    :apoderados, :numero_documento, :integer
    add_column    :apoderados, :calle, :string
    add_column    :apoderados, :numero, :string
    add_column    :apoderados, :piso, :string
    add_column    :apoderados, :depto, :string
    add_column    :apoderados, :comuna, :string
    add_column    :apoderados, :codigo_postal, :string
  end
end
