class AddListaRelationToUser < ActiveRecord::Migration
  def self.up
    add_column :usuarios, :lista_id, :integer
  end

  def self.down
    remove_column :usuarios, :lista_id
  end
end
