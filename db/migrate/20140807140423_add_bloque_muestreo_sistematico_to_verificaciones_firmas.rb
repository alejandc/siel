class AddBloqueMuestreoSistematicoToVerificacionesFirmas < ActiveRecord::Migration
	def change
		change_table :verificaciones_firmas do |t|
			t.integer :bloque_muestreo_sistematico
		end
	end
end
