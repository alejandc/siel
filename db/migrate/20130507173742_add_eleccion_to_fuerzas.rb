class AddEleccionToFuerzas < ActiveRecord::Migration
	def change
		change_table :fuerzas do |t|
			t.references :eleccion
		end
  	end
end
