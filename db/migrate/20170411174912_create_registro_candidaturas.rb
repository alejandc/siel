class CreateRegistroCandidaturas < ActiveRecord::Migration
  def change
    create_table :registro_candidaturas do |t|
      t.integer :numero_documento
      t.string  :sexo
      t.string  :tipo_documento
      t.integer :anio_candidatura

      t.timestamps null: false
    end
  end
end
