class AddFechaInicioSolicitudToAntecedentesPenales < ActiveRecord::Migration
  def self.up
    add_column :antecedente_penal, :fecha_inicio_solicitud, :datetime
  end

  def self.down
    remove_column :antecedente_penal, :fecha_inicio_solicitud
  end
end
