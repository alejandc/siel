class AddArchivoToElecciones < ActiveRecord::Migration
  def change
	change_table :elecciones do |t|
		t.references :archivo
	end
  end
end
