class AddExpedienteToElecciones < ActiveRecord::Migration
  def change
  	change_table :elecciones do |t|
  		t.string :expediente
  	end
  end
end
