class AddPerfilesToUsuarios < ActiveRecord::Migration
  def self.up
    add_column :usuarios, :perfiles, :string
  end

  def self.down
    remove_column :usuarios, :perfiles
  end
end
