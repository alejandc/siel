class UpdateDomicilioModel < ActiveRecord::Migration
  def change
    rename_column :domicilios, :calle, :calle_referencia
    add_column :domicilios, :calle_nombre, :string
  end
end
