class AddDomicilioCompletoToPersonas < ActiveRecord::Migration
  def self.up
    add_column :personas, :domicilio_completo, :string
  end

  def self.down
    remove_column :personas, :domicilio_completo
  end
end
