class CreateContenidoArchivos < ActiveRecord::Migration
  def change
    create_table :contenido_archivos do |t|
      t.references :archivo
      t.binary :contenido, limit: 15.megabyte

      t.timestamps
    end
    add_index :contenido_archivos, :archivo_id
  end
end
