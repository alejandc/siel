class CreateArchivoConformidadAdhesion < ActiveRecord::Migration
  def change
    create_table :archivo_conformidad_adhesiones do |t|
      t.integer :lista_cargo_id
      t.integer :archivo_id

      t.timestamps
    end
  end
end
