class AddObjectChangesToVersions < ActiveRecord::Migration
  
	def change
  		change_table :versions do |t|
  			t.text :object_changes
  		end
  	end
  	
end
