class AddErroresToListasAndListaCargos < ActiveRecord::Migration
  def change
    add_column :listas, :errores, :text
    add_column :lista_cargos, :errores, :text
  end
end
