class CreateFiscales < ActiveRecord::Migration
  def change
    create_table :fiscales do |t|

      t.string  :tipo_documento
      t.integer :numero_documento
      t.string  :sexo

      t.references :fuerza
      t.references :eleccion

      t.timestamps null: false

    end

    add_index :fiscales, :numero_documento
    add_index :fiscales, :fuerza_id
    add_index :fiscales, :eleccion_id
  end
end
