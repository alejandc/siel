class CreateMesaExtranjeros < ActiveRecord::Migration
  def change
    create_table :mesa_extranjeros do |t|
      t.integer :numero_mesa
      t.integer :comuna
      t.integer :total_inscriptos
      
      t.references :eleccion

      t.timestamps null: false
    end
  end
end
