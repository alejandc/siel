class CreateRenglonesPadron < ActiveRecord::Migration
  def change
    create_table :renglones_padron do |t|
      t.references :padron
      t.string :matricula
      t.string :clase
      t.string :apellido
      t.string :nombre
      t.string :profesion
      t.string :domicilio
      t.string :analfabeto
      t.string :tipo_documento
      t.string :seccion
      t.string :circuito
      t.string :mesa
      t.string :sexo
      t.string :desfor

      t.timestamps
    end
    add_index :renglones_padron, :padron_id
  end
end
