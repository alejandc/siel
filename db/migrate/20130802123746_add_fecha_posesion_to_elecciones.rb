class AddFechaPosesionToElecciones < ActiveRecord::Migration

	def change
		change_table :elecciones do |t|
			t.date :fecha_posesion
		end
	end

end
