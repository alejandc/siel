class CreateAntecedentePenal < ActiveRecord::Migration
  def change
    create_table :antecedente_penal do |t|
      
      t.integer :estado_cd
      t.datetime :fecha_ultima_consulta
      t.string :referencia_id
      
      t.references :archivo
      t.references :persona
      
      t.timestamps null: false
    end
  end
end
