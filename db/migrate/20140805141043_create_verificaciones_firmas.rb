class CreateVerificacionesFirmas < ActiveRecord::Migration
  def change
    create_table :verificaciones_firmas do |t|
      t.string :descripcion
      t.integer :cantidad_firmas_total
      t.float :confianza
      t.float :error
      t.text :observaciones
      t.integer :cantidad_firmas_muestra
      t.integer :cantidad_hojas
      t.integer :cantidad_firmas_hoja

      t.timestamps
    end
  end
end
