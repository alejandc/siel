class AddFechaBajaToFuerzas < ActiveRecord::Migration
  def change
  	change_table :fuerzas do |t|
  		t.date :fecha_baja
  	end
  end
end
