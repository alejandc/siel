class AddListaRelationToCandidaturas < ActiveRecord::Migration
  def self.up
    add_column :candidaturas, :lista_id, :integer
  end

  def self.down
    remove_column :candidaturas, :lista_id
  end
end
