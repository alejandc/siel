class RemoveBloqueMuestreoSistematicoFromVerificaciones < ActiveRecord::Migration
	def change
		change_table :verificaciones_firmas do |t|
			t.remove :bloque_muestreo_sistematico
		end
	end
end
