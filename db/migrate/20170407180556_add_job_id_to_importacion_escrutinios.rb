class AddJobIdToImportacionEscrutinios < ActiveRecord::Migration
  def self.up
    add_column :importacion_escrutinios, :job_id, :integer
  end

  def self.down
    remove_column :importacion_escrutinios, :job_id
  end
end
