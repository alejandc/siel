# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

cargos = [{ nombre: 'Jefe/a de Gobierno', orden: 1, cantidad_minima: 1, cantidad_maxima: 1 },
          { nombre: 'Vicejefe', orden: 2, cantidad_minima: 1, cantidad_maxima: 1 },
          { nombre: 'Diputados/as', orden: 3, cantidad_minima: 30, cantidad_maxima: 40 },
          { nombre: 'Junta Comunal 1', orden: 4, cantidad_minima: 7, cantidad_maxima: 11 },
          { nombre: 'Junta Comunal 2', orden: 5, cantidad_minima: 7, cantidad_maxima: 11 },
          { nombre: 'Junta Comunal 3', orden: 6, cantidad_minima: 7, cantidad_maxima: 11 },
          { nombre: 'Junta Comunal 4', orden: 7, cantidad_minima: 7, cantidad_maxima: 11 },
          { nombre: 'Junta Comunal 5', orden: 8, cantidad_minima: 7, cantidad_maxima: 11 },
          { nombre: 'Junta Comunal 6', orden: 9, cantidad_minima: 7, cantidad_maxima: 11 },
          { nombre: 'Junta Comunal 7', orden: 10, cantidad_minima: 7, cantidad_maxima: 11 },
          { nombre: 'Junta Comunal 8', orden: 11, cantidad_minima: 7, cantidad_maxima: 11 },
          { nombre: 'Junta Comunal 9', orden: 12, cantidad_minima: 7, cantidad_maxima: 11 },
          { nombre: 'Junta Comunal 10', orden: 13, cantidad_minima: 7, cantidad_maxima: 11 },
          { nombre: 'Junta Comunal 11', orden: 14, cantidad_minima: 7, cantidad_maxima: 11 },
          { nombre: 'Junta Comunal 12', orden: 15, cantidad_minima: 7, cantidad_maxima: 11 },
          { nombre: 'Junta Comunal 13', orden: 16, cantidad_minima: 7, cantidad_maxima: 11 },
          { nombre: 'Junta Comunal 14', orden: 17, cantidad_minima: 7, cantidad_maxima: 11 },
          { nombre: 'Junta Comunal 15', orden: 18, cantidad_minima: 7, cantidad_maxima: 11 }]

cargos.each do |cargo|
  Cargo.create!(cargo) unless Cargo.exists?(nombre: cargo[:nombre])
end

# FIXME: Se debe borrar de la base de staging y produccion el cargo Comuneros y su
# relación con EleccionCargo...
cargo_comuneros = Cargo.find_by(nombre: 'Comuneros')
if cargo_comuneros
  EleccionCargo.where(cargo_id: cargo_comuneros.id).destroy_all
  cargo_comuneros.destroy
end
