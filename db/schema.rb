# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170621165900) do

  create_table "adhesiones", force: :cascade do |t|
    t.integer  "persona_id",           limit: 4
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.text     "validaciones",         limit: 16777215
    t.integer  "lista_cargo_id",       limit: 4
    t.boolean  "adhesiones_multiples",                  default: false
  end

  add_index "adhesiones", ["persona_id"], name: "index_adhesiones_on_persona_id", using: :btree

  create_table "afiliaciones", force: :cascade do |t|
    t.integer  "numero_documento", limit: 4
    t.string   "sexo",             limit: 255
    t.string   "tipo_documento",   limit: 255
    t.integer  "fuerza_id",        limit: 4
    t.integer  "eleccion_id",      limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "fuerza_codigo",    limit: 4
  end

  add_index "afiliaciones", ["eleccion_id"], name: "index_afiliaciones_on_eleccion_id", using: :btree
  add_index "afiliaciones", ["fuerza_id"], name: "index_afiliaciones_on_fuerza_id", using: :btree
  add_index "afiliaciones", ["numero_documento"], name: "index_afiliaciones_on_numero_documento", using: :btree

  create_table "alianza_partidos", force: :cascade do |t|
    t.integer  "alianza_id", limit: 4
    t.integer  "partido_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "alianza_partidos", ["alianza_id"], name: "index_alianza_partidos_on_alianza_id", using: :btree
  add_index "alianza_partidos", ["partido_id"], name: "index_alianza_partidos_on_partido_id", using: :btree

  create_table "antecedente_penal", force: :cascade do |t|
    t.integer  "estado_cd",              limit: 4
    t.datetime "fecha_ultima_consulta"
    t.string   "referencia_id",          limit: 255
    t.integer  "persona_id",             limit: 4
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.text     "resultado",              limit: 65535
    t.datetime "fecha_inicio_solicitud"
    t.binary   "archivo_pdf",            limit: 16777215
  end

  create_table "apoderados", force: :cascade do |t|
    t.integer  "fuerza_id",        limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "persona_id",       limit: 4
    t.string   "nombres",          limit: 255
    t.string   "apellidos",        limit: 255
    t.string   "telefono",         limit: 255
    t.string   "email",            limit: 255
    t.integer  "numero_documento", limit: 4
    t.string   "calle",            limit: 255
    t.string   "numero",           limit: 255
    t.string   "piso",             limit: 255
    t.string   "depto",            limit: 255
    t.string   "comuna",           limit: 255
    t.string   "codigo_postal",    limit: 255
  end

  add_index "apoderados", ["fuerza_id"], name: "index_apoderados_on_fuerza_id", using: :btree

  create_table "archivo_conformidad_adhesiones", force: :cascade do |t|
    t.integer  "lista_cargo_id", limit: 4
    t.integer  "archivo_id",     limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "archivos", force: :cascade do |t|
    t.string   "nombre",         limit: 255
    t.string   "tipo_contenido", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "descripcion",    limit: 255
  end

  create_table "archivos_persona", force: :cascade do |t|
    t.integer  "persona_id",     limit: 4
    t.integer  "archivo_id",     limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "descripcion",    limit: 255
    t.string   "tipo_archivo",   limit: 255
    t.integer  "lista_cargo_id", limit: 4
  end

  add_index "archivos_persona", ["archivo_id"], name: "index_archivos_persona_on_archivo_id", using: :btree
  add_index "archivos_persona", ["persona_id"], name: "index_archivos_persona_on_persona_id", using: :btree

  create_table "boleta", force: :cascade do |t|
    t.integer  "fuerza_id",   limit: 4
    t.integer  "archivo_id",  limit: 4
    t.integer  "eleccion_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "boleta", ["archivo_id"], name: "index_boleta_on_archivo_id", using: :btree
  add_index "boleta", ["eleccion_id"], name: "index_boleta_on_eleccion_id", using: :btree
  add_index "boleta", ["fuerza_id"], name: "index_boleta_on_fuerza_id", using: :btree

  create_table "candidaturas", force: :cascade do |t|
    t.integer  "persona_id",           limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "orden",                limit: 4
    t.boolean  "fotocopia_dni"
    t.boolean  "constancia_domicilio"
    t.string   "tipo_nacionalidad",    limit: 255
    t.boolean  "cargo_dos_periodos"
    t.boolean  "aceptacion_firmada"
    t.boolean  "deudor_alimentario"
    t.boolean  "inhabilitado"
    t.datetime "fecha_oficializacion"
    t.boolean  "reside_en_comuna"
    t.datetime "fecha_renuncia"
    t.boolean  "dni_tarjeta"
    t.integer  "divisor",              limit: 4
    t.decimal  "cociente",                              precision: 12, scale: 4
    t.boolean  "tiene_escanio"
    t.integer  "eleccion_id",          limit: 4
    t.integer  "cargo_id",             limit: 4
    t.text     "validaciones",         limit: 16777215
    t.integer  "lista_cargo_id",       limit: 4
    t.integer  "antiguedad_persona",   limit: 4
    t.integer  "antiguedad_comuna",    limit: 4
    t.integer  "lista_cargo_paso_id",  limit: 4
  end

  add_index "candidaturas", ["cargo_id"], name: "index_candidaturas_on_cargo_id", using: :btree
  add_index "candidaturas", ["eleccion_id"], name: "index_candidaturas_on_eleccion_id", using: :btree
  add_index "candidaturas", ["persona_id"], name: "index_candidaturas_on_persona_id", using: :btree

  create_table "cargos", force: :cascade do |t|
    t.string   "nombre",          limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "orden",           limit: 4
    t.integer  "cantidad_minima", limit: 4
    t.integer  "cantidad_maxima", limit: 4
  end

  create_table "casos_verificacion", force: :cascade do |t|
    t.integer  "hoja",                   limit: 4
    t.integer  "fila",                   limit: 4
    t.integer  "numero_documento",       limit: 4
    t.string   "nombre",                 limit: 255
    t.string   "apellido",               limit: 255
    t.boolean  "figura_en_padron"
    t.boolean  "fila_vacia"
    t.integer  "verificacion_firmas_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "ilegible"
    t.string   "sexo",                   limit: 255
    t.boolean  "duplicado"
    t.boolean  "firmado"
  end

  add_index "casos_verificacion", ["verificacion_firmas_id"], name: "index_casos_verificacion_on_verificacion_firmas_id", using: :btree

  create_table "comunas", force: :cascade do |t|
    t.integer  "numero",      limit: 4
    t.string   "nombre",      limit: 255
    t.string   "descripcion", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "configuraciones", force: :cascade do |t|
    t.boolean  "habilitar_renaper_api"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.integer  "antiguedad_registro_candidaturas", limit: 4
  end

  create_table "contenido_archivos", force: :cascade do |t|
    t.integer  "archivo_id", limit: 4
    t.binary   "contenido",  limit: 16777215
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "contenido_archivos", ["archivo_id"], name: "index_contenido_archivos_on_archivo_id", using: :btree

  create_table "documentos_fuerzas", force: :cascade do |t|
    t.integer  "fuerza_id",   limit: 4
    t.integer  "archivo_id",  limit: 4
    t.string   "descripcion", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "documentos_fuerzas", ["archivo_id"], name: "index_documentos_fuerzas_on_archivo_id", using: :btree
  add_index "documentos_fuerzas", ["fuerza_id"], name: "index_documentos_fuerzas_on_fuerza_id", using: :btree

  create_table "domicilios", force: :cascade do |t|
    t.string   "calle_referencia", limit: 255
    t.string   "numero",           limit: 255
    t.string   "piso",             limit: 255
    t.string   "depto",            limit: 255
    t.string   "comuna",           limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "fecha"
    t.string   "codigo_postal",    limit: 255
    t.string   "calle_nombre",     limit: 255
    t.integer  "origen_cd",        limit: 4
  end

  create_table "eleccion_cargos", force: :cascade do |t|
    t.integer  "eleccion_id",       limit: 4
    t.integer  "cargo_id",          limit: 4
    t.boolean  "seleccionado"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "cantidad_escanios", limit: 4
    t.integer  "comuna_id",         limit: 4
  end

  add_index "eleccion_cargos", ["cargo_id"], name: "index_eleccion_cargos_on_cargo_id", using: :btree
  add_index "eleccion_cargos", ["eleccion_id"], name: "index_eleccion_cargos_on_eleccion_id", using: :btree

  create_table "elecciones", force: :cascade do |t|
    t.string   "descripcion",                     limit: 255
    t.date     "fecha"
    t.text     "notas",                           limit: 16777215
    t.text     "observaciones",                   limit: 16777215
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "archivo_id",                      limit: 4
    t.string   "expediente",                      limit: 255
    t.date     "fecha_posesion"
    t.decimal  "porcentaje_exclusion",                             precision: 10
    t.string   "tipo",                            limit: 255
    t.integer  "eleccion_relacionada_id",         limit: 4
    t.datetime "fecha_limite_carga_candidaturas"
    t.boolean  "habilitar_carga_candidaturas"
  end

  create_table "fiscales", force: :cascade do |t|
    t.string   "tipo_documento",         limit: 255
    t.integer  "numero_documento",       limit: 4
    t.string   "sexo",                   limit: 255
    t.integer  "fuerza_id",              limit: 4
    t.integer  "eleccion_id",            limit: 4
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "tipo_cd",                limit: 4
    t.integer  "codigo_establecimiento", limit: 4
    t.integer  "comuna",                 limit: 4
  end

  add_index "fiscales", ["eleccion_id"], name: "index_fiscales_on_eleccion_id", using: :btree
  add_index "fiscales", ["fuerza_id"], name: "index_fiscales_on_fuerza_id", using: :btree
  add_index "fiscales", ["numero_documento"], name: "index_fiscales_on_numero_documento", using: :btree

  create_table "fuerza_elecciones", force: :cascade do |t|
    t.float    "umbral",      limit: 24
    t.integer  "fuerza_id",   limit: 4
    t.integer  "eleccion_id", limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "fuerzas", force: :cascade do |t|
    t.integer  "numero",                   limit: 4
    t.string   "nombre",                   limit: 255
    t.string   "expediente",               limit: 255
    t.string   "tipo",                     limit: 255
    t.text     "observaciones",            limit: 65535
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "estado",                   limit: 255
    t.integer  "domicilio_constituido_id", limit: 4
    t.integer  "domicilio_real_id",        limit: 4
    t.date     "fecha_baja"
    t.integer  "eleccion_id",              limit: 4
    t.string   "escudo_file_name",         limit: 255
    t.string   "escudo_content_type",      limit: 255
    t.integer  "escudo_file_size",         limit: 4
    t.datetime "escudo_updated_at"
    t.string   "web",                      limit: 255
  end

  add_index "fuerzas", ["domicilio_constituido_id"], name: "index_fuerzas_on_domicilio_constituido_id", using: :btree
  add_index "fuerzas", ["domicilio_real_id"], name: "index_fuerzas_on_domicilio_real_id", using: :btree

  create_table "importacion_escrutinios", force: :cascade do |t|
    t.integer  "cantidad_registros", limit: 4
    t.integer  "eleccion_id",        limit: 4
    t.text     "data_info",          limit: 65535
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "job_id",             limit: 255
  end

  create_table "incidencias", force: :cascade do |t|
    t.string   "tipo_documento",   limit: 255
    t.integer  "numero_documento", limit: 4
    t.string   "sexo",             limit: 255
    t.string   "tipo_incidencia",  limit: 255
    t.integer  "eleccion_id",      limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "descripcion",      limit: 255
    t.integer  "cargo_id",         limit: 4
  end

  add_index "incidencias", ["cargo_id"], name: "fk_rails_eee1c3831f", using: :btree
  add_index "incidencias", ["eleccion_id"], name: "index_incidencias_on_eleccion_id", using: :btree
  add_index "incidencias", ["numero_documento"], name: "index_incidencias_on_numero_documento", using: :btree

  create_table "infracciones", force: :cascade do |t|
    t.integer  "fuerza_id",     limit: 4
    t.date     "fecha"
    t.string   "expediente",    limit: 255
    t.string   "observaciones", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lista_cargos", force: :cascade do |t|
    t.integer  "estado_candidaturas_cd", limit: 4
    t.text     "data_info",              limit: 16777215
    t.integer  "cargo_id",               limit: 4
    t.integer  "lista_id",               limit: 4
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.integer  "estado_adhesiones_cd",   limit: 4
    t.text     "errores",                limit: 16777215
    t.integer  "candidaturas_count",     limit: 4,        default: 0
    t.integer  "adhesiones_count",       limit: 4,        default: 0
    t.text     "alertas",                limit: 16777215
  end

  add_index "lista_cargos", ["cargo_id"], name: "index_lista_cargos_on_cargo_id", using: :btree
  add_index "lista_cargos", ["lista_id"], name: "index_lista_cargos_on_lista_id", using: :btree

  create_table "listas", force: :cascade do |t|
    t.string   "nombre",      limit: 255
    t.string   "estado",      limit: 255
    t.integer  "fuerza_id",   limit: 4
    t.integer  "eleccion_id", limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.text     "data_info",   limit: 16777215
    t.text     "errores",     limit: 16777215
    t.integer  "numero",      limit: 4
  end

  add_index "listas", ["nombre"], name: "index_listas_on_nombre", using: :btree

  create_table "mesa_extranjero_litas", force: :cascade do |t|
    t.integer  "cantidad_votos_jefe",      limit: 4
    t.integer  "cantidad_votos_diputados", limit: 4
    t.integer  "cantidad_votos_comuna",    limit: 4
    t.integer  "lista_id",                 limit: 4
    t.integer  "mesa_extranjero_id",       limit: 4
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "mesa_extranjeros", force: :cascade do |t|
    t.integer  "numero_mesa",      limit: 4
    t.integer  "comuna",           limit: 4
    t.integer  "total_inscriptos", limit: 4
    t.integer  "eleccion_id",      limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "observaciones", force: :cascade do |t|
    t.integer "lista_cargo_id",         limit: 4
    t.integer "observable_id",          limit: 4
    t.string  "nombre_candidato",       limit: 255
    t.string  "observable_type",        limit: 255
    t.string  "referencia_observacion", limit: 255
    t.integer "estado_cd",              limit: 4
    t.text    "comentarios",            limit: 16777215
  end

  create_table "padrones", force: :cascade do |t|
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.datetime "fecha"
    t.integer  "cantidad_renglones",   limit: 4
    t.boolean  "importacion_completa"
    t.string   "created_by",           limit: 255
  end

  create_table "personas", force: :cascade do |t|
    t.string   "tipo_documento",         limit: 255
    t.integer  "numero_documento",       limit: 4
    t.string   "nombres",                limit: 255
    t.string   "apellidos",              limit: 255
    t.string   "sexo",                   limit: 255
    t.date     "fecha_nacimiento"
    t.string   "lugar_nacimiento",       limit: 255
    t.string   "nombre_padre",           limit: 255
    t.string   "nombre_madre",           limit: 255
    t.string   "estado",                 limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "domicilio_id",           limit: 4
    t.string   "telefono",               limit: 255
    t.string   "email",                  limit: 255
    t.boolean  "nativo_caba"
    t.string   "nacionalidad_argentina", limit: 255
    t.string   "denominacion",           limit: 255
    t.string   "residencia",             limit: 255
    t.string   "domicilio_completo",     limit: 255
    t.string   "avatar_file_name",       limit: 255
    t.string   "avatar_content_type",    limit: 255
    t.integer  "avatar_file_size",       limit: 4
    t.datetime "avatar_updated_at"
    t.boolean  "en_padron",                          default: false
  end

  create_table "registro_candidaturas", force: :cascade do |t|
    t.integer  "numero_documento", limit: 4
    t.string   "sexo",             limit: 255
    t.string   "tipo_documento",   limit: 255
    t.integer  "anio_candidatura", limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "renglon_escrutinios", force: :cascade do |t|
    t.integer  "cantidad_votos",            limit: 4
    t.string   "codigo_cargo",              limit: 255
    t.integer  "codigo_partido",            limit: 4
    t.string   "descripcion_partido",       limit: 255
    t.string   "descripcion_cargo",         limit: 255
    t.integer  "codigo_seccion",            limit: 4
    t.integer  "codigo_circuito",           limit: 4
    t.integer  "numero_mesa",               limit: 4
    t.string   "sexo_mesa",                 limit: 255
    t.integer  "importacion_escrutinio_id", limit: 4
    t.integer  "cargo_id",                  limit: 4
    t.string   "contable_type",             limit: 255
    t.integer  "contable_id",               limit: 4
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "codigo_cargo_eleccion",     limit: 4
  end

  create_table "renglones_padron", force: :cascade do |t|
    t.integer  "padron_id",      limit: 4
    t.string   "clase",          limit: 255
    t.string   "apellido",       limit: 255
    t.string   "nombre",         limit: 255
    t.string   "profesion",      limit: 255
    t.string   "domicilio",      limit: 255
    t.string   "analfabeto",     limit: 255
    t.string   "tipo_documento", limit: 255
    t.string   "seccion",        limit: 255
    t.string   "circuito",       limit: 255
    t.string   "mesa",           limit: 255
    t.string   "sexo",           limit: 255
    t.string   "desfor",         limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "matricula",      limit: 4
  end

  add_index "renglones_padron", ["matricula"], name: "renglones_padron_matricula_index", using: :btree
  add_index "renglones_padron", ["padron_id"], name: "index_renglones_padron_on_padron_id", using: :btree

  create_table "resultado_escrutinios", force: :cascade do |t|
    t.integer  "eleccion_id",               limit: 4
    t.integer  "importacion_escrutinio_id", limit: 4
    t.integer  "numero_partido",            limit: 4
    t.integer  "lista_id",                  limit: 4
    t.integer  "total_votos",               limit: 4
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "codigo_cargo",              limit: 255
  end

  create_table "resultados_comunas", force: :cascade do |t|
    t.integer  "eleccion_id",         limit: 4
    t.integer  "comuna_id",           limit: 4
    t.integer  "cantidad_mesas",      limit: 4
    t.integer  "cantidad_inscriptos", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "resultados_comunas", ["comuna_id"], name: "index_resultados_comunas_on_comuna_id", using: :btree
  add_index "resultados_comunas", ["eleccion_id"], name: "index_resultados_comunas_on_eleccion_id", using: :btree

  create_table "resultados_elecciones", force: :cascade do |t|
    t.integer  "eleccion_cargo_id", limit: 4
    t.integer  "fuerza_id",         limit: 4
    t.integer  "cantidad_votos",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "comuna_id",         limit: 4
    t.boolean  "nulos"
    t.boolean  "en_blanco"
  end

  add_index "resultados_elecciones", ["eleccion_cargo_id"], name: "index_resultados_elecciones_on_eleccion_cargo_id", using: :btree
  add_index "resultados_elecciones", ["fuerza_id"], name: "index_resultados_elecciones_on_fuerza_id", using: :btree

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", limit: 255,      null: false
    t.text     "data",       limit: 16777215
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "siel_archivos", force: :cascade do |t|
    t.boolean  "activo"
    t.integer  "archivo_type_cd", limit: 4
    t.integer  "archivo_id",      limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "usuarios", force: :cascade do |t|
    t.string   "email",           limit: 255
    t.string   "nombre_usuario",  limit: 255
    t.string   "password_digest", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "perfiles",        limit: 255
    t.integer  "lista_id",        limit: 4
    t.integer  "eleccion_id",     limit: 4
    t.boolean  "habilitado",                  default: true
  end

  create_table "usuarios_fuerzas", force: :cascade do |t|
    t.integer  "fuerza_id",  limit: 4
    t.integer  "usuario_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "usuarios_fuerzas", ["fuerza_id"], name: "index_usuarios_fuerzas_on_fuerza_id", using: :btree
  add_index "usuarios_fuerzas", ["usuario_id"], name: "index_usuarios_fuerzas_on_usuario_id", using: :btree

  create_table "verificaciones_firmas", force: :cascade do |t|
    t.string   "descripcion",                limit: 255
    t.integer  "cantidad_firmas_total",      limit: 4
    t.float    "confianza",                  limit: 24
    t.float    "error",                      limit: 24
    t.text     "observaciones",              limit: 16777215
    t.integer  "cantidad_firmas_muestra",    limit: 4
    t.integer  "cantidad_hojas",             limit: 4
    t.integer  "cantidad_firmas_hoja",       limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "variable_p",                 limit: 24
    t.integer  "cantidad_firmas_requeridas", limit: 4
  end

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",      limit: 255,      null: false
    t.integer  "item_id",        limit: 4,        null: false
    t.string   "event",          limit: 255,      null: false
    t.string   "whodunnit",      limit: 255
    t.text     "object",         limit: 16777215
    t.datetime "created_at"
    t.text     "object_changes", limit: 16777215
    t.integer  "lista_cargo_id", limit: 4
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  add_foreign_key "incidencias", "cargos"
end
