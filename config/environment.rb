# Load the rails application
require File.expand_path('../application', __FILE__)

ActionView::Base.field_error_proc = proc do |html_tag, instance|
  if !(html_tag =~ /^<label/ || html_tag =~ /type=\"radio\"/)
    html = Nokogiri::HTML::DocumentFragment.parse(html_tag)
    html = html.at_css('input') || html.at_css('textarea')
    unless html.nil?
      css_class = html['class'] || ''
      html['class'] = css_class.split.push('input_with_error').join(' ')
      html_tag = html.to_s.html_safe
    end

    %(<div class="field_with_errors">#{html_tag}<label for="#{instance.send(:tag_id)}" class="error_message">#{instance.error_message.first.try(:capitalize)}</label></div>).html_safe
  else
    %(<div class="field_with_errors">#{html_tag}</div>).html_safe
  end
end

Excon.defaults[:ssl_verify_peer] = false

MILESTONE_VERSION = 'v2.4.8'
GIT_COMMIT = `git log --pretty=format:'%h' -n 1`
APP_VERSION = (Rails.env.production? ? MILESTONE_VERSION : "#{MILESTONE_VERSION}-#{GIT_COMMIT}")

# Load the General settings before the environments
SETTINGS = YAML.load(ERB.new(File.read("#{Rails.root}/config/settings.yml")).result)[Rails.env]

# Initialize the rails application
Siel::Application.initialize!
