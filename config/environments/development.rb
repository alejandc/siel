Siel::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # No need to load entire app for one request here
  config.eager_load = false

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin

  # Raise exception on mass assignment protection for Active Record models
  config.active_record.mass_assignment_sanitizer = :strict

  # Expands the lines which load the assets
  config.assets.debug = false
  config.assets.compile = true
  config.assets.digest = true

  config.logger = Logger.new(STDOUT)
  config.log_level = :debug

  Sidekiq::Logging.logger = Logger.new(STDOUT)
  Sidekiq::Logging.logger.level = :debug

  config.action_mailer.smtp_settings = { address: 'wks0781' }

  # Grupos del dominio con permisos
  GRUPOS_ACCESO = %w(administrador tsj apoderado lista_partidaria)
  GRUPOS_ADMIN = %w(GGSAO GGINFORMATICA)
  MAPA_GRUPOS_PERFILES = { 'GGINFORMATICA' => 'administrador', 'GGSAO' => 'tsj', 'apoderado' => 'apoderado', 'lista_partidaria' => 'lista_partidaria' }

  # Security web services param
  TSJ_AUTH_WS = {}
  TSJ_AUTH_WS['URL'] = 'https://corews.tsjbaires.gov.ar'
  TSJ_AUTH_WS['exists_response_tag'] = 'ExistsResponse'
  TSJ_AUTH_WS['exists_result_tag'] = 'ExistsResult'
  TSJ_AUTH_WS['auth_response_tag'] = 'AuthenticateResponse'
  TSJ_AUTH_WS['auth_result_tag'] = 'AuthenticateResult'
  TSJ_AUTH_WS['groups_response_tag'] = 'GroupsOfResponse'
  TSJ_AUTH_WS['groups_result_tag'] = 'GroupsOfResult'

  # Web service consulta expedientes
  CON_EXPE_WS_URL = 'http://svrcortazar/ExpedientesOnline/ExpedientesOnline.asmx?WSDL'

  # Parametros importacion de padron
  FILES_PATH = '/home/ajuszczyk/importacion'
  BULK_INSERT_SIZE = 1

  TSJ_SECURITY = TsjSecurity
end
