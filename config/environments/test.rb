Siel::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  config.active_support.test_order = :parallel

  # The test environment is used exclusively to run your application's
  # test suite. You never need to work with it otherwise. Remember that
  # your test database is "scratch space" for the test suite and is wiped
  # and recreated between test runs. Don't rely on the data there!
  config.cache_classes = true

  # No need to load entire app for one request here
  config.eager_load = false

  # Configure static asset server for tests with Cache-Control for performance
  config.serve_static_files = true
  config.static_cache_control = 'public, max-age=3600'

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Raise exceptions instead of rendering exception templates
  config.action_dispatch.show_exceptions = false

  # Disable request forgery protection in test environment
  config.action_controller.allow_forgery_protection = false

  # Tell Action Mailer not to deliver emails to the real world.
  # The :test delivery method accumulates sent emails in the
  # ActionMailer::Base.deliveries array.
  config.action_mailer.delivery_method = :test

  # Raise exception on mass assignment protection for Active Record models
  config.active_record.mass_assignment_sanitizer = :strict

  # Print deprecation notices to the stderr
  config.active_support.deprecation = :stderr

  config.log_level = :debug

  # Grupos del dominio con permisos
  # GRUPOS_ACCESO = ['Usuarios del Tribunal', 'GGSAO', 'GGINFORMATICA']
  GRUPOS_ACCESO = %w(administrador tsj apoderado lista_partidaria)
  GRUPOS_ADMIN = %w(GGSAO GGINFORMATICA)
  MAPA_GRUPOS_PERFILES = { 'GGINFORMATICA' => 'administrador', 'GGSAO' => 'tsj', 'apoderado' => 'apoderado', 'lista_partidaria' => 'lista_partidaria' }

  # Security web services param
  TSJ_AUTH_WS = {}
  TSJ_AUTH_WS['URL'] = 'https://corews.tsjbaires.gov.ar'
  TSJ_AUTH_WS['exists_response_tag'] = 'ExistsResponse'
  TSJ_AUTH_WS['exists_result_tag'] = 'ExistsResult'
  TSJ_AUTH_WS['auth_response_tag'] = 'AuthenticateResponse'
  TSJ_AUTH_WS['auth_result_tag'] = 'AuthenticateResult'
  TSJ_AUTH_WS['groups_response_tag'] = 'GroupsOfResponse'
  TSJ_AUTH_WS['groups_result_tag'] = 'GroupsOfResult'

  # Web service consulta expedientes
  CON_EXPE_WS_URL = 'http://svrcortazar/ExpedientesOnline/ExpedientesOnline.asmx?WSDL'

  # Parametros importacion de padron
  FILES_PATH = '/home/lgallo/importacion'
  BULK_INSERT_SIZE = 1

  TSJ_SECURITY = TsjSecurityMock
end
