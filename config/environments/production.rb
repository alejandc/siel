Siel::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # Code is not reloaded between requests
  config.cache_classes = true

  # To speed up app, load everything right away!
  config.eager_load = true

  # Full error reports are disabled and caching is turned on
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Disable Rails's static asset server (Apache or nginx will already do this)
  config.serve_static_files = true

  # Compress JavaScripts and CSS
  config.assets.js_compressor = :uglifier
  config.assets.css_compressor = :sass

  # Don't fallback to assets pipeline if a precompiled asset is missed
  config.assets.compile = false

  # Generate digests for assets URLs
  config.assets.digest = true

  # Defaults to nil and saved in location specified by config.assets.prefix
  # config.assets.manifest = YOUR_PATH

  # Specifies the header that your server uses for sending files
  # config.action_dispatch.x_sendfile_header = "X-Sendfile" # for apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for nginx

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # See everything in the log (default is :info)
  # config.log_level = :debug

  # Prepend all log lines with the following tags
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different cache store in production
  # config.cache_store = :mem_cache_store

  # Enable serving of images, stylesheets, and JavaScripts from an asset server
  # config.action_controller.asset_host = "http://assets.example.com"

  # Precompile additional assets (application.js, application.css, and all non-JS/CSS are already added)
  config.assets.precompile += %w( *.svg *.eot *.woff *.ttf rails.validations.js listas.js cargos.js elecciones.js fuerzas.js infracciones.js padrones.js personas.js usuarios.js candidaturas.js renglones_padron.js eleccion_cargos.js verificaciones_firmas.js )

  # Disable delivery errors, bad email addresses will be ignored
  # config.action_mailer.raise_delivery_errors = false

  # Enable threaded mode
  # config.threadsafe!

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation can not be found)
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners
  config.active_support.deprecation = :notify

  config.action_mailer.smtp_settings = {
    address: 'svrborges',
    enable_starttls_auto: false
  }

  exception_notification_config = {
    email: {
      email_prefix: '[SIEL PRODUCCION] ',
      sender_address: %('Siel Error Mailer' <siel-error-mailer@tsjbaires.gov.ar>),
      exception_recipients: %w(mcarniello@tsjbaires.gov.ar aclaveir@tsjbaires.gov.ar hkakazu@tsjbaires.gov.ar),
      smtp_settings: {
        address: 'svrborges',
        enable_starttls_auto: false
      }
    }
  }

  # # Error Mailer Configuration
  # Rails.application.config.middleware.use ExceptionNotification::Rack, exception_notification_config

  # # Graylog Config
  # config.logger = GELF::Logger.new('graylog.tsjbaires.gov.ar', 12201, 'WAN', host: 'graylog.tsjbaires.gov.ar', application_name: 'siel')

  # config.lograge.ignore_custom = lambda do |event|
  #   logger_rails = Logger.new(STDOUT)
  #   logger_rails.info(Lograge::Formatters::KeyValue.new.call(event.payload))
  #   true
  # end

  # config.log_level = :info
  # config.logger.level = GELF::INFO

  # GELF::Logger.send :include, ActiveRecord::SessionStore::Extension::LoggerSilencer

  # Log the query plan for queries taking more than this (works
  # with SQLite, MySQL, and PostgreSQL)
  # config.active_record.auto_explain_threshold_in_seconds = 0.5

  # Grupos del dominio con permisos
  GRUPOS_ACCESO = %w(administrador tsj apoderado lista_partidaria)
  GRUPOS_ADMIN = %w(GGSAO GGINFORMATICA)
  MAPA_GRUPOS_PERFILES = { 'GGINFORMATICA' => 'administrador', 'GGSAO' => 'tsj', 'apoderado' => 'apoderado', 'lista_partidaria' => 'lista_partidaria' }

  # Security web services param
  TSJ_AUTH_WS = {}
  TSJ_AUTH_WS['URL'] = 'https://corews.tsjbaires.gov.ar'
  TSJ_AUTH_WS['exists_response_tag'] = 'ExistsResponse'
  TSJ_AUTH_WS['exists_result_tag'] = 'ExistsResult'
  TSJ_AUTH_WS['auth_response_tag'] = 'AuthenticateResponse'
  TSJ_AUTH_WS['auth_result_tag'] = 'AuthenticateResult'
  TSJ_AUTH_WS['groups_response_tag'] = 'GroupsOfResponse'
  TSJ_AUTH_WS['groups_result_tag'] = 'GroupsOfResult'

  # Web service consulta expedientes
  CON_EXPE_WS_URL = 'http://wseo/ExpedientesOnline.asmx?WSDL'

  # Parametros importacion de padron
  FILES_PATH = '/opt/siel/padrones'
  BULK_INSERT_SIZE = 100

  TSJ_SECURITY = TsjSecurity
end
