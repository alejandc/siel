require 'paper_trail'
PaperTrail.config.track_associations = false
PaperTrail::Rails::Engine.eager_load!

module PaperTrail
  class Version < ActiveRecord::Base
    attr_accessible :item_type, :item_id, :event, :whodunnit, :object, :object_changes, :created_at, :updated_at, :lista_cargo_id
    paginates_per 30

    def self.eventos
      %w(create update destroy destroy_all import)
    end

    def self.tipos_lista_cargo
      %w(ListaCargo Candidatura Observacion Adhesion Persona Domicilio ArchivoPersona ArchivoConformidadAdhesion)
    end

    # scope :candidaturas_adhesiones_lista_cargo, -> (lista_cargo_id) do
    #   join_candidaturas = -> (scope) { scope.joins('LEFT JOIN candidaturas c ON c.id = item_id AND item_type = "Candidatura"').joins("JOIN lista_cargos lcc ON lcc.id = c.lista_cargo_id AND lcc.id = #{lista_cargo_id}") }
    #   join_adhesiones = -> (scope) { scope.joins('LEFT JOIN adhesiones a ON a.id = item_id AND item_type = "Adhesion"').joins("JOIN lista_cargos lca ON lca.id = a.lista_cargo_id AND lca.id = #{lista_cargo_id}") }
    #   query = self
    #   query = join_candidaturas.call(query).count > 0 ? join_candidaturas.call(query) : query
    #   query = join_adhesiones.call(query).count > 0 ? join_adhesiones.call(query) : query

    #   query.order('created_at DESC')
    # end

    scope :nombre_usuario, -> (nombre_usuario) { where('whodunnit LIKE ?', "%#{nombre_usuario.downcase}%") }
    scope :event, -> (evento) { where(event: evento) }
    scope :item_type, -> (item_type) { where(item_type: item_type) }
    scope :created_at_gteq, -> (fecha) { where('created_at >= ?', fecha) }
    scope :created_at_lteq, -> (fecha) { where('created_at <= ?', fecha) }
    scope :recientes, -> { reorder('created_at DESC, id DESC') }

    scope :versiones_lista_cargo, -> (lista_cargo_id) {
      where('lista_cargo_id = ? AND item_type IN (?)', lista_cargo_id, tipos_lista_cargo)
        .recientes
      # candidaturas_ids = lista_cargo.candidaturas.pluck(:id)
      # adhesiones_ids = lista_cargo.adhesiones.pluck(:id)
      # personas_ids = lista_cargo.adhesiones.pluck(:persona_id) + lista_cargo.candidaturas.pluck(:persona_id)

      # where('(item_type = ? AND item_id IN (?)) OR
      #       (item_type = ? AND item_id IN (?)) OR
      #       (item_type = ? AND item_id IN (?) AND event <> "create") OR
      #       (lista_cargo_id = ?)',
      #       'Candidatura', candidaturas_ids, 'Adhesion', adhesiones_ids, 'Persona', personas_ids, lista_cargo.id)
      # .order('created_at DESC')
    }
  end

end
