# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format
# (all these examples are active by default):
# ActiveSupport::Inflector.inflections do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
# end
#
# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections do |inflect|
#   inflect.acronym 'RESTful'
# end

ActiveSupport::Inflector.inflections do |inflect|
  inflect.irregular 'eleccion', 'elecciones'
  inflect.irregular 'cargo_eleccion', 'cargos_elecciones'
  inflect.irregular 'padron', 'padrones'
  inflect.irregular 'renglon_padron', 'renglones_padron'
  inflect.irregular 'archivo_persona', 'archivos_persona'
  inflect.irregular 'documento_fuerza', 'documentos_fuerzas'
  inflect.irregular 'infraccion', 'infracciones'
  inflect.irregular 'usuario_fuerza', 'usuarios_fuerzas'
  inflect.irregular 'resultado_eleccion', 'resultados_elecciones'
  inflect.irregular 'resultado_comuna', 'resultados_comunas'
  inflect.irregular 'verificacion_firmas', 'verificaciones_firmas'
  inflect.irregular 'caso_verificacion', 'casos_verificacion'
  inflect.irregular 'lista', 'listas'
  inflect.irregular 'adhesion', 'adhesiones'
  inflect.irregular 'incidencia', 'incidencias'
  inflect.irregular 'lista_cargo', 'lista_cargos'
  inflect.irregular 'afiliacion', 'afiliaciones'
  inflect.irregular 'fiscal', 'fiscales'
  inflect.irregular 'siel_archivo', 'siel_archivos'
  inflect.irregular 'observacion', 'observaciones'
  inflect.irregular 'mesa_extranjero', 'mesa_extranjeros'
  inflect.irregular 'mesa_extranjero_lista', 'mesa_extranjero_litas'
  inflect.irregular 'configuracion', 'configuraciones'
  inflect.irregular 'registro_candidatura', 'registro_candidaturas'
  inflect.irregular 'renglon_escrutinio', 'renglones_escrutinio'
  inflect.irregular 'resultado_escrutinio', 'resultados_escrutinio'
  inflect.irregular 'listas_generales', 'lista_general'
  inflect.irregular 'lista_cargos_generales', 'lista_cargo_general'
  inflect.irregular 'antecedentes_penales', 'antecedente_penal'
end
