require 'sidekiq'
require 'sidekiq/web'
require 'sidekiq-status/web'
require 'sidekiq-status'

Sidekiq.configure_server do |config|
  config.redis = {
    url: SETTINGS[:redis][:url],
    namespace: SETTINGS[:redis][:namespace]
  }

  config.server_middleware do |chain|
    # accepts :expiration (optional)
    chain.add Sidekiq::Status::ServerMiddleware
  end
end

Sidekiq.configure_client do |config|
  config.redis = {
    url: SETTINGS[:redis][:url],
    namespace: SETTINGS[:redis][:namespace]
  }

  config.client_middleware do |chain|
    # accepts :expiration (optional)
    chain.add Sidekiq::Status::ClientMiddleware
  end
end

Sidekiq.default_worker_options = { 'backtrace' => true }

Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
  [user, password] == [SETTINGS[:sidekiq_web][:user], SETTINGS[:sidekiq_web][:passwd]]
end unless Rails.env.development?
