# require 'rvm/capistrano'
require 'bundler/capistrano'
require 'capistrano/ext/multistage'
#require 'capistrano/sidekiq'

# set :rvm_ruby_string, 'ruby-2.3.1p112'
# set :rvm_type, :system

set :stages, %w(production staging)
set :default_stage, 'staging'

set :application, 'SIEL'

set :scm, :git
set :scm_verbose, true
set :use_sudo, false
set :keep_releases, 5

set :deploy_to, '/opt/siel/www'
##### set :repository,  "http://svrdesa/repos/repostsj/electoral/siel/trunk/"
set :repository, 'git@gitlab.tsjbaires.gov.ar:desarrollo/siel.git'
##### set :deploy_via, :copy
##### set :ssh_options, { :forward_agent => true, :port => 4321 }
default_run_options[:pty] = true
set :normalize_asset_timestamps, false

# TSJ - GPS - 20140213 - Por error de version de rake al hacer deploy en svrruby
set :rake, 'bundle exec rake'

set :shared_children, shared_children + %w(public/uploads tmp/import_files tmp/import_files_error)

# Ask for SSH username instead of taking the OS user.
set(:user) do
  Capistrano::CLI.ui.ask 'Give me a ssh user: '
end

namespace :deploy do
  task :restart do
    run "touch #{File.join(current_path, 'tmp', 'restart.txt')}"
  end
end

after 'deploy:update', 'deploy:cleanup'
