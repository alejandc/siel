set :rails_env, 'staging'
set :branch, 'staging'
set :domain, 'svrsieltest'

role :web, domain
role :app, domain
role :db,  domain, primary: true

# Corregir los permisos de los archivos que quedan mal al deployar
after 'deploy' do
  run 'sudo systemctl restart sidekiq.service'
end
after 'deploy:migrations' do
  run 'sudo systemctl restart sidekiq.service'
end

