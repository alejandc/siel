set :rails_env, 'production'
set :branch, 'master'
set :domain, 'svrsiel1'

role :web, domain
role :app, domain
role :db,  domain, primary: true

namespace :db do
  desc 'Create database password in shared path'
  task :password do
    set :db_password, proc { Capistrano::CLI.password_prompt('Remote database password: ') }
    run "mkdir -p #{shared_path}/config"
    put db_password, "#{shared_path}/config/dbpassword"
  end
end

# Precompile assets
after 'deploy:update_code' do
  run "cd #{release_path}; RAILS_ENV=production rake assets:precompile"
end

# Frenar OSync
before 'deploy' do
  run 'service osync-srv@siel.conf stop'
end
before 'deploy:migrations' do
  run 'service osync-srv@siel.conf stop'
end

# Corregir los permisos de los archivos que quedan mal al deployar
after 'deploy' do
  run 'chown -R www-data:www-data /opt/siel/www'
  run 'service osync-srv@siel.conf start'
  run 'systemctl restart sidekiq.service'
end
after 'deploy:migrations' do
  run 'chown -R www-data:www-data /opt/siel/www'
  run 'service osync-srv@siel.conf start'
  run 'systemctl restart sidekiq.service'
end

# Esto evita que la contrasenia de produccion quede escrita en el codigo fuente, SOLO EN PRODUCCION.
before 'deploy:setup', 'db:password'
