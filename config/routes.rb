require 'sidekiq/web'
require 'sidekiq-status/web'

Siel::Application.routes.draw do
  patch 'casos_verificacion/:id/firmar', to: 'casos_verificacion#firmar'
  patch 'casos_verificacion/:id/no_firmar', to: 'casos_verificacion#no_firmar'
  patch 'casos_verificacion/:id/borrar_datos', to: 'casos_verificacion#borrar_datos'

  # resources :verificaciones_firmas do
  #   resources :casos_verificacion
  #   patch :actualizar, on: :collection
  # end

  resources :usuarios do
    get :fuerza, on: :member
    get :find_by_email, on: :collection
    put :blanqueo_clave, on: :member
  end

  get 'usuario/cambiar_contrasenia', to: 'usuarios#cambiar_contrasenia'
  post 'usuario/cambiar_contrasenia', to: 'usuarios#cambiar_contrasenia'

  resources :archivos

  get 'archivos/descargar/:id', to: 'archivos#descargar'

  resources :renglones_padron
  resources :archivo_personas
  resources :archivo_lista_cargos
  resources :cargos

  resources :afiliaciones do
    delete :borrar_afiliaciones, on: :collection
  end

  resources :fiscales do
    delete :destroy_new, on: :member
    delete :borrar_fiscales, on: :collection
    post   :importar_fiscales, on: :collection
    get    :archivo_errores, on: :collection
    get    :exportar, on: :collection
    get    :establecimientos_por_comuna, on: :collection
    get    :cantidad_mesas_establecimiento, on: :collection
  end

  resources :personas do
    get :candidaturas, on: :member
    get :archivos_persona, on: :member
    get :busqueda_persona, on: :collection
    get :busqueda_persona_fiscal, on: :collection
    get :limpiar_formulario, on: :collection
    put :cargar_avatar, on: :collection
    get :ver_adjuntos, on: :member
    get :calcular_comuna, on: :collection
  end

  resources :mesa_extranjeros do
    post :importar_mesas_extranjeros, on: :collection
  end

  resources :importacion_escrutinios, only: [:index, :new, :create, :show, :destroy] do
    member do
      post :calcular_resultados
      get  :reparto_cargos
      post :generar_lista_eleccion_general
      get  :imprimir_reparto_cargos
    end

    collection do
      get :estado_importacion_archivo
    end
  end

  # TODO: ver si hay que borrar, los documento se suben por el controller ArchivoPersonas
  post 'personas/adjuntar_documento', to: 'personas#adjuntar_documento'
  get 'personas/descargar_documento/:archivo_persona_id', to: 'personas#descargar_documento'

  resources :padrones do
    get :renglones_importados, on: :member
  end
  get 'padrones/importar/:index', to: 'padrones#importar'

  resources :candidaturas do
    post :renunciar, on: :member
    get  :cumple_ley_cupos, on: :member
    get  :cumple_minimo, on: :member
    get  :eventos, on: :collection
    post :sugerir_orden, on: :collection
    post :aceptar_correccion_cupo, on: :collection
    post :buscar_por_documento, on: :collection
    put  :aplicar_validaciones, on: :collection
    post :cambiar_orden, on: :collection
  end
  get 'candidaturas/importar/lista', to: 'candidaturas#importar_lista'
  post 'candidaturas/procesar_archivo', to: 'candidaturas#procesar_archivo'
  post 'candidaturas/auto_ordenamiento', to: 'candidaturas#auto_ordenamiento'
  post 'candidaturas/deshacer_ordenamiento', to: 'candidaturas#deshacer_ordenamiento'
  post 'candidaturas/subir', to: 'candidaturas#subir'
  post 'candidaturas/bajar', to: 'candidaturas#bajar'
  post 'candidaturas/filtrar'
  post 'candidaturas/oficializar_lista'
  post 'candidaturas/desoficializar_lista'

  get 'expedientes/buscar'

  resources :fuerzas do
    resources :infracciones
    get    :candidaturas, on: :member
    get    :apoderados, on: :member
    get    :agregar_apoderado, on: :collection
    post   :listado_apoderados, on: :collection
    get    :select_fuerzas, on: :collection
    patch  :editar_escudo
    delete :eliminar_escudo

    member do
      delete :baja
    end
  end

  resources :fuerza_eleccion, only: [:update]

  post 'fuerzas/filtrar'

  resources :elecciones do
    get 'descargar_resolucion'
    get :fuerzas, on: :member
    get :cargos, on: :member
    get :exportar_apoderados, on: :member
    get :actual, on: :collection

    post   :cargar_reparto, on: :member
    post   :grabar_reparto, on: :member
    delete :destroy_reparto, on: :member
    get    :reparto_anexo_1, on: :member
    get    :reparto_anexo_2, on: :member
    get    :reparto_anexo_2_2009, on: :member
    get    :reparto_anexo_3_2009, on: :member
    get    :reparto_anexo_4_2009, on: :member
    get    :reparto_anexo_5_2009, on: :member
  end
  post 'elecciones/filtrar'

  resources :eleccion_cargos
  resources :incidencias do
    delete :borrar_incidencias, on: :collection
  end

  resources :listas do
    resources :lista_cargos do
      member do
        post :borrar_candidaturas
        post :borrar_adhesiones
        post :importar_personas
        get  :estado_importacion_archivo
        get  :descargar_archivo_errores_importacion
        post :validar
        post :cerrar_lista
        post :abrir_lista
        get  :imprimir_candidaturas
        get  :exportar_adhesiones
        get  :descargar_exportacion_adhesiones
        get  :imprimir_auto_ordenamiento
        get  :versiones
        get  :exportar_versiones
        get  :descargar_exportacion_versiones
        get  :observaciones
      end
    end

    member do
      post :validar
      post :oficializar
      post :desoficializar
      get  :imprimir_lista_prototipo
      get  :imprimir_acta_presentacion
      get  :bitacora
      put  :asignar_numero_escrutinio
    end

    collection do
      get :seccion_cargos_habilitados
      get :imprimir_resolucion_oficializacion
      get :imprimir_comunicado_articulo_26
    end
  end

  resources :listas_generales do
    resources :lista_cargos_generales do
      member do
        get  :imprimir_lista_general
      end
    end
  end

  resources :observaciones, only: [:index, :edit, :update] do
    collection do
      post :subsanar
    end
  end

  resources :adhesiones, only: [:index, :create, :destroy]
  resources :configuraciones, only: [:show, :update]
  resources :siel_archivos
  resources :registro_candidaturas do
    delete :borrar_registros_candidaturas, on: :collection
  end

  resources :calles, only: [:index]

  resources :antecedentes_penales do
    member do
      post :enviar_solicitud
      get :descargar_antecedente_penal_pdf
    end
  end

  root to: 'root#index'

  # Rutas del controller admin
  post 'admin/login'
  post 'admin/login_extranet'
  get  'admin/login'
  get  'admin/logout'

  mount Sidekiq::Web => '/sidekiq'
end
