﻿FactoryGirl.define do
  factory :resultados_comunas do
  end

  factory :alianza_partido do
  end

  factory :candidatura do
  end

  factory :resultados_elecciones do
  end

  factory :persona do
  end

  factory :cargo do
  end

  factory :eleccion_cargo do
    seleccionado true
  end

  factory :fuerza do
    nombre 'Partido político'
    numero 1
    association :domicilio_constituido, factory: :domicilio
    tipo 'PARTIDO'
    expediente '0'
  end

  factory :domicilio do
  end

  factory :eleccion do
    descripcion 'Legislativas'
    fecha_posesion '2013-12-10'
  end
end
