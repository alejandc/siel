﻿require 'test_helper'

class FuerzasControllerTest < ActionController::TestCase
  setup do
    @fuerza = fuerzas(:one)
    do_login
  end

  test 'exportar apoderados del registro de partidos' do
    post :listado_apoderados, expediente: '00001/0', format: 'xls'

    contenido_archivo = File.read('test/apoderados_1.xls')

    assert_equal contenido_archivo, @response.body, 'Archivo Excel con apoderados'
  end

  test 'buscar persona por numero_documento' do
    session[:user] = Usuario.create(email: 'email', nombre_usuario: 'usuario', password: 'password', password_confirmation: 'password', perfiles: 'tsj')
    xhr :get, :agregar_apoderado, numero_documento: 202, format: 'js'
    persona = Persona.find_by(numero_documento: 202)

    assert_response :success
    assert_equal persona, assigns(:persona), 'Encontrar la persona con DNI 202'
  end

  test 'should get index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:fuerzas)
  end

  test 'should get new' do
    get :new
    assert_response :success
  end

  test 'no debe ofrecer fuerzas dadas de baja para componer alianza' do
    get :new

    assert !assigns(:partidos).include?(fuerzas(:de_baja)), "La fuerza 'de baja' no se carga porque fue dada de baja"
  end

  test 'should create fuerza' do
    assert_difference('Fuerza.count') do
      post :create, fuerza: { domicilio_constituido_attributes: { calle: 'Cerrito', numero: 777, comuna: 1 }, expediente: @fuerza.expediente, nombre: @fuerza.nombre, numero: @fuerza.numero, observaciones: @fuerza.observaciones, tipo: @fuerza.tipo }
    end

    assert_redirected_to fuerzas_path
  end

  test 'should show fuerza' do
    get :show, id: @fuerza
    assert_response :success
  end

  test 'should get edit' do
    get :edit, id: @fuerza
    assert_response :success
  end

  test 'should update fuerza' do
    put :update, id: @fuerza, fuerza: { expediente: @fuerza.expediente, nombre: @fuerza.nombre, numero: @fuerza.numero, observaciones: @fuerza.observaciones, tipo: @fuerza.tipo }

    assert @fuerza.eleccion.nil?, "No debe tener elección"
    assert_redirected_to fuerzas_path
  end

  test 'should destroy fuerza' do
    assert_difference('Fuerza.count', -1) do
      delete :destroy, id: @fuerza
    end

    assert_redirected_to fuerzas_path
  end
end
