﻿require 'test_helper'

class EleccionesControllerTest < ActionController::TestCase
  setup do
    @eleccion = elecciones(:one)
    do_login
  end

  test 'should get new or edit reparto' do
    get :cargar_reparto, id: @eleccion

    assert_response :success
    assert_not_nil assigns(:eleccion)
    assert_not_nil assigns(:eleccion_cargos)
    assert_not_nil assigns(:fuerzas)
    assert_not_nil assigns(:comunas)
    assert_not_nil assigns(:comuna)
    assert_not_nil assigns(:resultados_comunas)
    assert_equal assigns(:comuna), Comuna.first, 'Debe cargar por defecto la primer comuna'
    assert_select 'input#eleccion_resultados_comunas_attributes_0_cantidad_mesas'
    assert_select 'input#eleccion_resultados_comunas_attributes_5_cantidad_mesas'
  end

  test 'should only update attributes' do
    post :grabar_reparto, id: @eleccion, resultados_comunas_attributes: { '0' => { cantidad_mesas: 10, cantidad_inscriptos: 10 } }

    assert_response 302
  end

  test 'should preserve attributes between tabs' do
    post :cargar_reparto, id: @eleccion, eleccion: { resultados_comunas_attributes: { '0' => { cantidad_mesas: 10, cantidad_inscriptos: 10 } } }

    assert_response :success
  end

  test 'should get repartos' do
    eleccion = create(:eleccion)
    ec_dipu = create(:eleccion_cargo, eleccion: eleccion, cargo: Cargo.find_by(nombre: Cargo::CARGOS[:diputado]))
    ec_comu = create(:eleccion_cargo, eleccion: eleccion, cargo: Cargo.find_by(nombre: Cargo::CARGOS[:comunero]))
    fuerza = create(:fuerza)
    create(:resultado_eleccion, fuerza: fuerza, eleccion_cargo: ec_dipu)
    create(:resultado_eleccion, fuerza: fuerza, eleccion_cargo: ec_comu)

    get :repartos

    assert_response :success
    assert_not_nil assigns(:elecciones)
    assert_equal 1, assigns(:elecciones).size, "Hay una sola elección con resultados"
    assert_not_nil assigns(:elecciones_sin_resultados)
  end

  test 'should get index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:elecciones)
  end

  test 'should filter elecciones from 2013' do
    post(:filtrar, anio: 2013, format: :js)

    assert_equal 2, assigns(:elecciones).size, 'Must retrieve the two elections from 2013'
  end

  test 'should download resolucion' do
    eleccion = elecciones(:one)
    get(:descargar_resolucion, eleccion_id: eleccion.id)

    assert_equal eleccion.archivo.contenido, @response.body, 'Must download the file from the election'
  end

  test 'index should be orderder by year desc' do
    get :index
    elecciones = assigns(:elecciones)

    elecciones.each_cons(2) do |a, b|
      assert ((a.fecha.year > b.fecha.year) or (a.descripcion <= b.descripcion)), 'Order: year DESC, descripcion ASC'
    end
  end

  test 'index table fields' do
    get :index

    assert_select 'table' do
      assert_select 'thead' do
        assert_select 'th', "Año"
        assert_select 'th', "Descripción"
        assert_select 'th', "Resolución"
      end
    end
  end

  test 'should show eleccion' do
    get :show, id: @eleccion
    assert_response :success
  end

  test 'should get new' do
    get :new
    assert_response :success
  end

  test 'should create eleccion' do
    assert_difference -> { Eleccion.count }, 2 do
      post :create, eleccion: { descripcion: @eleccion.descripcion, :'fecha(3i)' => @eleccion.fecha.day, :'fecha(2i)' => @eleccion.fecha.month, :'fecha(1i)' => @eleccion.fecha.year, :'fecha_posesion(3i)' => @eleccion.fecha_posesion.day, :'fecha_posesion(2i)' => @eleccion.fecha.month, :'fecha_posesion(1i)' => @eleccion.fecha.year, notas: @eleccion.notas, observaciones: @eleccion.observaciones }
    end

    paso, general = Eleccion.last(2)

    assert paso.paso?, 'La primera eleccion debe ser paso'
    assert general.general?, 'La segunda eleccion debe ser general'
    assert paso.general == general && general.paso == paso, 'Tienen que estar ambas relacionadas entre si'

    assert_redirected_to elecciones_path
  end

  test 'should get edit' do
    get :edit, id: @eleccion
    assert_response :success
  end

  test 'should update eleccion' do
    put :update, id: @eleccion, eleccion: { descripcion: @eleccion.descripcion, fecha: @eleccion.fecha, notas: @eleccion.notas, observaciones: @eleccion.observaciones }
    assert_redirected_to elecciones_path
  end

  test 'eliminar toda la info cargada para un reparto' do
    delete :destroy_reparto, id: @eleccion

    @eleccion.reload

    assert_redirected_to repartos_elecciones_path
    assert @eleccion.porcentaje_exclusion.nil?, "Borrar la información cargada en la elección"
  end

  test 'should destroy eleccion' do
    assert_difference('Eleccion.count', -1) do
      delete :destroy, id: @eleccion
    end

    assert_redirected_to elecciones_path
  end

end
