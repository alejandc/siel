require 'test_helper'

class EleccionCargosControllerTest < ActionController::TestCase

  setup do
    @eleccion = elecciones(:one)
    @cargo = cargos(:jefe)
    @eleccion_cargo = create(:eleccion_cargo, eleccion: @eleccion, cargo: @cargo)

    f1 = create(:fuerza)
    l1 = f1.listas.create!(nombre: 'ListaPrueba1', eleccion: @eleccion)
    f2 = create(:fuerza)
    l2 = f2.listas.create!(nombre: 'ListaPrueba2', eleccion: @eleccion)
    f3 = create(:fuerza)
    l3 = f3.listas.create!(nombre: 'ListaPrueba3', eleccion: @eleccion)

    cand1 = l1.crear_candidatura(@cargo, personas(:one))
    cand1.update_attributes!(cociente: 50, tiene_escanio: true)

    cand2 = l2.crear_candidatura(@cargo, personas(:two))
    cand2.update_attributes!(cociente: 100, tiene_escanio: true)

    cand3 = l3.crear_candidatura(@cargo, personas(:three))
    cand3.update_attributes!(cociente: 30, tiene_escanio: true)

    create(:resultado_eleccion, eleccion_cargo: @eleccion_cargo, fuerza: f1, cantidad_votos: 50)
    create(:resultado_eleccion, eleccion_cargo: @eleccion_cargo, fuerza: f2, cantidad_votos: 100)
    create(:resultado_eleccion, eleccion_cargo: @eleccion_cargo, fuerza: f3, cantidad_votos: 30)

    do_login
  end

  test 'should show eleccion_cargo' do
    get :show, id: @eleccion_cargo

    assert_response :success
    assert_not_nil assigns(:eleccion_cargo)
    assert_not_nil assigns(:fuerzas)
    assert_not_nil assigns(:candidaturas)
    assert_not_nil assigns(:cargos)

    assert_equal [100, 50, 30], assigns(:candidaturas).map { |c| c.cociente.to_i }, 'El listado inferior (de candidaturas) ordenado por cociente'
  end

end
