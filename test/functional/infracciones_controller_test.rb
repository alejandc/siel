require 'test_helper'

class InfraccionesControllerTest < ActionController::TestCase
  setup do
    @infraccion = infracciones(:one)
    @fuerza = fuerzas(:one)
    do_login
  end

  test 'should get index' do
    get :index, fuerza_id: @fuerza.id
    assert_response :success
    assert_not_nil assigns(:infracciones)
  end

  test 'should get new' do
    get :new, fuerza_id: @fuerza.id
    assert_response :success
  end

  test 'should create infraccion' do
    assert_difference('Infraccion.count') do
      post :create, fuerza_id: @fuerza.id, infraccion: { fuerza_id: @fuerza.id, expediente: @infraccion.expediente, fecha: @infraccion.fecha, observaciones: @infraccion.observaciones }
    end

    assert_redirected_to fuerza_infraccion_path(@fuerza, assigns(:infraccion))
  end

  test 'should show infraccion' do
    get :show, fuerza_id: @fuerza.id, id: @infraccion
    assert_response :success
  end

  test 'should get edit' do
    get :edit, fuerza_id: @fuerza.id, id: @infraccion
    assert_response :success
  end

  test 'should update infraccion' do
    put :update, fuerza_id: @fuerza.id, id: @infraccion, infraccion: { expediente: @infraccion.expediente, fecha: @infraccion.fecha, observaciones: @infraccion.observaciones }
    assert_redirected_to fuerza_infracciones_path(@fuerza)
  end

  test 'should destroy infraccion' do
    assert_difference('Infraccion.count', -1) do
      delete :destroy, fuerza_id: @fuerza.id, id: @infraccion
    end

    assert_redirected_to fuerza_infracciones_path(@fuerza)
  end
end
