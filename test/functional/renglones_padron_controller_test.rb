require 'test_helper'

class RenglonesPadronControllerTest < ActionController::TestCase
  setup do
    @renglon_padron = renglones_padron(:one)
    do_login
  end

  test 'should get index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:renglones_padron)
  end

  test 'should get new' do
    get :new
    assert_response :success
  end

  test 'should create renglon_padron' do
    assert_difference('RenglonPadron.count') do
      post :create, renglon_padron: { analfabeto: @renglon_padron.analfabeto, apellido: @renglon_padron.apellido, circuito: @renglon_padron.circuito, clase: @renglon_padron.clase, desfor: @renglon_padron.desfor, domicilio: @renglon_padron.domicilio, matricula: @renglon_padron.matricula, mesa: @renglon_padron.mesa, nombre: @renglon_padron.nombre, profesion: @renglon_padron.profesion, seccion: @renglon_padron.seccion, sexo: @renglon_padron.sexo, tipo_documento: @renglon_padron.tipo_documento }
    end

    assert_redirected_to renglon_padron_path(assigns(:renglon_padron))
  end

  test 'should show renglon_padron' do
    get :show, id: @renglon_padron
    assert_response :success
  end

  test 'should get edit' do
    get :edit, id: @renglon_padron
    assert_response :success
  end

  test 'should update renglon_padron' do
    put :update, id: @renglon_padron, renglon_padron: { analfabeto: @renglon_padron.analfabeto, apellido: @renglon_padron.apellido, circuito: @renglon_padron.circuito, clase: @renglon_padron.clase, desfor: @renglon_padron.desfor, domicilio: @renglon_padron.domicilio, matricula: @renglon_padron.matricula, mesa: @renglon_padron.mesa, nombre: @renglon_padron.nombre, profesion: @renglon_padron.profesion, seccion: @renglon_padron.seccion, sexo: @renglon_padron.sexo, tipo_documento: @renglon_padron.tipo_documento }
    assert_redirected_to renglon_padron_path(assigns(:renglon_padron))
  end

  test 'should destroy renglon_padron' do
    assert_difference('RenglonPadron.count', -1) do
      delete :destroy, id: @renglon_padron
    end

    assert_redirected_to renglones_padron_path
  end
end
