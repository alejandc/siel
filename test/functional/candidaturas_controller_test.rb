﻿require 'test_helper'

class CandidaturasControllerTest < ActionController::TestCase
  include CandidaturasHelper

  setup do
    @candidatura = candidaturas(:one)
    do_login
  end

  # FIXME: Chequear cuando se agreguen validaciones de candidaturas
  # test 'marcar candidaturas que no figuran en padron y no son nativos CABA' do
  #   doc_valido = '3399887'
  #   doc_invalido = '3399888'
  #   clase_css_destacado = 'linea_destacada'

  #   assert !Persona.exists?(numero_documento: doc_valido)
  #   assert !Persona.exists?(numero_documento: doc_invalido)

  #   eleccion = create(:eleccion)
  #   fuerza = create(:fuerza)
  #   ec = create(:eleccion_cargo, eleccion: eleccion, cargo: cargos(:diputados))

  #   p1 = create(:persona, numero_documento: doc_valido.to_i, nativo_caba: true, nombres: 'p1', apellidos: 'p1', sexo: 'M')
  #   p2 = create(:persona, numero_documento: doc_invalido.to_i, nativo_caba: false, nombres: 'p2', apellidos: 'p2', sexo: 'M')

  #   create(:candidatura, persona: p1, fuerza: fuerza, eleccion_cargo: ec, orden: 1)
  #   create(:candidatura, persona: p2, fuerza: fuerza, eleccion_cargo: ec, orden: 2)

  #   get :importar_lista, eleccion_id: eleccion.id, fuerza_id: fuerza.id

  #   assert_select 'tr' do |trs|
  #     trs.each do |tr|
  #       if tr.to_s.include?(doc_invalido)
  #         assert tr.attributes['class'].value == clase_css_destacado, 'Debe estar destacado con su clase css'
  #         assert tr.to_s.include?('No es nativo CABA y no figura en padrón'), 'Muestra un mensaje con el error'
  #       end

  #       if tr.to_s.include?(doc_valido)
  #         assert tr.attributes['class'].nil? || !tr.attributes['class'].include?(clase_css_destacado), 'No debe estar destacado'
  #       end
  #     end
  #   end
  # end

  test 'precargar archivo sin eleccion, fuerza o archivo' do
    post :procesar_archivo, format: 'js'

    assert_response :success
    assert_equal I18n.t('messages.requeridos_importa_archivo'), flash[:error], 'Debe dar un mensaje de error'
  end

  test 'agregar candidatura con persona no agregada a la lista' do
    eleccion = elecciones(:one)
    lista = listas(:pro)
    cantidad_inicial = lista.candidaturas_eleccion.size
    persona = personas(:one)

    post :create, tab: 'candidaturas', persona_id: persona.id, candidatura: { lista_id: lista.id, eleccion_id: eleccion.id, cargo_id: cargos(:diputados).id, persona_attributes: { id: persona.id } }, format: :js

    assert_response :success
    assert_equal cantidad_inicial + 1, lista.candidaturas_eleccion.size, "Tiene una candidatura más cargada"
  end

  test 'agregar candidatura con persona ya agregada a la lista' do
    eleccion = elecciones(:one)
    lista = listas(:pro)
    cantidad_inicial = lista.candidaturas_eleccion.size
    persona = lista.candidaturas.first.persona

    post :create, tab: 'candidaturas', persona_id: persona.id, candidatura: { lista_id: lista.id, eleccion_id: eleccion.id, cargo_id: cargos(:diputados).id, persona_attributes: { id: persona.id } }, format: :js

    assert_response :success
    assert_equal cantidad_inicial, lista.candidaturas_eleccion.size, "Tiene una candidatura más cargada"
  end

  test 'eliminar candidatura' do
    candidatura = candidaturas(:seventeen_prima2)
    lista = candidatura.lista
    cantidad_inicial = lista.candidaturas.count

    delete :destroy, id: candidatura.id, tab: 'candidaturas', format: :js

    assert_response :success
    assert_equal cantidad_inicial - 1, lista.candidaturas.count, "No se eliminó la candidatura correctamente"
  end

  # FIXME: Chequear lógica cuando se haga el importar precandidatos
  # test 'marcar aquellos que no figuran en el padron' do
  #   get :importar_lista, eleccion_id: elecciones(:one).id, fuerza_id: fuerzas(:one).id

  #   assert_select('td.numero_documento', text: personas(:one).numero_documento.to_s) do |tds|
  #     assert tds.first.to_s.include?('icon-warning-sign'), "Muestra la advertencia de no figurar en padrón"
  #   end

  #   assert_select('td.numero_documento', text: personas(:two).numero_documento.to_s) do |tds|
  #     assert !tds.first.to_s.include?('icon-warning-sign'), "No muestra la advertencia de no figurar en padrón"
  #   end
  # end

  test 'aplicar validaciones sin informacion' do
    put :aplicar_validaciones, validacion: 'cargo_dos_periodos'

    assert_response :success
  end

  # FIXME: Chequear cuando se agreguen validaciones de candidaturas
  # test 'aplicar validaciones en lote' do
  #   ec = eleccion_cargos(:one)
  #   fuerza = fuerzas(:one)
  #   p1 = personas(:one)
  #   p2 = personas(:two)

  #   c1 = create(:candidatura, eleccion_cargo: ec, fuerza: fuerza, persona: p1, cargo_dos_periodos: false)
  #   c2 = create(:candidatura, eleccion_cargo: ec, fuerza: fuerza, persona: p2, cargo_dos_periodos: false)

  #   put :aplicar_validaciones, validacion: 'cargo_dos_periodos', candidaturas: { c1.id => 'selected', c2.id => 'selected' }

  #   assert_response :success
  #   assert_not_nil assigns(:candidaturas), 'Crea la variable donde guarda las candidaturas modificadas'
  #   assert_equal 2, assigns(:candidaturas).size, "Modificó dos candidaturas y las guarda para mostrarlas"

  #   c1db = Candidatura.find(c1.id)
  #   c2db = Candidatura.find(c2.id)

  #   assert c1db.cargo_dos_periodos?, "Se marcó que c1 tiene cargo en los dos periodos"
  #   assert c2db.cargo_dos_periodos?, "Se marcó que c2 tiene cargo en los dos periodos"
  # end

  test "no permitir aplicar si no encontró candidaturas a validar" do
    eleccion_id = elecciones(:completas_2011).id
    validacion = Candidatura::VALIDACIONES[:cargo_dos_periodos]
    numeros_documento = "9999999333\n1"

    post :buscar_por_documento, eleccion_id: eleccion_id, validacion: validacion, numeros_documento: numeros_documento

    assert_response :success
    assert_select 'a#link_aplicar_validaciones', false, 'No se pueden aplicar si no se encontraron candidaturas'
  end

  test 'buscar candidaturas sin documento' do
    eleccion_id = elecciones(:completas_2011).id
    validacion = Candidatura::VALIDACIONES[:cargo_dos_periodos]

    post :buscar_por_documento, eleccion_id: eleccion_id, validacion: validacion

    assert_redirected_to :carga_validaciones_candidaturas
    assert_equal "Debe ingresar al menos un número de documento para realizar la búsqueda.", flash[:error], "Da error porque se buscó sin números de documento"
  end

  # FIXME: Chequear cuando se agreguen validaciones de candidaturas
  # test 'buscar candidaturas para validar' do
  #   eleccion_id = elecciones(:completas_2011).id
  #   validacion = Candidatura::VALIDACIONES[:cargo_dos_periodos]
  #   numeros_documento = "101\n1"
  #   archivo = Rack::Test::UploadedFile.new('test/numeros_documento')

  #   post :buscar_por_documento, eleccion_id: eleccion_id, validacion: validacion, numeros_documento: numeros_documento, archivo: archivo

  #   assert_response :success
  #   assert_not_nil assigns(:candidaturas), 'Crea la variable donde guarda las candidaturas encontradas'
  #   assert_not_nil assigns(:numeros_no_encontrados), 'Crea la variable donde deja los DNIs no encontrados'
  #   assert_equal 5, assigns(:candidaturas).size, 'Hay dos candidaturas que cumplen el criterio indicado'
  #   assert_equal 2, assigns(:numeros_no_encontrados).size, "Hay dos DNIs que no están"
  # end

  test "mostrar página de carga de validaciones" do
    get :carga_validaciones

    assert_response :success
  end

  test 'should get index' do
    get :index

    assert_response :success
    assert assigns(:candidaturas).empty?
  end

  def probar_filtro(key, value, size, format = :html)
    post :filtrar, format: format, key => value
    assert_equal size, assigns(:candidaturas).size, "En total hay #{size} candidaturas con #{key} en #{value}"
  end

  # FIXME: Chequear si se seguirá utilizando el index y filtrar de las candidaturas
  # test 'filtrar por figurar en padron' do
  #   post :filtrar, format: :js
  #   assert_equal 27, assigns(:candidaturas).size, 'En total hay n candidaturas'

  #   probar_filtro(:figura_en_padron, 'true', 7, :js)
  #   probar_filtro(:figura_en_padron, 'false', 20, :js)
  # end

  # FIXME: Chequear si se seguirá utilizando el index y filtrar de las candidaturas
  # test 'filtrar por nativo CABA' do
  #   probar_filtro(:nativo_caba, 'true', 5, :js)
  #   probar_filtro(:nativo_caba, 'false', 22, :js)
  # end

  # FIXME: Chequear si se seguirá utilizando el index y filtrar de las candidaturas
  # test 'filtrar por cargo en dos periodos seguidos' do
  #   probar_filtro(:cargo_dos_periodos, 'true', 1, :js)
  #   probar_filtro(:cargo_dos_periodos, 'false', 26, :js)
  # end

  # FIXME: Chequear si se seguirá utilizando el index y filtrar de las candidaturas
  # test 'filtrar por acredita residencia en CABA' do
  #   probar_filtro(:constancia_domicilio, 'true', 3, :js)
  #   probar_filtro(:constancia_domicilio, 'false', 24, :js)
  # end

  # FIXME: Chequear si se seguirá utilizando el index y filtrar de las candidaturas
  # test 'filtrar por DNI tarjeta' do
  #   probar_filtro(:dni_tarjeta, 'true', 2, :js)
  #   probar_filtro(:dni_tarjeta, 'false', 25, :js)
  # end

  # FIXME: Chequear si se seguirá utilizando el index y filtrar de las candidaturas
  # test 'should filter candidaturas' do
  #   get :index
  #   assert_response :success

  # FIXME: Chequear si se seguirá utilizando el index y filtrar de las candidaturas
  #   post :filtrar, format: :js, post: { cargo: cargos(:jefe).id }
  #   assert_not_nil assigns(:candidaturas)
  # end

  # FIXME: Chequear cuando se agregue el importar listas
  # test 'no incluir fuerzas dadas de baja en combo' do
  #   get :importar_lista

  #   assert_response :success
  #   assert_not_nil assigns(:fuerzas)
  #   assert !assigns(:fuerzas).include?(fuerzas(:de_baja))
  # end

  test 'should display sexo' do
    persona = build(:persona)
    assert_equal '-', letra_sexo(persona), "Si el sexo está vacío mostrar un guión"

    persona.sexo = 'f'
    assert_equal 'F', letra_sexo(persona), "Convierte la letra en mayúscula si tiene sexo"
  end

  # FIXME: Chequear lógica cuando se haga el importar precandidatos
  # test "shouldn't break on empty fields" do
  #   eleccion = create(:eleccion)
  #   ec = create(:eleccion_cargo, eleccion: eleccion, cargo: Cargo.find_by(nombre: Cargo::CARGOS[:diputado]))
  #   fuerza = create(:fuerza)
  #   p1 = create(:persona)
  #   create(:candidatura, eleccion_cargo: ec, fuerza: fuerza, persona: p1, orden: 1)

  #   get :importar_lista, eleccion_id: eleccion.id, fuerza_id: fuerza.id

  #   assert_response :success
  # end

  # FIXME: Chequear cuando se agreguen validaciones de candidaturas
  # test 'debe advertir otra candidatura existente' do
  #   post :agregar_candidatura, fuerza_id: fuerzas(:ucd).id, eleccion_id: elecciones(:one).id, cargo_id: cargos(:diputados).id, numero_documento: 1, format: :js

  #   assert_equal "MyString MyString ya se ha postulado para esta elección con Primera alianza y Primera fuerza", flash[:warning], "Avisa con qué partidos ya está postulado"
  # end

  # FIXME: Chequear si volverá a existir el new de la candidatura
  # test 'should get new' do
  #   get :new
  #   assert_response :success
  # end

  # FIXME: Chequear si aplica, ya que hay otros tests para agregar candidaturas
  # test 'should create candidatura' do
  #   assert_difference('Candidatura.count') do
  #     post :create, candidatura: {}
  #   end

  #   assert_redirected_to candidatura_path(assigns(:candidatura))
  # end

  test 'should show candidatura' do
    get :show, id: @candidatura
    assert_response :success
  end

  test 'should get edit' do
    get :edit, id: @candidatura

    assert_response :success
    assert_select 'input#candidatura_dni_tarjeta', 1
  end

  test 'should update candidatura' do
    candidatura = candidaturas(:ucd_2011_diputado_1)
    put :update, id: candidatura, candidatura: { dni_tarjeta: true }

    assert_redirected_to candidaturas_importar_lista_path

    candidatura = Candidatura.find(candidatura.id)

    assert candidatura.dni_tarjeta?, "Se marcó que presenta DNI tarjeta"
  end

end
