require 'test_helper'

class PersonasControllerTest < ActionController::TestCase

  setup do
    do_login
  end

  test 'busqueda persona existente' do
    eleccion = elecciones(:one)
    cargo = Cargo.first
    lista = listas(:pro)

    request = post :busqueda_persona, tab: 'candidaturas', eleccion_id: eleccion.id, cargo_id: cargo.id, lista_id: lista.id, persona: { tipo_documento: 'DNI', numero_documento: '555', sexo: 'M' }, format: :js

    assert_response :success
    assert_equal 'La persona se ha encontrado en el padrón', flash[:notice], 'Da error porque no encontró la persona en el padrón'
    assert_equal Nokogiri::HTML.parse(request.body.to_s.delete('\\')).css('#persona_id').first['value'].to_i, Persona.last.id
  end

  test 'busqueda persona inexistente en candidaturas' do
    eleccion = elecciones(:one)
    cargo = Cargo.first
    lista = listas(:pro)

    post :busqueda_persona, tab: 'candidaturas', eleccion_id: eleccion.id, cargo_id: cargo.id, lista_id: lista.id, persona: { tipo_documento: 'DNI', numero_documento: '37281721', sexo: 'M' }, format: :js

    assert_response :success
    assert_equal 'La persona no figura en el padrón, para continuar es necesario completar los campos del formulario y adjuntar la documentación respaldatoria.', flash[:sweet_alert_error], 'Da error porque encontró la persona en el padrón'
  end

  test 'busqueda persona inexistente en adhesiones' do
    eleccion = elecciones(:one)
    cargo = Cargo.first
    lista = listas(:pro)

    post :busqueda_persona, tab: 'adhesiones', eleccion_id: eleccion.id, cargo_id: cargo.id, lista_id: lista.id, persona: { tipo_documento: 'DNI', numero_documento: '37281721', sexo: 'M' }, format: :js

    assert_response :success
    assert_equal 'La persona buscada no existe en el padrón', flash[:error], 'Da error porque encontró la persona en el padrón'
  end
end
