﻿require 'test_helper'

class UsuariosControllerTest < ActionController::TestCase
  setup do
    @usuario = usuarios(:usu1)
    do_login
  end

  test 'find_by_email' do
    get :find_by_email, format: :json, email: 'usu1@usuarios.com'

    assert_equal Usuario.find_by(email: 'usu1@usuarios.com').to_json, @response.body, 'Debe devolver el usuario 1'
  end

  test 'blanqueo de clave' do
    usuario = Usuario.create!(email: 'email@email.com', nombre_usuario: 'usuario', password: 'password', password_confirmation: 'password', perfiles: 'apoderado')

    assert_difference 'ActionMailer::Base.deliveries.size', +1 do
      put :blanqueo_clave, id: usuario.id
    end

    mail = ActionMailer::Base.deliveries.last

    assert_equal "SIEL - Su contraseña se ha cambiado con éxito", mail.subject
    assert_equal 'email@email.com', mail.to[0]

    assert_redirected_to usuarios_path
    assert_equal 'La contraseña se blanqueó con éxito.', flash[:notice]
  end

  test 'should get index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:usuarios)
  end

  test 'should get new' do
    get :new
    assert_response :success
  end

  test 'should create usuario apoderado con fuerza' do
    assert_difference('Usuario.count') do
      post :create, usuario: { email: "#{@usuario.email}1", nombre_usuario: "#{@usuario.nombre_usuario}1", password: 'aaa', password_confirmation: 'aaa', perfiles: 'apoderado', usuarios_fuerzas_attributes: [fuerza_id: Fuerza.first.id] }
    end

    assert_redirected_to usuarios_path
  end

  test 'should create usuario_lista con fuerza y lista' do
    fuerza = Fuerza.first
    fuerza.listas.build(nombre: 'lista', eleccion: Eleccion.first).save

    assert_difference('Usuario.count') do
      post :create, usuario: { email: "#{@usuario.email}1", nombre_usuario: "#{@usuario.nombre_usuario}1", password: 'aaa', password_confirmation: 'aaa', perfiles: 'lista_partidaria', usuarios_fuerzas_attributes: [fuerza_id: Fuerza.first.id], lista_id: fuerza.listas.first }
    end

    assert_redirected_to usuarios_path
  end

  test 'should show usuario' do
    get :show, id: @usuario
    assert_response :success
  end

  test 'should get edit' do
    get :edit, id: @usuario
    assert_response :success
  end

  test 'should update usuario' do
    usuario = Usuario.create!(email: 'email', nombre_usuario: 'usuario', password: 'password', password_confirmation: 'password', perfiles: 'apoderado')
    put :update, id: usuario, usuario: { email: usuario.email, nombre_usuario: usuario.nombre_usuario, password_digest: usuario.password_digest }
    assert_redirected_to usuarios_path
  end

  test 'should destroy usuario' do
    assert_difference('Usuario.count', -1) do
      delete :destroy, id: @usuario
    end

    assert_redirected_to usuarios_path
  end

  test 'debería tener errores el formulario al cambiar la contraseña con la actual incorrecta' do
    usuario = Usuario.create(email: 'email', nombre_usuario: 'usuario', password: 'password', password_confirmation: 'password', perfiles: 'apoderado')
    session[:user] = usuario
    post :cambiar_contrasenia, usuario: { old_password: 'password_incorrecta', new_password: 'nueva_password' }
    assert assert_select('span.rojo').text, 'La contraseña actual es incorrecta'
  end

  test 'should cambiar_contrasenia usuario' do
    usuario = Usuario.create(email: 'email', nombre_usuario: 'usuario', password: 'password', password_confirmation: 'password', perfiles: 'apoderado')
    session[:user] = usuario
    session[:user_groups] = 'apoderado'
    post :cambiar_contrasenia, usuario: { old_password: 'password', new_password: 'nueva_password' }
    assert_equal 'La contraseña ha sido modificada de manera exitosa', flash[:notice]
  end
end
