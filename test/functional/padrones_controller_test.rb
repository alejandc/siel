require 'test_helper'

class PadronesControllerTest < ActionController::TestCase
  setup do
    @padron = padrones(:one)
    do_login
  end

  test 'should get index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:padrones)
  end

  test 'should get new' do
    get :new
    assert_response :success
  end

  test 'should create padron' do
    assert_difference('Padron.count') do
      post :create, padron: { fecha: @padron.fecha }
    end

    assert_redirected_to padron_path(assigns(:padron))
  end

  test 'should show padron' do
    get :show, id: @padron
    assert_response :success
  end

  test 'should get edit' do
    get :edit, id: @padron
    assert_response :success
  end

  test 'should update padron' do
    put :update, id: @padron, padron: { fecha: @padron.fecha }
    assert_redirected_to padron_path(assigns(:padron))
  end

  test 'should destroy padron' do
    assert_difference('Padron.count', -1) do
      delete :destroy, id: @padron
    end

    assert_redirected_to padrones_path
  end
end
