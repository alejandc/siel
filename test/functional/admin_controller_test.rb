require 'test_helper'

class AdminControllerTest < ActionController::TestCase

  setup do
    @usuario = usuarios(:usuario1) # usuario1 123siel
  end

  test 'should login with admin user' do
    post :login, username: 'usuario_mock', password: '123siel'
    assert_redirected_to root_path
    assert_equal nil, flash[:notice]
  end

  test 'should login with normal user' do
    post :login, username: 'usuario1', password: '123siel'
    assert_redirected_to root_path
    assert_equal nil, flash[:notice]
  end

  test "shouldn't log with wrong user" do
    post :login, username: 'usuario_incorrecto', password: '123siel'
    assert_equal I18n.t('messages.wrong_login'), flash[:notice]
  end

end
