# == Schema Information
#
# Table name: lista_cargos
#
#  id                     :integer          not null, primary key
#  estado_candidaturas_cd :integer
#  data_info              :text(16777215)
#  cargo_id               :integer
#  lista_id               :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  estado_adhesiones_cd   :integer
#  errores                :text(16777215)
#  candidaturas_count     :integer          default(0)
#  adhesiones_count       :integer          default(0)
#  alertas                :text(16777215)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :lista_cargo do
  end
end
