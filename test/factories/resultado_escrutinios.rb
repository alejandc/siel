# == Schema Information
#
# Table name: resultado_escrutinios
#
#  id                        :integer          not null, primary key
#  eleccion_id               :integer
#  importacion_escrutinio_id :integer
#  numero_partido            :integer
#  lista_id                  :integer
#  total_votos               :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  codigo_cargo              :string(255)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :resultado_escrutinio do
  end
end
