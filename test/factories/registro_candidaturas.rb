# == Schema Information
#
# Table name: registro_candidaturas
#
#  id               :integer          not null, primary key
#  numero_documento :integer
#  sexo             :string(255)
#  tipo_documento   :string(255)
#  anio_candidatura :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :registro_candidatura do
  end
end
