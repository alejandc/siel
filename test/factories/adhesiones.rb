# == Schema Information
#
# Table name: adhesiones
#
#  id                   :integer          not null, primary key
#  persona_id           :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  validaciones         :text(16777215)
#  lista_cargo_id       :integer
#  adhesiones_multiples :boolean          default(FALSE)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :adhesion do
  end
end
