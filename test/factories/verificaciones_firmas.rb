# == Schema Information
#
# Table name: verificaciones_firmas
#
#  id                         :integer          not null, primary key
#  descripcion                :string(255)
#  cantidad_firmas_total      :integer
#  confianza                  :float(24)
#  error                      :float(24)
#  observaciones              :text(16777215)
#  cantidad_firmas_muestra    :integer
#  cantidad_hojas             :integer
#  cantidad_firmas_hoja       :integer
#  created_at                 :datetime
#  updated_at                 :datetime
#  variable_p                 :float(24)
#  cantidad_firmas_requeridas :integer
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :verificacion_firmas do
    descripcion 'MyString'
    cantidad_firmas_total 1
    confianza 1.5
    error 1.5
    observaciones 'MyText'
    cantidad_firmas_muestra 1
    cantidad_hojas 1
    cantidad_firmas_hoja 1
  end
end
