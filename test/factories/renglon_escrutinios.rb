# == Schema Information
#
# Table name: renglon_escrutinios
#
#  id                        :integer          not null, primary key
#  cantidad_votos            :integer
#  codigo_cargo              :string(255)
#  codigo_partido            :integer
#  descripcion_partido       :string(255)
#  descripcion_cargo         :string(255)
#  codigo_seccion            :integer
#  codigo_circuito           :integer
#  numero_mesa               :integer
#  sexo_mesa                 :string(255)
#  importacion_escrutinio_id :integer
#  cargo_id                  :integer
#  contable_type             :string(255)
#  contable_id               :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  codigo_cargo_eleccion     :integer
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :renglon_escrutinio do
  end
end
