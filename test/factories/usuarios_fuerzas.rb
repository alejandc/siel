# == Schema Information
#
# Table name: usuarios_fuerzas
#
#  id         :integer          not null, primary key
#  fuerza_id  :integer
#  usuario_id :integer
#  created_at :datetime
#  updated_at :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :usuario_fuerza do
    fuerza nil
    usuario nil
  end
end
