# == Schema Information
#
# Table name: resultados_elecciones
#
#  id                :integer          not null, primary key
#  eleccion_cargo_id :integer
#  fuerza_id         :integer
#  cantidad_votos    :integer
#  created_at        :datetime
#  updated_at        :datetime
#  comuna_id         :integer
#  nulos             :boolean
#  en_blanco         :boolean
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :resultado_eleccion do
    eleccion_cargo nil
    fuerza nil
    cantidad_votos 1
  end
end
