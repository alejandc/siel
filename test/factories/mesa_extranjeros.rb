# == Schema Information
#
# Table name: mesa_extranjeros
#
#  id               :integer          not null, primary key
#  numero_mesa      :integer
#  comuna           :integer
#  total_inscriptos :integer
#  eleccion_id      :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :mesa_extranjero do
  end
end
