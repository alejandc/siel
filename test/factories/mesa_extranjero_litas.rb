# == Schema Information
#
# Table name: mesa_extranjero_litas
#
#  id                       :integer          not null, primary key
#  cantidad_votos_jefe      :integer
#  cantidad_votos_diputados :integer
#  cantidad_votos_comuna    :integer
#  lista_id                 :integer
#  mesa_extranjero_id       :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :mesa_extranjero_lista do
  end
end
