# == Schema Information
#
# Table name: resultados_comunas
#
#  id                  :integer          not null, primary key
#  eleccion_id         :integer
#  comuna_id           :integer
#  cantidad_mesas      :integer
#  cantidad_inscriptos :integer
#  created_at          :datetime
#  updated_at          :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :resultado_comuna do
    eleccion nil
    comuna nil
    cantidad_mesas 1
    cantidad_inscriptos 1
  end
end
