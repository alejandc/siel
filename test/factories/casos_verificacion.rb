# == Schema Information
#
# Table name: casos_verificacion
#
#  id                     :integer          not null, primary key
#  hoja                   :integer
#  fila                   :integer
#  numero_documento       :integer
#  nombre                 :string(255)
#  apellido               :string(255)
#  figura_en_padron       :boolean
#  fila_vacia             :boolean
#  verificacion_firmas_id :integer
#  created_at             :datetime
#  updated_at             :datetime
#  ilegible               :boolean
#  sexo                   :string(255)
#  duplicado              :boolean
#  firmado                :boolean
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :caso_verificacion do
    hoja 1
    fila 1
    numero_documento 1
    nombre 'MyString'
    apellido 'MyString'
    figura_en_padron false
    fila_vacia false
    verificacion_firmas nil
  end
end
