# == Schema Information
#
# Table name: listas
#
#  id          :integer          not null, primary key
#  nombre      :string(255)
#  estado      :string(255)
#  fuerza_id   :integer
#  eleccion_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  data_info   :text(16777215)
#  errores     :text(16777215)
#  numero      :integer
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :lista do
  end
end
