# == Schema Information
#
# Table name: configuraciones
#
#  id                               :integer          not null, primary key
#  habilitar_renaper_api            :boolean
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  antiguedad_registro_candidaturas :integer
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :configuracion do
  end
end
