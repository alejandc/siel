# == Schema Information
#
# Table name: importacion_escrutinios
#
#  id                 :integer          not null, primary key
#  cantidad_registros :integer
#  eleccion_id        :integer
#  data_info          :text(65535)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  job_id             :string(255)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :importacion_escrutinio do
  end
end
