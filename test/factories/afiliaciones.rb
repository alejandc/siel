# == Schema Information
#
# Table name: afiliaciones
#
#  id               :integer          not null, primary key
#  numero_documento :integer
#  sexo             :string(255)
#  tipo_documento   :string(255)
#  fuerza_id        :integer
#  eleccion_id      :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  fuerza_codigo    :integer
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :afiliacion do
  end
end
