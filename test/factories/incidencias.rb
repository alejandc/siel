# == Schema Information
#
# Table name: incidencias
#
#  id               :integer          not null, primary key
#  tipo_documento   :string(255)
#  numero_documento :integer
#  sexo             :string(255)
#  tipo_incidencia  :string(255)
#  eleccion_id      :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  descripcion      :string(255)
#  cargo_id         :integer
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :incidencia do
  end
end
