# == Schema Information
#
# Table name: usuarios
#
#  id              :integer          not null, primary key
#  email           :string(255)
#  nombre_usuario  :string(255)
#  password_digest :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  perfiles        :string(255)
#  lista_id        :integer
#  eleccion_id     :integer
#  habilitado      :boolean          default(TRUE)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :usuario do
    email 'MyString'
    nombre_usuario 'MyString'
    password_digest 'MyString'
  end
end
