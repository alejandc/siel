# == Schema Information
#
# Table name: siel_archivos
#
#  id              :integer          not null, primary key
#  activo          :boolean
#  archivo_type_cd :integer
#  archivo_id      :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :siel_archivo do
  end
end
