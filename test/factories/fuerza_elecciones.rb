# == Schema Information
#
# Table name: fuerza_elecciones
#
#  id          :integer          not null, primary key
#  umbral      :float(24)
#  fuerza_id   :integer
#  eleccion_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :fuerza_eleccion do
  end
end
