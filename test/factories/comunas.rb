# == Schema Information
#
# Table name: comunas
#
#  id          :integer          not null, primary key
#  numero      :integer
#  nombre      :string(255)
#  descripcion :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :comuna do
    numero 1
    nombre 'MyString'
    descripcion 'MyString'
  end
end
