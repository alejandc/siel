# == Schema Information
#
# Table name: verificaciones_firmas
#
#  id                         :integer          not null, primary key
#  descripcion                :string(255)
#  cantidad_firmas_total      :integer
#  confianza                  :float(24)
#  error                      :float(24)
#  observaciones              :text(16777215)
#  cantidad_firmas_muestra    :integer
#  cantidad_hojas             :integer
#  cantidad_firmas_hoja       :integer
#  created_at                 :datetime
#  updated_at                 :datetime
#  variable_p                 :float(24)
#  cantidad_firmas_requeridas :integer
#

require 'test_helper'

class VerificacionFirmasTest < ActiveSupport::TestCase

  test 'validar error en valores posibles' do
    v = verificaciones_firmas(:one)
    v.error = 5.0
    assert v.valid?, 'El 5% es válido'
    v.error = 2.5
    assert v.valid?, 'El 2.5% es válido'
    v.error = 3.0
    assert_not v.valid?, 'El 3% no es válido'
  end

  test "errores relativos de estimación válidos" do
    assert_equal 2, VerificacionFirmas::ERRORES_RELATIVOS.size, 'Hay dos errores válidos'
    assert VerificacionFirmas::ERRORES_RELATIVOS.include?(5.0)
    assert VerificacionFirmas::ERRORES_RELATIVOS.include?(2.5)
  end

  test "error de estimación de la proporción de firmas legítimas válidas" do
    assert_equal 2, VerificacionFirmas::VALORES_P.size, 'Hay dos valores válidos'
    assert VerificacionFirmas::VALORES_P.include?(0.8)
    assert VerificacionFirmas::VALORES_P.include?(0.9)
  end

  test "coeficientes de confiabilidad válidos" do
    assert_equal 1.645, VerificacionFirmas::COEF_CONFIABILIDAD[90.0]
    assert_equal 1.96, VerificacionFirmas::COEF_CONFIABILIDAD[95.0]
    assert_equal 2.58, VerificacionFirmas::COEF_CONFIABILIDAD[99.0]
  end

  test 'rango_confianza' do
    v = VerificacionFirmas.new
    v.cantidad_firmas_total = 1000
    v.confianza = 90.0
    v.cantidad_firmas_muestra = 107
    90.times { v.casos_verificacion << CasoVerificacion.new(figura_en_padron: true) }

    assert_equal 786, v.rango_confianza[0], 'Cota inferior del intervalo de confianza'
    assert_equal 896, v.rango_confianza[1], 'Cota superior del intervalo de confianza'
  end

  test 'error_de_estimacion' do
    v = VerificacionFirmas.new
    v.expects(:cantidad_firmas_total).returns(1000.0)
    v.expects(:cantidad_firmas_muestra).returns(107.0)
    v.expects(:proporcion_firmas_validas).returns(0.8411)

    assert_equal 33.55510189113615, v.error_de_estimacion, 'El error de estimación en base a las firmas válidas'
  end

  test 'proporcion_firmas_validas con filas vacias' do
    v = VerificacionFirmas.new(cantidad_firmas_muestra: 2)
    2.times { v.casos_verificacion.build }
    v.casos_verificacion.build(fila_vacia: true)

    assert_equal 1, v.proporcion_firmas_validas, 'Todas son válidas, la fila vacía no cuenta'
  end

  test 'proporcion_firmas_validas' do
    verif = VerificacionFirmas.new(cantidad_firmas_muestra: 100)
    { 30 => true, 70 => false }.each do |k, v|
      k.times do
        c = CasoVerificacion.new
        c.expects(:invalido?).returns(v)
        verif.casos_verificacion << c
      end
    end

    assert_equal 0.7, verif.proporcion_firmas_validas, 'Hay un 0.7 de firmas válidas'
  end

  test 'casos_validos' do
    v = VerificacionFirmas.new
    [false, false, false, true, true].each do |valido|
      caso = CasoVerificacion.new
      caso.expects(:invalido?).returns(valido)
      v.casos_verificacion << caso
    end

    assert_equal 3, v.casos_validos.size, 'Hay 3 válidos y 2 inválidos'
  end

  test 'casos_cargados' do
    v = VerificacionFirmas.new
    v.casos_verificacion << CasoVerificacion.new
    v.casos_verificacion << CasoVerificacion.new(numero_documento: 7)
    v.casos_verificacion << CasoVerificacion.new(fila_vacia: true)

    assert_equal 2, v.casos_cargados.size
  end

  test 'casos_a_firmar' do
    v = VerificacionFirmas.new
    v.casos_verificacion << CasoVerificacion.new(fila_vacia: true)
    v.casos_verificacion << CasoVerificacion.new
    v.casos_verificacion << CasoVerificacion.new(numero_documento: 7)
    v.casos_verificacion << CasoVerificacion.new(ilegible: true)
    v.casos_verificacion << CasoVerificacion.new(firmado: true)

    assert_equal 2, v.casos_a_firmar.size, 'Solamente hay dos casos que tienen que firmar'
  end

  test 'casos_a_cargar' do
    v = VerificacionFirmas.new
    v.casos_verificacion << CasoVerificacion.new(hoja: 10, fila: 7)
    v.casos_verificacion << CasoVerificacion.new(hoja: 10, fila: 1)
    v.casos_verificacion << CasoVerificacion.new(hoja: 4, fila: 4, numero_documento: 7)
    v.casos_verificacion << CasoVerificacion.new(hoja: 3, fila: 3, fila_vacia: true)
    v.casos_verificacion << CasoVerificacion.new(hoja: 2, fila: 2, ilegible: true)

    assert_equal 2, v.casos_a_cargar.size
  end

  test 'proximo_caso_a_cargar' do
    v = VerificacionFirmas.new
    v.casos_verificacion << CasoVerificacion.new(hoja: 10, fila: 7)
    v.casos_verificacion << CasoVerificacion.new(hoja: 10, fila: 1)
    v.casos_verificacion << CasoVerificacion.new(hoja: 4, fila: 4, numero_documento: 7)
    v.casos_verificacion << CasoVerificacion.new(hoja: 3, fila: 3, fila_vacia: true)
    v.casos_verificacion << CasoVerificacion.new(hoja: 2, fila: 2, ilegible: true)
    v.casos_verificacion << primero = CasoVerificacion.new(hoja: 5, fila: 15)

    assert_equal primero, v.proximo_caso_a_cargar
  end

  test 'generar_caso_adicional' do
    v = VerificacionFirmas.new(cantidad_hojas: 50, cantidad_firmas_hoja: 12, cantidad_firmas_muestra: 112)
    v.generar_casos

    coord_orig = v.casos_verificacion.map { |caso| [caso.hoja, caso.fila] }
    adicional = v.generar_caso_adicional

    assert_equal v.cantidad_firmas_muestra + 1, v.casos_verificacion.size, 'Hay un caso más de lo indicado'
    assert_not coord_orig.include?([adicional.hoja, adicional.fila]), 'No se generó un caso duplicado'
  end

  test 'generar_casos' do
    v = VerificacionFirmas.new(cantidad_hojas: 50, cantidad_firmas_hoja: 12, cantidad_firmas_muestra: 112)
    coordenadas = v.generar_coordenadas_muestra
    v.generar_casos

    assert_equal coordenadas.size, v.casos_verificacion.size
  end

  test 'generar_coordenadas_muestra' do
    v = VerificacionFirmas.new(cantidad_hojas: 50, cantidad_firmas_hoja: 20, cantidad_firmas_muestra: 209)
    # Coordenadas de toda la población en un sólo array
    coord_poblacion = v.generar_coordenadas_poblacion
    coord_muestra = v.generar_coordenadas_muestra # (lo mismo pero sólo para la muestra)

    assert_equal v.cantidad_firmas_muestra, coord_muestra.size, 'Genera la cantidad de items necesarios'

    # Tomar la ubicación en el array general de la muestra
    indices_muestra = coord_muestra.map { |cm| coord_poblacion.index(cm) }
    distancia_muestra = (coord_poblacion.size / v.cantidad_firmas_muestra).floor

    indices_muestra.each_cons(2) do |indices|
      distancia = (indices[1] - indices[0]).abs
      assert (distancia == distancia_muestra || distancia == (coord_poblacion.size - distancia_muestra)), 'Debe haber la misma distancia entre todos los elementos'
    end
  end

  test 'generar_coordenadas_poblacion' do
    v = VerificacionFirmas.new(cantidad_hojas: 4, cantidad_firmas_hoja: 3)
    items = [[1, 1], [1, 2], [1, 3], [2, 1], [2, 2], [2, 3], [3, 1], [3, 2], [3, 3], [4, 1], [4, 2], [4, 3]]
    assert_equal items, v.generar_coordenadas_poblacion
  end

  test 'calculo cantidad firmas muestra' do
    v = VerificacionFirmas.new(confianza: 90.0, variable_p: 0.8, error: 5.0, cantidad_firmas_total: 300_000)
    v.calcular_cantidad_firmas_muestra
    assert_equal 269, v.cantidad_firmas_muestra, "El tamaño de la muestra para este caso particular"
  end

  test "calculo tamaño muestra" do
    v = VerificacionFirmas.new(confianza: 90.0, variable_p: 0.8, error: 5.0)

    assert_equal 270, v.calcular_tamanio_muestra, "El cálculo principal de tamaño de muestra"
  end

  test "cálculo de las variables" do
    v = VerificacionFirmas.new(confianza: 90, error: 1)

    assert_equal 1.645, v.variable_t, 'La confianza para el 90%'
    assert_equal 0.01, v.variable_e, 'El coeficiente de error'
  end
end
