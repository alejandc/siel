# == Schema Information
#
# Table name: listas
#
#  id          :integer          not null, primary key
#  nombre      :string(255)
#  estado      :string(255)
#  fuerza_id   :integer
#  eleccion_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  data_info   :text(16777215)
#  errores     :text(16777215)
#  numero      :integer
#

require 'test_helper'

class ListaTest < ActiveSupport::TestCase
  test 'cantidad de listas en base' do
    assert_equal 5, Lista.count
  end

  test 'creacion de listas validas' do
    lista = Lista.new(nombre: 'ListaPrueba1', fuerza: Fuerza.first, eleccion: Eleccion.first)
    assert_equal true, lista.valid?
  end

  test 'atributos requeridos en listas' do
    lista = Lista.new
    lista.save

    assert_equal false, lista.valid?
    assert_includes lista.errors, :nombre
    assert_includes lista.errors, :fuerza_id
    assert_includes lista.errors, :eleccion_id
  end

  test 'validar nombre unico en listas' do
    lista = Lista.new(nombre: 'ListaPrueba1', fuerza: Fuerza.first, eleccion: Eleccion.first)
    lista.save
    lista_copia = lista.dup
    lista_copia.save

    assert_equal false, lista_copia.valid?
    assert_includes lista_copia.errors, :nombre
  end

  test 'validar estado incorrecto de lista' do
    lista = Lista.new(nombre: 'ListaPrueba1', fuerza: Fuerza.first, eleccion: Eleccion.first)
    lista.save
    lista.estado = 'ESTADO INVALIDO'

    assert_equal false, lista.valid?
    assert_includes lista.errors, :estado
  end

  test 'crear y eliminar candidatos/adhesiones' do
    lista = Lista.new(nombre: 'ListaPrueba1', fuerza: Fuerza.first, eleccion: Eleccion.first)
    lista.save
    cargo = cargos(:jefe)

    lista.crear_candidatura(cargo, personas(:one)).save
    lista.agregar_adhesion(cargo, personas(:two)).save

    assert_equal 1, lista.candidaturas.count
    assert_equal 1, lista.adhesiones.count

    lista.borrar_adhesiones(cargo)
    lista.borrar_candidaturas(cargo)

    assert_equal 0, lista.candidaturas.count
    assert_equal 0, lista.adhesiones.count
  end

  test 'posee candidato?' do
    eleccion = elecciones(:one)
    fuerza = fuerzas(:one)
    lista = fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)
    persona = personas(:one)
    cargo = cargos(:jefe)

    assert_equal false, lista.posee_candidato?(persona)

    Candidatura.create(eleccion: eleccion, cargo: cargo, persona: persona, lista: lista)

    assert_equal true, lista.posee_candidato?(persona)
  end

  test 'maximo_orden jefe y vicejefe' do
    create(:domicilio)
    eleccion = create(:eleccion, descripcion: 'Elección de prueba')
    jefe = Cargo.find_by(nombre: Cargo::CARGOS[:jefe])
    vice = Cargo.find_by(nombre: Cargo::CARGOS[:vicejefe])
    create(:eleccion_cargo, cargo: jefe, eleccion: eleccion)
    create(:eleccion_cargo, cargo: vice, eleccion: eleccion)
    fuerza = create(:fuerza)
    lista = fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)

    assert_equal 0, lista.maximo_orden(jefe), "Todavía no tiene candidaturas"

    persona1 = personas(:four)
    persona2 = personas(:two)
    persona3 = personas(:three)
    persona4 = personas(:four)

    lista.crear_candidatura(jefe, persona1).save
    lista.crear_candidatura(jefe, persona2).save
    lista.crear_candidatura(vice, persona3).save
    lista.crear_candidatura(vice, persona4).save

    assert_equal 1, lista.maximo_orden(jefe), 'Hay más de un jefe'
    assert_equal 1, lista.maximo_orden(vice), 'Hay más de un vicejefe'
  end

  test 'cumple_maximo?' do
    comu = cargos(:comuna_1)
    eleccion = create(:eleccion)
    create(:eleccion_cargo, eleccion: eleccion, cargo: comu)
    fuerza = create(:fuerza)
    lista = fuerza.listas.create(nombre: 'ListaPrueba1', eleccion: eleccion)
    personas = (1..10).map { |i| Persona.find_by(numero_documento: i) || create(:persona, nombres: 'Nombres', apellidos: 'Apellidos', numero_documento: i, sexo: 'F', en_padron: true) }

    11.times { |i| lista.crear_candidatura(comu, personas[i]).save }

    ultima_candidatura = lista.candidaturas.cargo(comu).last
    assert lista.cumple_maximo?(ultima_candidatura.orden, comu), 'Con 10 comuneros no alcanza'

    ultima_candidatura = lista.crear_candidatura(comu, create(:persona, nombres: 'Nombres', apellidos: 'Apellidos', numero_documento: 11, sexo: 'F', en_padron: true))
    ultima_candidatura.validate

    assert !lista.cumple_maximo?(ultima_candidatura.orden, comu), 'Con 11 si alcanza'
  end

  test 'agregar_candidatura' do
    cargo = cargos(:diputados)
    lista = listas(:one)

    maximo_anterior = lista.maximo_orden(cargo)
    candidatura = lista.crear_candidatura(cargo, personas(:one))
    candidatura.save

    assert_equal (maximo_anterior + 1), candidatura.orden, 'Debe haberse agregado al final'
  end

  # FIXME: falta purificar para que aplique el test...
  test 'obtener candidaturas por eleccion' do
    eleccion = create(:eleccion)
    fuerza = create(:fuerza)
    comu = cargos(:comuna_1)
    create(:eleccion_cargo, eleccion: eleccion, cargo: comu)

    lista = fuerza.listas.create(nombre: 'ListaPrueba1', eleccion: eleccion)

    candidaturas = []

    4.times do |i|
      candidatura = lista.crear_candidatura(comu, create(:persona, nombres: "Nombres#{i}", apellidos: "Apellidos#{i}", numero_documento: 4444444 + i, sexo: 'M', en_padron: true))
      candidatura.save
      candidaturas << candidatura
    end

    candidaturas[1].borrar
    candidaturas[2].renunciar Date.today

    assert_equal 2, lista.candidaturas_eleccion.size, 'Debe tener dos candidaturas'
  end

  test 'cerrar!' do
    lista = Lista.new
    lista.save(validate: false)
    estado_anterior = lista.estado

    begin
      lista.cerrar!
    rescue
      lista.save(validate: false)
    end

    assert_equal estado_anterior, lista.estado, 'La lista estaba validada'

    lista.estado = Lista::ESTADOS[:VALIDA]
    lista.save(validate: false)

    begin
      lista.cerrar!
    rescue
      lista.save(validate: false)
    end

    assert_equal Lista::ESTADOS[:CERRADA], lista.estado, 'La lista no estaba validada'
  end

  # test 'asignar_escanio' do
  #   lista  listas(:one)
  #   ec = create(:eleccion_cargo, eleccion: create(:eleccion), cargo: Cargo.find_by(nombre: Cargo::CARGOS[:diputado]))
  #   f = create(:fuerza)
  #   (1..3).each_with_index { |i| lista.candidaturas << create(:candidatura, eleccion_cargo: ec, fuerza: f, orden: i) }

  #   create(:resultado_eleccion, eleccion_cargo: ec, cantidad_votos: 100, fuerza: f)
  #   ec.candidaturas[0].tiene_escanio = true
  #   ec.candidaturas[0].save

  #   f.divisor = 2
  #   f.calcular_cociente(ec)
  #   f.save

  #   ganador = f.asignar_escanio(ec)

  #   assert_equal ec.candidaturas[1], ganador, 'El escanio se asigna a la segunda candidatura'
  #   assert ganador.tiene_escanio, 'La segunda candidatura tiene escanio'
  #   assert_equal 50, ganador.cociente, "Ganó con un cociente de 50"
  #   assert_equal 2, ganador.divisor, "Ganó con el divisor 2"
  # end

  # test 'cargos_postulados' do
  #   eleccion = create(:eleccion)
  #   comu = Cargo.find_by(nombre: Cargo::CARGOS[:comuna_1])
  #   dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
  #   create(:eleccion_cargo, eleccion: eleccion, cargo: comu)
  #   create(:eleccion_cargo, eleccion: eleccion, cargo: dipu)
  #   fuerza = create(:fuerza)

  #   create_candidaturas(create_personas(%w(M M)), eleccion, dipu, fuerza)
  #   create_candidaturas(create_personas(%w(M M)), eleccion, comu, fuerza)
  #   nombres_cargos = fuerza.cargos_postulados(eleccion).map(&:nombre)

  #   assert_equal 2, nombres_cargos.size, "Se postuló para 2 cargos"
  #   assert nombres_cargos.include?(Cargo::CARGOS[:comuna_1]), "Se postuló a comunero"
  #   assert nombres_cargos.include?(Cargo::CARGOS[:diputado]), "Se postuló a diputado"
  # end

  # test 'cumple_minimo?' do
  #   comu = cargos(:comuna_1)
  #   eleccion = create(:eleccion)
  #   create(:eleccion_cargo, eleccion: eleccion, cargo: comu)
  #   fuerza = create(:fuerza)
  #   Persona.find_by(numero_documento: 1) || create(:persona, numero_documento: 1)

  #   6.times { fuerza.crear_candidatura(eleccion, comu, 1, nil, nil, nil) }

  #   assert !fuerza.cumple_minimo?(eleccion, comu), 'Con 6 comuneros no alcanza'

  #   fuerza.crear_candidatura(eleccion, comu, 1, nil, nil, nil)

  #   assert fuerza.cumple_minimo?(eleccion, comu), 'Con 7 si alcanza'
  # end

  # test 'posee_candidaturas_para_asignar?' do
  #   ec = create(:eleccion_cargo, eleccion: create(:eleccion), cargo: Cargo.find_by(nombre: Cargo::CARGOS[:diputado]))
  #   fuerza = create(:fuerza)

  #   assert !fuerza.posee_candidaturas_para_asignar?(ec), 'No posee ninguna candidatura aun'

  #   fuerza.candidaturas << create(:candidatura, fuerza: fuerza, eleccion_cargo: ec, tiene_escanio: true)
  #   assert !fuerza.posee_candidaturas_para_asignar?(ec), 'Posee solo una candidatura, que ya tiene escanio'

  #   fuerza.candidaturas << create(:candidatura, fuerza: fuerza, eleccion_cargo: ec)
  #   assert fuerza.posee_candidaturas_para_asignar?(ec), 'Ahora posee una candidatura sin escanio'
  # end

  # test 'no oficializada si las oficializadas han sido renunciadas' do
  #   cargo = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
  #   eleccion = create(:eleccion)
  #   ec = create(:eleccion_cargo, eleccion: eleccion, cargo: cargo)
  #   fuerza = create(:fuerza)
  #   lista = fuerza.listas.create(nombre: 'ListaPrueba1', eleccion: eleccion)
  #   persona1 = personas(:one)
  #   persona2 = personas(:two)

  #   lista.crear_candidatura(ec, persona1)
  #   lista.crear_candidatura(ec, persona2)

  #   candidatura = fuerza.candidaturas_eleccion(eleccion).first
  #   candidatura.fecha_oficializacion = Date.today
  #   candidatura.fecha_renuncia = Date.today
  #   candidatura.save

  #   assert !fuerza.oficializada?(eleccion), 'No debe estar oficializada'
  # end

  # test 'oficializada?' do
  #   ucd = fuerzas(:ucd)
  #   completas_2011 = elecciones(:completas_2011)

  #   assert !ucd.oficializada?(completas_2011), 'Todavia no se oficializo'

  #   ucd.oficializar_lista(completas_2011)

  #   assert ucd.oficializada?(completas_2011), 'Ahora ya esta oficializada'
  # end

  # test 'oficializar y desoficializar lista' do
  #   ucd = fuerzas(:ucd)
  #   completas_2011 = elecciones(:completas_2011)

  #   ucd.oficializar_lista(completas_2011)

  #   one = candidaturas(:one)
  #   ucd_2011_1 = candidaturas(:ucd_2011_jefe_1)

  #   assert ucd_2011_1.oficializada?, 'Debe estar oficializada esta candidatura'
  #   assert !one.oficializada?, 'Esta otra no debe estar oficializada'

  #   ucd.desoficializar_lista(completas_2011)
  #   ucd_2011_1 = Candidatura.find(ucd_2011_1.id)

  #   assert !ucd_2011_1.oficializada?, 'Se tiene que haber DES oficializado'
  # end

  # test 'posee_candidaturas_invalidas?' do
  #   eleccion = create(:eleccion)
  #   dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
  #   create(:eleccion_cargo, eleccion: eleccion, cargo: dipu)
  #   fuerza = create(:fuerza)

  #   fuerza.crear_candidatura(eleccion, dipu, 1, nil, nil, nil)

  #   assert fuerza.posee_candidaturas_invalidas?(eleccion), "La candidatura agregada es inválida"
  # end

  # test 'cantidad_escanios_adjudicados' do
  #   fuerza = build(:fuerza)
  #   cargo = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
  #   ec1 = create(:eleccion_cargo, eleccion: create(:eleccion), cargo: cargo)
  #   ec2 = create(:eleccion_cargo, eleccion: create(:eleccion), cargo: cargo)
  #   fuerza.candidaturas << build(:candidatura, eleccion_cargo: ec1, tiene_escanio: true)
  #   fuerza.candidaturas << build(:candidatura, eleccion_cargo: ec1, tiene_escanio: true)
  #   fuerza.candidaturas << build(:candidatura, eleccion_cargo: ec2, tiene_escanio: true)
  #   fuerza.candidaturas << build(:candidatura, eleccion_cargo: ec1, tiene_escanio: false)

  #   # FIXME: la relacion fuerza -> candidatura no va a existir en un futuro inmediato
  #   # la relacion va a ser: fierza -> lista -> candidaturas
  #   assert_equal 2, fuerza.cantidad_escanios_adjudicados(ec1), "Tiene dos escaños en la elección cargo 1"
  # end

  # test 'get candidaturas by cargo and eleccion' do
  #   # FIXME: la relacion fuerza -> candidatura no va a existir en un futuro inmediato
  #   # la relacion va a ser: fierza -> lista -> candidaturas
  #   candidaturas = fuerzas(:one).candidaturas_cargo(elecciones(:one), cargos(:vicejefe))

  #   assert_equal 7, candidaturas.size, 'Debe tener dos candidaturas'
  # end

  # test "No agregar candidaturas para cargos que no están en la elección" do
  #   eleccion = create(:eleccion)
  #   dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
  #   comu = Cargo.find_by(nombre: Cargo::CARGOS[:comuna_1])

  #   ec_dipu = create(:eleccion_cargo, eleccion: eleccion, cargo: dipu, seleccionado: true)
  #   ec_comu_1 = create(:eleccion_cargo, eleccion: eleccion, cargo: comu, seleccionado: false)

  #   persona1 = personas(:one)
  #   persona2 = personas(:two)

  #   fuerza = create(:fuerza)
  #   lista = fuerza.listas.create(nombre: 'ListaPrueba1', eleccion: eleccion)

  #   assert lista.crear_candidatura(ec_dipu, persona1), 'El diputado se puede agregar'
  #   assert !lista.crear_candidatura(ec_comu_1, persona2), "El comunero no se debería agregar"
  # end

  # test 'cumple_minimos?' do
  #   eleccion = create(:eleccion)
  #   comu = Cargo.find_by(nombre: Cargo::CARGOS[:comuna_1])
  #   create(:eleccion_cargo, eleccion: eleccion, cargo: comu)
  #   f1 = create(:fuerza, nombre: 'f1')
  #   f2 = create(:fuerza, nombre: 'f2')

  #   create_candidaturas(create_personas(%w(M M F F M M F)), eleccion, comu, f1)
  #   create_candidaturas(create_personas(%w(M M F F M M)), eleccion, comu, f2)

  #   assert f1.cumple_minimos?(eleccion), 'Tiene 7 comuneros: cumple.'
  #   assert !f2.cumple_minimos?(eleccion), 'Tiene 6 comuneros: no cumple'
  # end

  # FIXME: Chequear cuando se sepa si un candidato puede estar en más de una lista dentro de un mismo partido
  # test 'listas ya postuladas' do
  #   eleccion = create(:eleccion)
  #   dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
  #   create(:eleccion_cargo, eleccion: eleccion, cargo: dipu)

  #   f1 = create(:fuerza)
  #   f1.crear_candidatura(eleccion, dipu, 1, nil, nil, nil)
  #   f2 = create(:fuerza)
  #   f2.crear_candidatura(eleccion, dipu, 1, nil, nil, nil)

  #   candidaturas = eleccion.posee_candidatura?(Persona.find_by(numero_documento: 1))
  #   fuerzas = candidaturas.map(&:fuerza).uniq

  #   assert fuerzas.include?(f1), "La persona se postuló con la fuerza f1"
  #   assert fuerzas.include?(f2), "La persona se postuló con la fuerza f2"
  # end

  def create_personas(sexos)
    gen = Random.new
    sexos.each.map { |s| create(:persona, sexo: s, numero_documento: gen.rand(99999999)) }
  end

  def create_candidaturas(personas, eleccion, cargo, fuerza)
    fuerza.listas.create(nombre: 'lista', eleccion: eleccion) if fuerza.listas.empty?
    personas.each_with_index.map do |persona, i|
      create(:candidatura, lista: fuerza.listas.first, persona: persona,
                           fuerza: fuerza, eleccion: eleccion, cargo: cargo, orden: i + 1)
    end
  end
end
