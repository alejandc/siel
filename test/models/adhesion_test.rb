# == Schema Information
#
# Table name: adhesiones
#
#  id                   :integer          not null, primary key
#  persona_id           :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  validaciones         :text(16777215)
#  lista_cargo_id       :integer
#  adhesiones_multiples :boolean          default(FALSE)
#

require 'test_helper'

class AdhesionTest < ActiveSupport::TestCase
  test 'creacion de adhesion valida' do
    adhesion = Adhesion.new(lista: Lista.first, persona: Persona.first)
    assert_equal true, adhesion.valid?
  end

  test 'atributos requeridos en adhesion' do
    adhesion = Adhesion.new
    adhesion.save

    assert_equal false, adhesion.valid?
    assert_includes adhesion.errors, :lista_id
    assert_includes adhesion.errors, :persona_id
    assert_includes adhesion.errors, :cargo_id
  end
end
