# == Schema Information
#
# Table name: casos_verificacion
#
#  id                     :integer          not null, primary key
#  hoja                   :integer
#  fila                   :integer
#  numero_documento       :integer
#  nombre                 :string(255)
#  apellido               :string(255)
#  figura_en_padron       :boolean
#  fila_vacia             :boolean
#  verificacion_firmas_id :integer
#  created_at             :datetime
#  updated_at             :datetime
#  ilegible               :boolean
#  sexo                   :string(255)
#  duplicado              :boolean
#  firmado                :boolean
#

require 'test_helper'

class CasoVerificacionTest < ActiveSupport::TestCase

  test 'desduplicar uno de dos' do
    v = verificaciones_firmas(:two)
    original = v.casos_verificacion.create(numero_documento: 5)
    duplicado = v.casos_verificacion.create(numero_documento: 5, duplicado: true)

    original.quitar_marca_duplicados

    assert_not CasoVerificacion.find(duplicado.id).duplicado, 'Ya no está más duplicado'
  end

  test 'desduplicar uno de tres' do
    v = verificaciones_firmas(:two)
    original = v.casos_verificacion.create(numero_documento: 5)
    dupli1 = v.casos_verificacion.create(numero_documento: 5, duplicado: true)
    dupli2 = v.casos_verificacion.create(numero_documento: 5, duplicado: true)

    dupli2.quitar_marca_duplicados

    assert_not CasoVerificacion.find(original.id).duplicado, 'El primero, original, sigue sin estar duplicado'
    assert CasoVerificacion.find(dupli1.id).duplicado, 'El segundo, duplicado, lo sigue estando'

    original.quitar_marca_duplicados

    assert !CasoVerificacion.find(dupli1.id).duplicado || !CasoVerificacion.find(dupli2.id).duplicado, 'Alguno se desduplicó'
  end

  test 'borrar_datos_cargados' do
    caso = casos_verificacion(:completo)
    caso.borrar_datos_cargados

    assert_equal 93, caso.hoja, 'La hoja no se borra'
    assert_equal 7, caso.fila, 'La fila no se borra'
    assert_nil caso.numero_documento, 'El DNI si se borra'
    assert_nil caso.nombre, 'El nombre si se borra'
    assert_nil caso.apellido, 'El apellido si se borra'
    assert_nil caso.figura_en_padron, 'Si figura en padrón se borrar'
    assert_nil caso.fila_vacia, 'Si está vacía si se borra'
    assert_nil caso.ilegible, 'Si es ilegible si se borra'
    assert_nil caso.sexo, 'El sexo si se borra'
    assert_nil caso.duplicado, 'Si está duplicado si se borra'
    assert_nil caso.firmado, 'Si está firmado si se borra'
  end

  test 'invalido?' do
    assert_not CasoVerificacion.new.invalido?, 'Apenas se crea no es inválido'
    assert_not CasoVerificacion.new(numero_documento: 5).invalido?, 'Al cargar el documento no es inválido'
    assert CasoVerificacion.new(ilegible: true).invalido?, 'El caso ilegible es inválido'
    assert CasoVerificacion.new(duplicado: true).invalido?, 'El caso duplicado es inválido'
    assert CasoVerificacion.new(figura_en_padron: false).invalido?, 'Si no figura en el padrón es inválido'
    assert CasoVerificacion.new(firmado: false).invalido?, 'Si no quisieron firmar, es inválido'
  end

  test 'marcar_duplicado' do
    dni333 = casos_verificacion(:dni333)
    assert_not dni333.duplicado, 'Al principio no está duplicado'

    one = casos_verificacion(:one).marcar_duplicado
    assert_not one.duplicado, 'El primero no está duplicado'

    dupli333 = CasoVerificacion.create(verificacion_firmas: verificaciones_firmas(:one), numero_documento: 333)
    dupli333.marcar_duplicado

    assert dupli333.duplicado, 'El nuevo 333 es un duplicado'
    assert_not CasoVerificacion.find(dni333.id).duplicado, 'Sólo hay que marcar el duplicado, el original no'
  end

  test 'buscar_en_padron' do
    one = casos_verificacion(:one).buscar_en_padron
    dni333 = casos_verificacion(:dni333).buscar_en_padron

    assert one.figura_en_padron
    assert_not dni333.figura_en_padron, 'Este item no figura en padrón'
  end

  test 'cargado?' do
    assert_not CasoVerificacion.new(hoja: 2, fila: 3).cargado?, 'No fue cargado'
    assert CasoVerificacion.new(numero_documento: 3).cargado?, 'Se cargó documento'
    assert CasoVerificacion.new(fila_vacia: true).cargado?, 'La fila está vacía'
    assert CasoVerificacion.new(fila_vacia: false).cargado?, 'La fila no está vacía'
    assert CasoVerificacion.new(ilegible: true).cargado?, 'El documento es ilegible'
    assert CasoVerificacion.new(ilegible: false).cargado?, 'El documento no ese ilegible'
  end

  test 'sort' do
    a =	CasoVerificacion.new(hoja: 2, fila: 2)
    b =	CasoVerificacion.new(hoja: 8, fila: 8)
    c = CasoVerificacion.new(hoja: 10, fila: 10)

    casos = [b, a, c].sort

    assert_equal a, casos[0]
    assert_equal b, casos[1]
    assert_equal c, casos[2]
  end
end
