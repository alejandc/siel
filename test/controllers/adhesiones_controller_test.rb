require 'test_helper'

class AdhesionesControllerTest < ActionController::TestCase
  setup do
    do_login
  end

  test 'creación existosa de adhesion' do
    lista = Lista.first

    assert_difference('Adhesion.count') do
      post :create, tab: 'candidaturas', persona_id: Persona.first.id, adhesion: { lista_id: lista.id, fuerza_id: lista.fuerza_id, eleccion_id: lista.eleccion_id, cargo_id: Cargo.first.id }, format: :js
    end

    assert_response :success
  end

  test 'eliminar adhesion' do
    adhesion = adhesiones(:one)
    lista = adhesion.lista
    cantidad_inicial = lista.adhesiones.count

    delete :destroy, id: adhesion.id, tab: 'adhesiones', format: :js

    assert_response :success
    assert_equal cantidad_inicial - 1, lista.adhesiones.count, "No se eliminó la adhesion correctamente"
  end

  # FIXME: Preguntar qué lógica tiene este test
  # test 'error creando adhesion por campos requeridos' do
  #   assert_difference('Adhesion.count', 0) do
  #     post :create, adhesion: { lista_id: nil, persona_id: nil, cargo_id: nil }, format: :js
  #   end

  #   assert_response :unprocessable_entity
  # end
end
