require 'test_helper'

class VerificacionesFirmasControllerTest < ActionController::TestCase
  setup do
    @verificacion = verificaciones_firmas(:one)
    do_login
  end

  test 'should get index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:verificaciones)
  end

  test 'should get new' do
    get :new
    assert_response :success
  end

  test 'should create verificacion_firmas' do
    assert_difference('VerificacionFirmas.count') do
      post :create, verificacion_firmas: { cantidad_firmas_hoja: @verificacion.cantidad_firmas_hoja, cantidad_firmas_muestra: @verificacion.cantidad_firmas_muestra, cantidad_firmas_total: @verificacion.cantidad_firmas_total, cantidad_hojas: @verificacion.cantidad_hojas, confianza: @verificacion.confianza, variable_p: @verificacion.variable_p, descripcion: @verificacion.descripcion, error: @verificacion.error, observaciones: @verificacion.observaciones, cantidad_firmas_requeridas: @verificacion.cantidad_firmas_requeridas }
    end

    assert_redirected_to verificacion_firmas_path(assigns(:verificacion))
    assert_not_nil assigns(:verificacion).cantidad_firmas_requeridas, 'Debe grabar las firmas requeridas'
  end

  test 'should show verificacion_firmas' do
    get :show, id: @verificacion
    assert_response :success
  end

  test 'should get edit' do
    get :edit, id: @verificacion
    assert_response :success
  end

  test 'should update verificacion_firmas' do
    patch :update, id: @verificacion, verificacion_firmas: { cantidad_firmas_hoja: @verificacion.cantidad_firmas_hoja, cantidad_firmas_muestra: @verificacion.cantidad_firmas_muestra, cantidad_firmas_total: @verificacion.cantidad_firmas_total, cantidad_hojas: @verificacion.cantidad_hojas, confianza: @verificacion.confianza, variable_p: @verificacion.variable_p, descripcion: @verificacion.descripcion, error: @verificacion.error, observaciones: @verificacion.observaciones, cantidad_firmas_requeridas: @verificacion.cantidad_firmas_requeridas }
    assert_redirected_to verificacion_firmas_path(assigns(:verificacion))
  end

  test 'should destroy verificacion_firmas' do
    assert_difference('VerificacionFirmas.count', -1) do
      delete :destroy, id: @verificacion
    end

    assert_redirected_to verificaciones_firmas_path
  end
end
