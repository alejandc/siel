require 'test_helper'

class CasosVerificacionControllerTest < ActionController::TestCase
  setup do
    @verificacion = verificaciones_firmas(:one)
    @caso = casos_verificacion(:one)
    do_login
  end

  test 'should get edit' do
    get :edit, verificacion_firmas_id: @verificacion.id, id: @caso
    assert_response :success
    assert_not_nil assigns(:caso)
    assert_not_nil assigns(:verificacion)
  end

  test 'should get index' do
    get :index, verificacion_firmas_id: @verificacion.id
    assert_response :success
    assert_not_nil assigns(:casos)
  end

  test 'should get cargados' do
    get :index, verificacion_firmas_id: @verificacion.id, estado: CasosVerificacionController::ESTADOS[:cargados]
    assigns(:casos).each { |caso| assert caso.cargado?, "El caso #{caso.id} está cargado" }
  end

  test "should generate new caso por fila vacía" do
    cantidad_inicial = @caso.verificacion_firmas.casos_verificacion.size

    patch :update, verificacion_firmas_id: @verificacion.id, id: @caso, caso_verificacion: { ilegible: true }, format: :js
    assert_equal cantidad_inicial, @caso.verificacion_firmas.casos_verificacion.size, 'No se marcó fila vacía, no agrega nada'

    patch :update, verificacion_firmas_id: @verificacion.id, id: @caso, caso_verificacion: { fila_vacia: true }, format: :js
    assert_equal cantidad_inicial + 1, @caso.verificacion_firmas.casos_verificacion.size, 'Hay un caso nuevo generado'
  end

  test 'should update caso' do
    patch :update, verificacion_firmas_id: @verificacion.id, id: @caso, caso_verificacion: { numero_documento: @caso.numero_documento }, format: :js

    assert assigns(:caso).figura_en_padron

    dni333 = casos_verificacion(:dni333)
    patch :update, verificacion_firmas_id: @verificacion.id, id: dni333, caso_verificacion: { numero_documento: dni333.numero_documento }, format: :js

    assert_not_nil assigns(:caso).figura_en_padron
    assert_not assigns(:caso).figura_en_padron, 'Este caso no figura'
  end

  test 'should desmarcar duplicados' do
    v = verificaciones_firmas(:two)
    original = v.casos_verificacion.create(numero_documento: 5)
    duplicado = v.casos_verificacion.create(numero_documento: 5, duplicado: true)

    patch :borrar_datos, id: original, format: :js

    assert_not CasoVerificacion.find(duplicado.id).duplicado, 'Le quita la marca al duplicado'
  end

  test "should borrar caso al borrar datos de fila vacía" do
    caso = casos_verificacion(:one)
    caso.fila_vacia = true
    caso.save

    patch :borrar_datos, id: caso, format: :js

    assert_raises(ActiveRecord::RecordNotFound) do
      caso = CasoVerificacion.find(caso.id)
    end
  end

  test 'should borrar datos' do
    caso = casos_verificacion(:one)

    assert_not caso.numero_documento.nil?, 'Al principio está cargado el caso'

    patch :borrar_datos, id: caso, format: :js

    caso = CasoVerificacion.find(caso.id)

    assert_nil caso.numero_documento, 'Luego está vacío'
  end

  test 'should firmar caso' do
    patch :firmar, id: casos_verificacion(:one), format: :js

    assert CasoVerificacion.find(casos_verificacion(:one).id).firmado, 'Ya fue firmado'
  end

  test 'should not firmar caso' do
    patch :no_firmar, id: casos_verificacion(:one), format: :js

    assert_not CasoVerificacion.find(casos_verificacion(:one).id).firmado, 'No pudo ser firmado'
  end

  test 'should not buscar en padron' do
    ilegible = casos_verificacion(:ilegible)
    patch :update, verificacion_firmas_id: @verificacion.id, id: ilegible, caso_verificacion: { numero_documento: ilegible.numero_documento, ilegible: ilegible.ilegible }, format: :js

    assert_nil assigns(:caso).figura_en_padron, 'No debe buscarlo'
  end

  test 'should marcar duplicado' do
    dni333 = casos_verificacion(:dni333)
    CasoVerificacion.create(verificacion_firmas: dni333.verificacion_firmas, numero_documento: dni333.numero_documento)

    patch :update, verificacion_firmas_id: @verificacion.id, id: dni333, caso_verificacion: { numero_documento: dni333.numero_documento }, format: :js
    assert assigns(:caso).duplicado, 'Se marcó como duplicado'
  end
end
