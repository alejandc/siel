require 'test_helper'

class ListasControllerTest < ActionController::TestCase
  setup do
    do_login
  end

  # FIXME: Chequear luego del refactor de listas
  # test 'listado de listas (index)' do
  #   get :index
  #   assert_response :success
  # end

  test 'sin permiso de acceso al listado de listas para usuario con rol lista partidaria' do
    do_login(usuarios(:usuario_lista_partidaria))
    get :index

    assert_not_empty flash[:alert]
    assert_response :redirect
  end

  # FIXME: Chequear para que se usa esta ruta
  # test 'carga show de lista para usuario con rol lista partidaria' do
  #   usuario = usuarios(:usuario_lista_partidaria)
  #   usuario.usuarios_fuerzas.build(usuario: usuario, fuerza: fuerzas(:one))
  #   usuario.lista = listas(:one)
  #   usuario.save!

  #   do_login(usuario)
  #   get :show, id: listas(:one)

  #   assert_response :success
  #   assert_select 'div.well', 1
  # end

  test 'creación existosa de lista' do
    assert_difference('Lista.count') do
      post :create, lista: { eleccion_id: elecciones(:one).id, fuerza_id: fuerzas(:one).id, nombre: 'Lista test' }, format: :js
    end

    assert_not_empty flash[:notice]

    assert_response :success
    assert_equal 'text/javascript', @response.content_type
  end

  # FIXME: Chequear luego del refactor de listas
  # test 'error creando lista por campos requeridos' do
  #   assert_difference('Lista.count', 0) do
  #     post :create, lista: { eleccion_id: nil, fuerza_id: nil, nombre: nil }, format: :js
  #   end

  #   assert_template :create
  #   assert_includes @response.body, 'field_with_errors'
  #   assert_equal 'text/javascript', @response.content_type
  # end
end
