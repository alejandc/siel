require 'simplecov'
SimpleCov.start
ENV['RAILS_ENV'] = 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'mocha/test_unit'
require 'pry'
require 'minitest/reporters'
Minitest::Reporters.use! [Minitest::Reporters::SpecReporter.new(color: true)]

module ActiveSupport
  class TestCase
    ActiveRecord::Migration.check_pending!

    # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
    #
    # Note: You'll currently still have to declare fixtures explicitly in integration tests
    # -- they do not yet inherit this setting
    fixtures :all

    # Add more helper methods to be used by all tests here...

    def do_login(usuario = usuarios(:usu1))
      # Información del login
      session[:logout] = true
      session[:user] = usuario
      session[:user_groups] = usuario.perfiles
    end

    def assert_not(condition, message)
      assert !condition, message
    end

    def logger
      RAILS_DEFAULT_LOGGER
    end

    # Test::Unit - mix-in Factory Girl
    class ActiveSupport::TestCase
      include FactoryGirl::Syntax::Methods
    end

  end
end
