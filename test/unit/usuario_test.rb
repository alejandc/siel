# == Schema Information
#
# Table name: usuarios
#
#  id              :integer          not null, primary key
#  email           :string(255)
#  nombre_usuario  :string(255)
#  password_digest :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  perfiles        :string(255)
#  lista_id        :integer
#  eleccion_id     :integer
#  habilitado      :boolean          default(TRUE)
#

require 'test_helper'

class UsuarioTest < ActiveSupport::TestCase

  test 'no duplicar los nombres de usuario' do
    email = 'repedito@email.com'
    pass = 'password'
    nombre_usuario = 'usuariorepetido'

    Usuario.create!(nombre_usuario: nombre_usuario, password: pass, password_confirmation: pass, email: "#{email}1", perfiles: 'apoderado')

    assert_raises ActiveRecord::RecordInvalid do
      Usuario.create!(nombre_usuario: nombre_usuario, password: pass, password_confirmation: pass, email: "#{email}2", perfiles: 'apoderado')
    end
  end

  test 'no duplicar los mails' do
    email = 'repedito@email.com'
    pass = 'password'

    Usuario.create!(nombre_usuario: 'usuario_nuevo1', password: pass, password_confirmation: pass, email: email, perfiles: 'apoderado')

    assert_raises ActiveRecord::RecordInvalid do
      Usuario.create!(nombre_usuario: 'usuario_nuevo2', password: pass, password_confirmation: pass, email: email, perfiles: 'apoderado')
    end
  end

  test "blanquear la contraseña" do
    original = 'original'

    usuario = Usuario.create!(email: 'email', nombre_usuario: 'usuario', password: original, password_confirmation: original, perfiles: 'apoderado')

    assert usuario.authenticate(original), 'Debe autenticar con el original'

    nuevo_pass = usuario.blanquear_clave
    usuario.save!

    assert usuario.authenticate(nuevo_pass), 'Debe autenticar con la nueva clave'
  end

  test 'cambiar contraseña con contraseña actual incorrecta' do
    usuario = Usuario.create!(email: 'email', nombre_usuario: 'usuario', password: 'password', password_confirmation: 'password', perfiles: 'apoderado')
    usuario.cambiar_contrasenia!('password_incorrecto', 'password_nuevo')
    assert usuario.errors[:wrong_password], 'La contraseña actual es incorrecta'
  end

  test 'cambiar contraseña con contraseña nueva menor a 6 caracteres' do
    usuario = Usuario.create!(email: 'email', nombre_usuario: 'usuario', password: 'password', password_confirmation: 'password', perfiles: 'apoderado')
    usuario.cambiar_contrasenia!('password', '12345')
    assert usuario.errors[:password_validations], 'La contraseña es muy débil'
  end

  test 'cambiar contraseña exitosamente' do
    usuario = Usuario.create!(email: 'email', nombre_usuario: 'usuario', password: 'password', password_confirmation: 'password', perfiles: 'apoderado')
    cambio_contrasenia = usuario.cambiar_contrasenia!('password', 'nueva_password')
    assert cambio_contrasenia, true
  end

  # FIXME: Chequear que pasa con los usuarios luego de terminada la eleccion (se borran, se dejan, etc.),
  # para saber como mantenemos el historico, en el caso de que haya que hacerlo
  test 'pertenece_a_fuerza?' do
    usuario = usuarios(:usuario_lista_partidaria)
    fuerza_pertenece = usuario.lista.fuerza
    fuerza_no_pertenece = fuerzas(:two)

    UsuarioFuerza.create!(usuario: usuario, fuerza: fuerza_pertenece)
    usuario.reload

    assert usuario.pertenece_a_fuerza?(fuerza_pertenece), 'El usuario no pertenece a la fuerza uno'
    assert_not usuario.pertenece_a_fuerza?(fuerza_no_pertenece), 'El usuario pertenece a la fuerza dos'
  end
end
