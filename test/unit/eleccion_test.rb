﻿
# == Schema Information
#
# Table name: elecciones
#
#  id                              :integer          not null, primary key
#  descripcion                     :string(255)
#  fecha                           :date
#  notas                           :text(16777215)
#  observaciones                   :text(16777215)
#  created_at                      :datetime
#  updated_at                      :datetime
#  archivo_id                      :integer
#  expediente                      :string(255)
#  fecha_posesion                  :date
#  porcentaje_exclusion            :decimal(10, )
#  tipo                            :string(255)
#  eleccion_relacionada_id         :integer
#  fecha_limite_carga_candidaturas :datetime
#  habilitar_carga_candidaturas    :boolean
#

require 'test_helper'

class EleccionTest < ActiveSupport::TestCase

  test 'cantidad_inscriptos' do
    cant1 = 99
    cant2 = 7

    eleccion = build(:eleccion)
    eleccion.resultados_comunas << build(:resultado_comuna, cantidad_inscriptos: cant1)
    eleccion.resultados_comunas << build(:resultado_comuna, cantidad_inscriptos: cant2)

    assert_equal cant1 + cant2, eleccion.cantidad_inscriptos, 'La cantidad de inscriptos es la suma de las cantidades por comuna'
  end

  test 'cantidad_mesas' do
    cant1 = 5
    cant2 = 8

    eleccion = build(:eleccion)
    eleccion.resultados_comunas.build(cantidad_mesas: cant1)
    eleccion.resultados_comunas.build(cantidad_mesas: cant2)
    eleccion.resultados_comunas.build

    assert_equal cant1 + cant2, eleccion.cantidad_mesas, "La cantidad de mesas de la elección es la suma de las cantidades por cada comuna"
  end

  test 'resultados_completos?' do
    eleccion = build(:eleccion)
    assert !eleccion.resultados_completos?, "Todavía no tiene nada"

    eleccion.porcentaje_exclusion = 0
    assert eleccion.resultados_completos?, "Ahora tiene su información cargada"

    ec = build(:eleccion_cargo, eleccion: eleccion)
    eleccion.eleccion_cargos << ec
    assert !eleccion.resultados_completos?, "Su elección cargo no está completo"

    ec.cantidad_escanios = 0
    assert eleccion.resultados_completos?, "Ahora está completa la elección cargo"

    r = build(:resultado_eleccion, eleccion_cargo: ec, cantidad_votos: nil)
    ec.resultados_elecciones << r
    assert !eleccion.resultados_completos?, 'Tiene un resultado sin datos'

    r.cantidad_votos = 0
    r.fuerza = build(:fuerza)
    assert eleccion.resultados_completos?, "Ahora está todo completo"
  end

  test 'primer_eleccion_cargo' do
    eleccion = Eleccion.new(descripcion: 'Eleccion Nueva', fecha_posesion: Date.today + 10, eleccion_cargos_attributes: [{ cargo_id: Cargo.find_by(nombre: Cargo::CARGOS[:jefe]).id, seleccionado: false }, { cargo_id: Cargo.find_by(nombre: Cargo::CARGOS[:diputado]).id, seleccionado: true }, { cargo_id: Cargo.find_by(nombre: Cargo::CARGOS[:comuna_1]).id, seleccionado: false }])

    eleccion.save!
    ec_dipu = eleccion.eleccion_cargos.detect(&:seleccionado)

    assert_equal ec_dipu, eleccion.primer_eleccion_cargo, 'El primero seleccionado es diputado'
  end

  # FIXME: Chequear cuando se defina la lógica de los resultados por comunas
  # test 'inicializar resultados' do
  #   dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
  #   eleccion = Eleccion.new(descripcion: 'Eleccion Nueva', fecha_posesion: Date.today + 10, porcentaje_exclusion: 5, eleccion_cargos_attributes: [{ cargo_id: dipu.id, seleccionado: true }, { cargo_id: Cargo.find_by(nombre: Cargo::CARGOS[:comuna_1]).id, seleccionado: false }])
  #   eleccion.save!
  #   f = create(:fuerza)
  #   lista = f.listas.create!(nombre: 'lista1', eleccion: eleccion)
  #   persona = personas(:one)

  #   create(:candidatura, fuerza: f, lista: lista, eleccion: eleccion, cargo: dipu, persona: persona)

  #   assert eleccion.general.resultados_elecciones.empty?, 'No tiene resultados cargados'

  #   eleccion.general.inicializar_resultados

  #   assert_equal ((Comuna.count + 1) * 3), eleccion.general.resultados_elecciones.size, "Tiene un resultado para la fuerza en la comuna, más un resultado para los votos en blanco y nulos de la comuna"
  #   assert_equal Comuna.count, eleccion.general.resultados_comunas.size, 'Tiene un resultado por comuna para las cantidades de mesas e inscriptos'
  #   assert_equal 3, eleccion.general.resultados_elecciones.select { |re| re.comuna.nil? }.size, "Hay tres registros para cargar sin comuna, es decir, distrito único"
  # end

  test 'elecciones con resultados cargados' do
    eleccion = create(:eleccion)
    dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    ec = create(:eleccion_cargo, eleccion: eleccion, cargo: dipu)
    create(:resultado_eleccion, eleccion_cargo: ec, cantidad_votos: 100)

    elecciones = Eleccion.con_resultados

    assert_not_nil elecciones
    assert_equal 1, elecciones.size
  end

  # FIXME: Cuando se haga la estructura de elegir lista ganadora y asignarsela a la fuerza
  # test 'borrar reparto de cargos' do
  #   eleccion = Eleccion.new(descripcion: 'Eleccion Nueva', fecha_posesion: Date.today + 10, porcentaje_exclusion: 5, eleccion_cargos_attributes: [{ cargo_id: Cargo.find_by(nombre: Cargo::CARGOS[:diputado]).id, cantidad_escanios: 5 }])
  #   eleccion.save!
  #   ec_dipu = eleccion.general.eleccion_cargos.first
  #   fuerza = create(:fuerza)
  #   lista = fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)
  #   c1 = create(:candidatura, fuerza: fuerza, eleccion_cargo: ec_dipu, divisor: 1, cociente: 2, tiene_escanio: true)
  #   create(:resultado_eleccion, eleccion_cargo: ec_dipu, fuerza: fuerza, cantidad_votos: 100)

  #   eleccion.general.borrar_reparto

  #   c1.reload
  #   ec_dipu.reload
  #   eleccion.reload

  #   assert ec_dipu.resultado(fuerza).nil?, "No existe más el resultado de la elección"
  #   assert c1.divisor.nil? && c1.cociente.nil? && c1.tiene_escanio.nil?, 'Borra lo calculado en la candidatura'
  #   assert ec_dipu.cantidad_escanios.nil?, "Borra la info cargada en la elección cargo"
  #   assert eleccion.general.porcentaje_exclusion.nil?, "Borra la info cargada en la elección"
  #   assert eleccion.general.resultados_comunas.empty?, 'Borra los resultados para las comunas'
  # end

  test 'nombres_archivos_expedientes' do
    eleccion = create(:eleccion, expediente: '00234/7,07332/4')

    assert_equal ['Expe-234-7.pdf', 'Expe-7332-4.pdf'], eleccion.nombres_archivos_expedientes, 'Los nombres de los expedientes de la eleccion'
  end

  test 'posee_candidatura?' do
    eleccion = create(:eleccion)
    dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    create(:eleccion_cargo, eleccion: eleccion, cargo: dipu)
    persona = Persona.find_by(numero_documento: 1)
    fuerza = create(:fuerza)
    lista = fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)
    lista.crear_candidatura(dipu, persona).save

    assert eleccion.posee_candidatura?(persona), "Esta persona ya posee una candidatura para esta elección"
  end

  test 'actual' do
    create(:eleccion, fecha: Date.new(2050, 01, 01))
    e2 = create(:eleccion, fecha: Date.new(2055, 01, 01))

    actual = Eleccion.actual

    assert_equal e2.general.id, actual.id, "La última es la actual"
  end

  # FIXME: Entender que se está queriendo testear
  # test 'fuerzas' do
  #   persona1 = create(:persona)
  #   persona2 = create(:persona, nombres: 'persona2')
  #   eleccion = create(:eleccion)
  #   jefe = Cargo.find_by(nombre: Cargo::CARGOS[:jefe])
  #   ec_jefe = create(:eleccion_cargo, eleccion: eleccion, cargo: jefe)
  #   e2 = create(:eleccion)
  #   a1 = create(:fuerza, tipo: 'ALIANZA', eleccion: eleccion, nombre: 'participa')
  #   f1 = create(:fuerza, tipo: 'ALIANZA', nombre: 'no_participa', eleccion: e2)
  #   p1 = create(:fuerza, tipo: 'PARTIDO', nombre: 'participa')
  #   f2 = create(:fuerza, tipo: 'PARTIDO', nombre: 'no_participa')
  #   l1 = p1.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)
  #   l2 = f2.listas.create!(nombre: 'ListaPrueba2', eleccion: eleccion)
  #   l1.crear_candidatura(ec_jefe, persona1)
  #   l2.crear_candidatura(ec_jefe, persona2)

  #   ids = eleccion.fuerzas.map(&:id)

  #   assert_equal 2, ids.size, 'Hay una alianza y un partido con candidatura'
  #   assert ids.include?(a1.id), 'Debe estar la alianza'
  #   assert ids.include?(p1.id), 'Debe estar el partido'
  # end

  test 'vigente?' do
    eleccion = elecciones(:completas_2011)
    assert !eleccion.vigente?, "El año 2011 ya pasó, no está vigente"

    eleccion.fecha = Date.today
    assert eleccion.vigente?, "Con la fecha de hoy está vigente"

    eleccion.fecha = Date.today - 1
    assert !eleccion.vigente?, "Con fecha de ayer no está vigente"

    eleccion.fecha = Date.today + 1
    assert eleccion.vigente?, "Con fecha de mañana está vigente"
  end

  test 'tienen cargos' do
    assert_equal 4, elecciones(:one).cargos.size, 'La primera tiene todos los cargos'
    assert_equal 2, elecciones(:two).cargos.size, 'La segunda solo tiene dos'
  end

  test 'tiene resolucion' do
    con_res = elecciones(:one)
    sin_res = elecciones(:two)

    assert con_res.tiene_resolucion?, 'La primera tiene archivo'
    assert_not sin_res.tiene_resolucion?, 'La segunda no tiene'
  end

end
