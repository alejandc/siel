# == Schema Information
#
# Table name: resultados_elecciones
#
#  id                :integer          not null, primary key
#  eleccion_cargo_id :integer
#  fuerza_id         :integer
#  cantidad_votos    :integer
#  created_at        :datetime
#  updated_at        :datetime
#  comuna_id         :integer
#  nulos             :boolean
#  en_blanco         :boolean
#

require 'test_helper'

class ResultadoEleccionTest < ActiveSupport::TestCase

  test 'votos_cargados?' do
    resultado = ResultadoEleccion.new
    assert !resultado.votos_cargados?, 'No tiene los votos cargados'

    resultado.cantidad_votos = 0
    assert resultado.votos_cargados?, "Tiene los votos con número 0, ergo están cargados"
  end

  test 'candidaturas' do
    eleccion = create(:eleccion)
    dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    comu = Cargo.find_by(nombre: Cargo::CARGOS[:comuna_1])
    ec_dipu = create(:eleccion_cargo, eleccion: eleccion, cargo: dipu)
    ec_comu = create(:eleccion_cargo, eleccion: eleccion, cargo: comu)
    fuerza1 = create(:fuerza)
    fuerza2 = create(:fuerza)
    fuerza1.listas.create(nombre: 'lista', eleccion: ec_dipu.eleccion)
    fuerza2.listas.create(nombre: 'lista2', eleccion: ec_dipu.eleccion)
    2.times { |i| create(:candidatura, lista: fuerza1.listas.first, persona: create(:persona, nombres: "Nombres-1-#{i}", apellidos: "Apellidos-1-#{i}", numero_documento: 4444444 + i, sexo: 'M', en_padron: true), eleccion: eleccion, cargo: dipu) }
    3.times { |i| create(:candidatura, lista: fuerza1.listas.first, persona: create(:persona, nombres: "Nombres-2-#{i}", apellidos: "Apellidos-2-#{i}", numero_documento: 5444444 + i, sexo: 'F', en_padron: true), eleccion: eleccion, cargo: comu) }
    4.times { |i| create(:candidatura, lista: fuerza2.listas.first, persona: create(:persona, nombres: "Nombres-3-#{i}", apellidos: "Apellidos-3-#{i}", numero_documento: 6444444 + i, sexo: 'F', en_padron: true), eleccion: eleccion, cargo: dipu) }
    5.times { |i| create(:candidatura, lista: fuerza2.listas.first, persona: create(:persona, nombres: "Nombres-4-#{i}", apellidos: "Apellidos-4-#{i}", numero_documento: 7444444 + i, sexo: 'M', en_padron: true), eleccion: eleccion, cargo: comu) }
    create(:resultado_eleccion, eleccion_cargo: ec_dipu, fuerza: fuerza1)
    create(:resultado_eleccion, eleccion_cargo: ec_comu, fuerza: fuerza1)
    r3 = create(:resultado_eleccion, eleccion_cargo: ec_dipu, fuerza: fuerza2)
    create(:resultado_eleccion, eleccion_cargo: ec_comu, fuerza: fuerza2)

    assert_equal 4, r3.candidaturas.size, 'Hay cuatro candidaturas para esta fuerza y cargo'
  end

  test 'scope por_comuna' do
    ec = create(:eleccion_cargo, eleccion: create(:eleccion), cargo: Cargo.find_by(nombre: Cargo::CARGOS[:diputado]))
    f = create(:fuerza)

    create(:resultado_eleccion, eleccion_cargo: ec, fuerza: f, comuna: comunas(:comuna1))
    create(:resultado_eleccion, eleccion_cargo: ec, fuerza: f, comuna: comunas(:comuna2))

    resultados = ResultadoEleccion.por_comuna(comunas(:comuna1))

    assert_equal 1, resultados.size, 'Hay un resultado para esa comuna'
  end

  test 'scope eleccion_cargo fuerza' do
    e1 = create(:eleccion)
    e2 = create(:eleccion)
    dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    comu = Cargo.find_by(nombre: Cargo::CARGOS[:comunero])
    ec1_dipu = create(:eleccion_cargo, eleccion: e1, cargo: dipu)
    ec2_comu = create(:eleccion_cargo, eleccion: e2, cargo: comu)
    f1 = create(:fuerza)
    f2 = create(:fuerza)

    r1 = create(:resultado_eleccion, eleccion_cargo: ec1_dipu, fuerza: f1)
    create(:resultado_eleccion, eleccion_cargo: ec1_dipu, fuerza: f2)
    create(:resultado_eleccion, eleccion_cargo: ec2_comu, fuerza: f1)
    create(:resultado_eleccion, eleccion_cargo: ec2_comu, fuerza: f2)

    resultados = ResultadoEleccion.por_eleccion_cargo(ec1_dipu).por_fuerza(f1)

    assert_equal 1, resultados.size, 'Un solo resultado coincide el criterio'
    assert_equal r1, resultados[0], 'El primer resultado es para dipu y f1'
  end

end
