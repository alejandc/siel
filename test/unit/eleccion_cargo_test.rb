# == Schema Information
#
# Table name: eleccion_cargos
#
#  id                :integer          not null, primary key
#  eleccion_id       :integer
#  cargo_id          :integer
#  seleccionado      :boolean
#  created_at        :datetime
#  updated_at        :datetime
#  cantidad_escanios :integer
#  comuna_id         :integer
#

require 'test_helper'
require 'reparto_d_hondt'
require 'reparto_error'

class EleccionCargoTest < ActiveSupport::TestCase

  test 'ganadoras' do
    eleccion = elecciones(:one)
    cargo = cargos(:diputados)

    ec = build(:eleccion_cargo, eleccion: eleccion, cargo: cargo)
    fuerza = fuerzas(:one)
    lista = fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)

    c1 = lista.crear_candidatura(cargo, personas(:one))
    c1.update_attributes!(tiene_escanio: false)

    c2 = lista.crear_candidatura(cargo, personas(:two))
    c2.update_attributes!(tiene_escanio: true, cociente: 100)

    c3 = lista.crear_candidatura(cargo, personas(:three))
    c3.update_attributes!(tiene_escanio: true, cociente: 200)

    c4 = lista.crear_candidatura(cargo, personas(:four))
    c4.update_attributes!(tiene_escanio: true, cociente: 50)

    assert_equal [c3, c2, c4], ec.ganadoras, "Hay tres candidaturas con escaño, y se ordenan por su cociente"
  end

  test 'resultados_completos?' do
    ec = build(:eleccion_cargo)
    assert !ec.resultados_completos?, 'No tiene nada cargado'

    ec.cantidad_escanios = 5
    assert ec.resultados_completos?, "Tiene los escaños cargados y ningún resultado"

    r1 = build(:resultado_eleccion, eleccion_cargo: ec, cantidad_votos: nil)
    ec.resultados_elecciones << r1
    assert !ec.resultados_completos?, "Tiene un resultado que no está completo"

    r1.cantidad_votos = 10
    r1.fuerza = build(:fuerza)
    assert ec.resultados_completos?, 'Ahora su resultado tiene toda la info cargada'
  end

  test 'desasignar_escanios' do
    eleccion = elecciones(:one)
    cargo = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    ec = create(:eleccion_cargo, eleccion: eleccion, cargo: cargo)
    fuerza = create(:fuerza, nombre: 'fuerza')
    lista = fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)
    create(:candidatura, fuerza: fuerza, lista: lista, persona: create(:persona, nombres: 'Nombres1', apellidos: 'Apellidos1', numero_documento: 4444444, sexo: 'M', en_padron: true), eleccion: eleccion, cargo: cargo, tiene_escanio: true, divisor: 1, cociente: 1)
    create(:candidatura, fuerza: fuerza, lista: lista, persona: create(:persona, nombres: 'Nombres2', apellidos: 'Apellidos2', numero_documento: 4444445, sexo: 'M', en_padron: true), eleccion: eleccion, cargo: cargo, tiene_escanio: false, divisor: 2, cociente: 2)

    ec.desasignar_escanios

    ec.candidaturas.each do |c|
      assert (c.tiene_escanio.nil? && c.divisor.nil? && c.cociente.nil?), 'No hay candidaturas con escanios asignados'
    end
  end

  test 'devolver resultado de una fuerza' do
    eleccion = create(:eleccion)
    ec_dipu = create(:eleccion_cargo, eleccion: eleccion, cargo: Cargo.find_by(nombre: Cargo::CARGOS[:diputado]))
    f1 = create(:fuerza)
    f2 = create(:fuerza)
    r1 = create(:resultado_eleccion, eleccion_cargo: ec_dipu, fuerza: f1)
    create(:resultado_eleccion, eleccion_cargo: ec_dipu, fuerza: f2)

    assert_equal r1, ec_dipu.resultado(f1), 'El resultado para el cargo de diputado y la fuerza 1'
  end

  test 'asignar escanios con empate' do
    eleccion = create(:eleccion)
    cargo = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    ec = create(:eleccion_cargo, eleccion: eleccion, cargo: cargo, cantidad_escanios: 9)

    f1 = create(:fuerza, nombre: 'f1')
    l1 = f1.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)

    f2 = create(:fuerza, nombre: 'f2')
    l2 = f2.listas.create!(nombre: 'ListaPrueba2', eleccion: eleccion)

    f3 = create(:fuerza, nombre: 'f3')
    l3 = f3.listas.create!(nombre: 'ListaPrueba3', eleccion: eleccion)

    (1..10).map { |num| create(:candidatura, lista: l1, persona: create(:persona, nombres: "Nombres-1-#{num}", apellidos: "Apellidos-1-#{num}", numero_documento: 4444444 + num, sexo: 'F', en_padron: true), eleccion: eleccion, cargo: cargo, orden: num) }
    (1..10).map { |num| create(:candidatura, lista: l2, persona: create(:persona, nombres: "Nombres-2-#{num}", apellidos: "Apellidos-2-#{num}", numero_documento: 5444444 + num, sexo: 'M', en_padron: true), eleccion: eleccion, cargo: cargo, orden: num) }
    (1..10).map { |num| create(:candidatura, lista: l3, persona: create(:persona, nombres: "Nombres-3-#{num}", apellidos: "Apellidos-3-#{num}", numero_documento: 6444444 + num, sexo: 'F', en_padron: true), eleccion: eleccion, cargo: cargo, orden: num) }
    ec.resultados_elecciones << create(:resultado_eleccion, eleccion_cargo: ec, fuerza: f1, cantidad_votos: 300)
    ec.resultados_elecciones << create(:resultado_eleccion, eleccion_cargo: ec, fuerza: f2, cantidad_votos: 100)
    ec.resultados_elecciones << create(:resultado_eleccion, eleccion_cargo: ec, fuerza: f3, cantidad_votos: 200)

    ganadoras = RepartoDHondt.asignar_escanios(ec).map { |c| c.fuerza.nombre }

    assert_equal %w(f1 f3 f1 f1 f3 f2 f1 f3 f1), ganadoras, "Así se resuelven los empates"
  end

  test 'asignar_escanios' do
    cantidad_escanios = 2

    ec1 = armar_datos_para_dhondt(cantidad_escanios, 'uno', 1)
    ec2 = armar_datos_para_dhondt(cantidad_escanios, 'dos', 2)

    ganadoras_dhondt = RepartoDHondt.asignar_escanios(ec1).map { |c| [c.fuerza.nombre, c.orden] }
    ganadoras_ec2 = ec2.asignar_escanios.map { |c| [c.fuerza.nombre, c.orden] }

    assert_equal ganadoras_dhondt, ganadoras_ec2, "El cargo de la elección debe repartir por D'Hondt y con los datos que posee"
  end

  def armar_datos_para_dhondt(cantidad_escanios, prefijo_lista, id_llamada)
    eleccion = create(:eleccion)
    cargo = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    ec = create(:eleccion_cargo, eleccion: eleccion, cargo: cargo, cantidad_escanios: cantidad_escanios)
    fuerzas = (1..2).map { |num| create(:fuerza, nombre: "Fuerza-#{num}") }
    fuerzas.each_with_index { |fuerza, i| fuerza.listas.create!(nombre: "ListaPrueba#{prefijo_lista + i.to_s}", eleccion: eleccion) }
    fuerzas.each_with_index do |f, i|
      (1..5).map { |orden| create(:candidatura, fuerza: f, lista: f.listas.first, persona: create(:persona, nombres: "Nombres-#{i}-#{orden}-#{id_llamada}", apellidos: "Apellidos-#{i}-#{orden}-#{id_llamada}", numero_documento: "#{id_llamada}444444".to_i * (i + 1) + orden, sexo: 'M', en_padron: true), eleccion: eleccion, cargo: cargo, orden: orden) }
    end

    ec.resultados_elecciones << create(:resultado_eleccion, fuerza: fuerzas[0], cantidad_votos: 7600, eleccion_cargo: ec)
    ec.resultados_elecciones << create(:resultado_eleccion, fuerza: fuerzas[1], cantidad_votos: 4200, eleccion_cargo: ec)

    ec
  end

  test "Reparto D'Hondt" do
    eleccion = create(:eleccion)
    cargo = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    ec = create(:eleccion_cargo, eleccion: eleccion, cargo: cargo, cantidad_escanios: 5)
    fuerzas = (1..5).map { |num| create(:fuerza, nombre: num) }
    fuerzas.each_with_index { |fuerza, i| fuerza.listas.create!(nombre: "ListaPrueba#{i}", eleccion: eleccion) }
    fuerzas.each_with_index do |f, i|
      # (1..5).map { |orden| create(:candidatura, lista: f.listas.first, persona: create(:persona, nombres: "Nombres-#{i}-#{orden}", apellidos: "Apellidos#{i}-#{orden}", numero_documento: 4444444 * i + orden, sexo: 'M'), eleccion: eleccion, cargo: cargo, orden: orden) }
      (1..5).map { |orden| create(:candidatura, lista: f.listas.first, persona: create(:persona, nombres: "Nombres-#{i}-#{orden}", apellidos: "Apellidos#{i}-#{orden}", numero_documento: "#{i}4444444".to_i + orden, sexo: 'M', en_padron: true), eleccion: eleccion, cargo: cargo, orden: orden) }
    end

    comuna1 = Comuna.find_by(numero: 1)
    comuna2 = Comuna.find_by(numero: 2)

    ec.resultados_elecciones << create(:resultado_eleccion, fuerza: fuerzas[0], cantidad_votos: 3800, eleccion_cargo: ec, comuna: comuna1)
    ec.resultados_elecciones << create(:resultado_eleccion, fuerza: fuerzas[1], cantidad_votos: 2100, eleccion_cargo: ec, comuna: comuna1)
    ec.resultados_elecciones << create(:resultado_eleccion, fuerza: fuerzas[2], cantidad_votos: 3300, eleccion_cargo: ec, comuna: comuna1)
    ec.resultados_elecciones << create(:resultado_eleccion, fuerza: fuerzas[3], cantidad_votos: 800, eleccion_cargo: ec, comuna: comuna1)
    ec.resultados_elecciones << create(:resultado_eleccion, fuerza: fuerzas[4], cantidad_votos: 700, eleccion_cargo: ec, comuna: comuna1)

    ec.resultados_elecciones << create(:resultado_eleccion, fuerza: fuerzas[0], cantidad_votos: 3800, eleccion_cargo: ec, comuna: comuna2)
    ec.resultados_elecciones << create(:resultado_eleccion, fuerza: fuerzas[1], cantidad_votos: 2100, eleccion_cargo: ec, comuna: comuna2)

    ganadoras = RepartoDHondt.asignar_escanios(ec)

    assert_equal 5, ganadoras.size, "Se asignan 5 escaños"

    assert_equal 3, ganadoras.select { |c| c.fuerza == fuerzas[0] }.size, "3 escaños a la fuerza 1"
    assert_equal 1, ganadoras.select { |c| c.fuerza == fuerzas[1] }.size, "1 escaño a la fuerza 2"
    assert_equal 1, ganadoras.select { |c| c.fuerza == fuerzas[2] }.size, "1 escaño a la fuerza 3"

    verificar_datos ganadoras[0], 7600, 1
    verificar_datos ganadoras[1], 4200, 1
    verificar_datos ganadoras[2], 3800, 2
    verificar_datos ganadoras[3], 3300, 1
    verificar_datos ganadoras[4], 7600 / 3, 3
  end

  test 'seleccionar' do
    eleccion_cargo = EleccionCargo.new

    assert_not eleccion_cargo.seleccionado, 'El cargo está seleccionado'
    eleccion_cargo.seleccionar
    assert eleccion_cargo.seleccionado, 'El cargo no está seleccionado'
  end

  # FIXME: Chequear cuando se defina la lógica del Dhondt
  # test 'cantidad de candidaturas insuficientes para repartir dhondt' do
  #   eleccion = elecciones(:one)
  #   cargo = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
  #   ec = create(:eleccion_cargo, eleccion: eleccion, cargo: cargo, cantidad_escanios: 5)
  #   fuerza = create(:fuerza)
  #   lista = fuerza.listas.create(nombre: 'ListaPrueba1', eleccion: eleccion)
  #   create(:candidatura, lista: lista, eleccion: eleccion, cargo: cargo, orden: 1)
  #   ec.resultados_elecciones << create(:resultado_eleccion, fuerza: fuerza, cantidad_votos: 100, eleccion_cargo: ec, comuna: Comuna.find_by(numero: 1))

  #   assert_raise RepartoError do
  #     RepartoDHondt.asignar_escanios(ec)
  #   end
  # end

  def verificar_datos(candidatura, cociente, divisor)
    assert candidatura.tiene_escanio, "Tiene un escaño asignado"
    assert_equal cociente, candidatura.cociente.to_i, "Su cociente fue #{cociente}"
    assert_equal divisor, candidatura.divisor, "Su divisor fue #{divisor}"
  end

end
