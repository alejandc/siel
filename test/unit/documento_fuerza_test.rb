# == Schema Information
#
# Table name: documentos_fuerzas
#
#  id          :integer          not null, primary key
#  fuerza_id   :integer
#  archivo_id  :integer
#  descripcion :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

require 'test_helper'

class DocumentoFuerzaTest < ActiveSupport::TestCase

  test 'archivo' do
    archivo = File.open('test/cpa_con_simbolo.24')
    archivo_subido = ActionDispatch::Http::UploadedFile.new(filename: 'toto', content_type: 'pdf', tempfile: archivo)

    doc = DocumentoFuerza.new
    doc.archivo = archivo_subido

    File.open('test/cpa_con_simbolo.24', 'rb') do |arch|
      assert_equal arch.read, doc.archivo.contenido, 'Debe tener el contenido del archivo'
    end
  end

end
