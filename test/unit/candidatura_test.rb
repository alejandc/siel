﻿
# == Schema Information
#
# Table name: candidaturas
#
#  id                   :integer          not null, primary key
#  persona_id           :integer
#  created_at           :datetime
#  updated_at           :datetime
#  orden                :integer
#  fotocopia_dni        :boolean
#  constancia_domicilio :boolean
#  tipo_nacionalidad    :string(255)
#  cargo_dos_periodos   :boolean
#  aceptacion_firmada   :boolean
#  deudor_alimentario   :boolean
#  inhabilitado         :boolean
#  fecha_oficializacion :datetime
#  reside_en_comuna     :boolean
#  fecha_renuncia       :datetime
#  dni_tarjeta          :boolean
#  divisor              :integer
#  cociente             :decimal(12, 4)
#  tiene_escanio        :boolean
#  eleccion_id          :integer
#  cargo_id             :integer
#  validaciones         :text(16777215)
#  lista_cargo_id       :integer
#  antiguedad_persona   :integer
#  antiguedad_comuna    :integer
#  lista_cargo_paso_id  :integer
#

require 'test_helper'

class CandidaturaTest < ActiveSupport::TestCase

  test 'resultado para la fuerza y eleccion cargo de la candidatura' do
    eleccion = create(:eleccion)
    dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    ec_dipu = create(:eleccion_cargo, eleccion: eleccion, cargo: dipu)
    persona1 = personas(:one)
    persona2 = personas(:two)
    f1 = create(:fuerza)
    f1.listas.create(nombre: 'ListaPrueba1', eleccion: eleccion)
    f2 = create(:fuerza)
    f2.listas.create(nombre: 'ListaPrueba2', eleccion: eleccion)
    r1 = create(:resultado_eleccion, eleccion_cargo: ec_dipu, fuerza: f1)
    create(:resultado_eleccion, eleccion_cargo: ec_dipu, fuerza: f2)
    c1 = create(:candidatura, fuerza: f1, lista: f1.listas.first, eleccion: eleccion, cargo: dipu, persona: persona1)
    create(:candidatura, fuerza: f2, lista: f2.listas.first, eleccion: eleccion, cargo: dipu, persona: persona2)

    assert_equal r1, c1.resultado, 'El resultado para el cargo de diputado y la fuerza 1'
  end

  test 'find_by_eleccion_documentos' do
    dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])

    e1 = elecciones(:one)
    e2 = elecciones(:two)
    e2.eleccion_cargos.create!(cargo: dipu)

    fuerza1 = fuerzas(:one)
    lista1 = fuerza1.listas.create!(nombre: 'ListaPrueba1', eleccion: e1)

    fuerza2 = fuerzas(:two)
    lista2 = fuerza2.listas.create!(nombre: 'ListaPrueba2', eleccion: e2)

    personas = (1001..1004).map { |nro_doc| create(:persona, nombres: 'Nombres', apellidos: 'Apellidos', numero_documento: nro_doc, sexo: 'F', en_padron: true) }

    c1 = lista1.crear_candidatura(dipu, personas[0])
    c1.save
    c2 = lista1.crear_candidatura(dipu, personas[1])
    c2.save

    lista1.crear_candidatura(dipu, personas[2]).save

    lista2.crear_candidatura(dipu, personas[3]).save

    candidaturas = Candidatura.find_by_eleccion_documentos(e1, [1001, 1002, 1004])

    assert_equal 2, candidaturas.size, "Hay dos candidaturas para esa elección y esos documentos"
    assert candidaturas.include?(c1), 'Incluye la candidatura de la persona 1001'
    assert candidaturas.include?(c2), 'Incluye la candidatura de la persona 1002'
  end

  test 'nativo?' do
    nativo = build(:candidatura, tipo_nacionalidad: 'NATIVO')
    por_opcion = build(:candidatura, tipo_nacionalidad: 'POR_OPCION')
    naturalizado = build(:candidatura, tipo_nacionalidad: 'NATURALIZADO')

    assert nativo.nativo?, 'El primero es nativo'
    assert !por_opcion.nativo?, "El por opción no es nativo"
    assert !naturalizado.nativo?, 'El naturalizado no es nativo'
  end

  test 'renunciar' do
    eleccion = create(:eleccion)
    comu = cargos(:comuna_1)
    create(:eleccion_cargo, eleccion: eleccion, cargo: comu)
    fuerza = create(:fuerza)
    lista = fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)

    personas = []
    candidaturas = []

    6.times do |i|
      personas[i] = Persona.find_by(numero_documento: i) || create(:persona, nombres: 'Nombres', apellidos: 'Apellidos', numero_documento: i, sexo: 'M', en_padron: true)
      candidaturas[i] = lista.crear_candidatura(comu, personas[i])
      candidaturas[i].save
    end

    candidaturas[3].renunciar Date.today
    dnis = lista.candidaturas_eleccion.map { |c| c.persona.numero_documento }
    ordenes = lista.candidaturas_eleccion.map(&:orden)

    assert_equal [0, 1, 2, 4, 5], dnis, 'Se debe haber renunciado el cuarto'
    assert_equal [1, 2, 3, 4, 5], ordenes, "Y los órdenes deben haber quedado acomodados"
  end

  test 'borrar' do
    eleccion = elecciones(:one)
    comu = cargos(:comuna_1)
    create(:eleccion_cargo, eleccion: eleccion, cargo: comu)
    fuerza = create(:fuerza)
    lista = fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)

    personas = []
    candidaturas = []

    6.times do |i|
      personas[i] = Persona.find_by(numero_documento: i) || create(:persona, nombres: 'Nombres', apellidos: 'Apellidos', numero_documento: i, sexo: 'F', en_padron: true)
      candidaturas[i] = lista.crear_candidatura(comu, personas[i])
      candidaturas[i].save
    end

    candidaturas[3].borrar
    dnis = lista.candidaturas_eleccion.map { |c| c.persona.numero_documento }
    ordenes = lista.candidaturas_eleccion.map(&:orden)

    assert_equal [0, 1, 2, 4, 5], dnis, 'Se debe haber borrado el cuarto'
    assert_equal [1, 2, 3, 4, 5], ordenes, "Y los órdenes deben haber quedado acomodados"
  end

  test 'mover_posiciones' do
    eleccion = create(:eleccion)
    comu = cargos(:comuna_1)
    create(:eleccion_cargo, eleccion: eleccion, cargo: comu)
    fuerza = create(:fuerza)
    lista = fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)

    candidaturas = []
    personas = []

    6.times do |i|
      personas[i] = Persona.find_by(numero_documento: i) || create(:persona, nombres: 'Nombres', apellidos: 'Apellidos', numero_documento: i, sexo: 'M', en_padron: true)
      candidaturas[i] = lista.crear_candidatura(comu, personas[i])
      candidaturas[i].save
    end

    movidas = Candidatura.mover_posiciones(candidaturas[3..5], -1)

    assert_equal [3, 4, 5], movidas.map(&:orden), "Debe haber restado una posición a cada uno"
  end

  test 'proximas' do
    eleccion = elecciones(:one)
    comu = cargos(:comuna_1)
    create(:eleccion_cargo, eleccion: eleccion, cargo: comu)
    fuerza = create(:fuerza)
    lista = fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)

    candidaturas = []
    personas = []

    10.times do |i|
      personas[i] = Persona.find_by(numero_documento: i) || create(:persona, nombres: 'Nombres', apellidos: 'Apellidos', numero_documento: i, sexo: 'M', en_padron: true)
      candidaturas[i] = lista.crear_candidatura(comu, personas[i])
      candidaturas[i].save
    end

    candidaturas[6].fecha_renuncia = Date.today
    candidaturas[6].save

    dnis = candidaturas[4].proximas.map { |c| c.persona.numero_documento }

    assert_equal [5, 7, 8, 9], dnis, "Los próximos del 4 son del 5 al 9"
  end

  test 'titular?' do
    eleccion = create(:eleccion)
    cargo = cargos(:comuna_1)
    create(:eleccion_cargo, eleccion: eleccion, cargo: cargo)
    fuerza = create(:fuerza)
    lista = fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)

    candidaturas = []

    11.times do |i|
      candidaturas[i] = lista.crear_candidatura(cargo, create(:persona, nombres: "Nombres#{i}", apellidos: "Apellidos#{i}", numero_documento: 4444444 + i, sexo: 'F', en_padron: true))
      candidaturas[i].save
    end

    assert candidaturas[6].titular?, 'Hasta el séptimo son titulares'
    assert !candidaturas[7].titular?, 'El octavo ya es suplente'
  end

  test 'documentacion_completa?' do
    constancia_domicilio = candidaturas(:ucd_2011_jefe_1)
    fotocopia_dni = candidaturas(:ucd_2011_vicejefe_1)
    aceptacion_firmada = candidaturas(:ucd_2011_diputado_1)
    nativo_caba_sin_constancia = candidaturas(:twenty)
    completo = candidaturas(:pro_2011_diputado_1)

    assert !constancia_domicilio.documentacion_completa?, 'Le falta constancia de domicilio'
    assert !fotocopia_dni.documentacion_completa?, "No presentó fotocopia del DNI"
    assert !aceptacion_firmada.documentacion_completa?, "No presentó la aceptación de candidatura firmada"
    assert nativo_caba_sin_constancia.documentacion_completa?, "No presentó constancia de domicilio, pero es nativo CABA"
    assert completo.documentacion_completa?, "Esta candidatura presentó todos sus documentos"
  end

  test 'documentacion incompleta con domicilio en blanco' do
    dom_incompleto = build(:domicilio)
    dom_completo = build(:domicilio, calle: 'calle', numero: 'numero')
    per_incompleta = build(:persona, domicilio: dom_incompleto, nativo_caba: true)
    per_completa = build(:persona, domicilio: dom_completo, nativo_caba: true, fecha_nacimiento: '1980-01-01')
    can_completa = build(:candidatura, persona: per_completa, constancia_domicilio: true, fotocopia_dni: true, aceptacion_firmada: true)
    can_incompleta = build(:candidatura, persona: per_incompleta, constancia_domicilio: true, fotocopia_dni: true, aceptacion_firmada: true)

    assert can_completa.documentacion_completa?, 'Posee el domicilio completo'
    assert !can_incompleta.documentacion_completa?, 'No posee un domicilio completo'
  end

  test 'figura_en_padron?' do
    no_figura = candidaturas(:three)
    figura = candidaturas(:ucd_2011_comunero_1)

    assert !no_figura.figura_en_padron?, 'La candidatura :three no figura'
    assert figura.figura_en_padron?, 'Alvaro si figura en el padron'
  end

  test 'renglon_padron' do
    candidatura = candidaturas(:ucd_2011_comunero_1)

    assert_equal renglones_padron(:one), candidatura.renglon_padron, "El renglón que corresponde a Alvaro"
  end

  test 'No dejar en warning candidatos sin su domicilio cargado' do
    eleccion = elecciones(:one)
    fuerza = fuerzas(:one)
    fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)
    domicilio = build(:domicilio)
    p = build(:persona, nativo_caba: true, domicilio: domicilio, fecha_nacimiento: '1980-01-01')
    c = build(:candidatura, fuerza: fuerza, lista: fuerza.listas.first, eleccion: mock_eleccion_cargo.eleccion, cargo: mock_eleccion_cargo.cargo, persona: p, constancia_domicilio: true, fotocopia_dni: true, aceptacion_firmada: true)

    assert c.documentacion_completa?, "La documentación está completa"
  end

  test "la candidatura es válida si no está cargada la fecha de nacimiento" do
    eleccion = elecciones(:one)
    fuerza = fuerzas(:one)
    fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)

    domicilio = build(:domicilio, calle: 'calle', numero: '333')
    p = build(:persona, nativo_caba: true, domicilio: domicilio)
    c = build(:candidatura, fuerza: fuerza, lista: fuerza.listas.first, eleccion: mock_eleccion_cargo.eleccion, cargo: mock_eleccion_cargo.cargo, persona: p, constancia_domicilio: true, fotocopia_dni: true, aceptacion_firmada: true)

    assert c.valida?, 'Le falta la fecha de nacimiento a la persona, pero eso no invalida.'
  end

  test "la documentación no está completa si no se cargó fecha de nacimiento" do
    eleccion = elecciones(:one)
    fuerza = fuerzas(:one)
    fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)
    domicilio = build(:domicilio, calle: 'calle', numero: '333')
    p = build(:persona, nativo_caba: true, domicilio: domicilio)
    c = build(:candidatura, fuerza: fuerza, lista: fuerza.listas.first, eleccion: mock_eleccion_cargo.eleccion, cargo: mock_eleccion_cargo.cargo, persona: p, constancia_domicilio: true, fotocopia_dni: true, aceptacion_firmada: true)

    assert !c.documentacion_completa?, "No tiene fecha de nacimiento, no está completa la documentación"
  end

  test 'lista insalvable de 3 diputados masculinos' do
    eleccion = elecciones(:one)
    fuerza = fuerzas(:one)
    fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)
    dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    personas = (0..2).map { |i| build(:persona, numero_documento: i, sexo: 'M') }
    candis = personas.each_with_index.map { |p, i| build(:candidatura, fuerza: fuerza, lista: fuerza.listas.first, persona: p, eleccion: eleccion, cargo: dipu, orden: i + 1) }

    Candidatura.sugerir_orden_cargo(candis)
    ordenadas = candis.map { |c| c.persona.numero_documento }

    assert_equal [0, 1, 2], ordenadas, 'No debe mover la lista'
  end

  test 'lista ordenable' do
    candidaturas = ordenar_from_sexos('M-M-M-M-F-F')
    dnis = candidaturas.map { |c| c.persona.numero_documento }

    assert_equal [0, 1, 4, 2, 3, 5], dnis, 'Acomoda la lista cambiando el tercero por la quinta'
  end

  # FIXME: ARREGLAR
  # test 'mover un item correctamente, detenerse y encontrar uno en falta' do
  #   candidaturas = ordenar_from_sexos('M-M-M-M-F-M-M')
  #   dnis = candidaturas.map { |c| c.persona.numero_documento }

  #   assert_equal [0, 1, 4, 2, 3, 5, 6], dnis, 'Solo mueve un item y luego no puede continuar'

  #   primero_en_falta = Candidatura.tercero_mismo_sexo(candidaturas)

  #   assert_equal 5, primero_en_falta.persona.numero_documento, 'La persona con DNI 5 es el primero que se cae de la lista'
  # end

  test 'no incumplir para distintos cargos' do
    candidaturas = ordenar_from_sexos('M-M-M')

    jefe = Cargo.find_by(nombre: Cargo::CARGOS[:jefe])
    ec = build(:eleccion_cargo, eleccion: build(:eleccion), cargo: jefe)
    candidaturas[2].eleccion = ec.eleccion
    candidaturas[2].cargo = ec.cargo

    Candidatura.marcar_incumplimientos_cupo!(candidaturas)

    assert !candidaturas[2].incumple_cupo, 'Al ser para otro cargo, no debe marcarlo'
  end

  test 'marcar_incumplimientos_cupo' do
    candidaturas = ordenar_from_sexos('M-M-M')

    Candidatura.marcar_incumplimientos_cupo!(candidaturas)

    assert !candidaturas[0].incumple_cupo, "El primer M está bien"
    assert !candidaturas[1].incumple_cupo, "El segundo M también está bien"
    assert candidaturas[2].incumple_cupo, "El tercero ya está incumpliendo"
  end

  def ordenar_from_sexos(sexos)
    ec = mock_eleccion_cargo
    fuerza = fuerzas(:one)
    fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: ec.eleccion)
    personas = sexos.split('-').each_with_index.map { |c, i| build(:persona, numero_documento: i, sexo: c) }
    candidaturas = personas.each_with_index.map { |p, i| build(:candidatura, fuerza: fuerza, lista: fuerza.listas.first, persona: p, eleccion: ec.eleccion, cargo: ec.cargo, orden: i + 1) }

    Candidatura.sugerir_orden_cargo(candidaturas)

    candidaturas
  end

  def mock_eleccion_cargo
    eleccion = elecciones(:one)
    dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    create(:eleccion_cargo, eleccion: eleccion, cargo: dipu)
  end

  test 'recuperar candidaturas borradas para eleccion y fuerza' do
    eleccion = elecciones(:one)
    dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    create(:eleccion_cargo, eleccion: eleccion, cargo: dipu)

    f1 = fuerzas(:one)
    f1.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)

    f2 = fuerzas(:two)
    f2.listas.create!(nombre: 'ListaPrueba2', eleccion: eleccion)

    cs1 = (1..2).map { |i| create(:candidatura, fuerza: f1, lista: f1.listas.first, persona: create(:persona, nombres: 'Nombres', apellidos: 'Apellidos', numero_documento: 3154 + i, sexo: 'F', en_padron: true), eleccion: eleccion, cargo: dipu, orden: i) }
    cs2 = (1..2).map { |i| create(:candidatura, fuerza: f2, lista: f2.listas.first, persona: create(:persona, nombres: 'Nombres', apellidos: 'Apellidos', numero_documento: 4154 + i, sexo: 'F', en_padron: true), eleccion: eleccion, cargo: dipu, orden: i) }

    cs1[1].destroy
    cs2[0].destroy

    borradas1 = Candidatura.destroyed(eleccion, f1)
    borradas2 = Candidatura.destroyed(eleccion, f2)

    assert_equal cs1[1], borradas1[0].reify, 'Recupera la segunda candidatura de la fuerza 1'
    assert_equal cs2[0], borradas2[0].reify, 'Recupera la primera candidatura de la fuerza 2'
  end

  test 'campos' do
    candidatura = build(:candidatura)

    assert candidatura.respond_to?(:dni_tarjeta), 'Tiene un campo para marcar DNI tarjeta'
  end

  # FIXME: Cuando se termine con la parte de inhibiciones
  # test 'valida?' do
  #   muy_joven = candidaturas(:pro_2011_diputado_1)
  #   deudor_alimentario = candidaturas(:ucd_2011_jefe_1)
  #   inhabilitado = candidaturas(:one)
  #   cargo_dos_periodos = candidaturas(:two)

  #   assert !muy_joven.valida?, 'No le da la edad para ser candidato'
  #   assert !deudor_alimentario.valida?, 'Es deudor alimentario'
  #   assert !inhabilitado.valida?, "Está inhabilitado"
  #   assert !cargo_dos_periodos.valida?, "Tuvo el mismo cargo en los últimos dos períodos"
  # end

  test 'edad_valida?' do
    alvaro_jefe = candidaturas(:ucd_2011_jefe_1)
    alvaro_comunero = candidaturas(:ucd_2011_comunero_1)

    assert alvaro_jefe.edad_valida?, "Álvaro tiene más de 30 al momento de la elección"
    assert alvaro_comunero.edad_valida?, "Alvaro tiene 18 al momento de la elección"
  end

  test 'fecha_validacion_edad' do
    completas_2011 = elecciones(:completas_2011)
    jefe = candidaturas(:ucd_2011_jefe_1)
    vicejefe = candidaturas(:ucd_2011_vicejefe_1)
    comunero = candidaturas(:ucd_2011_comunero_1)
    persona = personas(:one)
    fuerza = fuerzas(:one)
    fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: completas_2011)

    assert_equal completas_2011.fecha, jefe.fecha_validacion_edad, "Para jefe de gobierno se compara con la fecha de elección"
    assert_equal completas_2011.fecha, vicejefe.fecha_validacion_edad, "Para vicejefe se compara con la fecha de elección"
    assert_equal completas_2011.fecha, comunero.fecha_validacion_edad, "Para comuneros se compara con la fecha de elección"

    fecha_posesion = Date.new(2013, 12, 10)
    e2 = build(:eleccion, fecha: '2013-10-27', fecha_posesion: fecha_posesion)
    ec = build(:eleccion_cargo, eleccion: e2, cargo: Cargo.find_by(nombre: Cargo::CARGOS[:diputado]))
    c1 = build(:candidatura, fuerza: fuerza, lista: fuerza.listas.first, eleccion: ec.eleccion, cargo: ec.cargo, persona: persona)

    assert_equal fecha_posesion, c1.fecha_validacion_edad, "Los diputados se validan la edad contra la fecha de posesión del cargo"
  end

  test 'constancia_aceptacion' do
    ucd_2011_jefe_1 = candidaturas(:ucd_2011_jefe_1)
    assert_equal AceptacionJefePdf, ucd_2011_jefe_1.constancia_aceptacion.class, 'Debe generar la constancia para jefe'

    ucd_2011_vicejefe_1 = candidaturas(:ucd_2011_vicejefe_1)
    assert_equal AceptacionVicejefePdf, ucd_2011_vicejefe_1.constancia_aceptacion.class, 'Debe generar la constancia para vicejefe'

    ucd_2011_diputado_1 = candidaturas(:ucd_2011_diputado_1)
    assert_equal AceptacionDiputadoPdf, ucd_2011_diputado_1.constancia_aceptacion.class, 'Debe generar la constancia para diputado'

    ucd_2011_comunero_1 = candidaturas(:ucd_2011_comunero_1)
    assert_equal AceptacionComuneroPdf, ucd_2011_comunero_1.constancia_aceptacion.class, 'Debe generar la constancia para comunero'
  end

  test 'filtrar' do
    # FIXME: Chequear porque la candidatura no se relaciona más con la fuerza
    # de_partidos = Candidatura.filtrar(tipo_fuerza: 'PARTIDO')
    # assert_equal 14, de_partidos.size, 'Hay ocho candidaturas de partidos'

    de_eleccion = Candidatura.filtrar(eleccion: elecciones(:two).id)
    assert_equal 2, de_eleccion.size, 'Hay dos de la eleccion dos'

    de_vice = Candidatura.filtrar(cargo: cargos(:vicejefe).id)
    assert_equal 17, de_vice.size, 'Hay una candidatura de vice'

    de_persona_7 = Candidatura.filtrar(numero_documento: 7)
    assert_equal 2, de_persona_7.size, 'Hay dos candidaturas de la persona con DNI 7'

    de_gomez = Candidatura.filtrar(apellido: 'Gomez')
    assert_equal 5, de_gomez.size, 'Hay una candidatura de Gomez'
  end

  test 'primero?' do
    primera = build(:candidatura, orden: 1)
    segunda = build(:candidatura, orden: 2)

    assert primera.primero?, 'La primera es primera'
    assert !segunda.primero?, 'La segunda no es primera'
  end

  test 'ultimo?' do
    assert candidaturas(:twenty).ultimo?, 'El twenty es el ultimo'
    assert candidaturas(:two).ultimo?, 'El two es el ultimo en su cargo'
    assert !candidaturas(:one).ultimo?, 'El primero no es el ultimo'
    assert !candidaturas(:seven).ultimo?, 'El penultimo no es el ultimo'
  end

  test 'cambiar_orden hacia arriba' do
    tercera = candidaturas(:pro_2011_diputado_3)
    tercera.cambiar_orden!(1)
    primera = candidaturas(:pro_2011_diputado_1)
    segunda = candidaturas(:pro_2011_diputado_2)

    assert_equal 2, primera.orden, 'La primera tiene que estar en la posición dos'
    assert_equal 3, segunda.orden, 'La segunda tiene que estar en la posición tres'
  end

  test 'cambiar_orden hacia abajo' do
    segunda = candidaturas(:pro_2011_diputado_2)
    segunda.cambiar_orden!(4)
    tercera = candidaturas(:pro_2011_diputado_3)
    cuarta = candidaturas(:pro_2011_diputado_4)

    assert_equal 2, tercera.orden, 'La tercera tiene que estar en la posición dos'
    assert_equal 3, cuarta.orden, 'La cuarta tiene que estar en la posición tres'
  end

  # FIXME: Chequear si se seguiran usando los metodos
  # test 'subir' do
  #   ocho = candidaturas(:eight)
  #   ocho.subir

  #   siete = candidaturas(:seven)

  #   assert_equal 7, ocho.orden, 'El octavo debe haber subido a la posicion 7'
  #   assert_equal 8, siete.orden, 'El septimo debe haber quedado en la posicion 8'
  # end

  # test 'bajar' do
  #   one = candidaturas(:one)
  #   one.bajar

  #   two = candidaturas(:two)

  #   assert_equal 2, one.orden, 'El primero debe haber bajado a la posicion dos'
  #   assert_equal 1, two.orden, 'El segundo debe haber quedado en la posicion 1'
  # end

  # FIXME: ARREGLAR
  # test 'no modificar lista bien ordenada' do
  #   lista = listas(:three)

  #   candidaturas = lista.candidaturas_eleccion.to_a

  #   reordenado = Candidatura.auto_ordenamiento(candidaturas)
  #   dnis = reordenado.map { |c| c.persona.numero_documento }

  #   assert_equal [1, 2, 6, 3, 4, 7, 5, 8, 8, 202, 101], dnis, 'No se debe haber modificado'
  # end

  test 'primero_sexo' do
    lista1 = listas(:one)

    candidaturas = lista1.candidaturas_eleccion
    candidaturas = candidaturas.to_a.sort

    primero = Candidatura.primero_sexo(candidaturas, 'F')

    assert_equal 6, primero.persona.numero_documento, 'La persona :four es la primera de sexo femenino en la lista'
  end

  test 'mover_item' do
    eleccion = create(:eleccion)
    diputado = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    EleccionCargo.create(eleccion: eleccion, cargo: diputado, seleccionado: true)
    personas = [create(:persona, nombres: 'Nombres', apellidos: 'Apellidos', numero_documento: 4444444, sexo: 'M', en_padron: true), create(:persona, nombres: 'Nombres2', apellidos: 'Apellidos2', numero_documento: 4444445, sexo: 'F', en_padron: true)]
    fuerza = fuerzas(:one)
    fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)
    candidaturas = (1..2).map { |i| create(:candidatura, fuerza: fuerza, lista: fuerza.listas.first, persona: personas[i - 1], eleccion: eleccion, cargo: diputado, orden: i) }

    c1 = candidaturas[0]
    c2 = candidaturas[1]

    Candidatura.mover_item(candidaturas, candidaturas[1], 1)

    assert_equal [c2, c1], candidaturas, "Se cambió el orden de las candidaturas"
    assert_equal 2, c1.orden, "La primer candidatura se movió al segundo lugar"
    assert_equal 1, c2.orden, "La segunda candidatura se movió al primer lugar"

    c1_en_base = Candidatura.find(c1.id)

    assert_not_equal c1_en_base.orden, c1.orden, "No se grabó la información en la base de datos"
  end

  test 'mover_item!' do
    eleccion = elecciones(:one)
    jefe = Cargo.find_by(nombre: Cargo::CARGOS[:jefe])
    personas = build_personas(%w(M M M F M M))
    fuerza = fuerzas(:one)
    fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)
    candidaturas = build_candidaturas(eleccion, fuerza, personas, jefe)

    Candidatura.mover_item!(candidaturas, candidaturas[4], 2)

    dnis = candidaturas.map { |c| c.persona.numero_documento }

    assert_equal [301, 305, 302, 303, 304, 306], dnis, "Mover el item de la posición 4 a la 1"
  end

  test 'auto_ordenamiento' do
    eleccion = elecciones(:one)
    jefe = Cargo.find_by(nombre: Cargo::CARGOS[:jefe])
    vice = Cargo.find_by(nombre: Cargo::CARGOS[:vicejefe])
    dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    comu = Cargo.find_by(nombre: Cargo::CARGOS[:comuna_1])
    fuerza = fuerzas(:one)
    fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)

    personas = build_personas(%w(M M M M M F F F F F F M M M))
    candidaturas = build_candidaturas(eleccion, fuerza, [personas[0]], jefe)
    candidaturas += build_candidaturas(eleccion, fuerza, [personas[1]], vice)
    candidaturas += build_candidaturas(eleccion, fuerza, personas[2..7], dipu)
    candidaturas += build_candidaturas(eleccion, fuerza, personas[8..13], comu)

    reordenado = Candidatura.auto_ordenamiento(candidaturas)
    dnis = reordenado.map { |c| c.persona.numero_documento }

    assert_equal [301, 302, 303, 304, 306, 305, 307, 308, 309, 312, 310, 313, 311, 314], dnis, 'Debe cambiar el 4 por el 5 y el 10 por el 11'
  end

  test 'auto_ordenamiento_cargo' do
    eleccion = elecciones(:one)
    diputado = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    EleccionCargo.create(eleccion: eleccion, cargo: diputado, seleccionado: true)
    personas = build_personas(%w(M M M F F F M))
    fuerza = fuerzas(:one)
    lista = fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)
    candidaturas = personas.map do |persona|
      candidatura = lista.crear_candidatura(diputado, persona)
      candidatura.save
      candidatura
    end

    reordenado = Candidatura.auto_ordenamiento_cargo(candidaturas)
    dnis = reordenado.map { |c| c.persona.numero_documento }

    assert_equal [301, 302, 304, 303, 305, 306, 307], dnis, 'Reordenar acomodando un lugar'
  end

  test 'Grabar en la base los cambios del auto ordenamiento' do
    eleccion = elecciones(:one)
    diputado = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    fuerza = fuerzas(:one)
    lista = fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)
    create(:eleccion_cargo, eleccion: eleccion, cargo: diputado)
    personas = %w(M M M F).each_with_index.map { |sexo, i| create(:persona, nombres: 'Nombres', apellidos: 'Apellidos', sexo: sexo, numero_documento: 444 + i, en_padron: true) }
    candidaturas = personas.each_with_index.map { |p, i| create(:candidatura, fuerza: fuerza, lista: lista, eleccion: eleccion, cargo: diputado, persona: p, orden: i + 1) }

    Candidatura.auto_ordenamiento_cargo(candidaturas)
    c3 = Candidatura.find(candidaturas[2].id)

    assert_equal candidaturas[2].orden, c3.orden, 'Cambia el orden y lo guarda en la base de datos'
  end

  test 'sugerir_orden_cargo' do
    eleccion = create(:eleccion)
    diputado = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    personas = %w(M M M F).each_with_index.map { |sexo, index| create(:persona, nombres: 'Nombres', apellidos: 'Apellidos', numero_documento: 45879 + index, sexo: sexo, en_padron: true) }
    fuerza = fuerzas(:one)
    fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)
    EleccionCargo.create(eleccion: eleccion, cargo: diputado, seleccionado: true)
    candidaturas = personas.each_with_index.map { |p, i| create(:candidatura, fuerza: fuerza, lista: fuerza.listas.first, persona: p, eleccion: eleccion, cargo: diputado, orden: i + 1) }

    Candidatura.sugerir_orden_cargo(candidaturas)

    assert_equal [personas[0].sexo, personas[1].sexo, personas[3].sexo, personas[2].sexo], candidaturas.map { |c| c.persona.sexo }, 'Mueve el tercer masculino al final'

    c3 = Candidatura.find(candidaturas[2].id)

    assert_not_equal candidaturas[2].orden, c3.orden, 'Cambia el orden sugerido, pero no el guardado en la base'
  end

  test 'siguiente' do
    eleccion = create(:eleccion)
    jefe = Cargo.find_by(nombre: Cargo::CARGOS[:jefe])
    vice = Cargo.find_by(nombre: Cargo::CARGOS[:vicejefe])
    dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    comu = Cargo.find_by(nombre: Cargo::CARGOS[:comuna_1])
    persona = create(:persona, nombres: 'Nombres', apellidos: 'Apellidos', numero_documento: 4444444, sexo: 'M', en_padron: true)
    persona2 = create(:persona, nombres: 'Nombres2', apellidos: 'Apellidos3', numero_documento: 4444445, sexo: 'M', en_padron: true)
    persona3 = create(:persona, nombres: 'Nombres2', apellidos: 'Apellidos3', numero_documento: 4444446, sexo: 'M', en_padron: true)
    fuerza = create(:fuerza)
    lista = fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)
    EleccionCargo.create(eleccion: eleccion, cargo: jefe, seleccionado: true)
    EleccionCargo.create(eleccion: eleccion, cargo: vice, seleccionado: true)
    EleccionCargo.create(eleccion: eleccion, cargo: dipu, seleccionado: true)
    EleccionCargo.create(eleccion: eleccion, cargo: comu, seleccionado: true)

    create(:candidatura, fuerza: fuerza, lista: lista, eleccion: eleccion, cargo: jefe, persona: persona, orden: 1)
    create(:candidatura, fuerza: fuerza, lista: lista, eleccion: eleccion, cargo: vice, persona: persona, orden: 1)
    c3 = create(:candidatura, fuerza: fuerza, lista: lista, eleccion: eleccion, cargo: dipu, persona: persona, orden: 1)
    create(:candidatura, fuerza: fuerza, lista: lista, eleccion: eleccion, cargo: dipu, persona: persona2, orden: 2, fecha_renuncia: Date.today)
    c5 = create(:candidatura, fuerza: fuerza, lista: lista, eleccion: eleccion, cargo: dipu, persona: persona3, orden: 2)
    create(:candidatura, fuerza: fuerza, lista: lista, eleccion: eleccion, cargo: comu, persona: persona, orden: 2)

    assert_equal c5, c3.siguiente, 'El siguiente al 3 es el 5'
  end

  test 'anterior' do
    jefe = Cargo.find_by(nombre: Cargo::CARGOS[:jefe])
    vice = Cargo.find_by(nombre: Cargo::CARGOS[:vicejefe])
    dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    persona = create(:persona, nombres: 'Nombres', apellidos: 'Apellidos', numero_documento: 4444444, sexo: 'M', en_padron: true)
    persona2 = create(:persona, nombres: 'Nombres2', apellidos: 'Apellidos2', numero_documento: 4444445, sexo: 'M', en_padron: true)
    eleccion = create(:eleccion)
    fuerza = create(:fuerza)
    fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)
    EleccionCargo.create(eleccion: eleccion, cargo: jefe, seleccionado: true)
    EleccionCargo.create(eleccion: eleccion, cargo: vice, seleccionado: true)
    EleccionCargo.create(eleccion: eleccion, cargo: dipu, seleccionado: true)

    create(:candidatura, fuerza: fuerza, lista: fuerza.listas.first, eleccion: eleccion, cargo: jefe, persona: persona, orden: 1)
    create(:candidatura, fuerza: fuerza, lista: fuerza.listas.first, eleccion: eleccion, cargo: vice, persona: persona, orden: 1)
    c3 = create(:candidatura, fuerza: fuerza, lista: fuerza.listas.first, eleccion: eleccion, cargo: dipu, persona: persona, orden: 1)
    c4 = create(:candidatura, fuerza: fuerza, lista: fuerza.listas.first, eleccion: eleccion, cargo: dipu, persona: persona2, orden: 2)

    assert_equal c3, c4.anterior, 'El anterior al dos es el uno'
  end

  test 'mismo_sexo_consecutivo' do
    eleccion = elecciones(:one)
    diputados = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    personas = build_personas(%w(M F F F))
    fuerza = fuerzas(:one)
    fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)

    candidaturas = build_candidaturas(eleccion, fuerza, personas, diputados)
    candidaturas.sort!

    tercero = Candidatura.mismo_sexo_consecutivo(candidaturas)

    assert_equal personas[3], tercero.persona, 'La tercera candidatura de sexo femenino es la tercera del mismo sexo'
  end

  def build_candidaturas(eleccion, fuerza, personas, cargo)
    personas.each_with_index.map do |persona, i|
      build(:candidatura, fuerza: fuerza, lista: fuerza.listas.first, persona: persona, eleccion: eleccion, cargo: cargo, orden: i + 1)
    end
  end

  def build_personas(sexos)
    sexos.each_with_index.map { |sexo, i| build(:persona, nombres: 'Nombres', apellidos: 'Apellidos', sexo: sexo, numero_documento: i + 301) }
  end

  test 'comparison' do
    eleccion = elecciones(:one)
    ec_dipu = EleccionCargo.new(eleccion: eleccion, cargo: cargos(:diputados))
    ec_comu = EleccionCargo.new(eleccion: eleccion, cargo: cargos(:comuna_1))

    fuerza = fuerzas(:one)
    fuerza.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)

    c1 = Candidatura.create(lista: fuerza.listas.first, eleccion: eleccion, cargo: ec_dipu.cargo, persona: personas(:one), orden: 1)
    c2 = Candidatura.create(lista: fuerza.listas.first, eleccion: eleccion, cargo: ec_comu.cargo, persona: personas(:two), orden: 1)
    c3 = Candidatura.create(lista: fuerza.listas.first, eleccion: eleccion, cargo: ec_comu.cargo, persona: personas(:three), orden: 2)

    assert_equal(-1, (c1 <=> c2), 'El diputado es anterior al comunero')
    assert_equal(-1, (c1 <=> c3), 'El diputado es anterior al comunero')
    assert_equal(-1, (c2 <=> c3), 'Dentro de los comuneros, el 1 es previo al 2')
  end

  # FIXME: Cuando se arregle el método "importar" de las candidaturas
  # test 'importar archivo Excel' do
  #   s = Roo::Excel.new('test/candidatos1.xls')
  #   header = s.row(1)

  #   rows = (2..s.last_row).each.inject([]) { |a, i| a << Hash[[header, s.row(i)].transpose] }

  #   eleccion = elecciones(:one)
  #   fuerza = fuerzas(:one)

  #   cantidad_original = Candidatura.count

  #   Candidatura.importar(eleccion, fuerza, rows)

  #   assert_equal cantidad_original + 2, Candidatura.count, 'Debe haber insertado dos registros nuevos'

  #   personas = Persona.where(numero_documento: 11222333)

  #   assert_not_nil personas, 'Debe traer un listado de personas'
  #   assert_equal 1, personas.size, 'Tiene que traer una persona, la que acaba de importar'
  # end

end
