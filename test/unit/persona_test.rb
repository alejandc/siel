# == Schema Information
#
# Table name: personas
#
#  id                     :integer          not null, primary key
#  tipo_documento         :string(255)
#  numero_documento       :integer
#  nombres                :string(255)
#  apellidos              :string(255)
#  sexo                   :string(255)
#  fecha_nacimiento       :date
#  lugar_nacimiento       :string(255)
#  nombre_padre           :string(255)
#  nombre_madre           :string(255)
#  estado                 :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  domicilio_id           :integer
#  telefono               :string(255)
#  email                  :string(255)
#  nativo_caba            :boolean
#  nacionalidad_argentina :string(255)
#  denominacion           :string(255)
#  residencia             :string(255)
#  domicilio_completo     :string(255)
#  avatar_file_name       :string(255)
#  avatar_content_type    :string(255)
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  en_padron              :boolean          default(FALSE)
#

require 'test_helper'

class PersonaTest < ActiveSupport::TestCase

  test 'no permitir documentos duplicados' do
    numero_documento = 94555787
    create(:persona, nombres: 'Nombres', apellidos: 'Apellidos', numero_documento: numero_documento, sexo: 'M')

    duplicada = Persona.new
    duplicada.numero_documento = numero_documento

    assert !duplicada.save, 'El grabado debe devolver falso'
  end

  # FIXME: Ver si una persona puede tener más de una candidatura para una lista
  # test 'candidaturas_multiples?' do
  #   eleccion = create(:eleccion)
  #   dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
  #   ec_dipu = create(:eleccion_cargo, eleccion: eleccion, cargo: dipu)
  #   persona = Persona.find_by(numero_documento: 1)

  #   f1 = create(:fuerza)
  #   l1 = f1.listas.create!(nombre: 'ListaPrueba1', eleccion: eleccion)
  #   l1.crear_candidatura(dipu, persona)

  #   assert !persona.candidaturas_multiples?(eleccion), 'Tiene una sola candidatura'

  #   f2 = create(:fuerza)
  #   l2 = f2.listas.create!(nombre: 'ListaPrueba2', eleccion: eleccion)
  #   l2.crear_candidatura(dipu, persona)

  #   assert persona.candidaturas_multiples?(eleccion), 'Ahora tiene dos candidaturas'
  # end

  test 'es_mayor_a?' do
    fecha = DateTime.new(2013, 05, 17)
    two = personas(:two)

    assert two.es_mayor_a?(30, fecha), 'Es mayor a 30 años'
    assert !two.es_mayor_a?(50, fecha), 'pero no es mayor a 50'
  end

  test 'es_mayor_a? fecha_nacimiento nula' do
    fecha = DateTime.new(2013, 05, 17)
    two = personas(:two)
    two.fecha_nacimiento = nil

    assert !two.es_mayor_a?(30, fecha), 'Tiene fecha de nacimiento cargada'
  end

  test 'edad' do
    fecha = DateTime.new(2013, 05, 17)

    assert_equal 0, personas(:three).edad(fecha), 'No cumplio ni uno'
    assert_equal 43, personas(:two).edad(fecha), 'Tiene 43'
  end

  test 'edad fecha_nacimiento nula' do
    fecha = DateTime.new(2013, 05, 17)
    persona = personas(:three)
    persona.fecha_nacimiento = nil

    assert_equal(-1, personas(:three).edad(fecha), 'Tiene fecha de nacimiento cargada')
  end

  test 'nacionalidad' do
    persona = Persona.new

    persona.nacionalidad_argentina = Persona::NACIONALIDADES[:NATIVO]
    assert_equal 'NATIVO', persona.nacionalidad_argentina, 'La nacionalidad no es NATIVO'

    persona.nacionalidad_argentina = Persona::NACIONALIDADES[:POR_OPCION]
    assert_equal 'POR_OPCION', persona.nacionalidad_argentina, 'La nacionalidad no es POR_OPCION'

    persona.nacionalidad_argentina = Persona::NACIONALIDADES[:NATURALIZADO]
    assert_equal 'NATURALIZADO', persona.nacionalidad_argentina, 'La nacionalidad no es NATURALIZADO'
  end

  test 'residencia' do
    persona = Persona.new

    persona.residencia = Persona::RESIDENCIAS[:NATIVO_COMUNA]
    assert_equal 'NATIVO_COMUNA', persona.residencia, 'La nacionalidad no es NATIVO_COMUNA'

    persona.residencia = Persona::RESIDENCIAS[:RESIDENCIA_COMUNA]
    assert_equal 'RESIDENCIA_COMUNA', persona.residencia, 'La nacionalidad no es RESIDENCIA_COMUNA'
  end

  test 'candidaturas_multiples?' do
    persona1 = personas(:one)
    persona2 = personas(:eight)
    eleccion1 = elecciones(:one)
    eleccion2 = elecciones(:completas_2011)

    assert persona1.candidaturas_multiples?(eleccion1), 'La persona no tiene más de una candidatura para esa elección'
    assert_not persona2.candidaturas_multiples?(eleccion2), 'La persona tiene más de una candidatura para esa elección'
  end

end
