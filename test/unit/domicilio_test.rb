# == Schema Information
#
# Table name: domicilios
#
#  id               :integer          not null, primary key
#  calle_referencia :string(255)
#  numero           :string(255)
#  piso             :string(255)
#  depto            :string(255)
#  comuna           :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#  fecha            :date
#  codigo_postal    :string(255)
#  calle_nombre     :string(255)
#  origen_cd        :integer
#

require 'test_helper'

class DomicilioTest < ActiveSupport::TestCase

  test 'completo?' do
    assert build(:domicilio, calle: 'calle', numero: 'numero').completo?, "Con calle y número está completo"
    assert !build(:domicilio).completo?, "Si no tienen ningún campo completo no está completo"
    assert !build(:domicilio, calle: 'calle').completo?, "Con calle y sin número no está completo"
    assert !build(:domicilio, calle: '', numero: ' ').completo?, "Con strings vacíos tampoco está completo"
  end

end
