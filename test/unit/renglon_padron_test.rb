# encoding: utf-8
# == Schema Information
#
# Table name: renglones_padron
#
#  id             :integer          not null, primary key
#  padron_id      :integer
#  clase          :string(255)
#  apellido       :string(255)
#  nombre         :string(255)
#  profesion      :string(255)
#  domicilio      :string(255)
#  analfabeto     :string(255)
#  tipo_documento :string(255)
#  seccion        :string(255)
#  circuito       :string(255)
#  mesa           :string(255)
#  sexo           :string(255)
#  desfor         :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  matricula      :integer
#

require 'test_helper'

class RenglonPadronTest < ActiveSupport::TestCase

  test 'filtrar por nombre' do
    assert_equal [renglones_padron(:two)], RenglonPadron.filtrar(nombre: 'Alicia'), 'Hay un registro con nombre Alicia'
  end

  test 'filtrar por apellido' do
    assert_equal [renglones_padron(:one)], RenglonPadron.filtrar(apellido: 'Gomez'), 'Hay un registro con apellido Gomez'
  end

  test 'filtrar por matricula' do
    assert_equal [renglones_padron(:one)], RenglonPadron.filtrar(matricula: '101'), 'Hay un registro con documento 101'
  end

  test 'parse imported line to attributes hash' do
    line = '29277459|1982|LUNA|DIEGO ORLANDO||QUIRNO 76||DNI-EA|00007|0082|0000|M|CAP FED|01'
    params = RenglonPadron.parse_to_hash(line)

    assert_equal 29277459, params[:matricula], 'Comprobar la matricula'
    assert_equal 'LUNA', params[:apellido], 'Comprobar el apellido'
    assert_equal 'QUIRNO 76', params[:domicilio], 'Comprobar el domicilio'
  end

  test 'parse imported line with special chars' do
    line = "21473493    ABALO                                   MARIA ISABEL                            ESTUD  AV BRASIL 323 3°A                  DNI    000010001 0001F 008"
    RenglonPadron.parse_to_hash(line.force_encoding('UTF-8'))
  end

  test 'parse importe line from file with special chars' do
    File.open(Rails.root.join('test/cpa_con_simbolo.24'), 'rb').each_line do |line|
      transcoded = line.ensure_encoding('UTF-8', external_encoding: [::Encoding::UTF_8, ::Encoding::ISO_8859_1], invalid_characters: :transcode)
      RenglonPadron.parse_to_hash(transcoded)
    end
  end

  test 'parse empty line' do
    assert_nil RenglonPadron.parse_to_hash(' '), "Si es una línea sin datos también debe devolver nil"
    assert_nil RenglonPadron.parse_to_hash(''), "Si da línea vacía que devuelva nil"
  end

end
