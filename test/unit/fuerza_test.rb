# == Schema Information
#
# Table name: fuerzas
#
#  id                       :integer          not null, primary key
#  numero                   :integer
#  nombre                   :string(255)
#  expediente               :string(255)
#  tipo                     :string(255)
#  observaciones            :text(65535)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  estado                   :string(255)
#  domicilio_constituido_id :integer
#  domicilio_real_id        :integer
#  fecha_baja               :date
#  eleccion_id              :integer
#  escudo_file_name         :string(255)
#  escudo_content_type      :string(255)
#  escudo_file_size         :integer
#  escudo_updated_at        :datetime
#  web                      :string(255)
#

require 'test_helper'

class FuerzaTest < ActiveSupport::TestCase

  test 'vigente?' do
    f1 = build(:fuerza, estado: 'VIGENTE')
    f2 = build(:fuerza, estado: 'NO VIGENTE')

    assert f1.vigente?, 'El estado es VIGENTE'
    assert !f2.vigente?, 'Su estado es NO VIGENTE'
  end

  test 'calcular_cociente' do
    fuerza = build(:fuerza, divisor: 2)

    def fuerza.cantidad_votos(_)
      4000
    end

    fuerza.calcular_cociente(nil)

    assert_equal 2000, fuerza.cociente, 'El cociente es su cantidad de votos (4000) / 2'
  end

  test 'cantidad_votos' do
    fuerza = create(:fuerza)
    ec = create(:eleccion_cargo, eleccion: elecciones(:one), cargo: cargos(:diputados))
    r1 = create(:resultado_eleccion, eleccion_cargo: ec, fuerza: fuerza, cantidad_votos: 10)
    r2 = create(:resultado_eleccion, eleccion_cargo: ec, fuerza: fuerza, cantidad_votos: 10)

    assert_equal (r1.cantidad_votos + r2.cantidad_votos), fuerza.cantidad_votos(ec), 'Suma los votos de los dos resultados'
  end

  test 'partidos y alianzas eleccion' do
    cantidad_inicial = Fuerza.partidos.vigentes.activos.count

    e1 = create(:eleccion)
    e2 = create(:eleccion)
    a1 = create(:fuerza, tipo: 'ALIANZA', nombre: 'Alianza f1', eleccion: e1, estado: 'VIGENTE')
    create(:fuerza, tipo: 'ALIANZA', nombre: 'Alianza sin fuerzas', eleccion: e2, estado: 'VIGENTE')
    f1 = create(:fuerza, tipo: 'PARTIDO', nombre: 'f1', estado: 'VIGENTE')
    f2 = create(:fuerza, tipo: 'PARTIDO', nombre: 'f2', estado: 'VIGENTE')
    create(:fuerza, tipo: 'PARTIDO', nombre: 'f3', estado: 'NO VIGENTE')
    create(:fuerza, tipo: 'PARTIDO', nombre: 'f4', estado: 'VIGENTE', fecha_baja: '2013-01-01')
    create(:alianza_partido, alianza_id: a1.id, partido_id: f1.id)

    pya = Fuerza.partidos_y_alianzas(e1)

    assert_equal cantidad_inicial + 2, pya.size, 'Hay una alianza y un partido'
    assert pya.include?(a1), "'Alianza f1' es para esta elección"
    assert pya.include?(f2), "La fuerza f2 no es parte de la alianza y está vigente y activa"
  end

  test 'filtrar por eleccion' do
    e1 = create(:eleccion)
    e2 = create(:eleccion)
    a1 = create(:fuerza, tipo: 'ALIANZA', eleccion: e1)
    a2 = create(:fuerza, tipo: 'ALIANZA', eleccion: e2)

    assert_equal a1, Fuerza.alianzas_eleccion(e1).first, "Para la elección 1 está la alianza 1"
    assert_equal a2, Fuerza.alianzas_eleccion(e2).first, "Para la elección 2 está la alianza 2"
  end

  test 'resultado sin comuna' do
    ec = create(:eleccion_cargo, eleccion: create(:eleccion), cargo: Cargo.find_by(nombre: Cargo::CARGOS[:diputado]))
    f1 = create(:fuerza)
    f2 = create(:fuerza)
    r = create(:resultado_eleccion, eleccion_cargo: ec, fuerza: f1)

    assert_equal r, f1.resultado(ec), 'Debe traer el resultado creado sin comuna'
    assert f2.resultado(ec).new_record?, 'Debe crear un resultado para la fuerza 2 sin comuna'
  end

  test 'resultado' do
    e = create(:eleccion)
    dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    ec = create(:eleccion_cargo, eleccion: e, cargo: dipu)
    f1 = create(:fuerza)
    f2 = create(:fuerza)
    create(:resultado_eleccion, eleccion_cargo: ec, fuerza: f1, comuna: comunas(:comuna1))

    assert !f1.resultado(ec, comunas(:comuna1)).new_record?, 'Ya existe, no debe crearlo'
    assert f2.resultado(ec, comunas(:comuna1)).new_record?, 'No existe, lo crea y es nuevo'
  end

  test "comparar dos fuerzas políticas, utilizando su nombre" do
    f1 = Fuerza.new(nombre: 'A primera')
    f2 = Fuerza.new(nombre: 'B segunda')

    assert (f1 <=> f2) == -1, "La primera es 'menor' que la segunda"
  end

  # FIXME: Chequear si se sigue usando
  # test 'cumple_ley_cupos?' do
  #   eleccion = create(:eleccion)
  #   dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
  #   comu = Cargo.find_by(nombre: Cargo::CARGOS[:comuna_1])
  #   create(:eleccion_cargo, eleccion: eleccion, cargo: dipu)
  #   create(:eleccion_cargo, eleccion: eleccion, cargo: comu)
  #   fuerza = create(:fuerza)
  #   fuerza.listas.create(nombre: 'lista', eleccion: eleccion)

  #   create_candidaturas(create_personas(%w(M M F F M M F)), eleccion, dipu, fuerza)
  #   create_candidaturas(create_personas(%w(M M F F F)), eleccion, comu, fuerza)

  #   assert fuerza.cumple_ley_cupos_cargo?(eleccion, dipu), 'MMFFMMF es una lista que cumple la ley de cupos'
  #   assert !fuerza.cumple_ley_cupos_cargo?(eleccion, comu), 'MMFFF no cumple con la ley de cupos'
  #   assert !fuerza.cumple_ley_cupos?(eleccion), 'Uno cumple y otro no, por lo tanto no cumple'
  # end

  # FIXME: Chequear si se sigue usando
  # test 'cumple_ley_cupos_cargo?' do
  #   eleccion = create(:eleccion)
  #   dipu = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
  #   create(:eleccion_cargo, eleccion: eleccion, cargo: dipu)
  #   f1 = create(:fuerza)
  #   f2 = create(:fuerza)
  #   f1.listas.create(nombre: 'lista1', eleccion: eleccion)
  #   f2.listas.create(nombre: 'lista2', eleccion: eleccion)

  #   create_candidaturas(create_personas(%w(M M F F M M F)), eleccion, dipu, f1)
  #   create_candidaturas(create_personas(%w(M M F F F)), eleccion, dipu, f2)

  #   assert f1.cumple_ley_cupos_cargo?(eleccion, dipu), 'MMFFMMF es una lista que cumple la ley de cupos'
  #   assert !f2.cumple_ley_cupos_cargo?(eleccion, dipu), 'MMFFF no cumple con la ley de cupos'
  # end

  def create_candidaturas(personas, eleccion, cargo, fuerza)
    personas.each_with_index.map do |persona, i|
      create(:candidatura, lista: fuerza.listas.first, persona: persona,
                           eleccion: eleccion, cargo: cargo, orden: i + 1)
    end
  end

  def create_personas(sexos)
    gen = Random.new
    sexos.each.map { |s| create(:persona, nombres: 'Nombres', apellidos: 'Apellidos', sexo: s, numero_documento: gen.rand(99999999)) }
  end

  test 'nombres_archivos_expedientes' do
    fuerza = create(:fuerza, expediente: '00234/7,07332/4')

    assert_equal ['Expe-234-7.pdf', 'Expe-7332-4.pdf'], fuerza.nombres_archivos_expedientes, "Los nombres de los expedientes de la fuerza política"
  end

  test 'miembro_alianza?' do
    one = fuerzas(:one)
    ucd = fuerzas(:ucd)

    assert one.miembro_alianza?, 'El one es miembro de la alianza three'
    assert !ucd.miembro_alianza?, 'La UCD no es miembro de ninguna alianza'
  end

  test 'init_associations' do
    fuerza = Fuerza.new
    fuerza.init_associations

    assert_not fuerza.domicilio_constituido.nil?, 'Debe tener domicilio constituido'
    assert_not fuerza.domicilio_real.nil?, 'Debe tener domicilio real'
  end

  test 'add_documento' do
    fuerza = fuerzas(:one)
    descripcion = 'Archivo para probar funcionalidad'
    archivo = File.open('test/candidatos1.xls')
    archivo_subido = ActionDispatch::Http::UploadedFile.new(filename: 'toto', content_type: 'pdf', tempfile: archivo)

    fuerza.add_documento descripcion, archivo_subido

    assert_equal 1, fuerza.documentos_fuerzas.size, 'Debe tener un documento'
  end

  test 'add_boleta' do
    fuerza = fuerzas(:one)
    eleccion = elecciones(:one)
    archivo = File.open('test/candidatos1.xls')
    archivo_subido = ActionDispatch::Http::UploadedFile.new(filename: 'toto', content_type: 'pdf', tempfile: archivo)

    fuerza.add_boleta eleccion, archivo_subido

    assert_equal 1, fuerza.boletas.size, 'Debe tener una boleta adjunta'
  end

  test 'should filter by its properties' do
    resultado_uno = Fuerza.filtrar(numero: 1)

    assert_equal [fuerzas(:one)], resultado_uno, 'Debe encontrar el primer partido'

    resultado_dos = Fuerza.filtrar(nombre: 'Segunda')

    assert_equal [fuerzas(:two)], resultado_dos, 'Debe encontrar el segundo partido'
  end

  test 'should assing partidos by its IDs' do
    alianza = Fuerza.new
    alianza.ids_partidos = "#{fuerzas(:one).id},#{fuerzas(:two).id}"

    assert alianza.partidos.include?(fuerzas(:one)), 'Debe incluir el partido 1'
    assert alianza.partidos.include?(fuerzas(:two)), 'Debe incluir el partido 2'
  end

  test "should build a string with its partidos' IDs" do
    alianza = fuerzas(:three)
    ids = alianza.partidos.map(&:id).join(',')

    assert_equal ids, alianza.ids_partidos, 'Debe devolver el string con los dos ids'
  end

  test 'should retrieve only partidos' do
    partidos = Fuerza.partidos

    assert partidos.include?(fuerzas(:one)), 'Debe incluir un partido'
    assert_not partidos.include?(fuerzas(:three)), 'No debe incluir la alianza'
  end

  test 'should retrieve only alianzas' do
    alianzas = Fuerza.alianzas

    assert alianzas.include?(fuerzas(:three)), 'Debe tener la alianza'
    assert_not alianzas.include?(fuerzas(:one)), 'No debe tener partidos'
  end

  test 'should have 1 alianza with 2 partidos' do
    alianza = fuerzas(:three)

    assert_equal 2, alianza.partidos.size, 'Debe tener 2 partidos'
  end

  test 'should belong to one alianza' do
    partido = fuerzas(:one)

    assert_equal 'Primera alianza', partido.alianzas.first.nombre, 'Pertenece a la primera alianza'
  end

  test "should know if it's an alianza" do
    assert_not fuerzas(:two).es_alianza?, 'Es un partido'
    assert fuerzas(:three).es_alianza?, 'Es una alianza'
  end

  test 'should remove partidos from partido before saving' do
    alianza = Fuerza.where(numero: 3).first
    assert_not alianza.partidos_de_alianza.empty?, 'La alianza tiene partidos'

    alianza.tipo = 'PARTIDO'
    alianza.save!

    assert alianza.partidos_de_alianza.empty?, 'Ahora que es un partido ya no debe tener partidos'
  end

end
