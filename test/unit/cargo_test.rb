# == Schema Information
#
# Table name: cargos
#
#  id              :integer          not null, primary key
#  nombre          :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  orden           :integer
#  cantidad_minima :integer
#  cantidad_maxima :integer
#

require 'test_helper'

class CargoTest < ActiveSupport::TestCase

  test 'get_reparto' do
    jefe = Cargo.find_by(nombre: Cargo::CARGOS[:jefe])
    vice = Cargo.find_by(nombre: Cargo::CARGOS[:vicejefe])
    diputado = Cargo.find_by(nombre: Cargo::CARGOS[:diputado])
    comunero = Cargo.find_by(nombre: Cargo::CARGOS[:comuna_1])

    assert_equal RepartoDHondt, diputado.get_reparto, 'Para diputados el reparto es DHondt'
    assert jefe.get_reparto.nil?, 'Para jefe aun no hay reparto definido'
    assert vice.get_reparto.nil?, 'Para vice aun no hay reparto definido'
    assert comunero.get_reparto.nil?, 'Para comunero aun no hay reparto definido'
  end

  test 'edad_requerida' do
    assert_equal 30, cargos(:jefe).edad_requerida, 'Para jefe la edad es 30'
    assert_equal 30, cargos(:vicejefe).edad_requerida, 'Para vicejefe la edad es 30'
    assert_equal 18, cargos(:diputados).edad_requerida, 'Para diputados la edad requerida es 18'
    assert_equal 18, cargos(:comuna_1).edad_requerida, 'Para comuneros la edad requerida es 18'
  end

end
