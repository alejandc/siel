# == Schema Information
#
# Table name: archivos
#
#  id             :integer          not null, primary key
#  nombre         :string(255)
#  tipo_contenido :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  descripcion    :string(255)
#

require 'test_helper'

class ArchivoTest < ActiveSupport::TestCase

  test 'guardar contenido' do
    contenido = 'Contenido de prueba de un archivo'
    archivo = archivos(:one)

    BYTES_SEGMENTO = 10

    archivo.contenido = contenido

    assert_equal contenido, archivo.contenido, 'El contenido debe ser igual al que se guardo'
  end

end
