# == Schema Information
#
# Table name: padrones
#
#  id                   :integer          not null, primary key
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  fecha                :datetime
#  cantidad_renglones   :integer
#  importacion_completa :boolean
#  created_by           :string(255)
#

require 'test_helper'

class PadronTest < ActiveSupport::TestCase

  test 'cantidad_real_renglones' do
    assert_equal 3, padrones(:one).cantidad_real_renglones, 'La cantidad de renglones de un padron'
  end

  test 'build_values' do
    line = '29277459|1982|LUNA|DIEGO ORLANDO||QUIRNO 76||DNI-EA|00007|0082|0000|M|CAP FED|01'
    params = RenglonPadron.parse_to_hash(line)
    sql_values = Padron.build_values(params)
    expected = "'1982', 'LUNA', 'DIEGO ORLANDO', '', 'QUIRNO 76', '', 'DNI-EA', '00007', '0082', '0000', 'M', 'CAP FED'"

    assert_equal expected, sql_values, 'Armar el VALUES del INSERT para la importacion'
  end
end
