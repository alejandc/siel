# == Schema Information
#
# Table name: boleta
#
#  id          :integer          not null, primary key
#  fuerza_id   :integer
#  archivo_id  :integer
#  eleccion_id :integer
#  created_at  :datetime
#  updated_at  :datetime
#

require 'test_helper'

class BoletaTest < ActiveSupport::TestCase

  test 'archivo' do
    archivo = File.open('test/cpa_con_simbolo.24')
    archivo_subido = ActionDispatch::Http::UploadedFile.new(filename: 'toto', content_type: 'pdf', tempfile: archivo)

    boleta = Boleta.new
    boleta.archivo = archivo_subido

    File.open('test/cpa_con_simbolo.24', 'rb') do |arch|
      assert_equal arch.read, boleta.archivo.contenido, 'Debe tener el contenido del archivo'
    end
  end

end
