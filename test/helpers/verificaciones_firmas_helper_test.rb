require 'test_helper'

class VerificacionesFirmasHelperTest < ActionView::TestCase
  test 'porcentaje_carga_completado' do
    assert_equal 50.0, porcentaje_carga_completado(verificaciones_firmas(:one))
  end

  test 'variable' do
    assert_equal 'N', variable(nil, 'N'), 'Si es nulo tiene que poner el nombre de la variable'
    assert_equal 7, variable(7, 'N'), 'Si tiene valor, muestra el valor'
  end

  test 'confianzas_posibles' do
    assert_equal %w(90 95 99), confianzas_posibles, 'Los valores posibles del combo'
  end
end
