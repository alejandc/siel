require 'test_helper'

class ListasHelperTest < ActionView::TestCase
  test 'agrupar_cargos' do
    jefe = 'Jefe/a de Gobierno'
    diputados = 'Diputados/as'
    cargos =  [
      Cargo.find_by(nombre: jefe),
      Cargo.find_by(nombre: diputados),
      Cargo.find_by(nombre: 'Junta Comunal 1')
    ]

    cargos_agrupados = agrupar_cargos(cargos)

    assert cargos_agrupados.key?(jefe), 'No tiene la clave de jefe'
    assert_equal jefe, cargos_agrupados[jefe].first.nombre, 'El cargo jefe no tiene de valor al registro de jefe'
    assert cargos_agrupados.key?(diputados), 'No tiene la clave de diputados'
    assert_equal diputados, cargos_agrupados[diputados].first.nombre, 'El cargo jefe no tiene de valor al registro de diputados'
    assert cargos_agrupados.key?('comuna'), 'No tiene la clave de comuna'
    assert_equal 'Junta Comunal 1', cargos_agrupados['comuna'].first.nombre, 'El cargo jefe no tiene de valor al registro de comuna'
  end

  # FIXME: Chequear luego del refactor de lista
  # test 'mostrar_cargo jefe' do
  #   nombre_cargo = 'Jefe/a de Gobierno'
  #   cargos = [Cargo.find_by(nombre: nombre_cargo)]
  #   lista = Lista.find_by(nombre: 'Lista 3')

  #   match = mostrar_cargo(nombre_cargo, cargos, lista).match(%r{<div id=\"jefe\/a de gobierno\" class=\"col-md-2 center-block\"><a class=\"btn btn-default gris-claro\" href=\"\/cargos\/(\d*)\?lista_id=(\d*)\">Jefe\/a de Gobierno <i class='text-success glyphicon glyphicon-ok-sign'><\/i> <i class='text-success glyphicon glyphicon-list'><\/i><\/a><\/div>})

  #   assert match.present?, 'No armó bien el elemento html de jefe'
  # end

  # FIXME: Chequear luego del refactor de lista
  # test 'mostrar_cargo diputados' do
  #   nombre_cargo = 'Diputados/as'
  #   cargos = [Cargo.find_by(nombre: nombre_cargo)]
  #   lista = Lista.find_by(nombre: 'Lista 3')

  #   match = mostrar_cargo(nombre_cargo, cargos, lista).match(%r{<div id=\"diputados\/as\" class=\"col-md-2 center-block\"><a class=\"btn btn-default gris-claro\" href=\"\/cargos\/(\d*)\?lista_id=(\d*)\">Diputados\/as <i class='text-success glyphicon glyphicon-ok-sign'><\/i> <i class='text-success glyphicon glyphicon-list'><\/i><\/a><\/div>})

  #   assert match.present?, 'No armó bien el elemento html de diputados'
  # end

  # FIXME: Chequear luego del refactor de lista
  # test 'mostrar_cargo comuneros' do
  #   nombre_cargo = 'Junta Comunal 1'
  #   cargos = [Cargo.find_by(nombre: nombre_cargo)]
  #   lista = Lista.find_by(nombre: 'Lista 3')

  #   match = mostrar_cargo(nombre_cargo, cargos, lista).match(%r{<div id=\"Junta Comunal 1\" class=\"col-md-8 center-block\"><ul class=\"list-inline\"><li><a class=\"btn btn-sm btn-default gris-claro\" style=\"min-width: 165px;\" href=\"\/cargos\/1039226752\?lista_id=113629430\">Junta Comunal 1 <i class='text-success glyphicon glyphicon-ok-sign'><\/i> <i class='text-success glyphicon glyphicon-list'><\/i><\/a><\/li><\/ul><\/div>})

  #   assert match.present?, 'No armó bien el elemento html de comuna 1'
  # end
end
