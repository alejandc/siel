require 'test_helper'

class CasosVerificacionHelperTest < ActionView::TestCase
  test 'boton_firma' do
    sin_firmar = CasoVerificacion.new
    fila_vacia = CasoVerificacion.new(fila_vacia: true)
    firmado = CasoVerificacion.new(firmado: true)
    no_firmado = CasoVerificacion.new(firmado: false)
    duplicado = CasoVerificacion.new(duplicado: true)

    html_para_firmar = "<button class=\"btn btn-small btn-success\" onclick=\"javascript: firmar();\" title=\"La persona se presentó a firmar\"><i class=\"icon-edit\"></i></button>"
    assert_equal html_para_firmar, boton_firma(sin_firmar), 'Un caso que todavía no se chequeó'
    assert_equal nil, boton_firma(fila_vacia), 'La fila vacía no se puede firmar'
    assert_equal nil, boton_firma(firmado), 'El caso firmado, para deshacerlo'
    assert_equal html_para_firmar, boton_firma(no_firmado), 'El caso no firmado, para deshacerlo'
    assert_equal nil, boton_firma(duplicado), 'Los duplicados son inválidos, no pueden firmarse'
  end

  test 'boton_no_firma' do
    sin_firmar = CasoVerificacion.new
    fila_vacia = CasoVerificacion.new(fila_vacia: true)
    firmado = CasoVerificacion.new(firmado: true)
    no_firmado = CasoVerificacion.new(firmado: false)

    html_para_firmar = '<button class="btn btn-small btn-danger" onclick="javascript: no_firmar();" title="No se verifica la firma de la persona"><i class="icon-minus-sign"></i></button>'
    assert_equal html_para_firmar, boton_no_firma(sin_firmar), 'Un caso que todavía no se chequeó'
    assert_equal nil, boton_no_firma(fila_vacia), 'La fila vacía no se puede firmar'
    assert_equal html_para_firmar, boton_no_firma(firmado), 'El caso firmado, para deshacerlo'
    assert_equal nil, boton_no_firma(no_firmado), 'El caso no firmado, para deshacerlo'
  end

  test 'clase_fila_caso' do
    fila_vacia = 'fila_vacia'
    warning = 'warning'

    assert_equal fila_vacia, clase_fila_caso(CasoVerificacion.new(fila_vacia: true))
    assert_equal warning, clase_fila_caso(CasoVerificacion.new(figura_en_padron: false))
    assert_equal warning, clase_fila_caso(CasoVerificacion.new(ilegible: true))
    assert_equal warning, clase_fila_caso(CasoVerificacion.new(duplicado: true))
  end

  test 'descripcion_error' do
    assert_equal 'fila vacía', descripcion_error(CasoVerificacion.new(fila_vacia: true))
    assert_equal 'dni ilegible', descripcion_error(CasoVerificacion.new(ilegible: true))
    assert_equal 'duplicado', descripcion_error(CasoVerificacion.new(duplicado: true))
    assert_equal 'no figura en el padrón', descripcion_error(CasoVerificacion.new(figura_en_padron: false))
    assert_equal 'ok', descripcion_error(CasoVerificacion.new(figura_en_padron: true))
    assert_equal 'firmado', descripcion_error(CasoVerificacion.new(firmado: true, figura_en_padron: true))
  end
end
