require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test 'dato_o_guion' do
    dato = 'toto'

    assert_equal '-', dato_o_guion(nil), 'Nil da guión'
    assert_equal '-', dato_o_guion(''), 'String vacío da guión'
    assert_equal '-', dato_o_guion(' '), 'String en blanco da guión'
    assert_equal dato, dato_o_guion(dato), 'Cualquier otra cosa da esa misma cosa'
  end
end
