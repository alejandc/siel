#!/bin/bash
# Reemplazar la base de datos Rails local de desarrollo
# (db/development.sqlite3) con los datos de staging.

# IMPORTANTE: Se entiende que este script se está
#             ejecutando en el directorio railz de la
#             app Rails

# Interrumpir el script si algo falla
set -e

# 0 - Preguntar la clave para las operaciones por SSH
echo -n "Ingrese la clave, por favor: "
read -s clave

# 1 - Borrar la base de datos local
echo "Borrando la base local"
set +e # Es el único paso que puede fallar, por si no se ha generado la base aun
rm db/development.sqlite3
set -e

# 2 - Volver a crearla vacía
echo "Creando nuevamente la base vacía"
bundle exec rake db:migrate

# 3 - Copiar los archivos necesarios al server
echo "Copiando los archivos requeridos al server"
sshpass -p $clave scp mysql2sqlite.sh siel2sqlite.sh db/development.sqlite3 root@10.10.10.23:/tmp

# 4 - Borrar la base local generada
echo "Borrando la base local generada"
rm db/development.sqlite3

# 5 - Ejecutar el script en el server
echo "Generando el GZip de la base de staging"
sshpass -p $clave ssh root@10.10.10.23 \
   'cd /tmp &&
    ./siel2sqlite.sh'

# 6 - Descargar la nueva base
echo "Descargando el GZip con la base con datos de staging"
sshpass -p $clave scp root@10.10.10.23:/tmp/development.sqlite3.gz db/development.sqlite3.gz

# 7 - Borrando los archivos del servidor
echo "Borrando los archivos en el servidor"
sshpass -p $clave ssh root@10.10.10.23 'rm /tmp/development.sqlite3.gz /tmp/mysql2sqlite.sh /tmp/siel2sqlite.sh'

# 8 - Descomprimir la base descargada
echo "Descomprimiendo la base de datos"
gunzip db/development.sqlite3.gz

echo "¡Listo! (de nada)"

