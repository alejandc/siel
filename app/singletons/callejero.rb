require 'singleton'

class Callejero
  include Singleton

  attr_reader :calles, :circuitos, :secciones

  def initialize
    @calles = CallejeroApi.calles
    @circuitos = CallejeroApi.circuitos
    @secciones = CallejeroApi.secciones
  end

  def comuna_en_circuito(circuito_id)
    seccion_id = @circuitos.detect { |circuito| circuito['id'] == circuito_id }['seccion_id']
    @secciones.detect { |seccion| seccion['id'] == seccion_id }['numero']
  end

  def calle(id)
    calles.detect { |calle| calle['id'] == id }
  end

end
