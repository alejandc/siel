require 'singleton'

class Padrones
  include Singleton

  attr_reader :data

  def initialize
    actualizar_padrones
  end

  def inscriptos_en_comuna(numero_comuna)
    ultimo_padron['cantidad_renglones_comuna']["comuna_#{numero_comuna}"]
  end

  def ultimo_padron
    @data.max { |a, b| a['anio_padron'] <=> b['anio_padron'] }
  end

  def actualizar_padrones
    @data = PadronApi.padrones
  end

  # FIXME: esta funcionalidad debe estar en padron api....
  def antiguedad_persona(tipo_documento, numero_documento, sexo)
    condicion_corte = -> (anio_actual, anio_padron, _, _) { anio_padron != anio_actual }
    antiguedad(tipo_documento, numero_documento, sexo, condicion_corte, nil)
  end

  # FIXME: esta funcionalidad debe estar en padron api....
  def antiguedad_comuna(tipo_documento, numero_documento, sexo, cargo)
    condicion_corte = -> (anio_actual, anio_padron, comuna_padron, cargo_aux) { anio_padron != anio_actual || comuna_padron != cargo_aux.numero_comuna }
    antiguedad(tipo_documento, numero_documento, sexo, condicion_corte, cargo)
  end

  private

  # FIXME: esta funcionalidad debe estar en padron api....
  def antiguedad(tipo_documento, numero_documento, sexo, condicion_corte, cargo)
    antiguedad = 0

    padrones = PadronApi.padrones.sort { |a, b| b['anio_padron'] <=> a['anio_padron'] }
    listado_padrones_persona = PadronApi.antiguedad_persona(tipo_documento, numero_documento, sexo).sort { |a, b| b['anio_padron'] <=> a['anio_padron'] }

    padrones.each_with_index do |padron, idx|
      anio_actual = padron['anio_padron']
      break if listado_padrones_persona[idx].blank?
      anio_padron = listado_padrones_persona[idx]['anio_padron']
      comuna_padron = listado_padrones_persona[idx]['seccion'].to_i

      break if condicion_corte.call(anio_actual, anio_padron, comuna_padron, cargo)

      antiguedad = Time.now.year - anio_actual
    end

    antiguedad
  end

end
