require 'singleton'

class MesasComuna
  include Singleton

  attr_reader :data

  def initialize()
    @data = {}
  end
  

  def mesas_por_establecimiento(numero_comuna, codigo_establecimiento)
    mesas_por_comuna(numero_comuna).select {|mesa| mesa['codigo_establecimiento'] == codigo_establecimiento}
  end

  def establecimientos_por_comuna(numero_comuna)
    mesas(numero_comuna).deep_dup.each {|mesa| mesa.delete_if{ |k, v| ['cargos','created_at','updated_at','id','numero_mesa'].include? k } }.uniq
  end

  def mesas_por_comuna(numero_comuna)
    mesas(numero_comuna).deep_dup.each {|mesa| mesa.delete_if{|k, v| ['cargos','created_at','updated_at','id'].include? k } }.uniq
  end

  private

  def mesas(numero_comuna)
    @data[numero_comuna] ||= MesasComunaApi.mesas_por_comuna(numero_comuna)
  end

end
