module AntecedentesPenalesHelper
  
  def label_estado_antecedente_penal(ap)
    if ap.estado_no_solicitado?
      'default'
    elsif ap.estado_finalizado?
      'success'
    elsif ap.estado_inicio_solicitud?
      'info'
    elsif ap.estado_error_solicitud?
      'danger'
    else
      'primary'
    end
  end
  
end
