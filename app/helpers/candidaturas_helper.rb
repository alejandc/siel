﻿module CandidaturasHelper

  def letra_sexo(persona)
    if persona.present? && persona.sexo.present?
      persona.sexo.capitalize
    else
      '-'
    end
  end

  def leyenda(candidatura)
    if !candidatura.figura_en_padron? and !candidatura.persona.nativo_caba?
      "<br/><strong><small class='rojo'>No es nativo CABA y no figura en padrón</small></strong>".html_safe
    end
  end

  def atributos_tr_armado_lista(candidatura)
    css_class = ''
    title = ''

    if candidatura.persona.candidaturas_multiples(candidatura.eleccion).count > 1
      css_class << 'warning '
      title << 'Esta persona posee más de una candidatura en esta elección'
    end

    if !candidatura.figura_en_padron? && !candidatura.persona.nativo_caba?
      css_class << 'linea_destacada'
    end

    "class='#{css_class}' title='#{title}'".html_safe
  end

  def descripcion_cargo(candidatura)
    descripcion = candidatura.cargo.nombre

    unless candidatura.cargo.nombre == Cargo::CARGOS[:jefe] or candidatura.cargo.nombre == Cargo::CARGOS[:vicejefe]
      descripcion += (candidatura.titular? ? ' Titular' : ' Suplente')
    end

    descripcion
  end

  def cambios_significativos(cambios)
    cambios.select do |k, v|
      k != 'domicilio_id' && !(v[0].nil? && v[1] == false)
    end
  end

  def orden(candidatura)
    if candidatura.cargo.nombre == Cargo::CARGOS[:diputado]
      if !candidatura.titular?
        candidatura.orden - candidatura.cargo.cantidad_minima
      else
        candidatura.orden
      end
    else
      candidatura.orden
    end
  end

  def css_class_sugerido(original, sugerida, incorregible)
    if !incorregible.nil? && sugerida.orden >= incorregible.orden
      'error'
    elsif original.persona != sugerida.persona
      'success'
    else
      ''
    end
  end

  def css_class_estado(candidatura)
    if candidatura.valida?
      if candidatura.documentacion_completa?
        'btn-success'
      else
        'btn-warning'
      end
    else
      'btn-danger'
    end
  end

  def icono_estado(candidatura)
    if candidatura.valida?
      if candidatura.documentacion_completa?
        'icon-thumbs-up'
      else
        'icon-eye-open'
      end
    else
      'icon-thumbs-down'
    end
  end

  def lista_oficializada(fuerza, eleccion)
    !fuerza.nil? and !eleccion.nil? and fuerza.oficializada?(eleccion)
  end

  def opciones_booleanas
    { '' => nil, 'Si' => true, 'No' => false }
  end

end
