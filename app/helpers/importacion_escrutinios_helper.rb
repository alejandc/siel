module ImportacionEscrutiniosHelper
  def celda_color_umbral(porcentaje_votos)
    if porcentaje_votos == 0
      'alert alert-default'
    elsif porcentaje_votos >= 1.5
      'alert alert-success'
    else
      'alert alert-danger'
    end
  end
end
