module VerificacionesFirmasHelper
  def estado_requeridas(verificacion)
    rango = verificacion.rango_confianza
    cant_requeridas = verificacion.cantidad_firmas_requeridas

    if cant_requeridas <= rango[1]
      numero = content_tag(:p, number_with_delimiter(cant_requeridas), class: 'text-success cantidad_requeridas')
      icono = content_tag(:i, '', class: 'icon-ok text-success')
    else
      numero = content_tag(:p, number_with_delimiter(cant_requeridas), class: 'text-error cantidad_requeridas')
      icono = content_tag(:i, '', class: 'icon-remove text-error')
    end
    "#{numero} #{icono}".html_safe
  end

  def porcentaje_firmas_verificadas(verificacion)
    (1 - (verificacion.casos_a_firmar.size.to_f / verificacion.casos_verificacion.size.to_f)) * 100
  end

  def porcentaje_carga_completado(verificacion)
    verificacion.casos_cargados.size.to_f / verificacion.casos_verificacion.size.to_f * 100
  end

  def variable(valor, nombre)
    valor.nil? ? nombre : valor
  end

  def confianzas_posibles
    VerificacionFirmas::COEF_CONFIABILIDAD.keys.map { |k| k.to_i.to_s }
  end
end
