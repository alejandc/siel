﻿module CasosVerificacionHelper
  def boton_firma(caso)
    if !caso.fila_vacia && (!caso.invalido? && caso.firmado.nil? || caso.invalido? && !caso.firmado.nil? && !caso.firmado)
      icono = content_tag(:i, '', class: 'icon-edit').html_safe
      content_tag(:button, icono, class: 'btn btn-small btn-success', onclick: "javascript: firmar(#{caso.id});", title: "La persona se presentó a firmar").html_safe
    end
  end

  def boton_no_firma(caso)
    if !caso.fila_vacia && (!caso.invalido? && caso.firmado.nil? || caso.firmado)
      icono = content_tag(:i, '', class: 'icon-minus-sign').html_safe
      content_tag(:button, icono, class: 'btn btn-small btn-danger', onclick: "javascript: no_firmar(#{caso.id});", title: 'No se verifica la firma de la persona').html_safe
    end
  end

  def clase_fila_caso(caso)
    if caso.fila_vacia
      'fila_vacia'
    elsif !caso.figura_en_padron || caso.ilegible || caso.duplicado || (!caso.firmado.nil? && !caso.firmado)
      'warning'
    end
  end

  def descripcion_error(caso)
    if caso.fila_vacia
      'fila vacía'
    elsif caso.ilegible
      'dni ilegible'
    elsif caso.duplicado
      'duplicado'
    elsif !caso.figura_en_padron
      'no figura en el padrón'
    elsif caso.firmado
      'firmado'
    elsif caso.firmado.nil?
      'ok'
    elsif !caso.firmado
      'no firmado'
    end
  end
end
