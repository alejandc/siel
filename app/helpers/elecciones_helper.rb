module EleccionesHelper

  def descripcion_resultado(resultado)
    if resultado.nulos?
      I18n.t('votos_nulos')
    else
      I18n.t('votos_en_blanco')
    end
  end

  def descripcion_reparto(eleccion)
    if eleccion.resultados_completos?
      eleccion.descripcion
    else
      "<i class='icon-warning-sign' title='#{I18n.t('messages.eleccion_sin_resultados_completos')}'></i> #{eleccion.descripcion}".html_safe
    end
  end

  def css_tr_reparto(eleccion)
    eleccion.resultados_completos? ? '' : 'warning'
  end

  def tab_css(comuna1, comuna2)
    (comuna1.nil? && comuna2.nil? || !comuna1.nil? && !comuna2.nil? && comuna1.id == comuna2.id) ? "class='active'".html_safe : ''
  end

  def tab_label(comuna, comuna_seleccionada)
    (!comuna_seleccionada.nil? && comuna.id == comuna_seleccionada.id) ? "Comuna #{comuna.numero}" : comuna.numero
  end

end
