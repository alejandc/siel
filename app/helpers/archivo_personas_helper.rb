module ArchivoPersonasHelper

  def label_archivo_persona_requerido(tipo_archivo, persona, lista_cargo)
    label_tag '', class: "#{'rojo' if persona.persisted? && !persona.archivo_requerido_por_tipo_archivo(tipo_archivo, lista_cargo).try(:persisted?)}", id: "label_archivo_#{tipo_archivo}" do
      if tipo_archivo == ArchivoPersona::TIPO_ARCHIVO['DOCUMENTOS OBLIGATORIOS']['Aceptación Candidatura']
        aceptacion_candidatura_template = SielArchivo.aceptacion_candidatura.activos.first

        concat '* Adjuntar Aceptación de Candidatura y Declaración Jurada '
        concat "#{(aceptacion_candidatura_template.present?) ? link_to(" (Descargar Plantilla <i class='fa fa-download' aria-hidden='true'></i>)".html_safe, "#{root_url}/archivos/descargar/#{aceptacion_candidatura_template.archivo.id}") : ''}".html_safe
      elsif tipo_archivo == ArchivoPersona::TIPO_ARCHIVO['DOCUMENTOS OBLIGATORIOS']['Copia DNI']
        concat '* Adjuntar Fotocopia DNI'
      elsif tipo_archivo == ArchivoPersona::TIPO_ARCHIVO['DOCUMENTOS OBLIGATORIOS']['Certificado Reincidencia Criminal']
        concat '* Adjuntar Certificado Reincidencia Criminal'
      end
   end
  end
end
