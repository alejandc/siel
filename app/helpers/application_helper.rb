﻿module ApplicationHelper

  def current_user
    @current_user ||= Usuario.new(session[:user].attributes.except('created_at', 'updated_at')) if session[:user]
  end

  def user_groups
    current_user.try(:perfiles) || ''
  end

  def link_uba_doc
    link_to 'Fundamento teórico elaborado por la UBA', "#{root_url}/descargas/verificacion_firmas_uba.pdf", target: '_blank'
  end

  def link_estenssoro_jefe_naturalizados
    link_to 'Ciudadanos Naturalizados - Fallo Estenssoro (Expe nº 8099/2011) ', "#{root_url}/descargas/Estenssoro_-_Jefe_Naturalizados.doc", target: '_blank'
  end

  def icono(name, title = '')
    content_tag(:i, nil, class: "icon-#{name}", title: title).html_safe
  end

  def valor_cambio_version(valor)
    if valor.nil? || valor == false
      'no'
    elsif valor == true
      'si'
    else
      valor
    end
  end

  def titulo_con_linea(texto)
    "#{titulo(texto)}#{tag(:hr)}".html_safe
  end

  def titulo(texto)
    content_tag(:h2, texto.html_safe)
  end

  def boton(icono, texto, href, clase_boton = '', opciones = {})
    contenido = "<i class='icon-#{icono}'></i> #{texto}".html_safe
    link_to(*[contenido, href, { class: "btn #{clase_boton}" }.merge(opciones)])
  end

  def mensaje_cupos(candidaturas)
    mensaje = '<caption>'

    unless Candidatura.cumple_ley_cupos?(candidaturas)
      mensaje << "<span class='badge badge-important'>Esta lista no cumple con la ley de cupos.</span>&nbsp;"
    end

    unless candidaturas.nil? || candidaturas.empty?
      candidatura = candidaturas.first
      cargo = candidatura.cargo
      mensaje << "<span class='badge badge-important'>Para el cargo de #{cargo.nombre} debe presentar al menos #{cargo.cantidad_minima} candidaturas.</span>" unless candidatura.cumple_minimo?
    end

    (mensaje + '</caption>').html_safe
  end

  def back_button
    link_to("<i class='icon-arrow-left'></i> Volver".html_safe, (request.env['HTTP_REFERER'] || root_url), class: 'btn')
  end

  def options_from_partidos(partidos)
    options = ''
    partidos.each { |p| options += "<option value='#{p.id}'>#{p.nombre} - #{p.numero}</option>" }
    options.html_safe
  end

  def miembro_de?(grupos)
    grupos.each { |g| return true if current_user.es_del_grupo?(MAPA_GRUPOS_PERFILES[g]) }

    false
  end

  def etiqueta_edad_valida(cargo)
    case cargo.nombre
    when Cargo::CARGOS[:jefe] then ">= 30 años"
    when Cargo::CARGOS[:vicejefe] then ">= 30 años"
    when Cargo::CARGOS[:diputado] then "mayor a 18 años a la fecha de la elección"
    when Cargo::CARGOS[:comunero] then "mayor a 18 años a la fecha de la elección"
    end
  end

  def tipo_nacionalidad(valor)
    case valor
    when 'NATIVO' then 'Nativo'
    when 'POR_OPCION' then 'Por opción'
    when 'NATURALIZADO' then 'Naturalizado'
    end
  end

  def sexo(caracter)
    if caracter.present?
      caracter.downcase == 'f' ? 'femenino' : 'masculino'
    else
      '-'
    end
  end

  def render_filtro(id, collection)
    html_content = collection.respond_to?(:empty?) ? escape_javascript(render(collection)) : ''
    "$('\##{id}').html('#{html_content}');".html_safe
  end

  def menu_item(texto, path, controller, action)
    "<li #{"class='active'" if params[:controller] == controller && params[:action] == action}>#{link_to(texto, path)}</li>".html_safe
  end

  def link_to_add_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new

    fields = f.fields_for(association, new_object, child_index: "new_#{association}") do |builder|
      render(association.to_s.singularize + '_fields', f: builder)
    end

    onclick = "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")"
    link_to name, 'javascript: return false;', class: 'btn btn-info', onclick: onclick
  end

  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to(name, 'javascript:;', class: 'btn pull-right', onclick: 'remove_fields(this)')
  end

  def dato_o_guion(dato)
    (dato.nil? || dato.blank?) ? '-' : dato
  end

  def clase_navegador
    current_user.interno? ? 'navbar-inverse' : 'navbar-externo'
  end

  def alertas_flash_json
    { notice: flash[:notice], warning: flash[:warning], error: flash[:error], sweet_alert_notice: flash[:sweet_alert_notice], sweet_alert_warning: flash[:sweet_alert_warning], sweet_alert_error: flash[:sweet_alert_error] }.to_json.html_safe
  end

  def clase_alerta(tipo)
    case tipo
    when 'notice'
      'alert-success'
    when 'info'
      'alert-success'
    when 'warning'
      'alert-warning'
    when 'error'
      'alert-danger'
    end
  end
end
