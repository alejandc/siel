﻿module FuerzasHelper

  def apoderado_fields(fuerza)
    apoderado = fuerza.apoderados.build

    form_for(fuerza) do |f|
      fields = f.fields_for(:apoderados, apoderado, child_index: Random.rand(99999999)) do |builder|
        render('apoderado_fields', f: builder)
      end

      return escape_javascript(fields)
    end
  end

  def texto_infracciones(fuerza)
    if fuerza.infracciones.empty?
      'Sin infracciones'
    elsif fuerza.infracciones.size == 1
      "1 infracción"
    else
      "#{fuerza.infracciones.size} infracciones"
    end
  end

end
