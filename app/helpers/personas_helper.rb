module PersonasHelper
  def armar_opcion_calle(calle)
    "<option value='#{calle['id']}'>#{calle['nombre']}</option>".html_safe
  end
end
