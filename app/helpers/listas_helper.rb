module ListasHelper
  def agrupar_lista_cargos(lista_cargos)
    lista_cargos.inject({}) do |hash, lista_cargo|
      if lista_cargo.cargo.nombre =~ /Comuna/
        hash['comuna'].nil? ? hash['comuna'] = [lista_cargo] : hash['comuna'] << lista_cargo
        hash
      else
        hash.merge(lista_cargo.cargo.nombre => [lista_cargo])
      end
    end
  end

  def mostrar_lista_cargo(nombre_cargo, lista_cargos, lista, eleccion_paso)
    case nombre_cargo
    when Cargo::CARGOS[:jefe]
      cargo_content_tag(nombre_cargo, lista_cargos.first, lista, eleccion_paso, 'btn-default gris-claro')

    when Cargo::CARGOS[:diputado]
      cargo_content_tag(nombre_cargo, lista_cargos.first, lista, eleccion_paso, 'btn-default gris-claro')

    when /Comuna/i
      content_tag(:div, id: nombre_cargo, class: ['col-md-8', 'center-block']) do
        lista_cargos.each_slice(3) do |grupo_lista_cargos|
          concat(content_tag(:ul, class: 'list-inline') do
            grupo_lista_cargos.each do |lista_cargo|
              tiene_permiso = can?(:manage, lista_cargo)
              if lista.cargo_habilitado?(lista_cargo.cargo_nombre)
                concat(content_tag(:li) do
                  path_link = eleccion_paso ? lista_lista_cargo_path(lista, lista_cargo, lista_filtrada: params[:lista_id]) : listas_generales_lista_cargos_generales_path(lista, lista_cargo)
                  link_to(tiene_permiso ? path_link : 'javascript:;', class: ['boton-comunas', 'btn', 'btn-default', 'gris-claro'], style: 'min-width: 165px;', disabled: !tiene_permiso, data: { toggle: 'tooltip', placement: 'bottom' }, title: "#{'Podrá visualizar la lista cuando venza el plazo de presentación y cierre de listas' if !tiene_permiso}") do
                    "#{lista_cargo.cargo.nombre} #{lista_cargo.decorate.icono_estado_candidaturas(true) if eleccion_paso} #{lista_cargo.decorate.icono_estado_adhesiones(true) if eleccion_paso}".html_safe
                  end
                end)
              end
            end
          end)
        end
      end
    end
  end

  def notificaciones_lista_cargos(leyenda, nombres_cargos)
    unless nombres_cargos.empty?
      content_tag(:li) do
        "#{leyenda} #{nombres_cargos}"
      end
    end
  end

  def color_reloj(tiempo_restante, css_class_name)
    if current_user.lista_partidaria?
      if tiempo_restante < 12 && tiempo_restante >= 2
        return "#{css_class_name}orange"
      elsif tiempo_restante < 2
        return "#{css_class_name}red"
      end
    else
      if tiempo_restante < 24 && tiempo_restante >= 2
        return "#{css_class_name}orange"
      elsif tiempo_restante < 2
        return "#{css_class_name}red"
      end
    end
  end

  private

  def cargo_content_tag(nombre_cargo, lista_cargo, lista, eleccion_paso, estilo = 'btn-default')
    tiene_permiso = can?(:manage, lista_cargo)

    if lista.cargo_habilitado?(nombre_cargo)
      content_tag(:div, id: lista_cargo.cargo.nombre.downcase, class: ['col-md-2', 'center-block']) do
        path_link = eleccion_paso ? lista_lista_cargo_path(lista, lista_cargo, lista_filtrada: params[:lista_id]) : listas_generales_lista_cargos_generales_path(lista, lista_cargo)
        link_to(tiene_permiso ? path_link : 'javascript:;', class: ['btn', estilo], disabled: !tiene_permiso, data: { toggle: 'tooltip', placement: 'bottom' }, title: "#{'Podrá visualizar la lista cuando venza el plazo de presentación y cierre de listas' if !tiene_permiso}") do
          "#{nombre_cargo.gsub(Cargo::CARGOS[:diputado], 'Legisladores')} #{lista_cargo.decorate.icono_estado_candidaturas(true) if eleccion_paso} #{lista_cargo.decorate.icono_estado_adhesiones(true) if eleccion_paso}".html_safe
        end
      end
    end
  end

end
