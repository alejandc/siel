module ListaCargosHelper

  def path_eliminar_personas_en_lista(lista, lista_cargo, tab)
    if tab == 'candidaturas'
      borrar_candidaturas_lista_lista_cargo_path(lista, lista_cargo)
    elsif tab == 'adhesiones'
      borrar_adhesiones_lista_lista_cargo_path(lista, lista_cargo)
    end
  end

  def path_imprimir_personas_en_lista(lista, lista_cargo, tab)
    if tab == 'candidaturas'
      imprimir_candidaturas_lista_lista_cargo_path(lista, lista_cargo, format: :xls)
    elsif tab == 'adhesiones'
      exportar_adhesiones_lista_lista_cargo_path(lista, lista_cargo)
    end
  end

end
