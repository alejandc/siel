class AltaInformeAntecedentesPenales
  include Sidekiq::Worker

  sidekiq_options queue: :default, retry: 0

  def perform(antecedente_penal_id)
    @antecedente_penal = AntecedentePenal.find_by_id(antecedente_penal_id)

    if @antecedente_penal.present?
      nombres = @antecedente_penal.persona.nombres.split(' ')
      nom1 = nombres.first
      nom2 = nil
      nom3 = nil

      if nombres.size > 1
        nom2 = nombres.second
        if nombres.size > 2
          nom3 = nombres.third
        end
      end

      apellidos = @antecedente_penal.persona.apellidos.split(' ')
      ape1 = apellidos.first
      ape2 = nil

      if apellidos.size > 1
        ape2 = apellidos.second
      end

      respuesta = RegistroNacionalReincidenciaApi.alta_informe_antecedentes_penales(@antecedente_penal.persona)

      if respuesta['exito'] && respuesta.key?("ID")
        @antecedente_penal.update_attributes!(estado: :en_proceso, referencia_id: respuesta["ID"], resultado: nil)
        ConsultaInformeAntecedentesPenales.perform_at(30.minutes.from_now, antecedente_penal_id)
      else
        @antecedente_penal.update_attributes!(estado: :error_solicitud, resultado: respuesta['errores'].join(' - '))
      end
    end
  end

end
