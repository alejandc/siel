class ImportarPersonasJob

  def verificar_errores_importacion(objetos_con_error, path_errors_file)
    if !objetos_con_error.empty?
      generar_archivo_con_errores(objetos_con_error, path_errors_file)
    elsif File.exist?(path_errors_file)
      File.delete(path_errors_file)
    end
  end

  def generar_archivo_con_errores(errores, path_errors_file)
    Spreadsheet.client_encoding = 'UTF-8'
    errores_xls = Spreadsheet::Workbook.new

    errores_sheet = errores_xls.create_worksheet name: 'Errores'
    errores_sheet.row(0).concat(['Tipo Documento', 'DNI', 'Sexo', 'Errores'])

    errores.each_with_index do |error, index|
      errores_sheet[(index + 1), 0] = error[:tipo_documento]
      errores_sheet[(index + 1), 1] = error[:dni]
      errores_sheet[(index + 1), 2] = error[:sexo]
      errores_sheet[(index + 1), 3] = error[:error]
    end

    errores_xls.write(path_errors_file)
  end

end
