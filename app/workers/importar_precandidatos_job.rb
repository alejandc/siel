class ImportarPrecandidatosJob < ImportarPersonasJob
  include Sidekiq::Worker
  include Sidekiq::Status::Worker

  sidekiq_options queue: :default, retry: 0

  def perform(lista_cargo_id, nombre_usuario, limpiar_cargo)
    PaperTrail.whodunnit = nombre_usuario
    @lista_cargo = ListaCargo.find_by_id(lista_cargo_id)

    if @lista_cargo
      path_file = @lista_cargo.path_archivo_importar('Candidatura')

      @lista_cargo.cambiar_estado_candidaturas_sin_validar!

      if File.exist?(path_file)
        worksheet = Spreadsheet.open(path_file).worksheet 0
        total_precandidatos = (worksheet.count - 1)
        objetos_con_error = []

        logger.info "La cantidad de precandidatos es: #{total_precandidatos}"

        if limpiar_cargo
          @lista_cargo.borrar_candidaturas
          @lista_cargo.reload
        end

        PaperTrail::Version.create(whodunnit: nombre_usuario, item_type: 'Candidatura', item_id: @lista_cargo.id, event: 'import', lista_cargo_id: @lista_cargo.id) if total_precandidatos > 1

        # El valor 1 en el each indica la fila de la cual comenzar a iterar
        (1..total_precandidatos).each do |index|
          row = worksheet.row(index)
          renglon_padron = nil

          if row[0].present? && row[1].present? && row[2].present?
            tipo_documento = row[0].to_s.strip.upcase
            numero_documento = row[1].to_i.to_s.strip
            sexo = row[2].to_s.strip.upcase

            if !(numero_documento !~ /\D/) || !numero_documento.length.between?(6,8)
              objetos_con_error << { tipo_documento: tipo_documento, dni: numero_documento, sexo: sexo, error: 'Error de formato numérico en el campo DNI. Verificar dato.' }
            else
              begin
                renglon_padron = PadronApi.buscar_persona(tipo_documento, numero_documento, sexo)
              rescue PersonaNoExisteEnPadronError
                logger.info "La persona (#{row[1]}) que se desea importar como precandidato no existe en el padron."
                info_renaper = RenaperApi.ultimo_ejemplar(numero_documento, sexo)

                renglon_padron = { 'tipo_documento' => tipo_documento,
                                  'nombre' => info_renaper['nombres'].try(:upcase),
                                  'apellido' => info_renaper['apellido'].try(:upcase),
                                  'matricula' => numero_documento, 
                                  'sexo' => sexo, 'en_padron' => false }
              end

              begin
                persona = Persona.buscar_o_crear(renglon_padron, obtener_info_renaper: true)
                esta_en_lista = @lista_cargo.posee_candidato?(persona)

                if esta_en_lista
                  objetos_con_error << { tipo_documento: tipo_documento, dni: numero_documento, sexo: sexo, error: 'Ya esta incluido en la lista' }
                else
                  candidatura = Candidatura.new(lista_cargo: @lista_cargo, cargo: @lista_cargo.cargo,
                                          persona: persona, eleccion: @lista_cargo.eleccion)

                  objetos_con_error << { tipo_documento: tipo_documento, dni: numero_documento, sexo: sexo, error: candidatura.errors.full_messages.join(' - ') } unless candidatura.save
                end
              rescue Exception => e
                persona_nueva = Persona.find_by(tipo_documento: tipo_documento, numero_documento: numero_documento)
                persona_nueva.update_attributes!(nombres: "Nombre1", apellidos: "Apellido1") if persona_nueva.nombres.blank? && persona_nueva.apellidos.blank?
                candidatura = Candidatura.new(lista_cargo: @lista_cargo, cargo: @lista_cargo.cargo,
                                              persona: persona_nueva,
                                              eleccion: @lista_cargo.eleccion)
                
                objetos_con_error << { tipo_documento: tipo_documento, dni: numero_documento, sexo: sexo, error: candidatura.errors.full_messages.join(' - ') } unless candidatura.save
                #objetos_con_error << { tipo_documento: tipo_documento, dni: numero_documento, sexo: sexo, error: 'La persona no se encontró, verificar los datos. En caso de que los datos sean correctos agregar el precandidato a través de la carga individual del sitio web.' }
              end
            end

            at(((row.idx * 100) / total_precandidatos).ceil.to_i)
          else
            objetos_con_error << { tipo_documento: row[0], dni: row[1], sexo: row[2], error: 'Todos los campos son obligatorios (Tipo Documento, DNI y Sexo)' }
          end
        end

        path_errors_file = @lista_cargo.path_archivo_con_errores_importacion('Candidatura')
        verificar_errores_importacion(objetos_con_error, path_errors_file)
        File.delete(path_file)
        @lista_cargo.actualizar_importacion('Candidatura')
      end
    end
  end

end
