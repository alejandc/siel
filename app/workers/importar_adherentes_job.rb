class ImportarAdherentesJob < ImportarPersonasJob
  include Sidekiq::Worker
  include Sidekiq::Status::Worker

  sidekiq_options queue: :default, retry: 0

  def perform(lista_cargo_id, nombre_usuario, limpiar_cargo)
    PaperTrail.whodunnit = nombre_usuario
    @nombre_usuario = nombre_usuario
    @lista_cargo = ListaCargo.find_by_id(lista_cargo_id)

    if @lista_cargo
      path_file = @lista_cargo.path_archivo_importar('Adhesion')

      @lista_cargo.cambiar_estado_adhesiones_sin_validar!

      if File.exist?(path_file)
        worksheet = Spreadsheet.open(path_file).worksheet 0
        total_adherentes = (worksheet.count - 1)
        objetos_con_error = []

        logger.info "La cantidad de adherentes es: #{total_adherentes}"

        @lista_cargo.borrar_adhesiones if limpiar_cargo

        # El sleep es para que genere los registros de papertrail cronológicamente
        sleep(1)

        PaperTrail::Version.create(whodunnit: nombre_usuario, item_type: 'Adhesion', item_id: @lista_cargo.id, event: 'import', lista_cargo_id: @lista_cargo.id) if total_adherentes > 1

        # El sleep es para que genere los registros de papertrail cronológicamente
        sleep(1)

        cantidad_a_insertar = 0
        adhesiones_a_insertar = []

        # El valor 1 en el each indica la fila de la cual comenzar a iterar
        (1..total_adherentes).each do |index|
          row = worksheet.row(index)
          if row[0].present? && row[1].present? && row[2].present?
            begin
              renglon_padron = PadronApi.buscar_persona(row[0].to_s.strip, row[1].to_s.strip, row[2].to_s.strip)
              persona = Persona.buscar_o_crear(renglon_padron, obtener_info_renaper: false)

              esta_en_lista = @lista_cargo.posee_adherente?(persona)
              
              if esta_en_lista
                objetos_con_error << { tipo_documento: row[0], dni: row[1], sexo: row[2], error: 'Ya esta incluido en la lista' }
              else
                objeto = Adhesion.new(lista_cargo: @lista_cargo, persona: persona)
                adhesiones_a_insertar.push(objeto)
                adhesiones_a_insertar, cantidad_a_insertar = importar_adhesiones(adhesiones_a_insertar, cantidad_a_insertar, total_adherentes, index, objetos_con_error, true)
              end

            rescue PersonaNoExisteEnPadronError => error
              extranjero = SreeApi.busqueda_electores(row[0].to_s.strip, row[1].to_s.strip, row[2].to_s.strip, @lista_cargo.eleccion_fecha)
              if extranjero.present?
                adhesiones_a_insertar.push(Adhesion.new(lista_cargo: @lista_cargo, persona: Persona.buscar_o_crear_extranjero(extranjero, obtener_info_renaper: false)))
              else
                logger.info "La persona (#{row[1]}) que se desea importar como adhesion no existe en el padron."
                objetos_con_error << { tipo_documento: row[0], dni: row[1], sexo: row[2], error: error.message }
              end

              adhesiones_a_insertar, cantidad_a_insertar = importar_adhesiones(adhesiones_a_insertar, cantidad_a_insertar, total_adherentes, index, objetos_con_error, false)
            end

            at(((row.idx * 100) / total_adherentes).ceil.to_i)
          else
            objetos_con_error << { tipo_documento: row[0], dni: row[1], sexo: row[2], error: 'Todos los campos son obligatorios (Tipo Documento, DNI y Sexo)' }

            adhesiones_a_insertar, cantidad_a_insertar = importar_adhesiones(adhesiones_a_insertar, cantidad_a_insertar, total_adherentes, index, objetos_con_error, false)
          end
        end

        Adhesion.calculo_incremento_adhesiones_multiples!
        path_errors_file = @lista_cargo.path_archivo_con_errores_importacion('Adhesion')
        verificar_errores_importacion(objetos_con_error, path_errors_file)
        File.delete(path_file)
        @lista_cargo.actualizar_importacion('Adhesion')
        ListaCargo.reset_counters(lista_cargo_id, :adhesiones)
      end
    end
  end

  private

  def importar_adhesiones(adhesiones_a_insertar, cantidad_a_insertar, total_adherentes, index, objetos_con_error, calcular_errores)
    if calcular_errores ? (cantidad_a_insertar == 100 || total_adherentes == index) : (total_adherentes == index)

      if calcular_errores
        adhesiones_a_insertar.reject(&:valid?).each do |adhesion|
          unless adhesion.errors.empty?
            logger.info "La adhesion con error es: #{adhesion.persona_tipo_documento}, #{adhesion.persona_numero_documento}, #{adhesion.persona_sexo}"
            objetos_con_error << { tipo_documento: adhesion.persona_tipo_documento, dni: adhesion.persona_numero_documento, sexo: adhesion.persona_sexo, error: adhesion.errors.full_messages.join(' - ') }
          end
        end
      end

      Adhesion.transaction { adhesiones_a_insertar.each(&:save) }

      # Adhesion.transaction do
        # adhesiones_insertadas = Adhesion.find(Adhesion.import(adhesiones_a_insertar).ids)

        # versiones = adhesiones_insertadas.map do |adhesion|
        #   PaperTrail::Version.new(whodunnit: @nombre_usuario, item_type: 'Adhesion', item_id: adhesion.id, event: 'create', lista_cargo_id: @lista_cargo.id, object_changes: PaperTrail.serializer.dump({'lista_cargo_id' => [nil, @lista_cargo.id], 'persona_id' => [nil, adhesion.persona_id], 'created_at' => [nil, DateTime.now], 'updated_at' => [nil, DateTime.now], 'id' => [nil, adhesion.id]}))
        # end

        # PaperTrail::Version.import versiones
      # end

      cantidad_a_insertar = 0
      adhesiones_a_insertar = []
    else
      cantidad_a_insertar += 1
    end

    [adhesiones_a_insertar, cantidad_a_insertar]
  end

end
