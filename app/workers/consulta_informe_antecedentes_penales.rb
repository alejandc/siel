class ConsultaInformeAntecedentesPenales
  include Sidekiq::Worker

  sidekiq_options queue: :default, retry: 0

  def perform(antecedente_penal_id)
    @antecedente_penal = AntecedentePenal.find_by_id(antecedente_penal_id)

    if @antecedente_penal.present?
      respuesta = RegistroNacionalReincidenciaApi.consulta_informe_antecedentes_penales_pdf(@antecedente_penal.referencia_id)

      if respuesta['exito'] && !respuesta['bytes'].empty?
        binary_data = Base64.decode64(respuesta['bytes'])
        @antecedente_penal.update_attributes(estado: :finalizado, fecha_ultima_consulta: Time.now, resultado: nil, archivo_pdf: binary_data)
      else
        @antecedente_penal.update_attributes!(estado: :en_proceso, fecha_ultima_consulta: Time.now, resultado: respuesta['errores'].join(' - '))
        ConsultaInformeAntecedentesPenales.perform_at(30.minutes.from_now, antecedente_penal_id)
      end

    end
  end

end
