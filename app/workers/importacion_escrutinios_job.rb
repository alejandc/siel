class ImportacionEscrutiniosJob
  include Sidekiq::Worker
  include Sidekiq::Status::Worker

  sidekiq_options queue: :default, retry: 0

  def perform(importacion_escrutinio_id)
    @importacion_escrutinio = ImportacionEscrutinio.find(importacion_escrutinio_id)

    if @importacion_escrutinio
      path_file = @importacion_escrutinio.path_archivo_importar

      if File.exist?(path_file)
        tiempo_total_estimado = 1300
        hora_inicio = Time.now
        objetos_con_error = []

        cantidad_a_insertar = 0
        renglones_escrutinio_a_insertar = []

        index_global = 0

        CSV.foreach(path_file, headers: true, col_sep: '|') do |row|
          if ((0..6).to_a + (8..9).to_a).all? { |i| row[i].present? }
            renglon_escrutinio = RenglonEscrutinio.new(cantidad_votos: row[0], codigo_cargo_eleccion: row[1], codigo_partido: row[2], 
                                                       descripcion_partido: row[3].strip.upcase, codigo_cargo: row[4], descripcion_cargo: row[5], 
                                                       codigo_seccion: row[6], codigo_circuito: row[7].try(:value), numero_mesa: row[8], 
                                                       sexo_mesa: row[9], importacion_escrutinio_id: @importacion_escrutinio.id)
            renglones_escrutinio_a_insertar.push(renglon_escrutinio)
            renglones_escrutinio_a_insertar, cantidad_a_insertar = importar_renglones_escrutinio(renglones_escrutinio_a_insertar, cantidad_a_insertar, index_global, objetos_con_error, true)

            at((((Time.now - hora_inicio) * 100) / tiempo_total_estimado).ceil.to_i)
          else
            objetos_con_error << { cantidad_votos: row[0], codigo_cargo_eleccion: row[1], codigo_partido: row[2], descripcion_partido: row[3], codigo_cargo: row[4], descripcion_cargo: row[5], codigo_seccion: row[6], codigo_circuito: row[7].try(:value), numero_mesa: row[8], sexo_mesa: row[9] }

            renglones_escrutinio_a_insertar, cantidad_a_insertar = importar_renglones_escrutinio(renglones_escrutinio_a_insertar, cantidad_a_insertar, index_global, objetos_con_error, false)
          end

          index_global += 1
        end

        unless renglones_escrutinio_a_insertar.empty?
          at(100)
          RenglonEscrutinio.import(renglones_escrutinio_a_insertar)
          @importacion_escrutinio.update_attributes!(cantidad_registros: RenglonEscrutinio.importacion_escrutinio(@importacion_escrutinio).count)
        end

        path_errors_file = @importacion_escrutinio.path_archivo_con_errores_importacion
        verificar_errores_importacion(objetos_con_error, path_errors_file)
        File.delete(path_file)

        @importacion_escrutinio.calcular_resultados!
      end
    end
  end

  private

  def importar_renglones_escrutinio(renglones_escrutinio_a_insertar, cantidad_a_insertar, index, objetos_con_error, calcular_errores)
    if cantidad_a_insertar == 1000
      RenglonEscrutinio.import(renglones_escrutinio_a_insertar)
      @importacion_escrutinio.update_attributes!(cantidad_registros: RenglonEscrutinio.importacion_escrutinio(@importacion_escrutinio).count)

      cantidad_a_insertar = 0
      renglones_escrutinio_a_insertar = []
    else
      cantidad_a_insertar += 1
    end

    [renglones_escrutinio_a_insertar, cantidad_a_insertar]
  end

  def verificar_errores_importacion(objetos_con_error, path_errors_file)
    if !objetos_con_error.empty?
      generar_archivo_con_errores(objetos_con_error, path_errors_file)
    elsif File.exist?(path_errors_file)
      File.delete(path_errors_file)
    end
  end

  def generar_archivo_con_errores(errores, path_errors_file)
    Spreadsheet.client_encoding = 'UTF-8'
    errores_xls = Spreadsheet::Workbook.new

    errores_sheet = errores_xls.create_worksheet name: 'Errores'
    errores_sheet.row(0).concat(['Cant. Votos', 'Cod. Cargo Elección', 'Cod. Partido', 'Descripción Partido', 'Codigo Cargo', 'Descripción Cargo', 'Código Sección', 'Código Circuito', 'Nro. Mesa', 'Sexo Mesa'])

    errores.each_with_index do |error, index|
      errores_sheet[(index + 1), 0] = error[:cantidad_votos]
      errores_sheet[(index + 1), 1] = error[:codigo_cargo_eleccion]
      errores_sheet[(index + 1), 2] = error[:codigo_partido]
      errores_sheet[(index + 1), 3] = error[:descripcion_partido]
      errores_sheet[(index + 1), 4] = error[:codigo_cargo]
      errores_sheet[(index + 1), 5] = error[:descripcion_cargo]
      errores_sheet[(index + 1), 6] = error[:codigo_seccion]
      errores_sheet[(index + 1), 7] = error[:codigo_circuito]
      errores_sheet[(index + 1), 8] = error[:numero_mesa]
      errores_sheet[(index + 1), 9] = error[:sexo_mesa]
    end

    errores_xls.write(path_errors_file)
  end

end
