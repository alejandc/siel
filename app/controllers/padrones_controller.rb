# encoding: utf-8

class PadronesController < ApplicationController
  load_and_authorize_resource

  # GET /padrones
  # GET /padrones.json
  def index
    @padrones = Padron.order('fecha DESC')
    @ultima_fecha = Padron.maximum(:fecha)

    begin
      @files = import_files
    rescue Exception => e
      logger.error e
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @padrones }
    end
  end

  # GET /padrones/1
  # GET /padrones/1.json
  def show
    @padron = Padron.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @padron }
    end
  end

  # GET /padrones/new
  # GET /padrones/new.json
  def new
    @padron = Padron.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @padron }
    end
  end

  # GET /padrones/1/renglones_importados
  def renglones_importados
    padron = Padron.find(params[:id])

    importados = padron.cantidad_real_renglones.to_f
    estimados = padron.cantidad_renglones.to_f

    @progreso = (importados / estimados * 100).to_i

    if @progreso > 98
      @padrones = Padron.order('fecha DESC')
      @ultima_fecha = Padron.maximum(:fecha)
    end
  end

  # GET /padrones/1/edit
  def edit
    @padron = Padron.find(params[:id])
  end

  # POST /padrones
  # POST /padrones.json
  def create
    @padron = Padron.new(params[:padron])

    respond_to do |format|
      if @padron.save
        format.html { redirect_to @padron, notice: 'Padron was successfully created.' }
        format.json { render json: @padron, status: :created, location: @padron }
      else
        format.html { render action: 'new' }
        format.json { render json: @padron.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /padrones/importar/:nombre_archivo
  def importar
    filename = "#{FILES_PATH}/#{import_files[params[:index].to_i]}"

    # Por ahora se borran todos los renglones de los padrones anteriores.
    RenglonPadron.delete_all
    Padron.importar filename, current_user

    @padrones = Padron.order('fecha DESC')
    @ultima_fecha = Padron.maximum(:fecha)
  end

  # PUT /padrones/1
  # PUT /padrones/1.json
  def update
    @padron = Padron.find(params[:id])

    respond_to do |format|
      if @padron.update_attributes(params[:padron])
        format.html { redirect_to @padron, notice: 'Padron was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @padron.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /padrones/1
  # DELETE /padrones/1.json
  def destroy
    @padron = Padron.find(params[:id])
    @padron.destroy

    respond_to do |format|
      format.js
      format.html { redirect_to padrones_url }
      format.json { head :no_content }
    end
  end

  private

  def import_files
    Dir.entries(FILES_PATH).reject { |e| File.directory?(e) }
  end

end
