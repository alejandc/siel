class PersonasController < ApplicationController

  # GET /personas/1/archivos_persona.json
  def archivos_persona
    persona = Persona.find(params[:id])

    respond_to do |format|
      format.json { render json: persona.archivos_persona }
    end
  end

  # GET /personas/1/candidaturas.json
  def candidaturas
    persona = Persona.find(params[:id])

    respond_to do |format|
      format.json { render json: persona.candidaturas }
    end
  end

  # GET /personas/1.json
  def show
    persona = Persona.find(params[:id])

    respond_to do |format|
      format.json { render json: persona }
    end
  end

  # POST /personas/adjuntar_documento
  def adjuntar_documento
    persona = Persona.find(params[:persona_id])
    persona.adjuntar_archivo params[:archivo], params[:descripcion]

    @archivos = persona.archivos_persona
  end

  # GET "personas/descargar_documento/:archivo_persona_id"
  def descargar_documento
    archivo = ArchivoPersona.find(params[:archivo_persona_id]).archivo

    send_data archivo.contenido, filename: I18n.transliterate(archivo.nombre), type: archivo.tipo_contenido
  end

  # GET /personas/1/edit
  def edit
    @persona = Persona.find(params[:id])

    @persona.domicilio = Domicilio.new if @persona.domicilio.nil?
  end

  # PUT /personas/1
  # PUT /personas/1.json
  def update
    @persona = Persona.find(params[:id])

    if @persona.domicilio.nil?
      @persona.build_domicilio(params[:domicilio])
    else
      @persona.domicilio.update_attributes(params[:domicilio])
    end

    respond_to do |format|
      if @persona.update_attributes(params[:persona])
        format.html { redirect_to candidaturas_importar_lista_path(eleccion_id: session[:eleccion_importar_lista], fuerza_id: session[:fuerza_importar_lista]), notice: 'Persona was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @persona.errors, status: :unprocessable_entity }
      end
    end
  end

  def busqueda_persona
    @tab = params[:tab]
    @editar = params[:editar] == 'true'

    @lista_cargo = ListaCargo.includes(:eleccion, :fuerza, :cargo).find(params[:lista_cargo_id])
    @eleccion = @lista_cargo.eleccion
    @cargo = @lista_cargo.cargo
    @fuerza = @lista_cargo.fuerza

    begin
      @renglon_padron = PadronApi.buscar_persona(*params.require(:persona).permit(:tipo_documento, :numero_documento, :sexo).values)
      @persona = Persona.buscar_o_crear(@renglon_padron, obtener_info_renaper: true)
      
      flash.now[:notice] = "La persona se ha encontrado en el padrón" unless @editar
    rescue PersonaNoExisteEnPadronError

      if params[:tab] == 'candidaturas'
        @persona = Persona.where(params.require(:persona).permit(:numero_documento, :sexo, :tipo_documento)).first
        @persona ||= Persona.new(params.require(:persona).permit(:numero_documento, :sexo, :tipo_documento))

        @persona.paper_trail.without_versioning do
          @persona.obtener_info_renaper unless @persona.persisted?
          @persona.save

          if @persona.domicilio.blank?
            domicilio = @persona.build_domicilio(calle_referencia: '', calle_nombre: '', numero: '', piso: '', depto: '', codigo_postal: '', origen: :usuario)
            domicilio.paper_trail.without_versioning do
              @persona.save(validate: false)
            end
          end
        end

        @persona.archivos_persona.build if @persona.archivos_persona.empty?

        unless @editar
          if @persona.extranjero?
            flash.now[:sweet_alert_error] = 'Número documento no admitido: El DNI ingresado pertenece a una persona extranjera. Los candidatos deben ser argentinos nativos, naturalizados o por opción.'
          elsif !@persona.errors.has_key?(:numero_documento)
            flash.now[:sweet_alert_error] = 'La persona no figura en el padrón, para continuar es necesario completar los campos del formulario y adjuntar la documentación respaldatoria.'
          else
            flash.now[:sweet_alert_error] = "Hay errores en el formulario de búsqueda: #{@persona.errors[:numero_documento].join(', ')}"
          end
        end

        @no_encontrado = true
      else
        extranjero = SreeApi.busqueda_electores(*(params.require(:persona).permit(:tipo_documento, :numero_documento, :sexo).values + [@eleccion.fecha]))
        if extranjero.present?
          @persona = Persona.buscar_o_crear_extranjero(extranjero, obtener_info_renaper: true)
          @objeto = Adhesion.new(lista_cargo: @lista_cargo, persona: @persona)
          flash.now[:notice] = 'La persona se ha encontrado en el padrón de extranjeros.'
        else
          @persona = Persona.new
          @no_encontrado = true
          flash.now[:sweet_alert_error] = 'La persona no se encuentra en el padrón. No se pueden agregar adherentes sin empadronar.'
        end
      end
    end

    @objeto = if params[:tab] == 'candidaturas'
                Candidatura.new(eleccion: @eleccion, cargo: @cargo, lista_cargo: @lista_cargo, persona: @persona)
              else
                Adhesion.new(lista_cargo: @lista_cargo, persona: @persona)
              end

    respond_to do |format|
      format.js do
        if @lista_cargo.eleccion_paso?
          render 'lista_cargos/busqueda_persona'
        else
          render 'lista_cargos_generales/busqueda_persona'
        end
      end
    end
  end

  def busqueda_persona_fiscal
    @eleccion = Eleccion.find(params[:eleccion_id])
    @fuerza = Fuerza.find(params[:fuerza_id])

    begin
      @renglon_padron = PadronApi.buscar_persona(*params.require(:persona).permit(:tipo_documento, :numero_documento, :sexo).values)
      @persona = Persona.buscar_o_crear(@renglon_padron, obtener_info_renaper: true)
      @fiscal = Fiscal.new(eleccion: @eleccion, fuerza: @fuerza, tipo_documento: params[:persona][:tipo_documento], numero_documento: params[:persona][:numero_documento], sexo: params[:persona][:sexo])
      flash.now[:notice] = "La persona se ha encontrado en el padrón"
    rescue PersonaNoExisteEnPadronError => error
      @persona = Persona.new
      @fiscal = Fiscal.new(eleccion: @eleccion, fuerza: @fuerza)
      flash.now[:error] = error.message
    end

    respond_to do |format|
      format.js do
        render 'fiscales/busqueda_persona'
      end
    end
  end

  def cargar_avatar
    persona = Persona.find(params[:persona][:id])

    begin
      persona.update_attributes!(params.require(:persona).permit(:avatar))
      flash[:notice] = 'La imagen del candidato ha sido actualizada correctamente'
    rescue
      flash[:error] = 'No se ha podido actualizar la imagen del candidato'
    end

    @tab = params[:tab]
    @lista_cargo = ListaCargo.find(params[:lista_cargo_id])
    @lista = @lista_cargo.lista

    if @lista_cargo.eleccion_paso?
      redirect_to lista_lista_cargo_path(@lista, @lista_cargo)
    else
      redirect_to listas_generales_lista_cargos_generales_path(@lista, @lista_cargo)
    end
  end

  def ver_adjuntos
    @persona = Persona.find(params[:id])
    @lista_cargo = ListaCargo.find(params[:lista_cargo_id])

    respond_to do |format|
      format.js { render 'lista_cargos/ver_adjuntos' }
    end
  end

  def calcular_comuna
    circuito_id = CallejeroApi.rangos_calles(params[:calle_id], params[:altura]).try(:first).try(:[], 'circuito_id')

    respond_to do |format|
      if circuito_id.present?
        format.json { render json: { msg: 'Comuna encontrada correctamente', comuna: Callejero.instance.comuna_en_circuito(circuito_id) }, status: :ok }
      else
        format.json { render json: { msg: 'Error al buscar la comuna' }, status: :unprocessable_entity }
      end
    end
  end

end
