class ListaCargosGeneralesController < ApplicationController

  def show
    @lista_cargo = ListaCargo.find(params[:id])
    authorize! :manage, @lista_cargo
    @lista = @lista_cargo.lista
    @eleccion = @lista.eleccion
    @fuerza = @lista.fuerza
    @cargo = @lista_cargo.cargo
    @cargos_ganadores = @lista.lista_cargos.includes(:candidaturas).reject { |lista_cargo| lista_cargo.candidaturas.empty? }
    @persona = Persona.new
    @candidatura = Candidatura.new(persona: @persona)
  end

  def imprimir_lista_general
    @lista_cargo = ListaCargo.find(params[:id])
    authorize! :manage, @lista_cargo
    respond_to do |format|
      format.pdf { send_lista_general_pdf }
    end
  end


  private

  def lista_general_pdf
    lista_cargo = ListaCargo.find(params[:id])

    ListaCargosGeneralPdf.new(lista_cargo)
  end
 
  def send_lista_general_pdf
    send_file lista_general_pdf.to_pdf,
      filename: lista_general_pdf.filename,
      type: "application/pdf",
      disposition: "inline"
  end

end
