class ArchivoPersonasController < ApplicationController

  def create
    @archivo_persona = ArchivoPersona.new(archivo_persona_params)

    respond_to do |format|
      if @archivo_persona.save
        format.json { render json: { msg: 'Archivo persona agregado', archivo_persona: @archivo_persona }, status: :ok }
      else
        format.json { render json: { msg: 'Error al agregar un archivo a la persona.' }, status: :unprocessable_entity }
      end
    end
  end

  def update
    @archivo_persona = ArchivoPersona.find(params[:id])

    respond_to do |format|
      if @archivo_persona.update_attributes(archivo_persona_params)
        @archivo_persona.paper_trail.touch_with_version
        format.json { render json: { msg: 'Archivo persona actualizado.', archivo_persona: @archivo_persona }, status: :ok }
      else
        format.json { render json: { msg: 'Error al actualizar el archivo.' }, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @lista_cargo = ListaCargo.find(params[:lista_cargo_id])
    @lista = @lista_cargo.lista
    @archivo_persona = ArchivoPersona.find(params[:id])
    @persona = @archivo_persona.persona
    @tipo_archivo = @archivo_persona.tipo_archivo

    if @tipo_archivo != ArchivoPersona::TIPO_ARCHIVO['Subsana Observación']
      validacion = Validators::ValidadorCandidatura::VALIDACIONES[@tipo_archivo.upcase.to_sym]
      @icono = validacion[:ICONO]
      @titulo = validacion[:TITULO].call(validacion[:PARAMETROS_TITULO].call(@lista_cargo.candidaturas.persona(@persona).first))
    end

    @archivo_persona.archivo.paper_trail.without_versioning do
      @archivo_persona.destroy
    end

    @json = {
              persona: {
                         id: @persona.id,
                         numero_documento: @persona.numero_documento,
                         tipo_documento: @persona.tipo_documento,
                         sexo: @persona.sexo
                       },
              cargo_id: @lista_cargo.cargo_id, eleccion_id: @lista.eleccion_id,
              fuerza_id: @lista.fuerza_id, lista_cargo_id: @lista_cargo.id,
              tipo_archivo: @tipo_archivo, icono: @icono,
              titulo: @titulo, msg: 'Archivo persona borrado.'
            }

    respond_to do |format|
      format.json { render json: @json, status: :ok }
    end
  end

  private

  def archivo_persona_params
    params.require(:archivo_persona).permit(:id, :descripcion, :_destroy, :archivo,
                                            :lista_cargo_id, :tipo_archivo, :persona_id) if params[:archivo_persona]
  end
end
