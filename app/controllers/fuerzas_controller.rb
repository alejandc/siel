class FuerzasController < ApplicationController
  load_and_authorize_resource

  # GET /fuerzas
  # GET /fuerzas.json
  def index
    @filtros = params[:filtros] || {}
    @filtros[:eleccion_id] = Eleccion.actual.paso.id unless @filtros.key?(:eleccion_id)
    @template = @filtros.delete(:template)
    @fuerzas_totales = Fuerza.filtrar(@filtros)
    @cantidad_fuerzas = @fuerzas_totales.count
    @fuerzas = @fuerzas_totales.page(params[:page])
    @elecciones = Eleccion.por_tipo('paso').all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @fuerzas }
      format.xls { render @template }
    end
  end

  # GET /fuerzas/1
  # GET /fuerzas/1.json
  def show
    @fuerza = Fuerza.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @fuerza }
    end
  end

  # GET /fuerzas/baja/1
  def baja
    @fuerza = Fuerza.find(params[:id])

    if @fuerza.es_alianza?
      ActiveRecord::Base.transaction do
        begin
          @fuerza.listas.destroy_all
          @fuerza.destroy
        rescue => e
          raise ActiveRecord::Rollback
        end
      end
    else
      @fuerza.fecha_baja = Date.today
      @fuerza.save!
    end

    redirect_to fuerzas_path
  end

  # GET /fuerzas/new
  # GET /fuerzas/new.json
  def new
    @back_to = request.env['HTTP_REFERER']
    @fuerza = Fuerza.new
    @fuerza.init_associations

    @partidos = Fuerza.partidos.vigentes.activos.order(:nombre)
    @elecciones = Eleccion.por_tipo('paso').all

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @fuerza }
    end
  end

  # GET /fuerzas/1/edit
  def edit
    eleccion_actual = Eleccion.actual
    @back_to = request.env['HTTP_REFERER']
    @fuerza = Fuerza.find(params[:id])
    @fuerza.build_domicilio_constituido if @fuerza.domicilio_constituido.blank?
    @fuerza.build_domicilio_real if @fuerza.domicilio_real.blank?
    @fuerza.fuerza_elecciones.build(eleccion_id: eleccion_actual.id) if @fuerza.fuerza_elecciones.eleccion(eleccion_actual).empty?

    @partidos = Fuerza.partidos
    @elecciones = Eleccion.por_tipo('paso').all
  end

  # GET /fuerzas/1/apoderados.json
  def apoderados
    fuerza = Fuerza.find(params[:id])

    respond_to do |format|
      format.json { render json: fuerza.apoderados }
    end
  end

  # GET /fuerzas/1/candidaturas.json
  def candidaturas
    fuerza = Fuerza.find(params[:id])
    eleccion = Eleccion.find(params[:eleccion_id])

    candidaturas = fuerza.candidaturas_eleccion(eleccion).sort
    Candidatura.marcar_incumplimientos_cupo!(candidaturas)

    respond_to do |format|
      format.json { render json: candidaturas }
    end
  end

  def filtrar
    eleccion = Eleccion.find(params[:eleccion_id]) unless params[:eleccion_id].nil? or params[:eleccion_id].empty?
    @fuerzas = Fuerza.partidos.filtrar(params).participo_de_eleccion?(eleccion.try(:id))

    respond_to do |format|
      format.js
      format.xls
    end
  end

  # POST /fuerzas/listado_apoderados
  def listado_apoderados
    eleccion = Eleccion.find(params[:eleccion_id]) unless params[:eleccion_id].nil? or params[:eleccion_id].empty?
    @fuerzas = Fuerza.partidos.filtrar(params).select { |f| eleccion.nil? or f.participo_de?(eleccion) }

    respond_to do |format|
      format.xls
    end
  end

  # POST /fuerzas
  # POST /fuerzas.json
  def create
    @fuerza = Fuerza.new(parametros_fuerza)

    respond_to do |format|
      if @fuerza.save
        format.html { redirect_to (params[:back_to] || fuerzas_path), notice: I18n.t('messages.saved') }
        format.json { render json: @fuerza, status: :created, location: @fuerza }
      else
        @partidos = Fuerza.partidos
        @elecciones = Eleccion.por_tipo('paso').all

        format.html { render action: 'new' }
        format.json { render json: @fuerza.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /fuerzas/1
  # PUT /fuerzas/1.json
  def update
    @fuerza = Fuerza.find(params[:id])

    respond_to do |format|
      if @fuerza.update_attributes(parametros_fuerza)
        format.html { redirect_to (params[:back_to] || fuerzas_path), notice: I18n.t('messages.saved') }
        format.json { head :no_content }
      else
        @partidos = Fuerza.partidos
        @elecciones = Eleccion.por_tipo('paso').all

        format.html { render action: 'edit' }
        format.json { render json: @fuerza.errors, status: :unprocessable_entity }
      end
    end
  end

  def editar_escudo
    @fuerza = current_user.fuerza
    @fuerza.update_attributes(escudo: params[:fuerza][:escudo])
    flash[:notice] = 'El escudo del partido ha sido modificado correctamente'
    redirect_to fuerza_path(@fuerza)
  end

  def eliminar_escudo
    @fuerza = current_user.fuerza
    if @fuerza.escudo.present?
      @fuerza.escudo.destroy
      @fuerza.escudo.clear
      @fuerza.save!
      flash[:notice] = 'El escudo del partido ha sido eliminado correctamente'
    else
      flash[:warning] = 'El partido no contiene escudo'
    end
    redirect_to fuerza_path(@fuerza)
  end

  # GET /fuerzas/agregar_apoderado
  def agregar_apoderado
    @fuerza = params[:fuerza_id].present? ? Fuerza.find(params[:fuerza_id]) : Fuerza.new
    # @persona = Persona.find_by(numero_documento: params[:numero_documento])

    # if @persona.nil?
    #   @persona = Persona.new
    #   @persona.numero_documento = params[:numero_documento]
    # end

    respond_to do |format|
      format.js
    end
  end

  # DELETE /fuerzas/1
  # DELETE /fuerzas/1.json
  def destroy
    @fuerza = Fuerza.find(params[:id])
    @fuerza.destroy

    respond_to do |format|
      format.html { redirect_to fuerzas_url }
      format.json { head :no_content }
    end
  end

  def select_fuerzas
    @fuerzas = params[:sin_alianzas].present? && params[:sin_alianzas] == 'true' ? Fuerza.partidos.vigentes.activos : Fuerza.partidos_y_alianzas(Eleccion.find(params[:eleccion_id]))

    respond_to do |format|
      format.json { render json: { fuerzas: @fuerzas }, status: :ok }
    end
  end

  def parametros_fuerza
    parametros = params[:fuerza]
    parametros[:fuerza_elecciones_attributes].first.last[:umbral].gsub!(',', '.') if parametros[:fuerza_elecciones_attributes].present?
    parametros
  end
end
