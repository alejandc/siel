class ListasController < ApplicationController
  authorize_resource
  skip_load_and_authorize_resource only: [:edit]
  before_filter :cargar_lista_y_validar, except: [:index, :new, :show, :create, :seccion_cargos_habilitados]
  before_filter :validar_carga_candidaturas_eleccion, except: [:index, :show, :seccion_cargos_habilitados]

  def index
    @eleccion = params[:eleccion_id].present? ? Eleccion.find(params[:eleccion_id]).paso : Eleccion.actual.paso
    @fuerzas = Fuerza.partidos_y_alianzas(@eleccion)
    @lista = Lista.includes(lista_cargos: :cargo).find_by_id(params[:lista_id])

    # Cargamos la agrupación politica
    cargar_fuerza_cargos

    # Buscamos las listas solo si tenemos fuerza politica seleccionada
    if @fuerza
      # Si se aplico filtro por lista partidaria solo mostramos la indicada
      @listas = if @lista && @lista.fuerza_id == @fuerza.id
                  Kaminari.paginate_array(Array(@lista)).page(params[:page])
                else
                  Lista.por_eleccion(@eleccion).por_fuerza(@fuerza).includes(lista_cargos: :cargo).page(params[:page])
                end
    end

    # @listas = @listas.try(:oficializadas) if current_user.tsj?
    @lista_nueva = Lista.new(eleccion_id: @eleccion.try(:id), fuerza_id: @fuerza.try(:id))

    respond_to do |format|
      format.html # index.html.erb
      format.js { render 'usuarios/select_lista' }
    end
  end

  def new
    @lista = Lista.new
    @eleccion = Eleccion.find(params[:eleccion_id])
    @fuerza = Fuerza.find(params[:fuerza_id]) if params[:fuerza_id].present?

    @lista.assign_attributes(eleccion_id: @eleccion.id, fuerza_id: @fuerza.try(:id))

    respond_to do |format|
      format.js
    end
  end

  def edit
    respond_to do |format|
      format.js
    end
  end

  def show
    # Esta ruta esta pensada para que la accedan usuarios con rol de lista partidaria
    # Buscamos la elección, la fuerza politica y los cargos correspondientes
    @eleccion = current_user.lista_partidaria? ? current_user.eleccion : Eleccion.actual
    cargar_fuerza_cargos
    @lista = current_user.lista
  end

  def create
    @lista = Lista.new(lista_params)
    @fuerza = @lista.fuerza
    @eleccion = @lista.eleccion
    @lista.build_lista_cargos

    if @eleccion.cargos_seleccionados.size == 1
      @cargos_habilitados = { Cargo::CARGOS.key(@eleccion.cargos_seleccionados.first.nombre) => true }
      @lista.asignar_cargos_habilitados(@cargos_habilitados.try(:keys).map(&:to_s))
    else
      @cargos_habilitados = params[:cargos_habilitados] || {}
      @lista.asignar_cargos_habilitados(params[:cargos_habilitados].try(:keys) || [])
    end

    if @lista.save
      flash.now[:notice] = "La lista '#{@lista.nombre}' fue creada."
    end

    respond_to do |format|
      format.js
    end
  end

  def update
    @fuerza = @lista.fuerza
    @eleccion = @lista.eleccion
    @cargos = @eleccion.cargos_seleccionados.compact if @eleccion
    @lista.assign_attributes(lista_params)
    @lista.asignar_cargos_habilitados(params[:cargos_habilitados].try(:keys) || []) if @eleccion.cargos_seleccionados.size > 1

    flash.now[:notice] = 'Lista actualizada correctamente' if @lista.save

    respond_to do |format|
      format.js
    end
  end

  def asignar_numero_escrutinio
    @lista = Lista.find(params[:id])
    fuerza_numero = (params[:lista][:numero] == "null") ? nil : params[:lista][:numero]

    respond_to do |format|
      
      if @lista.update_attributes(numero: fuerza_numero)
        format.json { respond_with_bip(@lista) }
      else
        format.json { render json: @lista.errors.full_messages, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @lista = Lista.find(params[:id])

    authorize! :manage, @lista
    @lista.destroy

    flash[:notice] = 'Lista eliminada correctamente'

    redirect_to listas_path(fuerza_id: params[:fuerza_id])
  end

  def configurar
  end

  def seccion_cargos_habilitados
    @lista = Lista.new(eleccion_id: params[:eleccion_id])

    respond_to do |format|
      format.js
    end
  end

  def validar
    @lista.validar!
    @lista.reload

    respond_to do |format|
      format.js
    end
  end

  def oficializar
    @lista.cerrar!
    @lista.oficializar!(current_user) if @lista.valida?
    @fuerza = @lista.fuerza

    respond_to do |format|
      format.js
    end
  end

  def desoficializar
    @lista.desoficializar_general!
    @fuerza = @lista.fuerza

    respond_to do |format|
      format.js
    end
  end

  def imprimir_lista_prototipo
    data = StringIO.new
    excel = ListaPrototipoExcel.new(@lista).imprimir
    excel.write(data)

    respond_to do |format|
      format.xls do
        send_data data.string, filename: I18n.transliterate("Candidatos - #{@lista.fuerza_nombre} - #{@lista.nombre}.xls"), type: 'application/xls'
      end
    end
  end

  def imprimir_acta_presentacion
    respond_to do |format|
      format.docx do
        render docx: 'imprimir_acta_presentacion', filename: I18n.transliterate("#{@lista.fuerza_nombre.upcase} - #{@lista.nombre.upcase}.docx")
      end
    end
  end

  def imprimir_resolucion_oficializacion
    @fuerza = Fuerza.find(params[:fuerza_id])
    @listas = Lista.por_fuerza(@fuerza).reject { |lista| lista.abierta? }

    if @listas.empty?
      flash[:warning] = 'No hay listas disponibles (Cerradas) para oficializar.'
      redirect_to listas_path(fuerza_id: @fuerza.id) and return
    end

    respond_to do |format|
      format.docx do
        render docx: 'imprimir_resolucion_oficializacion', filename: "Resolucion de Oficializacion.docx"
      end
    end
  end

  def imprimir_comunicado_articulo_26
    @fuerza = Fuerza.find(params[:fuerza_id])
    @listas = Lista.por_fuerza(@fuerza).reject { |lista| lista.abierta? }

    if @listas.empty?
      flash[:warning] = 'No hay listas disponibles (Cerradas) para oficializar.'
      redirect_to listas_path(fuerza_id: @fuerza.id) and return
    end

    respond_to do |format|
      format.docx do
        render docx: 'imprimir_comunicado_articulo_26', filename: "Comunicado Articulo 26.docx"
      end
    end
  end

  def bitacora
    @versiones = Lista.find(params[:id]).versions.recientes

    respond_to do |format|
      format.js { render 'bitacora' }
    end
  end

  private

  def cargar_lista_y_validar
    if params[:id].present?
      @lista = Lista.find(params[:id])

      if current_user.lista_partidaria? and current_user.lista_id != @lista.id
        raise CanCan::AccessDenied.new("No esta autorizado a realizar esta operación!", :manage, Lista)
      end

      if current_user.apoderado? and current_user.fuerza.id != @lista.fuerza_id
        raise CanCan::AccessDenied.new("No esta autorizado a realizar esta operación!", :manage, Lista)
      end
    end
  end

  def validar_carga_candidaturas_eleccion
    @eleccion = (current_user.externo?) ? current_user.eleccion : ((params[:eleccion_id].present?) ? Eleccion.find(params[:eleccion_id]).paso : Eleccion.actual.paso)

    verificar_carga_candidaturas(@eleccion)
  end

  def cargar_fuerza_cargos
    # Seteamos la fuerza politica si el usuario es apoderado o lista partidaria
    @fuerza = current_user.externo? ? current_user.fuerza : Fuerza.find_by_id(params[:fuerza_id])
    @cargos = @eleccion.cargos_seleccionados.compact if @eleccion
  end

  def lista_params
    if params[:action] == 'create'
      current_user.apoderado? ? params.require(:lista).permit(:nombre, :numero).merge(eleccion_id: current_user.eleccion_id, fuerza_id: current_user.fuerza.id) : params.require(:lista).permit(:eleccion_id, :fuerza_id, :nombre, :numero)
    else
      params.require(:lista).permit(:nombre)
    end
  end
end
