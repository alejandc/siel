
class AdminController < ApplicationController
  layout 'admin'

  def login_extranet
    logger.debug "LOGIN EXTRANET - Autenticando al usuario '#{params[:username]}'"
    usuario = Usuario.find_by(nombre_usuario: params[:username])

    if usuario.present?
      if usuario.authenticate(params[:password])
        render json: { message: 'ok', usuario_id: usuario.id }.to_json, status: :ok
      else
        render json: { message: 'wrong-password' }.to_json, status: :unauthorized
      end
    else
      render json: { message: 'not-found' }.to_json, status: :not_found
    end
  end

  def login
    if request.post?
      if params[:username].present? && params[:password].present?
        begin
          if Usuario.find_by_nombre_usuario(params[:username])
            login_usuario
          elsif TSJ_SECURITY.authenticate(params[:username], params[:password]) == 'true'
            login_admin
          else
            flash[:notice] = I18n.t 'messages.wrong_login'
          end
        rescue TsjSecurityError => e
          logger.error e
          flash[:notice] = I18n.t 'messages.ws_comm_problem'
        end
      else
        flash[:notice] = I18n.t 'messages.user_pass_required'
      end
    end
  end

  def logout
    flash[:sweet_alert_warning] = params[:logout_message] if params[:logout_message].present?
    session[:user] = nil
    session[:logout] = true
    redirect_to :root
  end

  private

  def login_admin
    logger.debug "El usuario #{params[:username]} pertenece a los grupos #{session[:user_groups]}."
    session[:user] = Usuario.new(nombre_usuario: params[:username])
    current_user.perfiles.present? ? login_exitoso : login_usuario_sin_permisos
  end

  def login_usuario
    logger.debug "LOGIN EXTRANET - Autenticando al usuario '#{params[:username]}'"
    usuario = Usuario.find_by(nombre_usuario: params[:username])

    if usuario.present? && usuario.authenticate(params[:password]) && usuario.habilitado
      logger.debug "El usuario #{params[:username]} pertenece a los grupos #{session[:user_groups]}."
      session[:user] = usuario
      login_exitoso
    else
      if !usuario.habilitado
        flash[:notice] = "Usuario deshabilitado, pongase en contacto con el Tribunal Superior de Justicia"
      else
        flash[:notice] = I18n.t 'messages.wrong_login'
      end
    end
  end

  def login_usuario_sin_permisos
    session[:user] = nil
    session[:logout] = true
    flash[:notice] = 'Usuario sin autorización para acceder al sistema'
    redirect_to :root
  end

  def login_exitoso
    uri = (current_user.interno?) ? :root : (current_user.apoderado?) ? listas_path : lista_path(current_user.lista)
    redirect_to(uri)
    flash[:notice] = nil
  end
end
