class ConfiguracionesController < ApplicationController
  authorize_resource
  
  def show
    @configuracion = Configuracion.first
  end

  def update
    @configuracion = Configuracion.first

    respond_to do |format|
      if @configuracion.update_attributes(configuracion_params)
        format.json { respond_with_bip(@configuracion) }
      else
        format.json { respond_with_bip(@configuracion) }
      end
    end
  end

  private

  def configuracion_params
    params.require(:configuracion).permit(:habilitar_renaper_api, :antiguedad_registro_candidaturas) if params[:configuracion].present?
  end

end
