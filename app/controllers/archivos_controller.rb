class ArchivosController < ApplicationController

  # GET /archivos/1
  def show
    @archivo = Archivo.find(params[:id])

    respond_to do |format|
      format.json { render json: @archivo }
    end
  end

  # GET /archivos/descargar/1
  def descargar
    archivo = Archivo.find(params[:id])

    send_data archivo.contenido, filename: archivo.nombre, type: archivo.tipo_contenido
  end

end
