class FuerzaEleccionController < ApplicationController

  def update
    @fuerza_eleccion = FuerzaEleccion.find(params[:id])

    respond_to do |format|
      if @fuerza_eleccion.update_attributes(fuerza_eleccion_params)
        format.json { respond_with_bip(@fuerza_eleccion) }
      else
        format.json { render json: @fuerza_eleccion.errors.full_messages, status: :unprocessable_entity }
      end
    end
  end

  private

  def fuerza_eleccion_params
    parametros = params.require(:fuerza_eleccion).permit(:umbral)
    parametros[:umbral] = parametros[:umbral].gsub(',', '.') if parametros.key?(:umbral)
    parametros
  end

end
