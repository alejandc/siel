class ObservacionesController < ApplicationController
  load_and_authorize_resource
  skip_load_and_authorize_resource only: [:index]

  def index
    @pagina_observaciones = params[:page]
    @lista_cargo = ListaCargo.find(params[:lista_cargo_id])
    @tab = 'observaciones'
    @observaciones_search = ObservacionesSearch.new(observaciones_search_params)
    @observaciones = @observaciones_search.results

    ids_candidaturas_con_observaciones = @observaciones.select('DISTINCT observable_id')
    @candidaturas_con_observaciones = Candidatura.where('id IN (?)', ids_candidaturas_con_observaciones).order('candidaturas.orden ASC').page(@pagina_observaciones)
    @observaciones = @observaciones.observables_ids(@candidaturas_con_observaciones.pluck(:id)).orden_posicion

    respond_to do |format|
      format.js { render 'lista_cargos/observaciones' }
    end
  end

  def edit
    respond_to do |format|
      format.js { render 'lista_cargos/edit_observacion' }
    end
  end

  def update
    @observacion.update_attributes(params.require(:observacion).permit(:comentarios))

    respond_to do |format|
      format.js { render 'lista_cargos/update_observacion' }
    end
  end

  def subsanar
    estado = params[:estado]

    respond_to do |format|
      if can?(:manage, @observacion) && @observacion.cambiar_estado!(estado)
        format.json { render json: { id: @observacion.id, color_estado: @observacion.decorate.color_estado }, status: :ok }
      else
        format.json { render json: { id: @observacion.id, color_estado: @observacion.decorate.color_estado }, status: :unprocessable_entity }
      end
    end
  end

  private

  def observaciones_search_params
    search_params = params.try(:[], :observaciones_search) || {}
    search_params.merge(lista_cargo_eq: @lista_cargo.id)
  end

end
