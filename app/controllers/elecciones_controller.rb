require 'reparto_d_hondt'
require 'reparto_error'

require 'reparto_anexo_1_pdf'
require 'reparto_anexo_2_pdf'
require 'reparto_anexo_2_2009_pdf'
require 'reparto_anexo_3_2009_pdf'
require 'reparto_anexo_4_2009_pdf'
require 'reparto_anexo_5_2009_pdf'

class EleccionesController < ApplicationController
  load_and_authorize_resource

  # GET /elecciones
  # GET /elecciones.json
  def index
    @anios = [''] + Eleccion.select(:fecha).map { |e| e.fecha.year }.uniq
    @elecciones = Eleccion.order('fecha DESC, descripcion ASC')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @elecciones }
    end
  end

  # GET /elecciones/actual.json
  def actual
    eleccion = Eleccion.actual

    respond_to do |format|
      format.json { render json: eleccion }
    end
  end

  # GET /elecciones/1/cargos
  def cargos
    eleccion = Eleccion.find(params[:id])

    respond_to do |format|
      format.json { render json: eleccion.cargos_seleccionados }
    end
  end

  # GET /elecciones/1/fuerzas
  def fuerzas
    @actual = Eleccion.find(params[:id]).general
    @fuerzas = @actual.fuerzas unless @actual.nil?
    @elecciones = Eleccion.por_tipo('general').order('fecha DESC')
  end

  # GET /elecciones/1/exportar_apoderados
  def exportar_apoderados
    actual = Eleccion.find(params[:id])
    @fuerzas = actual.fuerzas unless actual.nil?

    respond_to do |format|
      format.xls
    end
  end

  def filtrar
    anio = params[:anio].to_i
    @elecciones = anio != 0 ? Eleccion.where(fecha: Date.new(anio, 01, 01)..Date.new(anio, 12, 31)).order('fecha DESC, descripcion ASC') : Eleccion.order('fecha DESC, descripcion ASC')
  end

  def descargar_resolucion
    archivo = Eleccion.find(params[:eleccion_id]).archivo

    send_data archivo.contenido, filename: archivo.nombre, type: archivo.tipo_contenido
  end

  # GET /elecciones/1
  # GET /elecciones/1.json
  def show
    @eleccion = Eleccion.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @eleccion }
    end
  end

  # GET /elecciones/new
  # GET /elecciones/new.json
  def new
    @eleccion = Eleccion.new

    # La primera vez hay que vincular la elección a los cargos
    @eleccion.asignar_cargos(Cargo.all)

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @eleccion }
    end
  end

  # GET /elecciones/1/edit
  def edit
    @eleccion = Eleccion.find(params[:id])
  end

  # POST /elecciones
  # POST /elecciones.json
  def create
    @eleccion = Eleccion.new(parametros_permitidos)
    
    @eleccion.adjuntar_archivo(params[:archivo]) if params[:archivo].present?

    respond_to do |format|
      if @eleccion.save
        format.html { redirect_to elecciones_path, notice: I18n.t('messages.saved') }
        format.json { render json: @eleccion, status: :created, location: @eleccion }
      else
        format.html { render action: 'new' }
        format.json { render json: @eleccion.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /elecciones/1
  # PUT /elecciones/1.json
  def update
    @eleccion = Eleccion.find(params[:id])

    if params[:archivo].present?
      @eleccion.archivo.destroy if @eleccion.archivo.present?
      @eleccion.adjuntar_archivo(params[:archivo])
    end

    respond_to do |format|
      if @eleccion.update_attributes(parametros_permitidos)
        format.html { redirect_to elecciones_path, notice: I18n.t('messages.saved') }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @eleccion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /elecciones/1
  # DELETE /elecciones/1.json
  def destroy
    @eleccion = Eleccion.find(params[:id])
    @eleccion.destroy

    respond_to do |format|
      format.html { redirect_to elecciones_url }
      format.json { head :no_content }
    end
  end

  # GET /elecciones/repartos
  def repartos
    @elecciones_sin_resultados = Eleccion.all
    @elecciones = Eleccion.con_resultados

    @elecciones_sin_resultados -= @elecciones
  end

  # POST /elecciones/1/cargar_reparto
  def cargar_reparto
    @eleccion = Eleccion.find(params[:id])
    @eleccion.inicializar_resultados if @eleccion.resultados_elecciones.empty?

    preparar_variables_carga(@eleccion)

    # Ir guardando lo escrito al cambiar de tab
    @eleccion.assign_attributes(params[:eleccion]) if params[:eleccion]
  end

  # POST /elecciones/1/grabar_reparto
  def grabar_reparto
    @eleccion = Eleccion.find(params[:id])

    if @eleccion.update_attributes(params[:eleccion])
      if @eleccion.resultados_completos?
        begin
          # Asignar escanios y grabar el resultado en el mismo momento
          @eleccion.eleccion_cargos.each(&:desasignar_escanios)
          @eleccion.asignar_escanios.each(&:save)
        rescue Exception => e
          @eleccion.errors.add :base, e.message

          preparar_variables_carga(@eleccion)

          render action: 'cargar_reparto'
        end
      end
      redirect_to :repartos_elecciones
    else
      preparar_variables_carga(@eleccion)

      render action: 'cargar_reparto'
    end
  end

  # GET /elecciones/1/reparto_anexo_1.pdf
  def reparto_anexo_1
    eleccion = Eleccion.find(params[:id])
    comunas = Comuna.all

    respond_to do |format|
      format.pdf do
        send_data RepartoAnexo1Pdf.new(eleccion, comunas).render, filename: "eleccion_#{eleccion.fecha}_anexo_I.pdf", type: 'application/pdf'
      end
    end
  end

  # GET /elecciones/1/reparto_anexo_2.pdf
  def reparto_anexo_2
    @eleccion = Eleccion.find(params[:id])

    respond_to do |format|
      format.pdf do
        send_data RepartoAnexo2Pdf.new(@eleccion).render, filename: "eleccion_#{@eleccion.fecha}_anexo_II.pdf", type: 'application/pdf'
      end
    end
  end

  # GET /elecciones/1/reparto_anexo_2_2009.pdf
  def reparto_anexo_2_2009
    eleccion = Eleccion.find(params[:id])
    comunas = Comuna.all

    respond_to do |format|
      format.pdf do
        send_data RepartoAnexo22009Pdf.new(eleccion, comunas).render, filename: "eleccion_#{eleccion.fecha}_anexo_II.pdf", type: 'application/pdf'
      end
    end
  end

  # GET /elecciones/1/reparto_anexo_3_2009.pdf
  def reparto_anexo_3_2009
    eleccion = Eleccion.find(params[:id])

    respond_to do |format|
      format.pdf do
        send_data RepartoAnexo32009Pdf.new(eleccion).render, filename: "eleccion_#{eleccion.fecha}_anexo_III.pdf", type: 'application/pdf'
      end
    end
  end

  # GET /elecciones/1/reparto_anexo_4_2009.pdf
  def reparto_anexo_4_2009
    eleccion = Eleccion.find(params[:id])

    respond_to do |format|
      format.pdf do
        send_data RepartoAnexo42009Pdf.new(eleccion).render, filename: "eleccion_#{eleccion.fecha}_anexo_IV.pdf", type: 'application/pdf'
      end
    end
  end

  # GET /elecciones/1/reparto_anexo_5_2009.pdf
  def reparto_anexo_5_2009
    eleccion = Eleccion.find(params[:id])

    respond_to do |format|
      format.pdf do
        send_data RepartoAnexo52009Pdf.new(eleccion).render, filename: "eleccion_#{eleccion.fecha}_anexo_V.pdf", type: 'application/pdf'
      end
    end
  end

  # DELETE /elecciones/1/destroy_reparto
  # DELETE /elecciones/1/destroy_reparto.json
  def destroy_reparto
    @eleccion = Eleccion.find(params[:id])
    @eleccion.borrar_reparto

    respond_to do |format|
      format.html { redirect_to :repartos_elecciones }
      format.json { head :no_content }
    end
  end

  private

  def preparar_variables_carga(eleccion)
    @comunas = Comuna.all
    @comuna = (params[:comuna] ? @comunas.select { |c| c.id == params[:comuna].to_i } : @comunas).first
    @eleccion_cargos = eleccion.eleccion_cargos.select(&:seleccionado?)
    @fuerzas = eleccion.fuerzas.sort { |f1, f2| f1.numero <=> f2.numero }
    @resultados_comuna = eleccion.resultados_elecciones.select { |r| r.comuna == @comuna && !r.fuerza.nil? }
    @blancos_nulos = eleccion.resultados_elecciones.select { |r| r.comuna == @comuna && r.fuerza.nil? }
    @otros_resultados = eleccion.resultados_elecciones - @resultados_comuna - @blancos_nulos
    @resultados_comunas = @eleccion.resultados_comunas
  end

  def parametros_permitidos
    params.require(:eleccion).permit(:descripcion, :'fecha', :'fecha_posesion', :expediente, :notas,
                                     :observaciones, :fecha_limite_carga_candidaturas, :habilitar_carga_candidaturas,
                                     eleccion_cargos_attributes: [:id, :cargo_id, :seleccionado])
  end

end
