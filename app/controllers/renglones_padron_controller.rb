class RenglonesPadronController < ApplicationController
  load_and_authorize_resource

  # GET /renglones_padron
  # GET /renglones_padron.json
  def index
    @renglones_padron_sin_paginar = (params[:filtro].present? ? RenglonPadron.filtrar(params[:filtro]) : RenglonPadron.none)
    @renglones_padron = @renglones_padron_sin_paginar.page(params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @renglones_padron }
    end
  end

  # GET /renglones_padron/1
  # GET /renglones_padron/1.json
  def show
    @renglon_padron = RenglonPadron.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @renglon_padron }
    end
  end

  # GET /renglones_padron/new
  # GET /renglones_padron/new.json
  def new
    @renglon_padron = RenglonPadron.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @renglon_padron }
    end
  end

  # GET /renglones_padron/1/edit
  def edit
    @renglon_padron = RenglonPadron.find(params[:id])
  end

  # POST /renglones_padron
  # POST /renglones_padron.json
  def create
    @renglon_padron = RenglonPadron.new(params[:renglon_padron])

    respond_to do |format|
      if @renglon_padron.save
        format.html { redirect_to @renglon_padron, notice: 'Renglon padron was successfully created.' }
        format.json { render json: @renglon_padron, status: :created, location: @renglon_padron }
      else
        format.html { render action: 'new' }
        format.json { render json: @renglon_padron.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /renglones_padron/1
  # PUT /renglones_padron/1.json
  def update
    @renglon_padron = RenglonPadron.find(params[:id])

    respond_to do |format|
      if @renglon_padron.update_attributes(params[:renglon_padron])
        format.html { redirect_to @renglon_padron, notice: 'Renglon padron was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @renglon_padron.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /renglones_padron/1
  # DELETE /renglones_padron/1.json
  def destroy
    @renglon_padron = RenglonPadron.find(params[:id])
    @renglon_padron.destroy

    respond_to do |format|
      format.html { redirect_to renglones_padron_url }
      format.json { head :no_content }
    end
  end
end
