class RegistroCandidaturasController < ApplicationController
  authorize_resource

  def index
    @filtro_registro_candidaturas = RegistroCandidatura.new(registro_candidatura_params)

    @registro_candidaturas = if registro_candidatura_params
                               RegistroCandidatura.search_by_filter(@filtro_registro_candidaturas).page params[:page]
                             else
                               RegistroCandidatura.all.page params[:page]
                             end
  end

  def new
    @registro_candidatura = RegistroCandidatura.new
  end

  def edit
    @registro_candidatura = RegistroCandidatura.find(params[:id])
  end

  def create
    @registro_candidaturas_invalidas = RegistroCandidatura.procesar_registro_candidaturas(params[:datos])

    if @registro_candidaturas_invalidas.empty?
      flash[:info] = "Registro de Candidaturas cargadas exitosamente."
      redirect_to registro_candidaturas_path
    else
      @registro_candidatura = RegistroCandidatura.new

      total_registro_candidaturas_cargadas = params[:datos].strip.split(/[\n\r]/).delete_if(&:blank?).size - @registro_candidaturas_invalidas.size
      if total_registro_candidaturas_cargadas > 0
        flash[:warning] = "#{total_registro_candidaturas_cargadas} Registro de Candidaturas cargadas exitosamente. <br/><br/>" +
                          "#{@registro_candidaturas_invalidas.size} Registro de Candidaturas inválidas: <br/>#{@registro_candidaturas_invalidas.join(' , ')}"
      else
        flash[:error] = "#{@registro_candidaturas_invalidas.size} Registro de Candidaturas inválidas: <br/>#{@registro_candidaturas_invalidas.join(' , ')}"
      end

      render 'new'
    end
  end

  def destroy
    @registro_candidatura = RegistroCandidatura.find(params[:id])
    @registro_candidatura.destroy

    flash[:info] = "Registro de Candidatura eliminada exitosamente."
    redirect_to registro_candidaturas_path
  end

  def borrar_registros_candidaturas
    if params[:registro_candidaturas].present?
      RegistroCandidatura.delete_all(id: params[:registro_candidaturas].map(&:to_i))
    else
      RegistroCandidatura.delete_all
    end

    render js: "window.location.pathname='#{registro_candidaturas_path}'"
  end

  private

  def registro_candidatura_params
    params[:registro_candidatura].present? ? params.require(:registro_candidatura).permit(:numero_documento, :tipo_documento, :sexo, :anio_candidatura) : {}
  end
end
