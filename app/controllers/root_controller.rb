class RootController < ApplicationController
  def index
    if current_user.present?
      if current_user.interno?
        redirect_to elecciones_path
      else
        redirect_to(current_user.apoderado? ? listas_path : lista_path(current_user.lista_id))
      end
    end
  end
end
