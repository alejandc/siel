class VerificacionesFirmasController < ApplicationController
  before_action :set_verificacion, only: [:show, :edit, :update, :destroy]

  # GET /verificaciones_firmas
  def index
    @verificaciones = VerificacionFirmas.all
  end

  # GET /verificaciones_firmas/1
  def show
  end

  # GET /verificaciones_firmas/new
  def new
    @verificacion = VerificacionFirmas.new
    @verificacion.confianza = VerificacionFirmas::COEF_CONFIABILIDAD.keys[0]
    @verificacion.error = VerificacionFirmas::ERRORES_RELATIVOS[0]
    @verificacion.variable_p = VerificacionFirmas::VALORES_P[0]
  end

  # GET /verificaciones_firmas/1/edit
  def edit
  end

  # POST /verificaciones_firmas
  def create
    @verificacion = VerificacionFirmas.new(verificacion_firmas_params)
    @verificacion.generar_casos

    if @verificacion.save
      redirect_to @verificacion, notice: 'Verificacion firmas was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /verificaciones_firmas/1
  def update
    if @verificacion.update(verificacion_firmas_params)
      redirect_to @verificacion, notice: 'Verificacion firmas was successfully updated.'
    else
      render :edit
    end
  end

  # PATCH /verificaciones_firmas/1
  def actualizar
    @verificacion = VerificacionFirmas.new(verificacion_firmas_params)
  end

  # DELETE /verificaciones_firmas/1
  def destroy
    @verificacion.destroy
    redirect_to verificaciones_firmas_url, notice: 'Verificacion firmas was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_verificacion
    @verificacion = VerificacionFirmas.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def verificacion_firmas_params
    params.require(:verificacion_firmas).permit(:descripcion, :cantidad_firmas_total, :confianza, :variable_p, :error, :observaciones, :cantidad_firmas_muestra, :cantidad_hojas, :cantidad_firmas_hoja, :cantidad_firmas_requeridas)
  end
end
