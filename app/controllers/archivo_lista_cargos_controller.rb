class ArchivoListaCargosController < ApplicationController

  def new
    @lista_cargo = ListaCargo.find(params[:lista_cargo_id])
    @archivo_lista_cargo = ArchivoConformidadAdhesion.new
  end

  def create
    @lista_cargo = ListaCargo.find(params[:archivo_conformidad_adhesion][:lista_cargo_id])
    # @lista_cargo.archivo_conformidad_adhesion.try(:destroy)
    actualizar = @lista_cargo.archivo_conformidad_adhesion.present? ? 'actualizado' : 'agregado'
    @archivo_lista_cargo = ArchivoConformidadAdhesion.new(archivo_lista_cargo_params)

    respond_to do |format|
      if @archivo_lista_cargo.save
        format.json { render json: { mensaje: "Archivo de conformidad de adhesiones #{actualizar} correctamente." }, status: :ok }
      else
        format.json { render json: { mensaje: 'Error al agregar un archivo de conformidad de adhesiones.' }, status: :unprocessable_entity }
      end
    end
  end

  private

  def archivo_lista_cargo_params
    params.require(:archivo_conformidad_adhesion).permit(:archivo, :lista_cargo_id) if params[:archivo_conformidad_adhesion]
  end
end
