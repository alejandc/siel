class AntecedentesPenalesController < ApplicationController

  def index
    @eleccion = Eleccion.actual.paso
    @fuerzas = Fuerza.partidos_y_alianzas(@eleccion)
    @lista = (current_user.lista_partidaria?) ? current_user.lista : Lista.includes(lista_cargos: :cargo).find_by_id(params[:lista_id])

    # Cargamos la agrupación politica
    cargar_fuerza_cargos

    @antecedentes_penales = @lista.present? ? @lista.lista_cargos.map(&:candidaturas).flatten.map(&:persona).map(&:antecedente_penal).compact : []

    # Buscamos las listas solo si tenemos fuerza politica seleccionada
    if @fuerza
      # Si se aplico filtro por lista partidaria solo mostramos la indicada
      @listas = if @lista && @lista.fuerza_id == @fuerza.id
                  Kaminari.paginate_array(Array(@lista)).page(params[:page])
                else
                  Lista.por_eleccion(@eleccion).por_fuerza(@fuerza).includes(lista_cargos: :cargo).page(params[:page])
                end
    end

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  def enviar_solicitud
    @antecedente_penal = AntecedentePenal.find(params[:id])
    cargar_fuerza_cargos
    @lista = Lista.find(params[:lista_id])

    if @antecedente_penal.persona.valida_consulta_antecedentes_penales?
      if @antecedente_penal.contiene_certificado_valido?
        flash[:notice] = 'El precandidato ya posee certificado de reincidencia válido.'
      else
        AltaInformeAntecedentesPenales.perform_async(@antecedente_penal.id)
        @antecedente_penal.update_attributes(estado: :inicio_solicitud, fecha_inicio_solicitud: Time.now)
        flash[:notice] = 'Se ha enviado la consulta con éxito.'
      end
      
    else
      flash[:error] = 'No se ha enviado la consulta: al precandidato le faltan datos requeridos. Debe completarlo desde el formulario de la candidatura.'
    end
    
    redirect_to antecedentes_penales_index_path(fuerza_id: @fuerza.id, lista_id: @lista.id)
  end

  def descargar_antecedente_penal_pdf
    @antecedente_penal = AntecedentePenal.find(params[:id])

    # File.open('/home/alejandro/sarlanga.pdf', 'w:UTF-8:ASCII-8BIT') {|out| out.write(AntecedentePenal.first.archivo_pdf)}
    file = Tempfile.new(["certificado_reincidencia_#{@antecedente_penal.referencia_id}", '.pdf'])
    file.binmode
    file.write(@antecedente_penal.archivo_pdf)

    send_file(
      file,
      filename: "reincidencia_#{@antecedente_penal.persona.nombre_completo.downcase.gsub(' ', '_')}.pdf"
    )
  end
  

  private

  def cargar_fuerza_cargos
    # Seteamos la fuerza politica si el usuario es apoderado o lista partidaria
    @fuerza = current_user.externo? ? current_user.fuerza : Fuerza.find_by_id(params[:fuerza_id])
    @cargos = @eleccion.cargos_seleccionados.compact if @eleccion
  end

end
