class IncidenciasController < ApplicationController

  def index
    @filtro_incidencias = Incidencia.new(incidencia_params)

    @incidencias =  if incidencia_params
                      Incidencia.search_by_filter(@filtro_incidencias).page params[:page]
                    else
                      Incidencia.all.page params[:page]
                    end
  end

  def new
    @incidencia = Incidencia.new
  end

  def create
    @incidencias_invalidas = Incidencia.procesar_incidencias(params[:incidencia], params[:datos])

    if @incidencias_invalidas.empty?
      flash[:info] = "Incidencias cargadas exitosamente."
      redirect_to incidencias_path
    else
      @incidencia = Incidencia.new(params.require(:incidencia).permit(:tipo_incidencia, :eleccion_id))

      total_incidencias_cargadas = params[:datos].strip.split(/[\n\r]/).delete_if(&:blank?).size - @incidencias_invalidas.size
      if total_incidencias_cargadas > 0
        flash[:warning] = "#{total_incidencias_cargadas} Incidencias cargadas exitosamente. <br/><br/>" +
                          "#{@incidencias_invalidas.size} Incidencias inválidas: <br/>#{@incidencias_invalidas.join(' , ')}"
      else
        flash[:error] = "#{@incidencias_invalidas.size} Incidencia/s inválida/s: <br/>#{@incidencias_invalidas.join(' , ')}"
      end

      render 'new'
    end
  end

  def destroy
    @incidencia = Incidencia.find(params[:id])
    @incidencia.destroy

    redirect_to incidencias_path
  end

  def borrar_incidencias
    if params[:incidencia].blank?
      Incidencia.delete_all
    else
      @filtro_incidencias = Incidencia.new(incidencia_params)
      Incidencia.search_by_filter(@filtro_incidencias).delete_all
    end

    redirect_to incidencias_path(incidencia: params[:incidencia])
  end

  private

  def incidencia_params
    params.require(:incidencia).permit(:numero_documento, :tipo_incidencia, :eleccion_id) if params[:incidencia]
  end

end
