class EleccionCargosController < ApplicationController
  load_and_authorize_resource

  # GET /eleccion_cargos/1
  # GET /eleccion_cargos/1.json
  def show
    @eleccion_cargo = EleccionCargo.find(params[:id])
    @fuerzas = @eleccion_cargo.fuerzas.vigentes.uniq.sort { |a, b| b.cantidad_votos(@eleccion_cargo) <=> a.cantidad_votos(@eleccion_cargo) }
    @candidaturas = @eleccion_cargo.candidaturas.select(&:tiene_escanio?).sort { |a, b| b.cociente <=> a.cociente }
    @cargos = @eleccion_cargo.eleccion.cargos_seleccionados

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @eleccion_cargo }
    end
  end

end
