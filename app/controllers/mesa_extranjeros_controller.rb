class MesaExtranjerosController < ApplicationController
  authorize_resource

  def index
    @filtro_mesa_extranjero = MesaExtranjero.new(mesa_extranjero_params)
    @eleccion = (Eleccion.find_by(id: @filtro_mesa_extranjero[:eleccion_id]) || Eleccion.actual.paso.id)
    @mesa_extranjeros = MesaExtranjero.all.page params[:page]
  end

  def show
    @mesa_extranjero = MesaExtranjero.find(params[:id])
  end

  def importar_mesas_extranjeros

  end

  private

  def mesa_extranjero_params
    params[:mesa_extramjero].present? ? params.require(:mesa_extramjero).permit(:eleccionh_id, :comuna, :total_inscriptos, :numero_mesa) : {}
  end
end
