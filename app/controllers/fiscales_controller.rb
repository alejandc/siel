class FiscalesController < ApplicationController
  authorize_resource

  def index
    fiscal_params_aux = (fiscal_params[:eleccion_id].blank?) ? fiscal_params.merge(eleccion_id: Eleccion.actual.paso.id) : fiscal_params
    fiscal_params_aux = (current_user.externo?) ? fiscal_params_aux.merge(fuerza: current_user.fuerza) : fiscal_params_aux
    @filtro_fiscales = Fiscal.new(fiscal_params_aux)
    @eleccion = (current_user.externo?) ? current_user.eleccion : (Eleccion.find_by(id: @filtro_fiscales[:eleccion_id]) || Eleccion.actual.paso.id)
    @fuerza = (current_user.externo?) ? current_user.fuerza : Fuerza.find_by_id(@filtro_fiscales[:fuerza_id])

    @fiscales = if fiscal_params
                  Fiscal.search_by_filter(@filtro_fiscales).page params[:page]
                else
                  Fiscal.eleccion(@eleccion).page params[:page]
                end
  end

  def new
    respond_to do |format|
      format.html do
        if !current_user.externo? && params[:eleccion_id].blank? && params[:fuerza_id].blank?
          flash[:warning] = 'Debe seleccionar una eleccion y una fuerza para continuar'
          redirect_to fiscales_path
        else
          @eleccion = (current_user.externo?) ? current_user.eleccion : Eleccion.find(params[:eleccion_id])
          @fuerza = (current_user.externo?) ? current_user.fuerza : Fuerza.find(params[:fuerza_id])
          @fiscal = Fiscal.new(eleccion: @eleccion, fuerza: @fuerza)
          @fiscales = Fiscal.eleccion(@eleccion).fuerza(@fuerza)
          @persona = Persona.new
        end
      end
    end
  end

  def create
    @eleccion = Eleccion.find(params[:fiscal][:eleccion_id])
    @fuerza = Fuerza.find(params[:fiscal][:fuerza_id])
    @persona = Persona.new
    @fiscal = Fiscal.new(eleccion: @eleccion, fuerza: @fuerza)

    @esta_en_lista = !Fiscal.eleccion(@eleccion).fuerza(@fuerza).persona(params[:fiscal][:tipo_documento], params[:fiscal][:numero_documento], params[:fiscal][:sexo]).empty?

    if !@esta_en_lista
      @fiscal_agregar = Fiscal.new(params[:fiscal])
      if @fiscal_agregar.save
        @persona = Persona.new
        flash.now[:notice] = 'El fiscal ha sido agregado correctamente'
      else
        flash.now[:sweet_alert_error] = @fiscal_agregar.errors.full_messages.join(', ')
      end
    else
      @fiscal_agregar = Fiscal.new(params[:fiscal])
      flash.now[:sweet_alert_error] = 'El fiscal ya figura en la lista'
    end

    @fiscales = Fiscal.eleccion(@eleccion).fuerza(@fuerza)

    respond_to do |format|
      format.js { render 'fiscales/agregar_fiscal' }
    end
  end

  def destroy_new
    @fiscal = Fiscal.find(params[:id])
    @eleccion = @fiscal.eleccion
    @fuerza = @fiscal.fuerza
    @fiscal.destroy
    @fiscales = Fiscal.eleccion(@eleccion).fuerza(@fuerza)

    flash.now[:notice] = 'El fiscal ha sido eliminado correctamente'

    respond_to do |format|
      format.js { render 'fiscales/eliminar_fiscal' }
    end
  end

  def destroy
    @fiscal = Fiscal.find(params[:id])
    @fiscal.destroy

    redirect_to fiscales_path
  end

  def borrar_fiscales
    if params[:fiscales].present?
      Fiscal.delete_all(id: params[:fiscales].map(&:to_i))
    else
      Fiscal.delete_all
    end

    render js: "window.location.pathname='#{fiscales_path}'"
  end

  def importar_fiscales
    @eleccion = Eleccion.find(params[:eleccion_id])
    @fuerza = Fuerza.find(params[:fuerza_id])
    @archivo = params[:archivo]
    @limpiar_fiscales = params[:limpiar_fiscales] == 'true'

    @objetos_con_error = FiscalesExcel.importar(@eleccion, @fuerza, @archivo, @limpiar_fiscales)
    @fiscales = Fiscal.eleccion(@eleccion).fuerza(@fuerza)

    @path = FiscalesExcel.excel_con_errores(@objetos_con_error, @fuerza) unless @objetos_con_error.empty?
  end

  def archivo_errores
    @path = params[:path]
    archivo_con_errores = Spreadsheet.open(@path)
    data = StringIO.new
    archivo_con_errores.write(data)
    send_data data.string, filename: 'Fiscales con errores.xls', type:  'application/vnd.ms-excel'
    FileUtils.rm(@path)
  end

  def exportar
    @eleccion = Eleccion.find(params[:eleccion_id])
    @fuerza = Fuerza.find_by(id: params[:fuerza_id])
    data = StringIO.new
    FiscalesExcel.exportar(@eleccion, @fuerza).write(data)
    send_data data.string, filename: 'Fiscales.xls', type: 'application/vnd.ms-excel'
  end

  def establecimientos_por_comuna
    respond_to do |format|
      format.json { render json: { establecimientos: MesasComuna.instance.establecimientos_por_comuna(params[:numero_comuna].to_i) }, status: :ok }
    end
  end

  def cantidad_mesas_establecimiento
    respond_to do |format|
      format.json { render json: { mesas_establecimiento: MesasComuna.instance.mesas_por_establecimiento(params[:numero_comuna].to_i, params[:codigo_establecimiento].to_i) }, status: :ok }
    end
  end

  private

  def fiscal_params
    params[:fiscal].present? ? params.require(:fiscal).permit(:tipo_documento, :numero_documento, :sexo, :comuna,
                                                              :fuerza_id, :eleccion_id, :tipo_cd, :cargo_id,
                                                              :codigo_establecimiento, :comuna) : {}
  end

end
