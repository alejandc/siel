class ImportacionEscrutiniosController < ApplicationController
  # authorize_resource

  def index
    authorize! :manage, ImportacionEscrutinio

    @anios = [''] + Eleccion.select(:fecha).map { |e| e.fecha.year }.uniq
    @elecciones = Eleccion.order('fecha DESC, descripcion ASC')
    @importacion_escrutinio = ImportacionEscrutinio.importacion_activa.first
    @job_id = @importacion_escrutinio.try(:job_id)
  end

  def new
    authorize! :manage, ImportacionEscrutinio

    @eleccion_id = params[:eleccion_id]
    @importacion_escrutinio = ImportacionEscrutinio.new(eleccion_id: @eleccion_id)
  end

  def create
    authorize! :manage, ImportacionEscrutinio

    @formato_archivo_incorrecto = false

    if validar_formato_archivo_importacion(params[:archivo])
      ActiveRecord::Base.transaction do
        importacion_escrutinio_a_eliminar = ImportacionEscrutinio.where(eleccion_id: params[:eleccion_id]).last
        if importacion_escrutinio_a_eliminar.present?
          RenglonEscrutinio.where(importacion_escrutinio_id: importacion_escrutinio_a_eliminar.id).delete_all
          importacion_escrutinio_a_eliminar.destroy
        end
        @importacion_escrutinio = ImportacionEscrutinio.create(eleccion_id: params[:eleccion_id])
        guardar_archivo(params[:archivo], @importacion_escrutinio.path_archivo_importar)
      end

      @job_id = ImportacionEscrutiniosJob.perform_async(@importacion_escrutinio.id)
      @importacion_escrutinio.actualizar_importacion(@job_id)
    else
      @formato_archivo_incorrecto = true
    end

    respond_to do |format|
      format.js
    end
  end

  def estado_importacion_archivo
    @job_id = params[:job_id]
    @importacion_escrutinio = ImportacionEscrutinio.find_by(job_id: @job_id)

    @job_completado = Sidekiq::Status.complete?(@job_id)
    @job_progreso = Sidekiq::Status.at(@job_id)
    @job_error = Sidekiq::Status.failed?(@job_id)
    @job_estado = Sidekiq::Status.status(@job_id)

    if @job_completado
      @importacion_escrutinio.actualizar_importacion(nil)

      if File.exist?(@importacion_escrutinio.path_archivo_con_errores_importacion)
        flash.now[:sweet_alert_error] = 'La importación se completo con errores. Descargue el archivo de errores para ver los detalles.'
      else
        flash.now[:notice] = 'La importación se realizó satisfactoriamente.'
      end
    end

    respond_to do |format|
      format.js
    end
  end

  def show
    authorize! :manage, ImportacionEscrutinio

    @importacion_escrutinio = ImportacionEscrutinio.find(params[:id])
    @cargos_seleccionados_eleccion = @importacion_escrutinio.cargos_seleccionados_eleccion
    @partidos_con_totales = @importacion_escrutinio.partidos_con_totales
    @listas_en_escrutinio = @importacion_escrutinio.votos_positivos.select_partido
    @fuerzas_en_eleccion = Fuerza.partidos_y_alianzas(@importacion_escrutinio.eleccion).reject { |fuerza| fuerza.listas.por_eleccion(@importacion_escrutinio.eleccion).empty? }
  end

  def calcular_resultados
    authorize! :manage, ImportacionEscrutinio

    @importacion_escrutinio = ImportacionEscrutinio.find(params[:id])
    @fuerzas_en_eleccion = Fuerza.partidos_y_alianzas(@importacion_escrutinio.eleccion).reject { |fuerza| fuerza.listas.por_eleccion(@importacion_escrutinio.eleccion).empty? }

    if @fuerzas_en_eleccion.empty?
      @importacion_escrutinio.calcular_resultados!
      flash[:notice] = 'Se han calculado los resultados del escrutinio'
    else
      flash[:warning] = 'No se han recalculado los resultados debido a que hay listas sin asociar!'
    end
    
    redirect_to importacion_escrutinio_path(@importacion_escrutinio)
  end

  def destroy
    authorize! :manage, ImportacionEscrutinio

    @importacion_escrutinio = ImportacionEscrutinio.find(params[:id])
    @importacion_escrutinio.destroy

    flash[:notice] = 'Se han eliminado los datos del escrutinio'
    redirect_to importacion_escrutinios_path
  end

  def reparto_cargos
    @importacion_escrutinio = ImportacionEscrutinio.find(params[:id])
    @fuerza = Fuerza.find_by(id: params[:fuerza_id])
    @cargo = Cargo.find_by(id: params[:cargo_id])
    @fuerza_eleccion = @fuerza.fuerza_elecciones.eleccion(@importacion_escrutinio.eleccion).first if @fuerza

    @lista_candidaturas = if @importacion_escrutinio.eleccion.paso? && @fuerza.present? && @cargo.present?
      RepartoDHondt.lista_sugerida(@importacion_escrutinio, @fuerza, @cargo)
    elsif @cargo.present?
      RepartoDHondt.lista_ganadora(@importacion_escrutinio, @cargo)
    end
  end

  def generar_lista_eleccion_general
    @importacion_escrutinio = ImportacionEscrutinio.find(params[:id])
    @fuerza = Fuerza.find_by(id: params[:fuerza_id])
    @cargo = Cargo.find_by(id: params[:cargo_id])

    generacion_exitosa = RepartoDHondt.generar_listas_eleccion_general(@importacion_escrutinio, @fuerza, @cargo)

    if generacion_exitosa
      flash[:notice] = 'La lista ha sido generada correctamente'
    else
      flash[:error] = 'Ha habido un error'
    end

    redirect_to reparto_cargos_importacion_escrutinio_path(@importacion_escrutinio, fuerza_id: @fuerza.id, cargo_id: @cargo.id)
  end

  def imprimir_reparto_cargos
    respond_to do |format|
      format.pdf { send_reparto_cargos_pdf }
    end
  end


  private

  def reparto_cargos_pdf
    importacion_escrutinio = ImportacionEscrutinio.find(params[:id])
    fuerza = Fuerza.find_by(id: params[:fuerza_id])
    cargo = Cargo.find(params[:cargo_id])

    RepartoCargosPdf.new(importacion_escrutinio, fuerza, cargo)
  end
 
  def send_reparto_cargos_pdf
    send_file reparto_cargos_pdf.to_pdf,
      filename: reparto_cargos_pdf.filename,
      type: "application/pdf",
      disposition: "inline"
  end

  def validar_formato_archivo_importacion(archivo)
    archivo.content_type == 'text/csv' || archivo.content_type == 'text/plain'
  end

  def guardar_archivo(archivo_cargado, path_archivo_importar)
    File.open(path_archivo_importar, 'wb') do |file|
      file.write(archivo_cargado.read)
    end
  end

end
