class SielArchivosController < ApplicationController
  authorize_resource
  before_filter :setting_tab

  def index
    @siel_archivos = SielArchivo.all.group_by(&:archivo_type)
  end

  def create
    @siel_archivo = SielArchivo.new(siel_archivo_params)
    flash[:info] = @siel_archivo.save ? 'Archivo cargado en el sistema!' : 'Error al cargar archivo en el sistema!'
    redirect_to siel_archivos_path(tab: @siel_archivo.archivo_type)
  end

  def update
    @siel_archivo = SielArchivo.find(params[:id])

    if @siel_archivo.update_attributes(activo: true)
      SielArchivo.desactivar_archivos(@siel_archivo)
      flash[:info] = 'Archivo actualizado correctamente!'
    else
      flash[:info] = 'Hubo un error al actualizar el archivo en el sistema!'
    end

    redirect_to siel_archivos_path(tab: params[:tab])
  end

  def destroy
    @siel_archivo = SielArchivo.find(params[:id])
    @tab = @siel_archivo.archivo_type

    @siel_archivo.destroy
    redirect_to siel_archivos_path(tab: params[:tab])
  end

  private

  def setting_tab
    @tab = params[:tab] || 'conformidad_adhesiones'
  end

  def siel_archivo_params
    params.require(:siel_archivo).permit(:archivo, :activo, :archivo_type_cd, :archivo_type) if params[:siel_archivo]
  end

end
