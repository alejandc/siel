class ListaCargosController < ApplicationController
  authorize_resource
  before_filter :cargar_lista_cargo_y_validar
  before_filter :validar_carga_candidaturas_eleccion, except: [:show, :versiones, :exportar_versiones, :observaciones, :exportar_adhesiones, :descargar_exportacion_adhesiones, :imprimir_candidaturas]

  def show
    authorize! :manage, @lista_cargo
    @pagina_adhesiones = params[:pagina_adhesiones] || 1
    @pagina_versiones = params[:pagina_versiones] || 1
    @lista_filtrada = params[:lista_filtrada]
    @lista = @lista_cargo.lista
    @cargo = @lista_cargo.cargo
    @adhesiones_search = AdhesionesSearch.new(params.try(:[], :adhesiones_search) || {})
    # @versiones = PaperTrail::Version.versiones_lista_cargo(@lista_cargo.id).page(@pagina_versiones)

    @pagina_observaciones = 1
    @observacion = Observacion.new
    @observaciones_search = ObservacionesSearch.new
    @observaciones = Observacion.where(lista_cargo_id: @lista_cargo.id).page(1)

    authorize! :manage, @lista

    # Buscamos la elección, la fuerza politica y los cargos correspondientes
    @eleccion = @lista_cargo.eleccion
    cargar_fuerza_cargos

    @candidatura = Candidatura.new(lista_cargo: @lista_cargo, persona: Persona.new(archivos_persona: [ArchivoPersona.new]), cargo: @cargo)
    @adhesion = Adhesion.new(lista_cargo: @lista_cargo, persona: Persona.new(archivos_persona: [ArchivoPersona.new]))

    @cantidad_adhesiones = @lista_cargo.adhesiones.count

    @importar_precandidatos_id = @lista_cargo.importacion_job('Candidatura')
    @importar_adherentes_id = @lista_cargo.importacion_job('Adhesion')

    @adhesiones = @adhesiones_search.results.lista_cargo(@lista_cargo).orden_creacion.page(@pagina_adhesiones).includes(:persona)

    respond_to do |format|
      format.html do
        render :show
      end
    end
  end

  def importar_personas
    authorize! :manage, @lista_cargo
    raise "Importación inválida" unless params[:tipo].downcase =~ /adhesion|candidatura/

    @tipo_importacion = params[:tipo]
    @limpiar_cargo = params[:limpiar_cargo] == 'true'
    @formato_archivo_incorrecto = false

    if validar_formato_archivo_importacion(params[:archivo])
      guardar_archivo(params[:archivo], @lista_cargo.path_archivo_importar(@tipo_importacion))

      @job_id = if @tipo_importacion.eql? 'Adhesion'
                  ImportarAdherentesJob.perform_async(@lista_cargo.id, current_user.nombre_usuario, @limpiar_cargo)
                else
                  ImportarPrecandidatosJob.perform_async(@lista_cargo.id, current_user.nombre_usuario, @limpiar_cargo)
                end

      @lista_cargo.setear_importacion(@tipo_importacion, @job_id)
    else
      @formato_archivo_incorrecto = true
    end

    respond_to do |format|
      format.js
    end
  end

  def estado_importacion_archivo
    @pagina_adhesiones = params[:pagina_adhesiones] || 1
    @lista = @lista_cargo.lista
    @job_id = params[:job_id]
    @tab = params[:tab]

    @job_completado = Sidekiq::Status.complete?(@job_id)
    @job_progreso = Sidekiq::Status.at(@job_id)
    @job_error = Sidekiq::Status.failed?(@job_id)
    @job_estado = Sidekiq::Status.status(@job_id)

    if @job_completado
      @adhesiones_search = AdhesionesSearch.new(params.try(:[], :adhesiones_search) || {})
      @cantidad_adhesiones = @lista_cargo.adhesiones.count
      @adhesiones = @lista_cargo.adhesiones.orden_creacion.page(@pagina_adhesiones).includes(:persona) if @tab == 'adhesiones'

      if File.exist?(@lista_cargo.path_archivo_con_errores_importacion(((@tab == 'candidaturas') ? 'Candidatura' : 'Adhesion')))
        flash.now[:sweet_alert_warning] = 'La importación se completó. Existen registros con error. Descargue el archivo para ver los detalles'
      else
        flash.now[:notice] = 'La importación se realizó satisfactoriamente.'
      end
    elsif @job_error or @job_estado.nil?
      @lista_cargo.actualizar_importacion(((@tab == 'candidaturas') ? 'Candidatura' : 'Adhesion'))
    end

    respond_to do |format|
      format.js
    end
  end

  def descargar_archivo_errores_importacion
    nombre_archivo = if params[:modelo].eql? 'Adhesion'
                       "errores_importacion_adhesiones_#{@lista_cargo.cargo.nombre_snake_case}.xls"
                     else
                       "errores_importacion_precandidaturas_#{@lista_cargo.cargo.nombre_snake_case}.xls"
                     end

    send_file(
      @lista_cargo.path_archivo_con_errores_importacion(params[:modelo]),
      filename: I18n.transliterate(nombre_archivo)
    )
  end

  def borrar_candidaturas
    authorize! :manage, @lista_cargo
    @lista_cargo.borrar_candidaturas

    render json: { msg: 'Se borraron las candidaturas de la lista.' }, status: :ok
  end

  def borrar_adhesiones
    authorize! :manage, @lista_cargo
    @lista_cargo.borrar_adhesiones
    Adhesion.asignar_adhesiones_multiples_default!
    Adhesion.calculo_incremento_adhesiones_multiples!

    render json: { msg: 'Se borraron las adhesiones de la lista.' }, status: :ok
  end

  def imprimir_candidaturas
    @lista = @lista_cargo.lista
    data = StringIO.new
    excel = ListaPrototipoExcel.new(@lista_cargo.lista, @lista_cargo).imprimir
    excel.write(data)

    respond_to do |format|
      format.xls do
        send_data data.string, filename: I18n.transliterate("Candidatos - #{@lista.fuerza_nombre} - #{@lista.nombre} - #{@lista_cargo.cargo_nombre.gsub(Cargo::CARGOS[:diputado], 'Legisladores')}.xls"), type: 'application/xls'
      end
    end
  end

  def exportar_adhesiones
    @lista_cargo = ListaCargo.find(params[:id])
    TablaAdhesionesPdf.new(@lista_cargo)
    @path = @lista_cargo.path_exportacion_adhesiones

    respond_to do |format|
      format.js { render 'exportar_adhesiones' }
    end
  end

  def descargar_exportacion_adhesiones
    @lista_cargo = ListaCargo.find(params[:id])
    @path = params[:path]
    send_data File.read(@path), filename: I18n.transliterate("Adherentes - #{@lista_cargo.fuerza_nombre} - #{@lista_cargo.lista_nombre} - #{@lista_cargo.cargo_nombre.gsub(Cargo::CARGOS[:diputado], 'Legisladores')}.pdf"), type:  'application/pdf'
    FileUtils.rm(@path)
  end

  def validar
    authorize! :manage, @lista_cargo
    @tab = params[:tab]
    @errores, @alertas = @lista_cargo.send("validar_#{@tab}!")

    @titulo_alerta = I18n.t("activerecord.errors.models.lista_cargo.estados.#{@lista_cargo.send("estado_#{@tab}")}_titulo", categoria: I18n.t(@tab).capitalize)

    respond_to do |format|
      format.js
    end
  end

  def cerrar_lista
    authorize! :manage, @lista_cargo
    @tab = params[:tab]
    @lista_cargo.send("cerrar_#{@tab}!")

    @lista_cargo.errors.empty? ? flash[:notice] = "#{I18n.t(@tab).capitalize} han sido cerradas correctamente" : flash[:error] = @lista_cargo.errors[:cerrar_lista].try(:first)
    params_redirect = { controller: 'lista_cargos', action: 'show', id: @lista_cargo.id, lista_id: @lista_cargo.lista_id, tab: @tab }
    params_redirect[:origin] = 'cerrar_lista' unless @lista_cargo.send("estado_#{@tab}_cerrada?")

    if current_user.lista_partidaria? && @lista_cargo.lista_cerrada?
      Usuario.find(current_user.id).update_attribute(:habilitado, false)
      redirect_to admin_logout_path(logout_message: 'Ha completado la carga de la Lista. Su usuario quedo deshabilitado. Ante cualquier duda o reclamo puede dirigirse a la Mesa de Entradas Electoral, sita en Cerrito 760 Planta Baja, o comuníquese por teléfono al 4370-8500 interno 1903 o interno 777, o por email a siel@tsjbaires.gov.ar')
    else
      redirect_to params_redirect
    end
  end

  def abrir_lista
    authorize! :manage, @lista_cargo
    @tab = params[:tab]
    estado_apertura = params[:abierta_lista] == 'true' ? Lista::ESTADOS[:ABIERTA] : Lista::ESTADOS[:REABIERTA]
    @lista_cargo.send("abrir_#{@tab}!",  estado_apertura)
    flash[:notice] = "#{I18n.t(@tab).capitalize} han sido abiertas correctamente"

    redirect_to controller: 'lista_cargos', action: 'show', id: @lista_cargo.id, lista_id: @lista_cargo.lista_id, tab: @tab
  end

  def imprimir_auto_ordenamiento
    @ordenadas = @lista_cargo.candidaturas.sort
    Candidatura.sugerir_orden_cargo(@ordenadas)
    @ordenadas.sort!

    @originales = @lista_cargo.candidaturas.reload.sort

    respond_to do |format|
      format.pdf do
        send_data TablasComparativasPdf.new(@lista_cargo, @originales, @ordenadas).render, filename: I18n.transliterate("Auto Ordenamiento de Candidatos - #{@lista_cargo.lista_nombre} - #{@lista_cargo.cargo_nombre.gsub(Cargo::CARGOS[:diputado], 'Legisladores')}.pdf"), type: 'application/pdf'
      end
    end
  end

  def versiones
    @bitacora_search = BitacoraSearch.new(parametros_bitacora_search)
    @versiones = @bitacora_search.results
    @versiones_count = @versiones.count
    @versiones = @versiones.page(params[:pagina_versiones])
    @pagina = params[:pagina_versiones].to_i + 1

    respond_to do |format|
      format.js { render 'versiones' }
    end
  end

  def exportar_versiones
    @lista_cargo = ListaCargo.find(params[:id])
    @bitacora_search = BitacoraSearch.new(parametros_bitacora_search)
    @versiones = @bitacora_search.results
    @path = BitacoraExcel.exportar(@lista_cargo, @versiones)

    respond_to do |format|
      format.js { render 'exportar_versiones' }
    end
  end

  def descargar_exportacion_versiones
    @lista_cargo = ListaCargo.find(params[:id])
    @path = params[:path]
    archivo_con_errores = Spreadsheet.open(@path)
    data = StringIO.new
    archivo_con_errores.write(data)
    send_data data.string, filename: I18n.transliterate("Bitácora de #{@lista_cargo.fuerza_nombre} - #{@lista_cargo.lista_nombre} - #{@lista_cargo.cargo_nombre}.xls"), type:  'application/vnd.ms-excel'
    FileUtils.rm(@path)
  end

  def observaciones
    @pagina_observaciones = 1
    ids_candidaturas_con_observaciones = Observacion.where(lista_cargo_id: @lista_cargo.id).select('DISTINCT observable_id')
    @candidaturas_con_observaciones = Candidatura.where('id IN (?)', ids_candidaturas_con_observaciones).order('candidaturas.orden ASC').page(@pagina_observaciones)
    @observaciones = Observacion.join_candidaturas.lista_cargo_id(@lista_cargo.id).observables_ids(@candidaturas_con_observaciones.pluck(:id)).orden_posicion
    @observaciones_search = ObservacionesSearch.new

    respond_to do |format|
      format.js { render 'observaciones' }
    end
  end

  private

  def cargar_lista_cargo_y_validar
    if params[:id].present?
      @lista_cargo = ListaCargo.includes(:lista, :eleccion, :cargo).find(params[:id])

      if current_user.lista_partidaria? and current_user.lista_id != @lista_cargo.lista.id
        raise CanCan::AccessDenied.new("No esta autorizado a realizar esta operación!", :manage, ListaCargo)
      end

      if current_user.apoderado? and current_user.fuerza.id != @lista_cargo.lista.fuerza_id
        raise CanCan::AccessDenied.new("No esta autorizado a realizar esta operación!", :manage, ListaCargo)
      end
    end
  end

  def validar_carga_candidaturas_eleccion
    verificar_carga_candidaturas(@lista_cargo.eleccion)
  end

  def validar_formato_archivo_importacion(archivo)
    archivo.content_type =~ /vnd.ms-excel/ and archivo.original_filename.split('.').last.eql?('xls')
  end

  def guardar_archivo(archivo_cargado, path_archivo_importar)
    File.open(path_archivo_importar, 'wb') do |file|
      file.write(archivo_cargado.read)
    end
  end

  def cargar_fuerza_cargos
    # Seteamos la fuerza politica si el usuario es apoderado o lista partidaria
    @fuerza = current_user.externo? ? current_user.fuerza : @lista_cargo.fuerza
    @cargos = @eleccion.cargos_seleccionados.compact if @eleccion
  end

  def parametros_bitacora_search
    params[:bitacora_search].present? ? params[:bitacora_search] : { lista_cargo_eq: params[:id] }
  end
end
