class AdhesionesController < ApplicationController
  before_filter :validar_carga_candidaturas_eleccion, except: [:index]

  def index
    @pagina_adhesiones = params[:pagina_adhesiones] || 1
    @lista_cargo = ListaCargo.find(params[:lista_cargo_id])
    @cantidad_adhesiones = @lista_cargo.adhesiones.count
    @tab = 'adhesiones'
    @adhesiones_search = AdhesionesSearch.new(adhesiones_search_params)
    @adhesiones = @adhesiones_search.results.orden_creacion.page(@pagina_adhesiones).includes(:persona)
    @partial = @lista_cargo.estado_adhesiones_cerrada? ? 'adhesiones_cerrada' : 'adhesiones'

    respond_to do |format|
      format.js { render "lista_cargos/#{@partial}" }
    end
  end

  def create
    @pagina_adhesiones = 1
    # Cargar modelos para vaciar los formularios
    cargar_modelos_vaciar_formularios

    @esta_en_lista = @lista_cargo.posee_adherente?(@persona)

    if !@esta_en_lista
      @adhesion_agregar = @lista_cargo.agregar_adhesion(@persona)
      if @adhesion_agregar.save
        @adhesion_agregar.asignar_adhesiones_multiples!(al: :crear)
        @persona = Persona.new
        flash.now[:notice] = 'La adhesión ha sido agregada correctamente'
      else
        flash.now[:sweet_alert_error] = "La adhesión no se ha podido agregar:\n- #{@adhesion_agregar.errors.values.flatten.join("\n- ")}"
      end
    else
      flash.now[:sweet_alert_error] = 'El adherente ya figura en la lista'
    end

    @adhesiones_search = AdhesionesSearch.new({})

    @cantidad_adhesiones = @lista_cargo.adhesiones.count
    @adhesiones = @lista_cargo.adhesiones.orden_creacion.page(@pagina_adhesiones).includes(:persona)

    respond_to do |format|
      format.js { render 'lista_cargos/agregar_adhesion' }
    end
  end

  def destroy
    @pagina_adhesiones = (params[:pagina_adhesiones] || 1).to_i
    @tab = params[:tab]
    @adhesion = Adhesion.find(params[:id])
    @lista_cargo = @adhesion.lista_cargo
    @cargo = @adhesion.cargo

    Adhesion.transaction do
      @adhesion.asignar_adhesiones_multiples!(al: :eliminar)
      @adhesion.destroy
      flash.now[:notice] = 'La adhesion ha sido eliminada correctamente'
      @cantidad_adhesiones = @lista_cargo.adhesiones.count
    end

    @adhesiones_search = AdhesionesSearch.new(adhesiones_search_params)
    @adhesiones = @adhesiones_search.results.orden_creacion.page(@pagina_adhesiones).includes(:persona)

    if @cantidad_adhesiones > 0 && @adhesiones.empty?
      @pagina_adhesiones -= 1
      @adhesiones = @adhesiones_search.results.orden_creacion.page(@pagina_adhesiones).includes(:persona)
    end

    respond_to do |format|
      format.js { render 'lista_cargos/eliminar_adhesion' }
    end
  end

  private

  def cargar_modelos_vaciar_formularios
    @tab = params[:tab]
    @persona = Persona.find(params[:persona_id])
    @lista_cargo = ListaCargo.includes(:eleccion, :cargo).find(params[:adhesion][:lista_cargo_id])
    @eleccion = @lista_cargo.eleccion
    @cargo = @lista_cargo.cargo
    @adhesion = Adhesion.new(lista_cargo: @lista_cargo, persona: Persona.new)
  end

  def adhesiones_search_params
    search_params = params.try(:[], :adhesiones_search) || {}
    search_params.merge(lista_cargo_eq: @lista_cargo.id, fuerza_id: @lista_cargo.lista_fuerza_id, eleccion_id: @lista_cargo.lista_eleccion_id)
  end

  def validar_carga_candidaturas_eleccion
    @eleccion = if params[:id].present?
      Adhesion.includes(lista_cargo: :eleccion).find(params[:id]).lista_cargo.eleccion
    else
      Eleccion.find(params[:adhesion][:eleccion_id])
    end

    verificar_carga_candidaturas(@eleccion)
  end

end
