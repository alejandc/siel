class CasosVerificacionController < ApplicationController
  before_action :set_caso, only: [:edit, :update, :firmar, :no_firmar, :borrar_datos]

  ESTADOS = { cargados: 'cargados', a_cargar: 'a_cargar' }

  def index
    @verificacion = VerificacionFirmas.find(params[:verificacion_firmas_id])

    @casos = case params[:estado]
             when ESTADOS[:cargados] then @verificacion.casos_cargados.sort.reverse
             when ESTADOS[:a_cargar] then @verificacion.casos_a_cargar.sort.reverse
             else @verificacion.casos_verificacion.sort.reverse
             end
  end

  def edit
  end

  # PATCH /casos_verificacion/1/firmar
  def firmar
    @caso.firmado = true
    @caso.save
  end

  # PATCH /casos_verificacion/1/no_firmar
  def no_firmar
    @caso.firmado = false
    @caso.save
  end

  # PATCH /casos_verificacion/1/borrar_datos
  def borrar_datos
    @caso_id = @caso.id

    if @caso.fila_vacia
      @caso.destroy
      @caso = @caso.verificacion_firmas.proximo_caso_a_cargar
    else
      @duplicado = @caso.quitar_marca_duplicados
      @caso.borrar_datos_cargados
      @caso.save
    end
  end

  # PATCH/PUT /verificaciones_firmas/1/caso_verificacion/1.js
  def update
    if @caso.update(caso_verificacion_params)
      @caso.marcar_duplicado unless @caso.numero_documento.nil?
      @caso.buscar_en_padron unless @caso.ilegible || @caso.fila_vacia
      @caso.save
    else
      render :edit
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_caso
    @caso = CasoVerificacion.find(params[:id])
    @verificacion = @caso.verificacion_firmas
  end

  # Only allow a trusted parameter "white list" through.
  def caso_verificacion_params
    params.require(:caso_verificacion).permit(:numero_documento, :ilegible, :fila_vacia)
  end
end
