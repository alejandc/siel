﻿class CandidaturasController < ApplicationController
  load_and_authorize_resource
  skip_load_and_authorize_resource only: [:create]
  before_filter :validar_carga_candidaturas_eleccion, except: [:index, :buscar_por_documento, :cumple_ley_cupos, :cumple_minimo,
                                                           :filtrar, :eventos, :show, :new]

  # GET /candidaturas
  # GET /candidaturas.json
  def index
    @candidaturas_sin_paginar = (params[:filtro].present? ? Candidatura.filtrar(params[:filtro]) : Candidatura.none)
    @candidaturas = @candidaturas_sin_paginar.page(params[:page])

    @elecciones = [nil] + Eleccion.all
    @cargos = [nil] + Cargo.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @candidaturas }
    end
  end

  # FIXME: borraaarrrrr
  # POST /candidaturas/buscar_por_documento
  def buscar_por_documento
    @eleccion = Eleccion.find(params[:eleccion_id])

    numeros_documento = []

    numeros_documento += params[:numeros_documento].split("\n").map(&:to_i) unless params[:numeros_documento].nil?
    numeros_documento += File.read(params[:archivo].path).split("\n").map(&:to_i) unless params[:archivo].nil?

    if !numeros_documento.empty?
      @candidaturas = Candidatura.find_by_eleccion_documentos(@eleccion, numeros_documento)

      numeros_encontrados = @candidaturas.map { |c| c.persona.numero_documento }
      @numeros_no_encontrados = numeros_documento.reject { |num| numeros_encontrados.include?(num) }
    else
      redirect_to :carga_validaciones_candidaturas, flash: { error: I18n.t('messages.numero_documento_required') }
    end
  end

  # FIXME: borraaarrrrr
  # PUT /candidaturas/aplicar_validaciones
  def aplicar_validaciones
    @candidaturas = params[:candidaturas].present? ? params[:candidaturas].keys.map { |k| Candidatura.find(k) } : []

    @candidaturas.each do |c|
      c.send "#{params[:validacion]}=".to_sym, true
      c.save
    end
  end

  # GET /candidaturas/1/cumple_ley_cupos
  def cumple_ley_cupos
    candidatura = Candidatura.find(params[:id])
    fuerza = candidatura.fuerza
    eleccion = candidatura.eleccion_cargo.eleccion
    cargo = candidatura.eleccion_cargo.cargo

    candidaturas = fuerza.candidaturas_cargo(eleccion, cargo).sort

    respond_to do |format|
      format.json { render json: Candidatura.cumple_ley_cupos?(candidaturas) }
    end
  end

  # GET /candidaturas/1/cumple_minimo
  def cumple_minimo
    candidatura = Candidatura.find(params[:id])

    respond_to do |format|
      format.json { render json: candidatura.cumple_minimo? }
    end
  end

  # POST /candidaturas/filtrar
  def filtrar
    @candidaturas = Candidatura.filtrar(params)

    respond_to do |format|
      format.js
      format.xls
    end
  end

  # POST /candidaturas/oficializar_lista
  def oficializar_lista
    @eleccion = Eleccion.find(params[:eleccion_id])
    @fuerza = Fuerza.find(params[:fuerza_id])

    repartir_por_cargo(@fuerza.oficializar_lista(@eleccion))
  end

  # POST /candidaturas/desoficializar_lista
  def desoficializar_lista
    @eleccion = Eleccion.find(params[:eleccion_id])
    @fuerza = Fuerza.find(params[:fuerza_id])

    repartir_por_cargo(@fuerza.desoficializar_lista(@eleccion))

    render 'oficializar_lista'
  end

  # GET /candidaturas/importar/lista/:eleccion_id/:fuerza_id
  def importar_lista
    @elecciones = Eleccion.order('fecha DESC')
    @fuerzas = params[:eleccion_id].present? ? Fuerza.partidos_y_alianzas(Eleccion.find(params[:eleccion_id])).sort : []
    cargar_cargos_y_candidaturas
  end

  # POST /candidaturas/1/renunciar
  def renunciar
    candidatura = Candidatura.find(params[:id])
    candidatura.renunciar params[:fecha_renuncia]

    @candidaturas = candidatura.persona.candidaturas
  end

  def procesar_archivo
    if params[:eleccion].present? && params[:fuerza].present? && params[:archivo].present?
      eleccion = Eleccion.find(params[:eleccion])
      fuerza = Fuerza.find(params[:fuerza])

      s = Roo::Excel.new(params[:archivo].path, nil, :ignore)
      header = s.row(1)

      if header.eql?(%w(Cargo DNI Nombres Apellidos Sexo))
        rows = (2..s.last_row).each.inject([]) { |a, i| a << Hash[[header, s.row(i)].transpose] }

        Candidatura.importar(eleccion, fuerza, rows)

        repartir_por_cargo(fuerza.candidaturas_eleccion(eleccion))

        respond_to do |format|
          format.js { render 'redibujar_tablas' }
          format.json { head :no_content }
        end
      else
        flash[:error] = 'El formato del archivo es incorrecto'
        render 'pop_error'
      end
    else
      flash[:error] = I18n.t('messages.requeridos_importa_archivo')
      render 'pop_error'
    end
  end

  def cambiar_orden
    @candidatura = Candidatura.find(params[:id])
    logger.info "CAMBIAR ORDEN: CANDIDATURA #{@candidatura.id}"
    logger.info "ANTIGUO ORDEN #{@candidatura.orden}"
    logger.info "NUEVO ORDEN #{params[:orden]}"

    @persona = @candidatura.persona
    @lista_cargo = @candidatura.lista_cargo
    @lista = @lista_cargo.lista

    orden_cambiado = @candidatura.cambiar_orden!(params[:orden].to_i)

    if orden_cambiado
      @candidatura.lista_cargo_cambiar_estado_candidaturas_sin_validar!
      @candidatura.lista_cargo.save!
    end

    @primer_candidato = @lista_cargo.candidaturas.first

    @json = {
              status: orden_cambiado,
              primer_candidato_id: @primer_candidato.id,
              persona: {
                         id: @persona.id,
                         numero_documento: @persona.numero_documento,
                         tipo_documento: @persona.tipo_documento,
                         sexo: @persona.sexo
                       },
              cargo_id: @lista_cargo.cargo_id, eleccion_id: @lista.eleccion_id,
              fuerza_id: @lista.fuerza_id, lista_cargo_id: @lista_cargo.id, msg: 'Archivo persona borrado.'
            }

    respond_to do |format|
      format.json { render json: @json }
    end
  end

  # GET /candidaturas/eventos
  def eventos
    eleccion = Eleccion.find(params[:eleccion_id])
    fuerza = Fuerza.find(params[:fuerza_id])

    candidaturas = fuerza.candidaturas_eleccion(eleccion).sort

    updates = candidaturas.map do |c|
      versiones_persona = c.persona.versions.select do |v|
        v.event == 'update' && v.created_at > c.created_at && !v.changeset.select { |key, value| key != 'domicilio' && !(value[0].nil? && value[1] == false) }.empty?
      end
      versiones_persona + c.versions.select { |v| v.event == 'update' && v.created_at > c.created_at && !v.changeset.empty? }
    end

    deletes = Candidatura.destroyed(eleccion, fuerza)

    @versiones = (updates.flatten + deletes).sort { |v1, v2| v1.created_at <=> v2.created_at }

    respond_to do |format|
      format.js
    end
  end

  # POST /candidaturas/sugerir_orden
  def sugerir_orden
    @lista_cargo = ListaCargo.find(params[:lista_cargo_id])
    alerta_conflicto = "La lista NO cumple con el cupo. La lista de candidatos no puede incluir mas del setenta por ciento de personas del mismo sexo ni incluir a #{(@lista_cargo.cargo_nombre =~ /Comuna/) ? 'dos' : 'tres'} personas de un mismo sexo en orden consecutivo"
    @ordenadas = @lista_cargo.candidaturas.sort
    @originales = @lista_cargo.reload.candidaturas.sort

    if @lista_cargo.cumple_ley_cupos?
      Candidatura.sugerir_orden_cargo(@ordenadas)
      @ordenadas.sort!
      
      @incorregible = Candidatura.mismo_sexo_consecutivo(@ordenadas)

      @mantiene_ordenamiento = @originales == @ordenadas

      if @mantiene_ordenamiento && @incorregible.blank?
        flash.now[:sweet_alert_notice] = 'No hay conflictos con el cupo.'
      elsif @mantiene_ordenamiento && @incorregible.present?
        flash.now[:sweet_alert_error] = alerta_conflicto
      else
        @auto_ordenamiento_posible = true
      end
    else
      @cupo_corregido = Candidatura.corregir_cupo(@ordenadas)
      @mantiene_ordenamiento = true
    end

    respond_to do |format|
      if @lista_cargo.eleccion_paso?
        format.js { render 'lista_cargos/sugerir_orden' }
      else
        format.js { render 'lista_cargos_generales/sugerir_orden' }
      end
    end
  end

  def aceptar_correccion_cupo
    @lista_cargo = ListaCargo.find(params[:lista_cargo_id])
    @candidaturas = @lista_cargo.candidaturas.sort

    @candidaturas_cupo_corregido = Candidatura.corregir_cupo(@candidaturas)

    @lista_cargo.candidaturas.each do |candidatura|
      if @candidatura_cc = @candidaturas_cupo_corregido.detect {|cc| cc.persona.numero_documento == candidatura.persona.numero_documento}
        candidatura.update_attributes(orden: @candidatura_cc.orden)
      else
        candidatura.destroy
      end
    end

    respond_to do |format|
      if @lista_cargo.eleccion_paso?
        format.js { render 'lista_cargos/auto_ordenamiento' }
      else
        format.js { render 'lista_cargos_generales/auto_ordenamiento' }
      end
    end
  end

  # POST /candidaturas/auto_ordenamiento
  def auto_ordenamiento
    @lista_cargo = ListaCargo.find(params[:lista_cargo_id])

    @candidaturas = @lista_cargo.candidaturas.sort
    @ids_deshacer_ordenamiento = @candidaturas.map { |c| c.id.to_s }.join(',')

    Candidatura.auto_ordenamiento_cargo(@candidaturas)
    @candidaturas.sort!

    repartir_por_cargo(@candidaturas)

    @lista_cargo.reload
    @lista_cargo.validar_candidaturas!

    flash.now[:notice] = 'El ordenamiento sugerido ha sido aplicado correctamente.'

    respond_to do |format|
      if @lista_cargo.eleccion_paso?
        format.js { render 'lista_cargos/auto_ordenamiento' }
      else
        format.js { render 'lista_cargos_generales/auto_ordenamiento' }
      end
    end
  end

  # POST /candidaturas/deshacer_ordenamiento
  def deshacer_ordenamiento
    @eleccion = Eleccion.find(params[:eleccion_id])
    @fuerza = Fuerza.find(params[:fuerza_id])

    ordenadas = params[:ids_deshacer_ordenamiento].split(',').map { |id| Candidatura.find(id.to_i) }
    agrupadas = ordenadas.group_by { |c| c.eleccion_cargo.cargo.id }

    agrupadas.each_value do |candidaturas|
      candidaturas.each_with_index do |c, i|
        c.orden = i + 1
        c.save
      end
    end

    session["deshacer_ordenamiento_#{@eleccion.id}_#{@fuerza.id}"] = nil

    repartir_por_cargo(@fuerza.candidaturas_eleccion(@eleccion).sort)

    respond_to do |format|
      format.js { render 'redibujar_tablas' }
      format.json { head :no_content }
    end
  end

  # GET /candidaturas/1
  # GET /candidaturas/1.json
  def show
    @candidatura = Candidatura.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @candidatura }
      format.pdf do
        send_data @candidatura.constancia_aceptacion.render, filename: 'constancia_aceptacion_candidatura.pdf', type: 'application/pdf'
      end
    end
  end

  # GET /candidaturas/new
  # GET /candidaturas/new.json
  def new
    @candidatura = Candidatura.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @candidatura }
    end
  end

  # GET /candidaturas/1/edit
  def edit
    @candidatura = Candidatura.find(params[:id])
    @renglon = @candidatura.renglon_padron
  end

  # POST /candidaturas
  # POST /candidaturas.json
  def create
    # FIXME: Por este metodo pasa el update, corregir la funcionalidad para que pase por el action update...
    # Cargar modelos para vaciar los formularios
    cargar_modelos_vaciar_formularios

    if @persona.persisted?
      parametros_persona = parametros_permitidos_persona
      parametros_persona.except!(:tipo_documento, :numero_documento, :sexo)
      parametros_persona.except!(:nombres, :apellidos) if @persona.en_padron

      @persona.update_attributes(parametros_persona)

      if @candidatura.persisted? && params[:editar] == 'true'
        if @persona.valid?
          @candidatura_agregar = @candidatura
          @candidatura_agregar.paper_trail.without_versioning { @candidatura_agregar.update_attributes(updated_at: DateTime.now) }
          @persona = Persona.new
          @candidatura = Candidatura.new(eleccion: @eleccion, cargo: @cargo, lista_cargo: @lista_cargo, persona: @persona)
          flash.now[:notice] = 'La información del candidato ha sido actualizada correctamente'
        else
          @no_encontrado = true
          @editar = true
          @candidatura.persona = @persona
          flash.now[:sweet_alert_error] = 'La información del candidato no se ha podido actualizar: verifique los errores del formulario.'
        end
      else
        @candidatura_agregar = @lista_cargo.crear_candidatura(@persona)
        @candidatura_agregar.assign_attributes(lista_cargo_paso_id: @lista_cargo.id) if @lista_cargo.eleccion_general?

        if @candidatura_agregar.save
          @persona = Persona.new
          flash.now[:notice] = 'La candidatura ha sido agregada correctamente'
        else
          @candidatura = @candidatura_agregar

          if !@persona.errors.empty?
            @no_encontrado = true
            flash.now[:sweet_alert_error] = 'La candidatura no se ha podido agregar: verifique los errores del formulario.'
          else
            candidatura_agregada = @lista_cargo.candidaturas.persona(@persona).first
            candidatura_agregada.paper_trail.without_versioning { candidatura_agregada.update_attributes(updated_at: DateTime.now) } if candidatura_agregada.present?
            flash.now[:sweet_alert_error] = "La candidatura no se ha podido agregar:\n- #{@candidatura_agregar.errors.values.flatten.join("\n- ")}"
          end
        end
      end

    else
      @candidatura = Candidatura.new(eleccion: @eleccion, cargo: @cargo, lista_cargo: @lista_cargo, persona: @persona)
      @no_encontrado = true
    end

    respond_to do |format|
      if @lista_cargo.eleccion_paso?
        format.js { render 'lista_cargos/agregar_candidatura' }
      else
        format.js { render 'lista_cargos_generales/agregar_candidatura' }
      end
    end
  end

  # PUT /candidaturas/1
  # PUT /candidaturas/1.json
  def update
    @candidatura = Candidatura.find(params[:id])

    respond_to do |format|
      if @candidatura.update_attributes(params[:candidatura])
        format.html { redirect_to candidaturas_importar_lista_path(eleccion_id: session[:eleccion_importar_lista], fuerza_id: session[:fuerza_importar_lista]), notice: I18n.t('messages.saved') }
        format.json { head :no_content }
      else
        @renglon = @candidatura.renglon_padron

        format.html { render action: 'edit' }
        format.json { render json: @candidatura.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /candidaturas/1
  # DELETE /candidaturas/1.json
  # FIXME: Chequear por qué tarda tanto el borrado
  def destroy
    @tab = params[:tab]
    @candidatura = Candidatura.find(params[:id])
    @lista_cargo = @candidatura.lista_cargo

    @eleccion = @candidatura.eleccion
    @fuerza = @lista_cargo.fuerza

    if !@candidatura.oficializada?
      @candidatura.borrar
      flash.now[:notice] = 'La candidatura ha sido eliminada correctamente'
    else
      flash.now[:warning] = 'La candidatura no ha podido ser eliminada'
    end

    # repartir_por_cargo(@lista.candidaturas_eleccion.sort)

    respond_to do |format|
      format.html { flash[:notice] = 'La candidatura ha sido eliminada correctamente'; redirect_to listas_generales_lista_cargos_generales_path(@lista_cargo.lista, @lista_cargo) }
      format.js { render 'lista_cargos/eliminar_candidatura' }
      # format.json { head :no_content }
    end
  end

  private

  def cargar_cargos_y_candidaturas
    session[:eleccion_importar_lista] = params[:eleccion_id]
    session[:fuerza_importar_lista] = params[:fuerza_id]

    if params[:eleccion_id] and params[:fuerza_id]
      @eleccion = Eleccion.find(session[:eleccion_importar_lista])
      @fuerza = Fuerza.find(session[:fuerza_importar_lista])
      @cargos = Eleccion.find(params[:eleccion_id]).cargos_seleccionados.sort

      @candidaturas = @fuerza.candidaturas_eleccion(@eleccion).sort

      repartir_por_cargo(@candidaturas)
    else
      @cargos = []
    end
  end

  def validar_carga_candidaturas_eleccion
    @eleccion = if params[:eleccion_id].present?
                  Eleccion.find(params[:eleccion_id])
                elsif params[:id].present?
                  Candidatura.find(params[:id]).eleccion
                elsif params[:lista_cargo_id].present?
                  ListaCargo.find(params[:lista_cargo_id]).eleccion
                else
                  Eleccion.find(params[:candidatura][:eleccion_id])
                end

    verificar_carga_candidaturas(@eleccion)
  end

  def repartir_por_cargo(candidaturas)
    # FIXME: Chequear para que se usa
    # Candidatura.marcar_incumplimientos_cupo!(candidaturas)

    @jefaturas = candidaturas.select { |c| c.cargo.nombre == Cargo::CARGOS[:jefe] or c.cargo.nombre == Cargo::CARGOS[:vicejefe] }
    @diputados = candidaturas.select { |c| c.cargo.nombre == Cargo::CARGOS[:diputado] }
    @comuneros = candidaturas.select { |c| c.cargo.nombre == Cargo::CARGOS[:comunero] }
  end

  def cargar_modelos_vaciar_formularios
    @tab = params[:tab]
    @calles = Callejero.instance.calles
    @persona = params[:persona_id].present? ? Persona.find(params[:persona_id]) : Persona.create(parametros_permitidos_persona)
    @lista_cargo = ListaCargo.find(params[:candidatura][:lista_cargo_id])
    @eleccion = Eleccion.find(params[:candidatura][:eleccion_id])
    @cargo = @lista_cargo.cargo
    @candidatura = @lista_cargo.candidaturas.cargo(@cargo).persona(@persona).first || Candidatura.new(eleccion: @eleccion, cargo: @cargo, lista_cargo: @lista_cargo, persona: Persona.new)
    @editar = false
  end

  def parametros_permitidos_persona
    params.require(:candidatura).require(:persona_attributes)
          .permit(:tipo_documento, :numero_documento, :sexo, :nombres, :apellidos, :denominacion,
                  :fecha_nacimiento, :nacionalidad_argentina, :residencia,
                  archivos_persona_attributes: [:id, :descripcion, :_destroy, :archivo, :tipo_archivo],
                  domicilio_attributes: [:id, :calle_referencia, :calle_nombre, :numero, :piso, :depto, :comuna, :codigo_postal])
  end

end
