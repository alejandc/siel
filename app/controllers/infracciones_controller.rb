class InfraccionesController < ApplicationController
  load_and_authorize_resource

  # GET /infracciones
  # GET /infracciones.json
  def index
    @fuerza = Fuerza.find(params[:fuerza_id])
    @infraccion = Infraccion.new
    @infracciones = @fuerza.infracciones

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: [@fuerza, @infracciones] }
    end
  end

  # GET /infracciones/1
  # GET /infracciones/1.json
  def show
    @infraccion = Infraccion.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: [@fuerza, @infraccion] }
    end
  end

  # GET /infracciones/new
  # GET /infracciones/new.json
  def new
    @fuerza = Fuerza.find(params[:fuerza_id])
    @infraccion = Infraccion.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: [@fuerza, @infraccion] }
    end
  end

  # GET /infracciones/1/edit
  def edit
    @fuerza = Fuerza.find(params[:fuerza_id])
    @infraccion = Infraccion.find(params[:id])
  end

  # POST /infracciones
  # POST /infracciones.json
  def create
    @infraccion = Infraccion.new(params[:infraccion])

    respond_to do |format|
      if @infraccion.save
        @fuerza = Fuerza.find(params[:fuerza_id])
        @infracciones = @fuerza.infracciones

        format.html { redirect_to [@fuerza, @infraccion], notice: 'Infraccion was successfully created.' }
        format.json { render json: @infraccion, status: :created, location: @infraccion }
      else
        format.html { render action: 'new' }
        format.json { render json: @infraccion.errors, status: :unprocessable_entity }
      end
      format.js
    end
  end

  # PUT /infracciones/1
  # PUT /infracciones/1.json
  def update
    @infraccion = Infraccion.find(params[:id])

    respond_to do |format|
      if @infraccion.update_attributes(params[:infraccion])
        format.html { redirect_to fuerza_infracciones_url, notice: 'Infraccion was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @infraccion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /infracciones/1
  # DELETE /infracciones/1.json
  def destroy
    @infraccion = Infraccion.find(params[:id])
    @infraccion.destroy

    @fuerza = @infraccion.fuerza

    respond_to do |format|
      format.html { redirect_to fuerza_infracciones_url }
      format.json { head :no_content }
      format.js { @infracciones = @fuerza.infracciones }
    end
  end
end
