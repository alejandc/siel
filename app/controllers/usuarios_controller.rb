﻿class UsuariosController < ApplicationController
  load_and_authorize_resource
  skip_load_and_authorize_resource only: [:cambiar_contrasenia, :create, :update]
  before_filter -> { mantener_filtros usuario_params_aux }, only: [:index]

  # allow_access_to GRUPOS_ADMIN

  # GET /usuarios/find_by_email.json
  def find_by_email
    usuario = Usuario.find_by(email: params[:email])

    respond_to do |format|
      format.json { render json: usuario }
    end
  end

  # PUT /usuarios/1/blanqueo_clave
  # PUT /usuarios/1/blanqueo_clave.json
  def blanqueo_clave
    usuario = Usuario.find(params[:id])
    usuario.blanquear_clave

    respond_to do |format|
      if usuario.save
        # Avisar que se ha creado e informar credenciales
        UsuarioMailer.clave_blanqueada(usuario).deliver_now

        format.html { redirect_to usuarios_path, notice: 'La contraseña se blanqueó con éxito.' }
        format.json { head :no_content }
      else
        format.html { render action: 'index' }
        format.json { render json: usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /usuarios
  # GET /usuarios.json
  def index
    @filtro_usuarios = Usuario.new(obtener_filtros_guardados)
    @filtro_usuarios.limpiar_filtros if params[:borrar_filtros].present?
    @usuarios = Usuario.search_by_filter(@filtro_usuarios).page(params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @usuarios }
    end
  end

  # GET /usuarios/1
  # GET /usuarios/1.json
  def show
    @usuario = Usuario.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @usuario }
    end
  end

  # GET /usuarios/1/fuerza.json
  def fuerza
    fuerza = Usuario.find(params[:id]).fuerza

    respond_to do |format|
      format.json { render json: fuerza }
    end
  end

  # GET /usuarios/new
  # GET /usuarios/new.json
  def new
    @elecciones = Eleccion.por_tipo('paso')
    @eleccion = Eleccion.actual.paso
    @usuario = Usuario.new(perfiles: Usuario::PERFILES[3])
    @usuario.usuarios_fuerzas.build

    @fuerzas = Fuerza.partidos_y_alianzas(@eleccion)
    @perfiles = Usuario.perfiles_permitidos.to_a
    @listas = current_user.try(:fuerza).try(:listas)

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @usuario }
    end
  end

  # GET /usuarios/1/edit
  def edit
    @usuario = Usuario.find(params[:id])
    @elecciones = Eleccion.por_tipo('paso')
    @eleccion = @usuario.eleccion

    @fuerzas = Fuerza.partidos_y_alianzas(@eleccion)
    @perfiles = Usuario.perfiles_permitidos.to_a
    @listas = @usuario.fuerza.listas.por_eleccion(@eleccion)
  end

  # POST /usuarios
  # POST /usuarios.json
  def create
    authorize! :create, Usuario
    @usuario = Usuario.new(parametros_permitidos)

    respond_to do |format|
      if @usuario.save
        # Avisar que se ha creado e informar credenciales
        # UsuarioMailer.welcome_email(@usuario).deliver_now

        format.html { redirect_to usuarios_path, notice: I18n.t('messages.saved') }
        format.json { render json: @usuario, status: :created, location: @usuario }
      else
        @elecciones = Eleccion.por_tipo('paso')
        @eleccion = Eleccion.actual.paso.id
        @fuerzas = Fuerza.partidos_y_alianzas(@eleccion)
        @perfiles = Usuario.perfiles_permitidos.to_a
        @listas = current_user.try(:fuerza).try(:listas) || Lista.por_fuerza(parametros_permitidos.try(:[], :usuario).try(:[], :usuarios_fuerzas_attributes).try(:[], '0').try(:[], :fuerza_id))
        format.html { render action: 'new' }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /usuarios/1
  # PUT /usuarios/1.json
  def update
    authorize! :update, Usuario
    @usuario = Usuario.find(params[:id])

    respond_to do |format|
      if @usuario.update_attributes(parametros_permitidos)
        # Avisar que se modificó la información
        UsuarioMailer.updated(@usuario).deliver_now if @usuario.password_digest_changed?

        format.html { redirect_to usuarios_path, notice: I18n.t('messages.saved') }
        format.json { head :no_content }
      else
        @elecciones = Eleccion.por_tipo('paso')
        @eleccion = @usuario.eleccion
        @fuerzas = Fuerza.activos.vigentes
        @perfiles = Usuario.perfiles_permitidos.to_a
        @listas = @usuario.fuerza.listas.por_eleccion(@eleccion)
        format.html { render action: 'edit' }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /usuarios/1
  # DELETE /usuarios/1.json
  def destroy
    @usuario = Usuario.find(params[:id])
    @usuario.destroy

    respond_to do |format|
      format.html { redirect_to usuarios_url }
      format.json { head :no_content }
    end
  end

  def cambiar_contrasenia
    authorize! :cambiar_contrasenia, Usuario

    @usuario = Usuario.find(current_user.id)

    if request.post?
      permitted_params = params.require(:usuario).permit(:old_password, :new_password)
      if @usuario.cambiar_contrasenia!(permitted_params[:old_password], permitted_params[:new_password])
        flash[:notice] = I18n.t('messages.cambio_contrasenia_exitoso')
      end
    end
  end

  private

  def usuario_params_aux
    filtros = current_user.apoderado? ? usuario_params.merge('eleccion_id' => current_user.eleccion_id, 'fuerza_id' => current_user.fuerza.id, 'perfil' => Usuario::PERFILES[4]) : usuario_params
    filtros['eleccion_id'].blank? ? filtros.merge('eleccion_id' => Eleccion.actual.paso.id) : filtros
  end

  def parametros_permitidos
    if current_user.interno?
      parametros = params.require(:usuario).permit(:eleccion_id, :perfiles, :email, :habilitado, :nombre_usuario, :password, :password_confirmation, usuarios_fuerzas_attributes: [:id, :fuerza_id])
      parametros[:perfiles] == 'lista_partidaria' ? parametros.merge(lista_id: params[:usuario][:lista_id]) : parametros
    elsif current_user.apoderado?
      parametros = params.require(:usuario).permit(:email, :nombre_usuario, :password, :password_confirmation, :lista_id)
      params[:action] == 'update' ? parametros.merge(eleccion_id: current_user.eleccion_id, perfiles: 'lista_partidaria', usuarios_fuerzas_attributes: { '0' => { id: @usuario.usuarios_fuerzas.first.id, fuerza_id: current_user.fuerza.id } }) : parametros.merge(eleccion_id: current_user.eleccion_id, perfiles: 'lista_partidaria', usuarios_fuerzas_attributes: { '0' => { fuerza_id: current_user.fuerza.id } })
    end
  end

  def usuario_params
    params[:usuario].present? ? params.require(:usuario).permit(:eleccion_id, :habilitado, :nombre_usuario, :fuerza_id, :perfil) : {}
  end
end
