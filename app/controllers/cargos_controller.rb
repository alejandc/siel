﻿
class CargosController < ApplicationController

  # GET /cargos.json
  def index
    cargos = Cargo.all

    respond_to do |format|
      format.json { render json: cargos }
    end
  end

end
