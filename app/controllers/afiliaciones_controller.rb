class AfiliacionesController < ApplicationController
  authorize_resource
  before_filter -> { mantener_filtros(afiliacion_params[:eleccion_id].blank? ? afiliacion_params.merge(eleccion_id: Eleccion.actual.paso.id) : afiliacion_params) }, only: [:index]

  def index
    @filtro_afiliaciones = Afiliacion.new(obtener_filtros_guardados)
    
    @afiliaciones = unless afiliacion_params.empty?
                      Afiliacion.includes(:eleccion).search_by_filter(@filtro_afiliaciones).page params[:page]
                    else
                      @filtro_afiliaciones.limpiar_filtros
                      Afiliacion.includes(:eleccion).all.page params[:page]
                    end

    @fuerzas_hash = Fuerza.all.inject({}) {|h, f| h[f.numero] = f.nombre ; h }
  end

  def new
    @afiliacion = Afiliacion.new
  end

  def edit
    @afiliacion = Afiliacion.find(params[:id])
  end

  def create
    @afiliaciones_invalidas = Afiliacion.procesar_afiliaciones(params[:afiliacion], params[:datos])

    if @afiliaciones_invalidas.empty?
      flash[:info] = "Afiliacion/es cargadas exitosamente."
      redirect_to afiliaciones_path
    else
      @afiliacion = Afiliacion.new(params.require(:afiliacion).permit(:eleccion_id))
      

      total_afiliaciones_cargadas = params[:datos].strip.split(/[\n\r]/).delete_if(&:blank?).size - @afiliaciones_invalidas.size
      if total_afiliaciones_cargadas > 0
        flash[:warning] = "#{total_afiliaciones_cargadas} Afiliaciones cargadas exitosamente. <br/><br/>" +
                          "#{@afiliaciones_invalidas.size} Afiliaciones inválidas: <br/>#{@afiliaciones_invalidas.join(' , ')}"
      else
        flash[:error] = "#{@afiliaciones_invalidas.size} Afiliación/es inválidas: <br/>#{@afiliaciones_invalidas.join(' , ')}"
      end

      render 'new'
    end
  end

  def destroy
    @afiliacion = Afiliacion.find(params[:id])
    @afiliacion.destroy

    flash[:info] = "Afiliación eliminada exitosamente."
    redirect_to afiliaciones_path
  end

  def borrar_afiliaciones
    if params[:afiliaciones].present?
      Afiliacion.delete_all(id: params[:afiliaciones].map(&:to_i))
    else
      Afiliacion.delete_all
    end

    render js: "window.location.pathname='#{afiliaciones_path}'"
  end

  private

  def afiliacion_params
    params[:afiliacion].present? ? params.require(:afiliacion).permit(:numero_documento, :tipo_documento, :sexo, :eleccion_id, :fuerza_codigo) : {}
  end

end
