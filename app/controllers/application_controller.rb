require 'controller_authorizer'

class ApplicationController < ActionController::Base
  include ApplicationHelper

  include ControllerAuthorizer

  protect_from_forgery

  before_filter :authorize, except: [:login, :logout]
  before_filter :set_paper_trail_originator

  allow_base_access_to GRUPOS_ACCESO

  # Para obtener el host cuando se loguean los requests por consola desde lograge
  def append_info_to_payload(payload)
    super
    payload[:host] = request.host
  end

  def info_for_paper_trail
    if (controller_name == 'candidaturas' && action_name == 'cambiar_orden') || (controller_name == 'observaciones')
      {}
    else
      { lista_cargo_id: controller_name == 'lista_cargos' ? params[:id] : params.to_hash.valores_hash_multidimencional('lista_cargo_id').first }
    end
  end

  rescue_from CanCan::AccessDenied do
    uri = (current_user.interno?) ? :root : (current_user.apoderado?) ? listas_path : lista_path(current_user.lista)
    flash[:warning] = 'No tiene autorización de acceso'

    respond_to do |format|
      format.json { head :forbidden }
      format.html { redirect_to uri, error: 'No tiene autorización de acceso' }
    end
  end

  rescue_from ActiveRecord::RecordNotFound do
    respond_to do |format|
      format.js { head :not_found }
      format.json { head :not_found }
      format.html do
        render file: "#{Rails.root}/public/404.html", status: 404,
               alert: 'La página que esta buscando no existe...'
      end
    end
  end

  def set_paper_trail_originator
    PaperTrail.whodunnit = current_user.nombre_usuario if current_user.present?
  end

  def mantener_filtros(parametros_busqueda)
    session[controller_name] = (session[controller_name] || {}).merge(parametros_busqueda)
  end

  def obtener_filtros_guardados
    session[controller_name]
  end

  protected

  def authorize
    user_agent = request.user_agent

    if user_agent == EXTRANET_USER_AGENT
      token = request.env['HTTP_AUTHORIZATION'].split('=')[1]

      unless request.env['REQUEST_METHOD'] == 'GET'
        # Sólo se solicita el usuario de la sesión para los requests de tipo POST, PUT y DELETE (es decir, los que modifican datos)
        render(file: 'public/400', formats: [:html], status: :bad_request) if current_user.nil? || current_user.empty?
      end
      render(file: 'public/401', formats: [:html], status: :unauthorized) unless token == TOKEN_API
    else
      authorize_user_login
    end
  end

  def authorize_user_login
    begin
      unless session[:user] # A menos que esté logueado
        if session[:logout] # Si se deslogueó o no vino un usuario válido via NTLM, ir al login
          redirect_to controller: 'admin', action: 'login'
          return
        end
      end

      unless acceso_autorizado
        session[:user] = nil
        session[:user_groups] = nil
        # flash[:notice] = 'Disculpe, pero no lo han autorizado a ingresar al sistema.'

        redirect_to admin_login_url
      end
    rescue TsjSecurityError => e
      logger.error e
      flash[:notice] = I18n.t 'messages.ws_comm_problem'
      redirect_to admin_login_url
    end
  end

  def acceso_autorizado
    user_groups.present? && GRUPOS_ACCESO.detect { |grupo| current_user.es_del_grupo?(grupo) }
  end

  private

  def verificar_carga_candidaturas(eleccion)
    if current_user.externo? && !eleccion.habilitar_carga_candidaturas
      respond_to do |format|
        format.js do
          render json: 'No està habilitada la carga de candidaturas para la elecciòn', status: 403
        end

        format.json do
          render json: 'No està habilitada la carga de candidaturas para la elecciòn', status: 403
        end

        format.html do
          flash[:error] = 'No està habilitada la carga de candidaturas para la elecciòn'
          redirect_to request.referer
        end
      end
    end
  end

end
