class CallesController < ApplicationController

  def index
    search = params[:q]
    page = params[:page].try(:to_i) || 0
    total_calles = Callejero.instance.calles
    calles = total_calles.select { |calle| calle['nombre'].match(Regexp.new(search, Regexp::IGNORECASE)) }.slice(page * 30, page * 30 + 30) || []

    respond_to do |format|
      format.json { render json: { calles: calles, count: calles.size, total_count: total_calles.size } }
    end
  end

end
