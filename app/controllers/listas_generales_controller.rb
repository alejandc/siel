class ListasGeneralesController < ApplicationController
  def index
    authorize! :index, :lista_general

    @eleccion = params[:eleccion_id].present? ? Eleccion.find(params[:eleccion_id]).general : Eleccion.actual.general
    @fuerzas = Fuerza.partidos_y_alianzas(@eleccion)

    # Cargamos la agrupación politica
    cargar_fuerza_cargos

    # Buscamos las listas solo si tenemos fuerza politica seleccionada
    @listas = Lista.por_eleccion(@eleccion).por_fuerza(@fuerza).includes(lista_cargos: :cargo) if @fuerza.present?

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  private

  def cargar_fuerza_cargos
    # Seteamos la fuerza politica si el usuario es apoderado o lista partidaria
    @fuerza = current_user.externo? ? current_user.fuerza : Fuerza.find_by_id(params[:fuerza_id])
    @cargos = @eleccion.cargos_seleccionados.compact if @eleccion.present?
  end
end
