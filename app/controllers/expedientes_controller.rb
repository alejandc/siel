
require 'expediente'
require 'consulta_expedientes'

class ExpedientesController < ApplicationController
  load_and_authorize_resource

  def buscar
    @numero = params[:be_numero]
    @caratula = params[:be_caratula]
    consulta = ConsultaExpedientes.new

    @expedientes = Kaminari.paginate_array(Array(consulta.buscar(expeNro: @numero, caratula: @caratula))).page(params[:page])
  end

end
