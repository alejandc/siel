// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var receiveSortableList = null;

function submitPersonaForm(form_reference) {
  $("#" + form_reference).submit();
}

function validarArchivosImportacion(archivo) {
  var allowedExtension = ['xls'];
  var extName;
  var maxFileSize = $(archivo).data('max-file-size');
  var sizeExceeded = false;
  var extError = false;

  $.each(archivo.files, function() {
    if (this.size && maxFileSize && this.size > parseInt(maxFileSize)) {sizeExceeded=true;};
    extName = this.name.split('.').pop();
    if ($.inArray(extName, allowedExtension) == -1) {extError=true;};
  });

  if (sizeExceeded) {
    swal(
          'Archivo para Importar',
          'El archivo excede el máximo de tamaño permitidopara importar (5 MB)',
          'warning'
        )
    $(archivo).val('');
  };

  if (extError) {
    swal(
          'Archivo para Importar',
          'Solamente estan permitidos archivos con formato XLS.',
          'warning'
        )
    $(archivo).val('');
  };
}

function validarFormularioBusquedaPersona(form) {
  var invalid = false;
  var sexo = $("[name='persona[sexo]']", $(form)).val();
  var numero_documento = $("[name='persona[numero_documento]']", $(form)).val();
  var tipo_documento = $("[name='persona[tipo_documento]']", $(form)).val();

  if (sexo === '' || numero_documento === '' || tipo_documento === '') {
    swal(
          'Buscar Persona en Padrón',
          'Debe completar todos los campos para realizar la busqueda de la persona.',
          'warning'
        )
    invalid = true;
  }

  return invalid;
}

function eliminar(tab, url_path) {
  swal({
    title: '',
    text: 'Está seguro que desea eliminar las ' + tab + ' de la Lista?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Confirmar',
    cancelButtonText: 'Cancelar'
  }).then(function() {
    $.ajax({
      type: 'POST',
      url: url_path,
      contentType: "application/json",
      dataType: 'json',
      success: function(data, text) {
        if (tab == 'candidaturas') {
          $('#tabla_candidaturas_0 tbody > tr').remove();
          $('#tabla_candidaturas_1 tbody > tr').remove();
          $('.js-tabla-candidatos').closest('.col-md-12').html('<div class="alert alert-warning" role="alert">Lista de precandidaturas vacia.</div>');
          $('.js-formulario-domicilio').remove();
          $('.js-formulario-archivos').remove();
          $('#candidatura_persona_attributes_nombres').val('');
          $('#candidatura_persona_attributes_apellidos').val('');
          $('#candidatura_persona_attributes_denominacion').val('');
          $('#input_fecha_nacimiento').val('');
          $('#buscar_persona_candidaturas_form')[0].reset();
          $('#contador_titulares').html("0");
          $('#contador_suplentes').html("0");
          $('#candidaturas .js-boton-errores-importacion').remove();
          $("#candidaturas .js-imagen-candidato").remove();
          $(".js-seccion-notificaciones-candidaturas").html('<div class="alert alert-danger"><ul><li>Cargo pendiente de validación</li></ul></div>');
          $("h4.js-estado-candidaturas-lista-cargo").html('<i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="Lista Sin Validar"></i>');
          swal('', 'Las candidaturas han sido eliminadas correctamente', 'success');
        } else {
          $('#tabla_adhesiones tbody > tr').remove();
          $('#contador_adhesiones').html("0");
          $('#adhesiones .js-boton-errores-importacion').remove();
          $('#adhesiones ul.pagination').remove();
          $('#buscar_persona_adhesiones_form')[0].reset();
          $('#adhesion_persona_attributes_nombres').val("");
          $('#adhesion_persona_attributes_apellidos').val("");
          $('.js-formulario-domicilio').remove();
          $(".js-seccion-notificaciones-adhesiones").html('<div class="alert alert-danger"><ul><li>Adherentes pendientes de validación</li></ul></div>');
          $("h4.js-estado-adhesiones-lista-cargo").html('<i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="Lista Sin Validar"></i>');
          $(".js-calculo-afiliados").html("0 (0%)");
          $(".js-form-filtros-adhesiones").closest(".row").remove();
          $(".js-tabla-adhesiones").next().remove();
          $(".js-tabla-adhesiones").next().remove();
          $(".js-tabla-adhesiones").next().remove();
          $(".js-tabla-adhesiones").remove();
          $(".js-seccion-adhesiones .alert.alert-warning").remove();
          $(".js-seccion-adhesiones").append('<div class="alert alert-warning">Lista de adherentes vacia.</div>');
          swal('', 'Las adhesiones han sido eliminadas correctamente', 'success');
        }
      }
    });
  })
}

function inicializar_validaciones_inputs() {
  function cambiar_color(selector) {
    if($(selector).val()) {
      $(selector).closest(".form-group").find("label").removeClass('rojo');
    } else {
      $(selector).closest(".form-group").find("label").addClass('rojo');
    }
  }

  $.each(["#candidatura_persona_attributes_nombres", "#candidatura_persona_attributes_apellidos", 
  "#candidaturas #input_fecha_nacimiento", "#candidatura_persona_attributes_domicilio_completo"], function(index, value) {
    $(value).on("keyup", function() {
      cambiar_color(this);
    });
  });

  $("#candidaturas #input_fecha_nacimiento").on("change", function() {
    cambiar_color(this);
  });

  $(".js-domicilio-numero").on("keyup", function() {
    if($(this).val()) {
      $(".js-label-domicilio-numero").removeClass('rojo');
    } else {
      $(".js-label-domicilio-numero").addClass('rojo');
    }
  });

  if(!$(".js-domicilio-calle option:selected").val()) {
    $(".js-label-domicilio-calle").addClass('rojo');
  }

  $(".js-domicilio-calle").on("change", function() {
    if($(this).find("option:selected").val()) {
      $(".js-label-domicilio-calle").removeClass('rojo');
    } else {
      $(".js-label-domicilio-calle").addClass('rojo');
    }
  });

  // $.each([".js-radio-nacionalidad input[type=radio]", ".js-radio-residencia input[type=radio]"], function(index, value) {
  //   $(value).on("change", function() {
  //     $(this).closest(".form-group").find("label").removeClass('rojo');
  //   });
  // });
}

function inicializar_boton_editar_candidato() {
  $(".js-boton-editar-candidatura").on("click", function(e) {
    e.preventDefault();
    e.stopPropagation();
    $(this).parent().submit();
  });
}

function inicializar_barra_progreso(job_id, tab) {
  function actualizar_estado_job(job_id, tab) {
    var params = {}
    params.job_id = job_id
    params.tab = tab
    params.format = 'js'

    $.ajax({
      type: 'GET',
      url: window.location.pathname + "/estado_importacion_archivo",
      data: params,
      global: false,
      contentType: "text/javascript",
      error: function(request, status, error) {
        alert('Falló el actualizado de la barra de progreso en ' + tab);
      }
    });
  }

  if(job_id) {
    var  lista_cargo_id = location.pathname.match(/\d+/)[1];
    setTimeout(function() { actualizar_estado_job(job_id, tab, lista_cargo_id); }, 1750);
  }
};

function inicializar_carga_avatar() {
  $('#persona_avatar').on('change', function() {
    $('#js-form-avatar').submit();
  });

  $('.js-avatar-candidato').on('click', function(e) {
    $('#persona_avatar').click();
    e.preventDefault();
  });
}

function inicializar_limpiar_filtro_adhesiones() {
  $(".js-limpiar-filtros-adhesiones").on("click", function(event) {
    event.preventDefault();
    $("#adhesiones_search_numero_documento_like").val("");
    $("#adhesiones_search_nombre_like").val("");
    $("#adhesiones_search_apellido_like").val("");
    $("#adhesiones_search_adhesiones_observadas_eq").val("0");
    $("#adhesiones_search_adhesiones_afiliadas_eq").val("0");
    $(".js-form-filtros-adhesiones").submit();
  });
}

function inicializar_limpiar_filtro_bitacora() {
  $(".js-limpiar-filtros-bitacora").on("click", function(event) {
    event.preventDefault();
    $("#bitacora_search_nombre_usuario_like").val("");
    $("#bitacora_search_event_eq").val("");
    $("#bitacora_search_item_type_eq").val("");
    $("#bitacora_input_fecha_mayor").val("");
    $("#bitacora_input_fecha_menor").val("");
    $(".js-form-filtros-bitacora").submit();
  });
}

function inicializar_sortable() {
  var actualizar_candidato = function(candidatura_id, orden) {
    return $.ajax({
      type: "POST",
      url: "/candidaturas/cambiar_orden",
      data: { id: candidatura_id, orden: orden }
    });
  };

  var helper = function(e, tr) {
    var $originales = tr.children();
    var $helper = tr.clone();

    $helper.children().each(function(index) {
      $(this).width($originales.eq(index).width())
    });

    return $helper;
  };

  var actualizar = function(e, ui) {
    // Con el "if" hacemos que cuando mueve un item de una tabla a la otra, el update se ejecute una sola vez
    // https://stackoverflow.com/questions/3492828/jquery-sortable-connectwith-calls-the-update-method-twice

    if(e.target.id === ui.item.parent().attr("id")) {
      var candidatura = ui.item;
      var primer_candidato = ui.item.parent().find("tr").first();

      var nuevo_orden = ui.item.prevAll().length + 1;

      if(ui.item.parent().attr("id") == 'suplentes') {
        nuevo_orden += $("#titulares tr").length;
      }
      
      actualizar_candidato(candidatura.data("id"), nuevo_orden)
        .done(function(data) {
          var cantidad_titulares = $('tr', '#tabla_candidaturas_0').size();

          $('tbody tr', '#tabla_candidaturas_0').each(function(i) {
            $(this).find("td.orden").html(i + 1);
            $(this).data("orden", i + 1);
          });

          $('tbody tr', '#tabla_candidaturas_1').each(function(i) {
            $(this).find("td.orden").html(i + 1);
            $(this).data("orden", cantidad_titulares + i + 1);
          });

          // $('tr', '.js-tabla-candidatos').each(function (i) {
          //   $(this).find("td.orden").html(i + 1);
          //   $(this).data("orden", i + 1);
          // });

          $("h4.js-estado-candidaturas-lista-cargo").html('<i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="Lista Sin Validar"></i>');

          $("i.js-observacion-" + data.tipo_archivo).closest("form").remove();

          if(data.primer_candidato_observado) {
            $(".js-tabla-candidatos tr[data-id=" + data.primer_candidato_id + "] .js-observaciones").append($(crearIconoObservacion(data)));
          }
          
          inicializar_tooltips();
        })
        .fail(function() {
          if(receiveSortableList !== null) {
            if(receiveSortableList) {
              $('#titulares tr:last').prependTo('#suplentes');
            } else {
              $('#suplentes tr:first').appendTo('#titulares');
            }

            receiveSortableList = null;
          }

          $(".js-tabla-candidatos tbody").sortable("cancel");
          crear_alerta_flash('danger', 'Ha ocurrido un error, intentelo de nuevo');
        });
    }
  };

  $(".js-tabla-candidatos tbody").sortable({
    helper: helper,
    update: actualizar,
    connectWith: ".js-tabla-candidatos tbody",
    receive: function(event, ui) {
      receiveSortableList = ui.sender.attr('id') == 'titulares';

      if (receiveSortableList) {
        $('#suplentes tr:first').appendTo('#titulares');
      } else {
        $('#titulares tr:last').prependTo('#suplentes');
      }
    }
  }).disableSelection();
}

function boton_eliminar(event, lista_cargo_id, tipo, id, pagina_adhesiones, adhesiones_search) {
  event.preventDefault();
  var tab = (tipo === "candidatura" ? "candidaturas" : "adhesiones");

  swal({
    title: '',
    text: 'Está seguro que desea eliminar la ' + tipo + ' de la Lista?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Confirmar',
    cancelButtonText: 'Cancelar'
  }).then(function() {
    $.ajax({
      type: 'DELETE',
      url: "/" + tab + "/" + id,
      data: { lista_cargo_id: lista_cargo_id, tab: tab, pagina_adhesiones: pagina_adhesiones, adhesiones_search: adhesiones_search, format: 'js' }
    });
  });
}

function inicializar_domicilio_numero_rango() {
  function limpiar_numero_y_comuna() {
    $(".js-domicilio-numero").val("");
    $("#candidaturas .js-domicilio-comuna").val("");
  }

  function calcular_comuna() {
    if($(".js-domicilio-numero").val()) {
      var params = {calle_id: $(".js-domicilio-calle option:selected").data().data.id, altura: $(".js-domicilio-numero").val()}

      $.ajax({
        type: 'GET',
        url: "/personas/calcular_comuna",
        data: params,
        contentType: "application/json",
        success: function(data, text) {
          $("#candidaturas .js-domicilio-comuna").val(data.comuna);
        },
        error: function(request, status, error) {
          swal('', 'No es posible identificar la comuna en base al domicilio ingresado', 'warning');
          $("#candidaturas .js-domicilio-comuna").val("");
        }
      });
    }
  }

  function cambiar_limites_altura(altura_minima, altura_maxima) {
    $(".js-limites-altura").html(altura_minima + " - " + altura_maxima);
  }

  $(".js-domicilio-numero").on("change", function() {
    calcular_comuna();
  });

  $(".js-domicilio-calle").on('select2:select', function (evt) {
    cambiar_limites_altura(evt.params.data.altura_minima, evt.params.data.altura_maxima);
    limpiar_numero_y_comuna();
  });
}

function inicializar_select_domicilio() {
  function formatear_resultados(resultado) {
    if(resultado.altura_minima && resultado.altura_maxima) {
      return $('<option value="' + resultado.id + '" data-altura-minima="' + resultado.altura_minima + '" data-altura-maxima="' + resultado.altura_maxima + '">' + resultado.nombre + '</option>');
    } else {
      return $('<option>' + resultado.text + '</option>');
    }
  }

  $(".js-domicilio-calle").select2({
    ajax: {
      url: "/calles",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params.term,
          page: params.page
        };
      },
      processResults: function (data, params) {
        // parse the results into the format expected by Select2
        // since we are using custom formatting functions we do not need to
        // alter the remote JSON data, except to indicate that infinite
        // scrolling can be used
        params.page = params.page || 0;

        return {
          results: data.calles,
          pagination: {
            more: (params.page * 30) < data.total_count
          }
        };
      },
      cache: true
    },
    escapeMarkup: function (markup) {
      return markup;
    },
    minimumInputLength: 3,
    templateResult: function(data) {
      return data.nombre;
    },
    templateSelection: formatear_resultados
  });
}

function inicializar_tab_versiones() {
  $(".js-boton-tab-bitacora").on("click", function() {
    var params = {};
    params.pagina_versiones = 1;
    params.format = 'js';
    params.bitacora_search = {};

    $.ajax({
      type: 'GET',
      url: window.location.pathname + "/versiones",
      data: params,
      contentType: "text/javascript",
      error: function(request, status, error) {
        alert('Falló el actualizado de la tab de bitácora');
      }
    });
  });
}

function inicializar_datepickers_versiones() {
  if($(".js-fecha-version-gteq").length > 0 && $(".js-fecha-version-lteq").length > 0) {
    var fechaDesde = $(".js-fecha-version-gteq").val().split("/");
    var fechaHasta = $(".js-fecha-version-lteq").val().split("/");
    var maximoDesde = fechaHasta ? new Date(parseInt(fechaHasta[2]), parseInt(fechaHasta[1]) - 1, parseInt(fechaHasta[0])) : null;
    var minimoHasta = fechaDesde ? new Date(parseInt(fechaDesde[2]), parseInt(fechaDesde[1]) - 1, parseInt(fechaDesde[0])) : null;

    $('.js-fecha-version-gteq').datepicker({
        dateFormat: 'dd/mm/yyyy',
        language: 'es',
        maxDate: maximoDesde
    }).on("change", function() {
      $(this).datepicker("hide");

      $(".js-fecha-version-lteq").datepicker('destroy');

      var fechaDesde = $(".js-fecha-version-gteq").val().split("/");
      var minimoHasta = fechaDesde ? new Date(parseInt(fechaDesde[2]), parseInt(fechaDesde[1]) - 1, parseInt(fechaDesde[0])) : null;

      $(".js-fecha-version-lteq").datepicker({
        dateFormat: 'dd/mm/yyyy',
        language: 'es',
        minDate: minimoHasta
      })
    });

    $(".js-fecha-version-gteq").inputmask("date");

    $(".js-fecha-version-lteq").datepicker({
        dateFormat: 'dd/mm/yyyy',
        language: 'es',
        minDate: minimoHasta
    }).on("change", function() {
      $(this).datepicker("hide");

      $(".js-fecha-version-gteq").datepicker('destroy');

      var fechaHasta = $(".js-fecha-version-lteq").val().split("/");
      var maximoDesde = fechaHasta ? new Date(parseInt(fechaHasta[2]), parseInt(fechaHasta[1]) - 1, parseInt(fechaHasta[0])) : null;

      $(".js-fecha-version-gteq").datepicker({
        dateFormat: 'dd/mm/yyyy',
        language: 'es',
        maxDate: maximoDesde
      })
    });

    $(".js-fecha-version-lteq").inputmask("date");
  }
}

function inicializar_formulario_observaciones() {
  $(".js-subsanar-observacion").each(function(index) {
    var that = this;
    var accion = $(this).data("estado") == "subsanada" ? "SUBSANADA" : "NO SUBSANADA";

    $(this).on("click", function() {
      $(that).blur();
      swal({
        title: "Está por marcar la observación como " + accion + ". Desea continuar?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar'
      }).then(function () {
        var data = { id: $(that).closest("td").data("id"), estado: $(that).data("estado") };

        $.ajax({
          type: 'POST',
          url: "/observaciones/subsanar",
          data: JSON.stringify(data),
          global: false,
          contentType: "application/json",
          success: function(data, text) {
            var tds = $("#observaciones table td[data-id='" + data.id + "']");
            tds.removeClass("warning");
            tds.removeClass("success");
            tds.removeClass("danger");
            tds.addClass(data.color_estado);
          },
          error: function(request, status, error) {
            swal('', 'No es posible cambiar el estado de la observacion', 'warning');
          }
        });
      });
    });
  });

  $(".js-limpiar-filtros-observaciones").on("click", function(event) {
    event.preventDefault();
    $("#observaciones_search_nombre_candidato_like").val("");
    $("#observaciones_search_referencia_observacion_eq").val("");
    $("#observaciones_search_subsanado_eq").val("");
    $(".js-form-filtros-observaciones").submit();
  });
}

function inicializar_tab_observaciones() {
  $(".js-boton-tab-observaciones").on("click", function() {
    var params = {}
    params.pagina_versiones = 1;
    params.format = 'js'
    params.page = 1;

    $.ajax({
      type: 'GET',
      url: window.location.pathname + "/observaciones",
      data: params,
      contentType: "text/javascript",
      success: function() {
        inicializar_popover();
      },
      error: function(request, status, error) {
        alert('Falló el actualizado de la tab de observaciones');
      }
    });
  });
}

function expandir_seccion(tab) {
  $(".js-seccion-notificaciones-" + tab + " #js-seccion-observaciones ul").show();
  $(".js-seccion-notificaciones-" + tab + " #js-seccion-observaciones ul").animate({height: String($(".js-seccion-notificaciones-" + tab + " #js-seccion-observaciones ul li").length * 24)}, 500);
  $(".js-seccion-notificaciones-" + tab + " #js-seccion-observaciones .fa-plus-square-o").hide();
  $(".js-seccion-notificaciones-" + tab + " #js-seccion-observaciones .fa-minus-square-o").show();
}

function contraer_seccion(tab) {
  $(".js-seccion-notificaciones-" + tab + " #js-seccion-observaciones ul").animate({height: '0'}, 500, function() { $(".js-seccion-notificaciones-" + tab + " #js-seccion-observaciones ul").hide(); });
  $(".js-seccion-notificaciones-" + tab + " #js-seccion-observaciones .fa-minus-square-o").hide();
  $(".js-seccion-notificaciones-" + tab + " #js-seccion-observaciones .fa-plus-square-o").show();
}

function inicializar_seccion_tab(contraer_al_inicializar, tab) {
  $(".js-seccion-notificaciones-" + tab + " #js-seccion-observaciones").on("click", function() {
    var that = this;

    if($(".js-seccion-notificaciones-" + tab + " #js-seccion-observaciones ul:hidden").length > 0) {
      expandir_seccion(tab);
    } else {
      contraer_seccion(tab);
    }
  });

  if(contraer_al_inicializar) {
    setTimeout(function() { contraer_seccion(tab) }, 1500);
  }
}

function inicializar_seccion_observaciones(contraer_al_inicializar) {
  inicializar_seccion_tab(contraer_al_inicializar, 'candidaturas');
  inicializar_seccion_tab(contraer_al_inicializar, 'adhesiones');
}

$(document).ready(function() {
  $('#input_fecha_nacimiento').datepicker({
    format: 'dd/mm/yyyy',
  });

  $('#cargo_id').on('change', function() {
    var lista_cargo_id = $(this).find("option:selected").val();
    window.location = window.location.pathname.replace(/\d+$/, lista_cargo_id);
  })

  inicializar_select_domicilio();
  inicializar_domicilio_numero_rango();
  inicializar_validaciones_inputs();
  inicializar_boton_editar_candidato();
  inicializar_barra_progreso($("#candidaturas .progress-bar").data("jobId"), 'candidaturas');
  inicializar_barra_progreso($("#adhesiones .progress-bar").data("jobId"), 'adhesiones');
  inicializar_limpiar_filtro_adhesiones();
  inicializar_limpiar_filtro_bitacora();
  inicializar_tab_versiones();
  inicializar_datepickers_versiones();
  inicializar_tab_observaciones();
  inicializar_seccion_observaciones(true);

  inicializar_sortable();
})
