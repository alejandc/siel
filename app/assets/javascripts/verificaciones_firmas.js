var campos = ['cantidad_firmas_total', 'confianza', 'error', 'variable_p'];
var bloque_establecido = false;

var actualizar_formulas = function () {
	console.log('Armando AJAX request para actualizar las fórmulas.');

	var datos = {};

	for (var i = 0; i < campos.length; i++) {
		datos['verificacion_firmas['+ campos[i] +']'] = $('#verif_'+ campos[i]).val();
	}
	if (bloque_establecido) {
		datos['verificacion_firmas[bloque_muestreo_sistematico]'] = $('#verif_bloque_muestreo_sistematico').val();
	}
	console.log("datos a enviar: "+ JSON.stringify(datos));

	$.ajax({
		url: '/verificaciones_firmas/actualizar',
		type: 'PATCH',
		global: false, // Para que no muestre la barra azul de Busy AJAX
		data: datos
	});
};

var habilitar_btn_agregar = function() {
	$('#btn_agregar_caso').prop('disabled', $('#caso_numero_documento').val().length === 0);
};

var init_form_caso = function() {

	$('#btn_agregar_caso').on('click', function() {
		$('#form_caso_verificacion').submit();
	});
	$('#btn_caso_fila_vacia').on('click', function() {
		$('#caso_verificacion_fila_vacia').val('true');
		$('#form_caso_verificacion').submit();
	});
	$('#btn_caso_ilegible').on('click', function() {
		$('#caso_verificacion_ilegible').val('true');
		$('#form_caso_verificacion').submit();
	});

	// Habilitar solamente si se escribió algo en el DNI.
	$('#caso_numero_documento').change(habilitar_btn_agregar);
	$('#caso_numero_documento').keyup(habilitar_btn_agregar);

	habilitar_btn_agregar();

	// Hacer foco en el input de DNI
	$('#caso_numero_documento').focus();
};

var firmar = function(id) {
	$.ajax({
		url: '/casos_verificacion/'+id+'/firmar',
		type: 'PATCH'
	});
};

var no_firmar = function(id) {
	$.ajax({
		url: '/casos_verificacion/'+id+'/no_firmar',
		type: 'PATCH'
	});
};

var borrar_caso_verificacion = function(id) {
	$.ajax({
		url: '/casos_verificacion/'+id+'/borrar_datos',
		type: 'PATCH'
	});
};

$(document).ready(function() {
	$('#verif_bloque_muestreo_sistematico').on('keyup change', function () {
		var valor = $('#verif_bloque_muestreo_sistematico').val();
		bloque_establecido = (valor !== null && valor !== '');
	});
	$('#btn_grabar_verificacion').on('click', function() {
		$('#form_verificacion').submit();
	});
	for (var i = 0; i < campos.length; i++) {
		$('#verif_'+ campos[i]).on('keyup change', actualizar_formulas);
	}
});
