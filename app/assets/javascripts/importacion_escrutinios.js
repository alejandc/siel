function inicializar_barra_progreso_importacion_escrutinio(job_id) {
  function actualizar_estado_job(job_id) {
    var params = {};
    params.job_id = job_id;
    params.format = 'js';

    $.ajax({
      type: 'GET',
      url: window.location.pathname + "/estado_importacion_archivo",
      data: params,
      global: false,
      contentType: "text/javascript",
      error: function(request, status, error) {
        alert('Falló el actualizado de la barra de progreso');
      }
    });
  }

  if(job_id) {
    setTimeout(function() { actualizar_estado_job(job_id); }, 1750);
  }
};

function inicializar_mascara_umbral_escrutinio() {
  $(".js-umbral-escrutinio").inputmask('9{0,2},9{0,1}');
}

function inicializar_separador_umbral() {
  function calcular_separador_umbral() {
    var porcentajeCalculado = parseFloat($(".js-umbral-escrutinio").val().replace(",", "."));

    $(".borde-tabla-umbral").each(function() {
      $(this).removeClass("borde-tabla-umbral");
    });

    $(".js-tabla-umbral tr").each(function() {
      if($(this).data("porcentaje") < porcentajeCalculado) {
        $(this).find("td").addClass("borde-tabla-umbral");
        return false;
      }
    });
  }

  $(".js-umbral-escrutinio").on("keyup", function() {
    setTimeout(calcular_separador_umbral, 500);
  });

  if($(".js-umbral-escrutinio").length > 0) {
    calcular_separador_umbral();
  }
}

function validarArchivosImportacionEscrutinio(archivo) {
  var allowedExtension = ['xlsx'];
  var extName;
  var extError = false;

  $.each(archivo.files, function() {
    extName = this.name.split('.').pop();
    if ($.inArray(extName, allowedExtension) == -1) {extError=true;};
  });

  if (extError) {
    swal(
          'Archivo para Importar',
          'Solamente estan permitidos archivos con formato XLSX.',
          'warning'
        )
    $(archivo).val('');
  };
}

function inicializar_input_umbral() {
  $('.js-umbral-escrutinio').bind("ajax:success", function () { location.reload() });
}

function inicializar_best_in_place_select2() {
  $('body').on('best_in_place:activate', '.best_in_place', function() {
    if($('.js-input-umbral-escrutinio').length > 0) {
      $(".js-input-umbral-escrutinio").val().replace(".", ",");
      $(".js-input-umbral-escrutinio").inputmask('9{0,2},9{0,1}');
    } else {
      $(".select2").select2().unbind('blur').bind('blur', {editor: $(this)}, BestInPlaceEditor.forms.select.blurHandler);
    }
    
  })
}

$(document).ready(function() {
  inicializar_barra_progreso_importacion_escrutinio($("#js-importacion-escrutinio").data("jobId"));
  inicializar_mascara_umbral_escrutinio();
  inicializar_separador_umbral();
  inicializar_input_umbral();
  inicializar_best_in_place_select2();
});