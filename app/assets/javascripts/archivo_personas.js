function existeTipoArchivo(tipo_archivo) {
  var  existeTipoArchivo = null;

  $('#tabla-archivos-persona tr').each(function() {
    if(tipo_archivo !== 'subsana_observacion' && $(this).data('tipo-archivo') === tipo_archivo) {
      existeTipoArchivo = {'referencia': $(this).data('referencia')}
      return false;
    }
  });

  return existeTipoArchivo;
}

function borrarArchivoPersona(link, update_table) {
  if (update_table === undefined || update_table === null) update_table = true;

  swal({
    title: 'Desea borrar el archivo?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, borrar!',
    cancelButtonText: 'Cancelar',
  }).then(function() {
    var lista_cargo_id = location.pathname.match(/lista_cargos\/(\d+)/)[1];

    $.ajax({
      url: '/archivo_personas/' + link.getAttribute('data-reference') + '?lista_cargo_id=' + lista_cargo_id,
      type: 'DELETE',
      contentType: 'application/json',
      dataType: 'json',
      success: function(data, textStatus, jqXHR) {
        swal('Archivo borrado!', 'La operación realizada fue exitosa.', 'success');

        if (update_table) {
          $(link).parent().parent().remove();

          if ($('#tabla-archivos-persona >tbody >tr').length <= 1) {
            $("#tabla-archivos-persona tr[id=tabla_archivos_persona_vacia]").show();
          }
        } else {
          $('a#link_descarga_'+data.tipo_archivo)[0].setAttribute('href',  '');
          $('a#link_eliminar_'+data.tipo_archivo)[0].setAttribute('data-reference', '')
          $(link).parent().parent().parent().hide();
          $('label#label_archivo_'+data.tipo_archivo).addClass('rojo');
        }

        $(".js-tabla-candidatos tr[data-persona-id=" + data.persona.id + "] .js-observaciones").append($(crearIconoObservacion(data))).tooltip();
        
        invalidarLista();
      }
    }).fail(function(jqXHR, textStatus, errorThrown) {
      swal('Hubo un error al borrar el archivo!', 'Reintente la operación.', 'error');
    });
  })
}

function crearActualizarArchivoPersona(url, request_type, form_data, update_table) {
  if (update_table === undefined || update_table === null) update_table = true;

  $.ajax({
    url: url,
    type: request_type,
    data: form_data,
    processData: false,
    contentType: false,
    success: function(data, textStatus, jqXHR) {
      document.getElementById("formulario_archivo_persona").reset();

      if(data.archivo_persona.tipo_archivo.match(/deudor_alimentario|inhabilitado|antiguedad_residencia_persona/)) {
        document.getElementById("formulario_archivo_persona").reset();
      } else {
        document.getElementById("formulario_archivo_persona_requerido_" + data.archivo_persona.tipo_archivo).reset();
      }

      if (request_type === 'POST') {
        var messageTitle = 'Archivo agregado!';
        var messageBody = 'Se pudo agregar el archivo exitosamente.';
      } else {
        var messageTitle = 'Archivo actualizado!';
        var messageBody = 'Se pudo actualizar el archivo exitosamente.';
      }
      
      if (update_table) {
        if (request_type === 'POST') {
          if ($("#tabla-archivos-persona tr[id=tabla_archivos_persona_vacia]").is(":visible")) {
            $("#tabla-archivos-persona tr[id=tabla_archivos_persona_vacia]").hide();
          }
          
          var htmlRow = crearRowArchivoPersona(data);
          $('#tabla-archivos-persona tbody').append(htmlRow);
        } else {
          var htmlRow = crearRowArchivoPersona(data);
          var oldFile = $("table#tabla-archivos-persona tr[data-referencia='" + data['archivo_persona']['id'] +"']");
          oldFile.after(htmlRow);
          oldFile.remove();
        }
      } else {
        // Actualizacion de HTML para archivos requeridos
        $('label#label_archivo_' + data.archivo_persona.tipo_archivo).removeClass('rojo');
        $('a#link_descarga_' + data.archivo_persona.tipo_archivo)[0].setAttribute('href',  window.location.origin + '/archivos/descargar/' + data.archivo_persona.archivo.id);
        $('a#link_eliminar_' + data.archivo_persona.tipo_archivo)[0].setAttribute('data-reference', data.archivo_persona.id)
        $('div#div_descarga_' + data.archivo_persona.tipo_archivo).show();
      }

      $(".js-tabla-candidatos tr[data-persona-id=" + data.archivo_persona.persona_id + "]").find("i.js-observacion-" + data.archivo_persona.tipo_archivo).closest("form").remove();

      invalidarLista();

      swal(messageTitle, messageBody, 'success');
    }
  }).fail(function(jqXHR, textStatus, errorThrown) {
    swal('Agregar archivo', 'Se genero un error al agregar un archivo!', 'error');
  });
}

function crearRowArchivoPersona(data) {
  var htmlRow = '<tr data-tipo-archivo="' + data['archivo_persona']['tipo_archivo'] + '" data-referencia="' + data['archivo_persona']['id'] + '">';

  var downloadLink = '<a href="' + window.location.origin + '/archivos/descargar/' + data['archivo_persona']['archivo']['id'] + '"> \
                      ' + data['archivo_persona']['archivo']['nombre'] + ' \
                     </a>';
  htmlRow = htmlRow + '<td>' + downloadLink + '</td>';
  htmlRow = htmlRow + '<td>' + data['archivo_persona']['tipo_archivo_descripcion'] + '</td>';
  var link = '<a rel="tooltip" title="Eliminar" onclick="borrarArchivoPersona(this)" data-reference=' + data['archivo_persona']['id'] + ' href="javascript:;"> \
                <span class="label label-danger"> \
                  <i class="fa fa-trash"></i> \
                </span> \
              </a>';
  htmlRow = htmlRow + '<td style="text-align: center;">' + link + '</td>';
  htmlRow = htmlRow + '</tr>';

  return htmlRow;
}

function crearIconoObservacion(data) {
  var htmlIcono = '<form class="boton-observacion-candidato" method="get" action="/personas/busqueda_persona?cargo_id=' + data.cargo_id + '&editar=true&eleccion_id=' + data.eleccion_id + '&fuerza_id=' + data.fuerza_id + '&lista_cargo_id=' + data.lista_cargo_id + '&persona[numero_documento]=' + data.persona.numero_documento + '&persona[sexo]=' + data.persona.sexo + '&persona[tipo_documento]=' + data.persona.tipo_documento + '&tab=candidaturas" data-remote="true">';
  htmlIcono = htmlIcono + '<button class="js-boton-editar-candidatura pull-right boton-icono no-padding-right">'
  htmlIcono = htmlIcono + '<i class="text-danger ' + data.icono + ' js-observacion-' + data.tipo_archivo + '" data-toggle="tooltip" data-placement="top" title="' + data.titulo + '"></i>';
  htmlIcono = htmlIcono + '</button>'
  htmlIcono = htmlIcono + '</form>'

  return htmlIcono;
}

function invalidarLista() {
  $("h4.js-estado-candidaturas-lista-cargo").html('<i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="Lista Sin Validar"></i>');
  $(".js-seccion-notificaciones-candidaturas").html('<div class="alert alert-danger"><ul><li>Cargo pendiente de validación</li></ul></div>');
  inicializar_tooltips();
}