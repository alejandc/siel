function perfil_lista() {
  return $("#usuario_perfiles option:selected").val() === "lista_partidaria";
}

function agregar_select_lista(eleccion_id, fuerza_id) {
  $.ajax({
    type: "GET",
    url: "/listas?eleccion_id=" + eleccion_id + "&fuerza_id=" + fuerza_id,
    dataType : "script"
  });
}

function remover_select_lista() {
  $("#js-select-lista .form-group").remove();
}

function inicializar_select_perfil() {
  $("#usuario_perfiles").on("change", function() {
    perfil_lista() ? agregar_select_lista($(".js-select-eleccion option:selected").val(), $(".js-select-fuerza option:selected").val()) : remover_select_lista();
  });
}

function cambiar_select_lista() {
  var eleccion_id = $(".js-select-eleccion option:selected").val();
  var fuerza_id = $(".js-select-fuerza").find("option:selected").val();
  remover_select_lista();
  agregar_select_lista(eleccion_id, fuerza_id);
}

function inicializar_select_lista() {
  $(".js-select-fuerza").on("change", function() {
    if($(".js-select-fuerza").length > 0 && perfil_lista()) {
      cambiar_select_lista();
    }
  });
}

function inicializar_select_fuerzas_usuario(selector_eleccion, selector_fuerza) {
  $(selector_eleccion).on("change", function() {
    cargar_fuerzas_select(selector_eleccion, selector_fuerza, false);
  });
}

$(document).ready(function() {
  $('#link_grabar_usuario').on('click', function() { $('#form_usuario').submit(); });

  inicializar_select_perfil();
  inicializar_select_lista();
  inicializar_select_fuerzas_usuario("#usuarios_search_eleccion_id_eq", "#usuarios_search_fuerza_id_eq");
  inicializar_select_fuerzas_usuario("#usuario_eleccion_id", "#usuario_usuarios_fuerzas_attributes_0_fuerza_id");

  $(document).on("select_fuerzas_renderizado", function(event, selector) {
    if(selector === "#usuario_usuarios_fuerzas_attributes_0_fuerza_id") {
        $("#usuario_usuarios_fuerzas_attributes_0_fuerza_id option:first").remove();
        $("#usuario_usuarios_fuerzas_attributes_0_fuerza_id").val($("#usuario_usuarios_fuerzas_attributes_0_fuerza_id option:first").val());
        if(perfil_lista()) {
          cambiar_select_lista();
          inicializar_select_lista();
        }
      }
  });
});
