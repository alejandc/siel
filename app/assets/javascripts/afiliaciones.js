function seleccioneMultipleAfiliado() {
  var isChecked = $('input:checkbox#borrar_afiliaciones').is(':checked')

  var table= $('#lista_afiliaciones');
  $('td input:checkbox',table).prop('checked', isChecked);
}

function borrarAfiliaciones() {
  var afiliacionesIDs = [];
  var title = "";

  $("#lista_afiliaciones input:checkbox:checked").map(function(){
    if ($(this).val() !== "") {
      afiliacionesIDs.push($(this).val());
    }
  });

  swal({
    title: (afiliacionesIDs.length > 0) ? "Desea borrar las Afiliaciones seleccionadas?" : "Desea borrar TODAS las Afiliaciones?",
    text: "Esta acción no se podra revertir!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Si, borrar!',
    cancelButtonText: 'Cancelar'
  }).then(function() {
    $.ajax({
      url: '/afiliaciones/borrar_afiliaciones',
      type: 'DELETE',
      data: {afiliaciones: afiliacionesIDs, format: 'js'},
      success: function(data, textStatus, jqXHR) {
        swal('Afiliaciones borradas!',
             'La operación realizada fue exitosa.',
             'success');
      }
    }).fail(function(jqXHR, textStatus, errorThrown) {
      swal('Hubo un error al borrar las Afiliaciones!',
           'Reintente la operación.',
           'error');
    });
  })
}

function inicializar_select_fuerzas_afiliacion() {
  $("#afiliacion_eleccion_id").on("change", function() {
    cargar_fuerzas_select("#afiliacion_eleccion_id", "#afiliacion_fuerza_id", true);
  });
}

$(document).ready(function() {
  inicializar_select_fuerzas_afiliacion();
});