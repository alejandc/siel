$(document).ready(function() {
  // Set up the AJAX indicator
  $('body').append('<div class="loading" align="center"> <img src="/spin.gif" alt="Cargando..."/> <p>CARGANDO</p> </div>');

  // AJAX activity indicator bound to ajax start/stop document events
  $(document).ajaxStart(function () {
    $('.loading').show();
  }).ajaxStop(function () {
    $('.loading').hide();
  });
});
