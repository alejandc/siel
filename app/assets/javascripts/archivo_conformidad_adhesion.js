var existeArchivoConformidadAdhesion = function() {
  return $(".js-boton-descarga-archivo-conformidad-adhesiones").length > 0;
};

var crearActualizarArchivoConformidadAdhesion = function(form_data) {
  $.ajax({
    url: '/archivo_lista_cargos',
    type: 'POST',
    data: form_data,
    processData: false,
    contentType: false,
    success: function(data, textStatus, jqXHR) {
      var mensaje = data.mensaje;
      $(".js-boton-archivo-conformidad-adhesiones").removeClass("btn-danger");
      $(".js-boton-archivo-conformidad-adhesiones").addClass("btn-success");
      $(".js-boton-archivo-conformidad-adhesiones").attr('data-original-title', "Ver/modificar el documento de firmas");
      $(".js-boton-archivo-conformidad-adhesiones").tooltip();
      $(".js-modal-conformidad-adhesiones").modal('toggle');
      swal('', mensaje, 'success');
    }
  }).fail(function(jqXHR, textStatus, errorThrown) {
    swal('Agregar archivo', 'Se genero un error al agregar un archivo!', 'error');
  });
};