
$(document).ready(function() {

	inicializar_carga_escudo();
	inicializar_mascara_umbral();
	inicializar_cp_apoderado();
	inicializar_numero_apoderado();

	$('select#fuerza_id').on('change', function() {
    var fuerza_id = $(this).find("option:selected").val();
    window.location.search = window.location.search.replace(/fuerza_id=\d+/, 'fuerza_id=' + fuerza_id)
  })

	$('#descargar_registro_top').on('click', function(){ descargar_registro(); });
	$('#descargar_registro_bottom').on('click', function(){ descargar_registro(); });

	$('#descargar_apoderados_top').on('click', function(){ descargar_apoderados(); return false; });
	$('#descargar_apoderados_bottom').on('click', function(){ descargar_apoderados(); return false; });

	$('#fuerza_numero').focus();
	$('#grabar_fuerza').on('click', function(){ grabar_fuerza(); });

	$('#agregar_partido').on('click', function(e) {
		e.preventDefault();

		id = $('#partidos').val()

		if(id) {
			lista = $('#fuerza_ids_partidos').val();
			
			ids = lista.split(',');

			if (ids.indexOf(id) < 0)
			{
				if (lista != '') lista += ',';
				lista += id;
							
				$('#fuerza_ids_partidos').val(lista);
				$('#lista_partidos').append
				(
					'<li id="partido_'+ id +'">'+ 
					$('#partidos :selected').text() +
					'<a href="javascript: remove_partido('+ id +')"><i class="glyphicon glyphicon-remove"></i></a>'+
					'</li>'
				);
			}

			$("#partidos option[value="+ id +"]").attr('disabled', 'disabled');
			$("#partidos").val('');
			$("#partidos").select2();
		}
	});
	$('#fuerza_tipo').on('change', function() 
	{
		if ($(this).val() == 'ALIANZA') $('#panel_alianza').slideDown();
		else $('#panel_alianza').slideUp();
	});
	$('#confirmar_baja_fuerza').on('click', function () 
	{  
		$('#form_baja_fuerza').submit();
		return false;
	});

	if ($('#fuerza_tipo').val() != 'ALIANZA')
	{
		$('#panel_alianza').hide();
	}

});

function agregar_apoderado(link, fuerza_id) {
	// numero_documento = $("#numero_documento").val();

	// var numeros_documento_repetidos = $(".js-apoderado-numero-documento").filter(function(index) {
 //    return $(this).val() === numero_documento;
 //  });

	// if(numeros_documento_repetidos.length === 0) {
	// 	if ($.trim(numero_documento) != '') {
	// 		$.ajax(
	// 		{
	// 			type: "GET",
	// 			url: "/fuerzas/agregar_apoderado?numero_documento="+ numero_documento +"&fuerza_id="+ fuerza_id
	// 		});
	// 	} else {
	// 		swal('DNI Inválido', 'Por favor ingrese el DNI del apoderado a agregar.', 'error');
	// 	}
	// } else {
	// 	swal('Apoderado Inválido', 'El apoderado ya se encuentra agregado al partido.', 'error');
	// }

	$.ajax({
		type: "GET",
		url: "/fuerzas/agregar_apoderado?fuerza_id="+ fuerza_id
	});
}

function copiar_filtro_para_descarga() {
	$('#descargar_registro_form input[name=nombre]').val($('#fuerza_filtros_nombre').val());
	$('#descargar_registro_form input[name=numero]').val($('#fuerza_filtros_numero').val());
	$('#descargar_registro_form input[name=expediente]').val($('#fuerza_filtros_expediente').val());
	$('#descargar_registro_form select[name=estado]').val($('#fuerza_filtros_estado').val());
	$('#descargar_registro_form input[name=observaciones]').val($('#fuerza_filtros_observaciones').val());
	$('#descargar_registro_form input[name=eleccion_id]').val($('#fuerza_filtros_eleccion_id').val());
	$('#descargar_registro_form input[name=apoderado]').val($('#fuerza_filtros_apoderado').val());
	$('#descargar_registro_form input[name=tipo]').val($('#fuerza_filtros_tipo').val());
	$('#descargar_registro_form input[name=con_infracciones]').val($('#filtros_con_infracciones').val());
}

function descargar_apoderados() {
	// copiar_filtro_para_descarga();
	$('#filtros_template').val('listado_apoderados');
	$('#descargar_registro_form').attr("action","/fuerzas.xls");
	$('#descargar_registro_form').submit();
}

function descargar_registro() {
	// copiar_filtro_para_descarga();
	$('#filtros_template').val('index');
	$('#descargar_registro_form').attr("action","/fuerzas.xls");
	$('#descargar_registro_form').submit();
}

function remove_partido(id) {
	if (confirm('¿Está seguro que desea quitar este partido?')) {
		$('#partido_'+ id).remove();
		
		ids = $('#fuerza_ids_partidos').val().split(",");
		newIds = "";
		
		for (i = 0; i < ids.length; i++)
		{
			if (ids[i] != id)
			{
				newIds += ids[i] +",";
			}
		}
		
		$('#fuerza_ids_partidos').val(newIds.substring(0, newIds.length - 1));

		$("#partidos option[value="+ id +"]").attr('disabled', false);
		$("#partidos").select2();
	}
}

function expediente_seleccionado(numero) {
	$('#fuerza_expediente').val(""+numero);
}

function grabar_fuerza() {
	$('#fuerza_form').submit();
}

function inicializar_carga_escudo() {
	$(".js-editar-escudo").on("click", function() {
		$("#fuerza_escudo").click();
	});

	$('#fuerza_escudo').on('change', function() {
    $('.js-editar-fuerza').submit();
  });
}

function inicializar_mascara_umbral() {
	$(".js-input-umbral").inputmask('9{0,2},9{0,1}');
}

function inicializar_cp_apoderado() {
	$(".js-cp-apoderado").inputmask("9{1,4}");
}

function inicializar_numero_apoderado() {
	$(".js-numero-apoderado").inputmask("9{1,5}");
}