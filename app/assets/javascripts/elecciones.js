
function inicializar_checkbox_comuneros() {
  $("#eleccion_comuna[type='checkbox']").on("change", function() {
    var checked = $(this).is(':checked');
    $(".js-checkbox-comunero input[type='checkbox']").each(function() {
      $(this).attr("checked", checked);
    })
  });
}

$(document).ready(function()
{
	$('#grabar_eleccion').click(function()
	{
		$('#form_eleccion').submit();
		return false;
	});
	$('#link_filtrar_fuerzas').click(function()
	{
		window.location = '/elecciones/'+ $('#eleccion_id').val() +'/fuerzas';
	});
	$('#link_nuevo_reparto').click(function()
	{
		$('#form_nuevo_reparto').attr('action', '/elecciones/'+ $('#eleccion').val() +'/cargar_reparto');
		$('#form_nuevo_reparto').submit();
	});
	$('#link_grabar_resultados').click(function()
	{
		$('#form_resultados').submit();
		return false;
	});

  inicializar_checkbox_comuneros();
});

function change_tab_comuna(action, comuna_id)
{
	$('#comuna_id').val(comuna_id);

	form = $('#form_resultados');

	form.attr("action", action);

	form.submit();

	return false;
}

function expediente_seleccionado(numero)
{
	valor_anterior = $('#eleccion_expediente').val();

	if (valor_anterior != null && valor_anterior != '')
	{
		$('#eleccion_expediente').val(valor_anterior +', '+ numero);
	}
	else
	{
		$('#eleccion_expediente').val(""+numero);
	}
}
