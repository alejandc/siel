function clear_list_form_errors() {
  $("label.error_message").remove();
  $( "#crear_lista_form" ).find( "div" ).removeClass( "field_with_errors" );
}

var inicializar_checkbox_masivo = function() {
  $(".js-todos-cargos-habilitados").on("click", function() {
    if($(this).is(":checked")) {
      $('.js-cargo-habilitado').prop('checked', true);
    } else {
      $('.js-cargo-habilitado').prop('checked', false);
    }
  });
};

var inicializar_select_elecciones = function() {
  var cambiar_cargos_habilitados = function(select) {
    var eleccion_id = $(select).find("option:selected").val();

    $.ajax({
      type: 'GET',
      url: "/listas/seccion_cargos_habilitados",
      data: { eleccion_id: eleccion_id, format: "js" },
      global: false,
      error: function(request, status, error) {
        alert('Falló cuando se quiso cambiar la eleccion');
      }
    });
  };

  $(".js-select-elecciones").on("change", function() {
    cambiar_cargos_habilitados(this);
    cargar_fuerzas_select("#lista_eleccion_id", "#lista_fuerza_id", false);
  });
};

function inicializar_input_numero() {
  $(".js-numero-lista").inputmask("9{1,}");
}

$(document).ready(function() {
  $(".js-tabla-listas .js-boton-toggle-lista").first().click();
  inicializar_checkbox_masivo();
  inicializar_select_elecciones();
  inicializar_input_numero();
});