function limpiar_formulario_persona(form_id) {
	var arrElems = document.forms[form_id].elements;
	document.getElementById(form_id).reset();

	for(xi=0;xi<arrElems.length;xi++){
      if(arrElems[xi].type.toLowerCase() == "text" ||
         arrElems[xi].type.toLowerCase() == "hidden" ||
         arrElems[xi].type.toLowerCase() == "password" ||
         arrElems[xi].type.toLowerCase() == "textarea")
        arrElems[xi].value="";
      else if(arrElems[xi].type.toLowerCase() == "radio" ||
              arrElems[xi].type.toLowerCase() == "checkbox")
        arrElems[xi].selected = false;
      else if(arrElems[xi].type.toLowerCase().indexOf("select") != -1)
        arrElems[xi].selectedIndex = -1;
    }

	$('#tabla-archivos-persona').remove()
	$('.fields').remove();
  $(".js-persona-form .rojo").removeClass("rojo");

  $('div#flash-alerts').html('');
}
