function seleccioneMultipleRegistrosCandidaturas() {
  var isChecked = $('input:checkbox#borrar_registros_candidaturas').is(':checked')

  var table= $('#lista_registro_candidaturas');
  $('td input:checkbox',table).prop('checked', isChecked);
}


function borrarRegistrosCandidatura() {
  var registroCandidaturaIDs = [];
  var title = "";

  $("#lista_registro_candidaturas input:checkbox:checked").map(function(){
    if ($(this).val() !== "") {
      registroCandidaturaIDs.push($(this).val());
    }
  });

  swal({
    title: (registroCandidaturaIDs.length > 0) ? "Desea borrar los Registros de Candidaturas seleccionados?" : "Desea borrar TODOS los Registros de Candidaturas?",
    text: "Esta acción no se podra revertir!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Si, borrar!',
    cancelButtonText: 'Cancelar'
  }).then(function() {
    $.ajax({
      url: '/registro_candidaturas/borrar_registros_candidaturas',
      type: 'DELETE',
      data: {registro_candidaturas: registroCandidaturaIDs, format: 'js'},
      success: function(data, textStatus, jqXHR) {
        swal('Registros de Candidaturas borradas!',
             'La operación realizada fue exitosa.',
             'success');
      }
    }).fail(function(jqXHR, textStatus, errorThrown) {
      swal('Hubo un error al borrar los Registros de Candidaturas!',
           'Reintente la operación.',
           'error');
    });
  })
}