function seleccioneMultipleFiscal() {
  var isChecked = $('input:checkbox#borrar_fiscales').is(':checked')

  var table= $('#lista_fiscales');
  $('td input:checkbox',table).prop('checked', isChecked);
}

function boton_eliminar_fiscal(id) {
  event.preventDefault();

  swal({
    title: '',
    text: 'Está seguro que desea eliminar el fiscal?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Confirmar',
    cancelButtonText: 'Cancelar'
  }).then(function() {
    $.ajax({
      type: 'DELETE',
      url: "/fiscales/" + id + "/destroy_new",
      data: { format: 'js' },
      error: function(request, status, error) {
        alert('Falló en el eliminado del fiscal');
      }
    });
  });
}

function validarArchivosImportacion(archivo) {
  var allowedExtension = ['xls'];
  var extName;
  var maxFileSize = $(archivo).data('max-file-size');
  var sizeExceeded = false;
  var extError = false;

  $.each(archivo.files, function() {
    if (this.size && maxFileSize && this.size > parseInt(maxFileSize)) {sizeExceeded=true;};
    extName = this.name.split('.').pop();
    if ($.inArray(extName, allowedExtension) == -1) {extError=true;};
  });

  if (sizeExceeded) {
    swal(
          'Archivo para Importar',
          'El archivo excede el máximo de tamaño permitidopara importar (5 MB)',
          'warning'
        )
    $(archivo).val('');
  };

  if (extError) {
    swal(
          'Archivo para Importar',
          'Solamente estan permitidos archivos con formato XLS.',
          'warning'
        )
    $(archivo).val('');
  };
}

function borrarFiscales() {
  var afiliacionesIDs = [];
  var title = "";

  $("#lista_fiscales input:checkbox:checked").map(function(){
    if ($(this).val() !== "") {
      fiscalesIDs.push($(this).val());
    }
  });

  swal({
    title: (afiliacionesIDs.length > 0) ? "Desea borrar las Fiscales seleccionados?" : "Desea borrar TODOS los Fiscales?",
    text: "Esta acción no se podra revertir!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Si, borrar!',
    cancelButtonText: 'Cancelar'
  }).then(function() {
    $.ajax({
      url: '/fiscales/borrar_fiscales',
      type: 'DELETE',
      data: {afiliaciones: afiliacionesIDs, format: 'js'},
      success: function(data, textStatus, jqXHR) {
        swal('Fiscales borrados!',
             'La operación realizada fue exitosa.',
             'success');
      }
    }).fail(function(jqXHR, textStatus, errorThrown) {
      swal('Hubo un error al borrar los Fiscales!',
           'Reintente la operación.',
           'error');
    });
  })
}

function inicializar_select_fuerzas_fiscal() {
  $("#fiscal_eleccion_id").on("change", function() {
    cargar_fuerzas_select("#fiscal_eleccion_id", "#fiscal_fuerza_id", false);
  });
}

function inicializar_select_fuerzas_fiscal_modal() {
  $("#eleccion_id").on("change", function() {
    cargar_fuerzas_select("#eleccion_id", "#fuerza_id", false);
  });
}

function cargar_establecimientos_por_comuna_select(comuna_select_id, establecimiento_select_id) {
  var opciones_establecimientos = function(establecimientos) {
    return "<option value=''>Establecimiento...</option>" + establecimientos.map(function(establecimiento) {
      return "<option value='" + establecimiento.codigo_establecimiento + "'>" + establecimiento.nombre + "</option>";
    });
  };

  var numero_comuna = $(comuna_select_id).find("option:selected").val();
  
  if(numero_comuna !== "") {
    $.ajax({
      type: 'GET',
      url: "/fiscales/establecimientos_por_comuna",
      data: { numero_comuna: numero_comuna, format: "json" },
      global: false,
      success: function(data, textStatus, jqXHR) {
        $(establecimiento_select_id).html(opciones_establecimientos(data.establecimientos));
        $(establecimiento_select_id).select2();
      },
      error: function(request, status, error) {
        alert('Falló cuando se quiso cambiar los establecimiento por la comuna seleccionada');
      }
    });
  }
}

function obtener_cantidad_mesas_por_establecimiento(numero_comuna, codigo_establecimiento) {
  if(codigo_establecimiento !== "" && comuna !== "") {
    $.ajax({
      type: 'GET',
      url: "/fiscales/cantidad_mesas_establecimiento",
      data: { numero_comuna: numero_comuna, codigo_establecimiento: codigo_establecimiento, format: "json" },
      global: false,
      success: function(data, textStatus, jqXHR) {
        $('p#cantidad_mesas').html("Cantida de mesas del establecimiento: " + data.mesas_establecimiento.length);
      }
    });
  }
}

$(document).ready(function() {
  inicializar_select_fuerzas_fiscal();
  inicializar_select_fuerzas_fiscal_modal();
});