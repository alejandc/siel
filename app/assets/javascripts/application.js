 // This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require jquery.remotipart

//= require_tree ./bootstrap
//= require ../../../vendor/assets/javascripts/inputmask.js
//= require ../../../vendor/assets/javascripts/jquery.inputmask.js
//= require ../../../vendor/assets/javascripts/inputmask.extensions.js
//= require ../../../vendor/assets/javascripts/inputmask.date.extensions.js
//= require ../../../vendor/assets/javascripts/select2.js
//= require ../../../vendor/assets/javascripts/jquery-ui.js
//= require ../../../vendor/assets/javascripts/sweetalert2.js
//= require moment
//= require moment/es
//= require bootstrap-datetimepicker
//= require ajax_busy
//= require best_in_place
//= require best_in_place.jquery-ui
//= require jquery.purr
//= require best_in_place.purr
//= require_tree .


// Manage 401/403/500 callbacks from ajax requests
$.ajaxSetup({
  statusCode: {
    401: function(){
      // Redirec the to the login page.
      location.href = "../users/sign_in";
    },
    403: function(request, status, error) {
      swal(
        'Error',
        request.responseText,
        'error'
      );
    }
  }
});

function add_fields(link, association, content)
{
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g");

  $(link).parent().before(content.replace(regexp, new_id));
}

function remove_fields(link)
{
  if (confirm('¿Está seguro que desea quitar este item?'))
  {
    $(link).prev("input[type=hidden]").val("1");
    $(link).closest(".fields").hide();
  }
}

function pop_window(cuerpo, titulo, texto_boton_ok)
{
  $('#windowDialogHeader').html(titulo);
  $('#windowDialogMessage').html(cuerpo);
  $('#windowDialog').modal();

  if (typeof(texto_boton_ok)==='undefined')
  {
    $('#windowDialogOkButton').hide();
  }
  else
  {
    $('#windowDialogOkButton').show();
    $('#windowDialogOkButton').html('<i class="glyphicon glyphicon-ok"></i> ' + texto_boton_ok);
  }
}

function pop_info(mensaje, titulo)
{
  if(typeof(titulo)==='undefined') titulo = 'Atenci&oacute;n';

  $('#infoDialogHeader').html(titulo);
  $('#infoDialogMessage').html(mensaje);
  $('#infoDialog').modal();
}

function pop_error(mensaje)
{
  $('#errorDialogMessage').html(mensaje);
  $('#errorDialog').modal();
}

function inicializar_numero_documento_con_mascara()
{
  $(".js-input-numero-documento").inputmask("9{1,9}");
}

function inicializar_fecha_con_mascara()
{
  $('.js-fecha-mascara').datepicker({
      dateFormat: 'dd/mm/yyyy',
      language: 'es'
  }).on("change", function() {
    $(this).datepicker("hide");
  });

  $(".js-fecha-mascara").inputmask("date");
}

function inicializar_select_2()
{
  $(".select2").select2();
}

function crear_alerta_flash(tipo, mensaje) {
  $("#flash-alerts").html("");

  if(mensaje) {
    var alerta = "<div class='alert alert-" + tipo + "' id='fixed_alert'><a class='close' data-dismiss='alert'>x</a><div id='flash_error'>" + mensaje + "</div></div>";
    $("#flash-alerts").html(alerta);

    setear_timeout_fixed_alerts();
  }
}

function limpiar_flying_alerts() {
  $("#flash-alerts").html("");
}

function crear_alerta_sweet(tipo, mensaje) {
  limpiar_flying_alerts();
  var tipo_aux = tipo;
  swal({title: '', text: mensaje, type: tipo_aux, confirmButtonText: 'Cerrar'});
}

function crear_flying_alertas(alerts) {
  if(alerts.notice) {
    crear_alerta_flash('success', alerts.notice);
  } else if(alerts.warning) {
    crear_alerta_flash('warning', alerts.warning);
  } else if(alerts.error) {
    crear_alerta_flash('danger', alerts.error);
  } else if(alerts.sweet_alert_notice) {
    crear_alerta_sweet('success', alerts.sweet_alert_notice);
  } else if(alerts.sweet_alert_warning) {
    crear_alerta_sweet('warning', alerts.sweet_alert_warning);
  } else if(alerts.sweet_alert_error) {
    crear_alerta_sweet('error', alerts.sweet_alert_error);
  }
}

function inicializar_tooltips() {
  $('[data-toggle="tooltip"]').tooltip();
}

function inicializar_popover() {
  $('[data-toggle="popover"]').popover({
    trigger: "hover"
  });
}

function setear_timeout_fixed_alerts() {
  $("#fixed_alert").fadeTo(4000, 500).slideUp(500, function(){
    $("#fixed_alert").alert('close');
  });
}

function limpiar_formulario(form_id, tabSection) {
  document.getElementById(form_id).reset();
  $('div#flash-alerts').html('');

  if(tabSection === 'candidaturas') {
    $('#formulario_carga_candidatura').hide();
  } else {
    $('#formulario_carga_adhesion').hide();
  }
}

function cargar_fuerzas_select(eleccion_select_id, fuerza_select_id, sin_alianzas) {
  var opciones_fuerzas = function(fuerzas) {
    return "<option value=''>Seleccione Agrupación...</option>" + fuerzas.map(function(fuerza) {
      return "<option value='" + fuerza.id + "'>" + fuerza.nombre + "</option>";
    });
  };

  var eleccion_id = $(eleccion_select_id).find("option:selected").val();

    $.ajax({
      type: 'GET',
      url: "/fuerzas/select_fuerzas",
      data: { eleccion_id: eleccion_id, sin_alianzas: sin_alianzas, format: "json" },
      global: false,
      success: function(data, textStatus, jqXHR) {
        $(fuerza_select_id).html(opciones_fuerzas(data.fuerzas));
        $(document).trigger("select_fuerzas_renderizado", [fuerza_select_id]);
        $(fuerza_select_id).select2();
      },
      error: function(request, status, error) {
        alert('Falló cuando se quiso cambiar la eleccion');
      }
    });
}

//****************************************************************************//
//Override the default confirm dialog by rails
$.rails.allowAction = function(link){
  if (link.data("confirm") == undefined){
    return true;
  }
  $.rails.showConfirmationDialog(link);
  return false;
}

//User click confirm button
$.rails.confirmed = function(link){
  link.data("confirm", null);
  link.trigger("click.rails");
}

//Display the confirmation dialog
$.rails.showConfirmationDialog = function(link){
  var message = link.data("confirm");
  swal({
    title: message,
    type: 'warning',
    confirmButtonText: 'Confirmar',
    showCancelButton: true,
    cancelButtonText: 'Cancelar'
  }).then(function(e){
    $.rails.confirmed(link);
  });
};
//****************************************************************************//

$(document).ready(function()
{
  // Activating Best In Place
  jQuery(".best_in_place").best_in_place();

  $(".best_in_place").bind("ajax:success", function() {
    $(this).parent().effect('highlight', {color: '#37bc9b'}, 1200);
  });

  $(".best_in_place").bind("ajax:error", function(request, status) {
    $(this).parent().effect('highlight', {color: '#C66'}, 1200);
  });

  jQuery.each($(".navbar-ex6-collapse li.dropdown"), function(index, each) {
    if($(each).find("li.active").length > 0) {
      $(each).addClass("active");
    }
  });

  inicializar_numero_documento_con_mascara();
  inicializar_fecha_con_mascara();
  inicializar_select_2();
  inicializar_tooltips();
  inicializar_popover();
  setear_timeout_fixed_alerts();

  if (!window.console) console = {log: function() {}};
});
