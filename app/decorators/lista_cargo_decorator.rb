class ListaCargoDecorator < Draper::Decorator
  delegate_all

  def icono_estado_candidaturas(con_descripcion)
    icono_estado(con_descripcion, 'candidaturas', estado_candidaturas)
  end

  def icono_estado_adhesiones(con_descripcion)
    icono_estado(con_descripcion, 'adhesiones', estado_adhesiones, 'glyphicon glyphicon-list')
  end

  def notificaciones_estado_errores_candidaturas
    notificaciones_estado_errores('candidaturas', estado_candidaturas)
  end

  def notificaciones_estado_alertas_candidaturas
    notificaciones_estado_alertas('candidaturas')
  end

  def notificaciones_estado_errores_adhesiones
    notificaciones_estado_errores('adhesiones', estado_adhesiones)
  end

  def notificaciones_estado_alertas_adhesiones
    notificaciones_estado_alertas('adhesiones')
  end

  def color_estado(asociacion)
    send("estado_#{asociacion}_cerrada?") ? 'success' : 'danger'
  end

  private

  def icono_estado(con_descripcion, asociacion, estado, icono = nil)
    i18n_path = 'activerecord.errors.models.lista_cargo.estados'
    clase, title, descripcion = case estado
                                when :sin_validar
                                  [icono || 'glyphicon glyphicon-question-sign', I18n.t("#{i18n_path}.sin_validar_titulo", categoria: texto_asociacion(asociacion)), (con_descripcion ? (-> { I18n.t("#{i18n_path}.sin_validar_descripcion") }) : -> { '' })]
                                when :valida
                                  ["text-success #{icono || 'glyphicon glyphicon-ok'}", I18n.t("#{i18n_path}.valida_titulo", categoria: texto_asociacion(asociacion)), (con_descripcion ? (-> { I18n.t("#{i18n_path}.valida_descripcion") }) : -> { '' })]
                                when :incompleta
                                  ["amarillo-patito #{icono || 'fa fa-hourglass'}", I18n.t("#{i18n_path}.incompleta_titulo", categoria: texto_asociacion(asociacion)), (con_descripcion ? (-> { I18n.t("#{i18n_path}.incompleta_descripcion", asociacion: asociacion, faltantes: send("#{asociacion}_faltantes")) }) : -> { '' })]
                                when :con_errores
                                  ["text-danger #{icono || 'glyphicon glyphicon-remove'}", I18n.t("#{i18n_path}.con_errores_titulo", categoria: texto_asociacion(asociacion)), (con_descripcion ? (-> { I18n.t("#{i18n_path}.con_errores_descripcion") + send("listado_observaciones_#{asociacion}") }) : -> { '' })]
                                when :cerrada
                                  ["text-info #{icono || 'glyphicon glyphicon-lock'}", I18n.t("#{i18n_path}.cerrada_titulo", categoria: texto_asociacion(asociacion)), (con_descripcion ? (-> { I18n.t("#{i18n_path}.cerrada_descripcion") }) : -> { '' })]
                                end

    if con_descripcion
      h.content_tag(:i, '', class: clase, data: { toggle: 'popover', placement: 'top', html: 'true', content: descripcion.call }, title: title)
    else
      h.content_tag(:i, '', class: clase, data: { toggle: 'tooltip', placement: 'top' }, title: title)
    end
  end

  def notificaciones_estado_errores(asociacion, estado)
    case estado
    when :incompleta
      h.content_tag(:li, I18n.t('activerecord.errors.models.lista_cargo.estados.incompleta_descripcion', asociacion: asociacion, faltantes: send("#{asociacion}_faltantes")))
    when :con_errores
      (errores[asociacion.to_sym] || []).inject('') { |item, error| item + "<li>#{error}</li>" }.html_safe
    else
      h.content_tag(:li, I18n.t("activerecord.errors.models.lista_cargo.estados.notificaciones.#{send('estado_' + asociacion)}_#{asociacion}"))
    end
  end

  def notificaciones_estado_alertas(asociacion)
    (alertas[asociacion.to_sym] || []).inject('') { |item, observacion| item + "<li>#{observacion}</li>" }.html_safe
  end

  def listado_observaciones_candidaturas
    ('<ul class="observaciones-lista-popover">' + (errores[:candidaturas] || []).map { |error| "<li>#{error}</li>" }.join('<hr>') + '</ul>').html_safe
  end

  def listado_observaciones_adhesiones
    ('<ul class="observaciones-lista-popover">' + (errores[:adhesiones] || []).map { |error| "<li>#{error}</li>" }.join('<hr>') + '</ul>').html_safe
  end

  def texto_asociacion(asociacion)
    asociacion == 'candidaturas' ? 'Precandidatos' : 'Adhesiones'
  end

end
