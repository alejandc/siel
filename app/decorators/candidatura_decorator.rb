class CandidaturaDecorator < Draper::Decorator
  delegate_all

  def nombre_completo
    persona_denominacion.present? ? persona_denominacion : "#{persona_nombres} #{persona_apellidos}"
  end

  def color_sexo
    case persona_sexo
    when 'M'
      'info'
    when 'F'
      'danger'
    end
  end

  def color_sexo_pdf
    case persona_sexo
    when 'M'
      'D9EDF7'
    when 'F'
      'F2DEDE'
    end
  end

  def color_reordenado_pdf
    '666463'
  end

  def nacionalidad
    I18n.t("persona_atributos.nacionalidad_argentina.#{persona_nacionalidad_argentina.downcase}")
  end

  def residencia
    I18n.t("persona_atributos.residencia.#{persona_residencia.downcase}")
  end

  def boton_eliminar
    h.button_to '', method: :delete, class: 'pull-right boton-icono no-padding-left', onclick: "boton_eliminar(event, #{lista_cargo_id}, 'candidatura', #{id});" do
      '<i class="glyphicon glyphicon-remove text-danger pull-right" style="margin: 0 3px" data-toggle="tooltip" data-placement="top" title="Eliminar"></i>'.html_safe
    end
  end

  def boton_editar(icon = nil)
    h.button_to h.busqueda_persona_personas_path(tab: 'candidaturas', eleccion_id: eleccion_id, cargo_id: cargo_id, fuerza_id: fuerza.id, lista_cargo_id: lista_cargo_id, editar: true, persona: { tipo_documento: persona_tipo_documento, numero_documento: persona_numero_documento, sexo: persona_sexo }), method: :get, remote: true, class: 'js-boton-editar-candidatura pull-right boton-icono no-padding-right', form_class: 'boton-observacion-candidato' do
      (icon.present? ? icon : '<i class="glyphicon glyphicon-edit pull-right" style="margin: 0 3px" data-toggle="tooltip" data-placement="top" title="Editar"></i>').html_safe
    end
  end

  def orden(opciones = {})
    if candidatura.titular?
      candidatura.orden
    else
      (opciones[:con_descripcion] ? "Suplente #{candidatura.orden - candidatura.cargo_cantidad_minima}" : candidatura.orden - candidatura.cargo_cantidad_minima)
    end
  end

end
