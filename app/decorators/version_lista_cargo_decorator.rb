class VersionListaCargoDecorator < Draper::Decorator
  delegate_all

  def self.campos_actualizables_persona
    %w(nombres apellidos denominacion fecha_nacimiento)
  end

  def self.campos_actualizables_domicilio
    %w(calle numero piso depto codigo_postal)
  end

  def self.campos_actualizables_candidatura
    %w(orden)
  end

  def accion
    case event
    when 'create'
      'Creación'
    when 'update'
      'Edición'
    when 'destroy'
      'Borrado'
    when 'destroy_all'
      'Borrado Completo'
    when 'import'
      'Importación'
    end
  end

  def cambios
    case event
    when 'create'
      accion_sobre_modelo('creacion')
    when 'update'
      accion_sobre_modelo('cambios')
    when 'destroy'
      accion_sobre_modelo('eliminado')
    when 'destroy_all'
      accion_sobre_modelo('eliminado_masivo')
    when 'import'
      accion_sobre_modelo('importar')
    end
  end

  private

  # CREATES ---------------------------------------------------------------------------------------------------------------------
  def creacion_adhesion
    "<strong>Agregado a la lista de adhesiones:</strong> <em>#{nombre_persona(changeset['persona_id'].last)}</em>".html_safe
  end

  def creacion_archivo_conformidad_adhesion
    "<strong>Se agregó un nuevo archivo de conformidad de adhesiones: </strong> <em>#{Archivo.find(changeset['archivo_id'].last).nombre}</em>".html_safe
  end

  def creacion_archivo_persona
    "<strong>Se agregó un archivo de persona: </strong> <em>#{I18n.t("activerecord.attributes.archivos_persona.tipo_archivo.#{changeset['tipo_archivo'].last}")}</em> <strong>para el candidato:</strong> <em>#{nombre_persona(changeset['persona_id'].last)}</em>".html_safe
  end

  def creacion_candidatura
    cargo_cantidad_minima = ListaCargo.find(lista_cargo_id).cargo_cantidad_minima
    orden_candidatura = changeset['orden'].last > cargo_cantidad_minima ? "suplente #{changeset['orden'].last - cargo_cantidad_minima}" : changeset['orden'].last
    "<strong>Agregado a la lista de precandidaturas:</strong> <em>#{nombre_persona(changeset['persona_id'].last)}</em> <strong>en la posición:</strong> <em>#{orden_candidatura}</em>".html_safe
  end
  # -----------------------------------------------------------------------------------------------------------------------------

  # UPDATES ---------------------------------------------------------------------------------------------------------------------
  def cambios_archivo_persona
    "<strong>Se actualizó un archivo de persona: </strong> <em>#{I18n.t("activerecord.attributes.archivos_persona.tipo_archivo.#{reify.tipo_archivo}")}</em> <strong>para el candidato:</strong> <em>#{nombre_persona(reify.persona_id)}</em>".html_safe
  end

  def cambios_candidatura
    cargo_cantidad_minima = ListaCargo.find(lista_cargo_id).cargo_cantidad_minima
    changeset['orden'].last > cargo_cantidad_minima ? "suplente #{changeset['orden'].last - cargo_cantidad_minima}" : changeset['orden'].last
    orden_candidatura_antiguo = changeset['orden'].first > cargo_cantidad_minima ? "suplente #{changeset['orden'].first - cargo_cantidad_minima}" : changeset['orden'].first
    orden_candidatura_actual = changeset['orden'].last > cargo_cantidad_minima ? "suplente #{changeset['orden'].last - cargo_cantidad_minima}" : changeset['orden'].last
    "<strong>Candidatura</strong> <em>#{nombre_persona(item.try(:persona_id) || object_deserialized['persona_id'])}</em> <br> <strong>Posición anterior:</strong> <em>#{orden_candidatura_antiguo}</em>, <strong>Posición nueva:</strong> <em>#{orden_candidatura_actual}</em>".html_safe
  end

  def cambios_domicilio
    cambios_modelo(VersionListaCargoDecorator.campos_actualizables_domicilio).unshift("<strong>Persona</strong> <em>#{nombre_persona_con_domicilio(item.try(:id) || object_deserialized['id'])}</em> ").join('').html_safe
  end

  def cambios_lista_cargo
    cambios_estado = []
    cambios_estado.push("<strong>Se actualizó el estado de precandidaturas de:</strong> <em>#{I18n.t("activerecord.errors.models.lista_cargo.estados.#{ListaCargo.estado_candidaturas.hash.key(changeset['estado_candidaturas_cd'].first)}")}</em> <strong>a:</strong> <em>#{I18n.t("activerecord.errors.models.lista_cargo.estados.#{ListaCargo.estado_candidaturas.hash.key(changeset['estado_candidaturas_cd'].last)}")}</em>") if changeset.key?('estado_candidaturas_cd')
    cambios_estado.push("<strong>Se actualizó el estado de adhesiones de:</strong> <em>#{I18n.t("activerecord.errors.models.lista_cargo.estados.#{ListaCargo.estado_adhesiones.hash.key(changeset['estado_adhesiones_cd'].first)}")}</em> <strong>a:</strong> <em>#{I18n.t("activerecord.errors.models.lista_cargo.estados.#{ListaCargo.estado_adhesiones.hash.key(changeset['estado_adhesiones_cd'].last)}")}</em>") if changeset.key?('estado_adhesiones_cd')
    cambios_estado.join('<br>').html_safe
  end

  def cambios_observacion
    nombre_observacion = I18n.t("activerecord.attributes.candidaturas.descripcion_observaciones.#{object.reify.referencia_observacion.downcase}")
    nombre_candidato = object.reify.nombre_candidato

    if changeset.key?('comentarios')
      "<strong>Se actualizaron los comentarios de la observación:</strong> <em>#{nombre_observacion}</em> <strong>del candidato:</strong> <em>#{nombre_candidato}</em>. <strong>Comentarios:</strong> <em>#{changeset[:comentarios].last}</em>".html_safe
    else
      "<strong>Se actualizó el estado de la observación:</strong> <em>#{nombre_observacion}</em> <strong>del candidato:</strong> <em>#{nombre_candidato}</em>. <strong>Estado:</strong> <em>#{Observacion.estados.key(changeset[:estado_cd].last).to_s.split('_').map(&:capitalize).join(' ')}</em>".html_safe
    end
  end

  def cambios_persona
    cambios_modelo(VersionListaCargoDecorator.campos_actualizables_persona).unshift("<strong>Persona</strong> <em>#{item.try(:nombres) || object_deserialized['nombres']} #{item.try(:apellidos) || object_deserialized['apellidos']}</em> ").join('').html_safe
  end
  # -----------------------------------------------------------------------------------------------------------------------------

  # DESTROYS --------------------------------------------------------------------------------------------------------------------
  def eliminado_adhesion
    "<strong>Eliminado de la lista de adhesiones:</strong> <em>#{nombre_persona(reify.persona_id)}</em>".html_safe
  end

  def eliminado_archivo_persona
    "<strong>Se eliminó un archivo de persona: </strong> <em>#{I18n.t("activerecord.attributes.archivos_persona.tipo_archivo.#{reify.tipo_archivo}")}</em> <strong>para el candidato:</strong> <em>#{nombre_persona(reify.persona_id)}</em>".html_safe
  end

  def eliminado_candidatura
    "<strong>Eliminado de la lista de precandidaturas:</strong> <em>#{nombre_persona(reify.persona_id)}</em>".html_safe
  end

  def eliminado_masivo_adhesion
    '<strong>Eliminado completo de la lista de adhesiones</strong>'.html_safe
  end

  def eliminado_masivo_candidatura
    '<strong>Eliminado completo de la lista de precandidaturas</strong>'.html_safe
  end
  # -----------------------------------------------------------------------------------------------------------------------------

  # IMPORTS ---------------------------------------------------------------------------------------------------------------------
  def importar_adhesion
    '<strong>Importación de Adhesiones</strong>'.html_safe
  end

  def importar_candidatura
    '<strong>Importación de Precandidaturas</strong>'.html_safe
  end
  # -----------------------------------------------------------------------------------------------------------------------------

  def nombre_persona(id)
    persona = Persona.find(id)
    "#{persona.nombres} #{persona.apellidos}"
  end

  def nombre_persona_con_domicilio(id_domicilio)
    persona = Persona.find_by(domicilio_id: id_domicilio)
    "#{persona.nombres} #{persona.apellidos}"
  end

  def cambios_modelo(campos_actualizables)
    campos_actualizables.map do |campo|
      if changeset.keys.include?(campo) && changeset[campo].last.present?
        "<br><strong>#{campo.split('_').map(&:capitalize).join(' ')}:</strong> <em>#{send("render_#{campo}")}</em> "
      end
    end.select(&:present?)
  end

  (campos_actualizables_persona + campos_actualizables_domicilio + campos_actualizables_candidatura).reject { |campo| %w(fecha_nacimiento calle).include?(campo) }.each do |campo|
    define_method("render_#{campo}") do
      changeset[campo].last
    end
  end

  def render_fecha_nacimiento
    changeset['fecha_nacimiento'].last.strftime('%d/%m/%Y')
  end

  def render_calle
    Callejero.instance.calle(changeset['calle'].last.to_i).try(:[], 'nombre')
  end

  def accion_sobre_modelo(accion)
    send("#{accion}_#{item_type.split(/(?=[A-Z])/).map(&:downcase).join('_')}")
  end

end
