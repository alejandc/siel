class VersionListaDecorator < Draper::Decorator
  delegate_all

  def cambios
    case event
    when 'create'
      "<strong>- Lista creada con nombre</strong> <em>#{changeset['nombre'].last}</em> <strong>y cargos</strong> <em>#{cargos_habilitados}</em>".html_safe
    when 'update'
      mostrar_cambios.join(' ').html_safe
    else
      'Lista eliminada'
    end
  end

  private

  def mostrar_cambios
    campos_actualizables.map do |campo|
      send("mostrar_#{campo}") if changeset.key?(campo)
    end.compact
  end

  def mostrar_nombre
    "<strong>-</strong> #{I18n.t('papertrail.lista.nombre', anterior: changeset['nombre'].first, actual: changeset['nombre'].second)}"
  end

  def mostrar_estado
    "<strong>-</strong> #{I18n.t('papertrail.lista.estado', anterior: I18n.t("activerecord.errors.models.lista.estados.#{changeset['estado'].first.downcase}"), actual: I18n.t("activerecord.errors.models.lista.estados.#{changeset['estado'].second.downcase}"))}"
  end

  def mostrar_data_info
    cargos_habilitados_aux = cargos_habilitados
    cargos_deshabilitados_aux = cargos_deshabilitados
    [cargos_habilitados_aux.empty? ? '' : "<strong>#{I18n.t('papertrail.lista.data_info_habilitados')}</strong> <em>#{cargos_habilitados_aux}</em>",
     cargos_deshabilitados_aux.empty? ? '' : "<strong>#{I18n.t('papertrail.lista.data_info_deshabilitados')}</strong> <em>#{cargos_deshabilitados_aux}</em>"].select(&:present?).map { |mensaje| "<strong>-</strong> #{mensaje}" }.join(' ')
  end

  def campos_actualizables
    %w(nombre data_info estado)
  end

  def cargos_habilitados
    changeset['data_info'].last.keys.reject { |cargo| changeset['data_info'].first.keys.include?(cargo) }.map { |cargo| Cargo::CARGOS[cargo.to_sym] }.join(', ')
  end

  def cargos_deshabilitados
    changeset['data_info'].first.keys.reject { |cargo| changeset['data_info'].last.keys.include?(cargo) }.map { |cargo| Cargo::CARGOS[cargo.to_sym] }.join(', ')
  end

end
