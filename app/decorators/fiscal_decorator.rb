class FiscalDecorator < Draper::Decorator
  delegate_all

  def nombre_completo
    "#{persona.nombres} #{persona.apellidos}"
  end

  def boton_eliminar
    h.button_to '', method: :delete, class: 'pull-right boton-icono no-padding-left', onclick: "boton_eliminar_fiscal(#{id});" do
      '<i class="glyphicon glyphicon-remove text-danger pull-right" style="margin: 0 3px" data-toggle="tooltip" data-placement="top" title="Eliminar"></i>'.html_safe
    end
  end

end
