class ObservacionDecorator < Draper::Decorator
  delegate_all

  def orden
    observable.decorate.orden(con_descripcion: true)
  end

  def descripcion
    archivo_persona = archivos_persona.detect { |archivo_persona_aux| archivo_persona_aux.tipo_archivo == observacion.referencia_observacion.downcase }
    observacion = Validators::ValidadorCandidatura::VALIDACIONES[referencia_observacion.to_sym]

    if observacion.present?
      archivo_persona.present? ? observacion[:TITULO_VERIFICACION].call(observacion[:PARAMETROS_TITULO].call(observable)) : observacion[:TITULO].call(observacion[:PARAMETROS_TITULO].call(observable))
    else
      ''
    end
  end

  def documento_adjunto
    archivo_persona = archivos_persona.detect { |archivo_persona_aux| archivo_persona_aux.tipo_archivo == observacion.referencia_observacion.downcase }
    h.link_to(archivo_persona.archivo.nombre, "#{h.root_url}/archivos/descargar/#{archivo_persona.archivo.id}") if archivo_persona.present?
  end

  def color_estado
    case estado_cd
    when Observacion.estados[:pendiente]
      'warning'
    when Observacion.estados[:subsanada]
      'success'
    when Observacion.estados[:no_subsanada]
      'danger'
    end
  end

  def comentarios
    if observacion.comentarios.present?
      observacion.comentarios.split("\n").map { |comentario| "<p>#{comentario}</p>" }.join('').delete("\r")
    else
      'No hay comentarios'
    end
  end

end
