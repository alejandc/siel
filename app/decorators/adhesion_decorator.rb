class AdhesionDecorator < Draper::Decorator
  delegate_all

  def nombre_completo
    "#{persona_nombres} #{persona_apellidos}"
  end

  def boton_eliminar(pagina_adhesiones, adhesiones_search)
    h.button_to '', method: :delete, class: 'pull-right boton-icono no-padding-left', onclick: "boton_eliminar(event, #{lista_cargo_id}, 'adhesion', #{id}, #{pagina_adhesiones}, #{adhesiones_search.to_json});" do
      '<i class="glyphicon glyphicon-remove text-danger pull-right" style="margin: 0 3px" data-toggle="tooltip" data-placement="top" title="Eliminar"></i>'.html_safe
    end
  end

  def icono_afiliado
    validacion_afiliado = adhesion.validar_afiliacion
    "<i class='text-info #{validacion_afiliado.first}' data-toggle='tooltip' data-placement='top' title='#{validacion_afiliado.second}'></i>".html_safe if validar_afiliacion
  end

end
