class EleccionDecorator < Draper::Decorator
  delegate_all

  def cargos_seleccionados_concatenados
    (cargos_seleccionados.inject([]) { |nombres, cargo| cargo.present? && !cargo.nombre.match(/comuna/i) ? nombres.push(cargo.nombre) : nombres } + (eleccion.cargos_seleccionados.detect { |cargo| cargo.nombre.match(/comuna/i) } ? ['Comuneros'] : [''])).reject(&:blank?).join(', ')
  end

  def link_nombres_archivos_expedientes
    eleccion.nombres_archivos_expedientes.map do |nombre|
      h.link_to nombre, "#{EXP_BASE_URL}/#{nombre}", class: 'btn btn-default', target: '_blank'
    end.join(', ').html_safe
  end

  def eleccion_vigente
    eleccion.vigente? ? 'Sí' : 'No'
  end

  def tipo
    object.tipo.upcase
  end

end
