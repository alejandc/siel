class ListaDecorator < Draper::Decorator
  delegate_all

  def cargos_candidaturas_sin_validar
    concatenar_errores(lista_cargos_habilitados.estado_candidaturas_sin_validars)
  end

  def cargos_candidaturas_invalidos
    concatenar_errores(lista_cargos_habilitados.estado_candidaturas_con_errores)
  end

  def cargos_candidaturas_incompletos
    concatenar_errores(lista_cargos_habilitados.estado_candidaturas_incompleta)
  end

  def cargos_adhesiones_sin_validar
    concatenar_errores(lista_cargos_habilitados.estado_adhesiones_sin_validars)
  end

  def cargos_adhesiones_invalidos
    concatenar_errores(lista_cargos_habilitados.estado_adhesiones_con_errores)
  end

  def cargos_adhesiones_incompletos
    concatenar_errores(lista_cargos_habilitados.estado_adhesiones_incompleta)
  end

  def cargos_candidaturas_observadas
    concatenar_errores(lista_cargos_habilitados.con_alertas_en_candidaturas)
  end

  def cargos_adhesiones_observadas
    concatenar_errores(lista_cargos_habilitados.con_alertas_en_adhesiones)
  end

  def nombre_con_estado
    estado_lista = if oficializada_tsj?
                     'Lista Oficializada por la Autoridad de Aplicación'
                   elsif oficializada_junta?
                     'Finalizada por la Junta Electoral Partidiaría'
                   elsif cerrada?
                     'Cerrada'
                   elsif reabierta?
                     'Lista Reabierta'
                   else
                     'No Cerrada'
                   end

    "#{nombre.camelize} (#{estado_lista})"
  end

  private

  def concatenar_errores(cargos_con_errores)
    cargos_con_errores.map(&:cargo_nombre).join(', ').gsub(Cargo::CARGOS[:diputado], 'Legisladores')
  end

end
