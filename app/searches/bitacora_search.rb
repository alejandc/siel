require 'searchlight/adapters/action_view'

class BitacoraSearch < Searchlight::Search
  include Searchlight::Adapters::ActionView

  def base_query
    PaperTrail::Version.all
  end

  def search_lista_cargo_eq
    query.versiones_lista_cargo(options[:lista_cargo_eq])
  end

  def search_nombre_usuario_like
    query.nombre_usuario(options[:nombre_usuario_like])
  end

  def search_event_eq
    options[:event_eq] == '' ? query : query.event(options[:event_eq])
  end

  def search_item_type_eq
    options[:item_type_eq] == '' ? query : query.item_type(options[:item_type_eq])
  end

  def search_created_at_gteq
    query.created_at_gteq(DateTime.parse(options[:created_at_gteq]))
  end

  def search_created_at_lteq
    query.created_at_lteq(DateTime.parse(options[:created_at_lteq]))
  end
end
