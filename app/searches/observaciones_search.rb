require 'searchlight/adapters/action_view'

class ObservacionesSearch < Searchlight::Search
  include Searchlight::Adapters::ActionView

  def base_query
    Observacion.all
  end

  def search_lista_cargo_eq
    query.lista_cargo_id(options[:lista_cargo_eq]).join_candidaturas
  end

  def search_nombre_candidato_like
    query.nombre_candidato(options[:nombre_candidato_like]) if options[:nombre_candidato_like].present?
  end

  def search_referencia_observacion_eq
    query.referencia_observacion(options[:referencia_observacion_eq]) if options[:referencia_observacion_eq].present?
  end

  def search_subsanado_eq
    query.subsanado(options[:subsanado_eq]) if options[:subsanado_eq].present?
  end

end
