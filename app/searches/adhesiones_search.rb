require 'searchlight/adapters/action_view'

class AdhesionesSearch < Searchlight::Search
  include Searchlight::Adapters::ActionView

  def base_query
    @fuerza_id = options[:fuerza_id]
    @eleccion_id = options[:eleccion_id]
    Adhesion.all.join_personas
  end

  def search_lista_cargo_eq
    query.lista_cargo(options[:lista_cargo_eq])
  end

  def search_numero_documento_like
    query.numero_documento(options[:numero_documento_like])
  end

  def search_nombre_like
    query.nombre(options[:nombre_like])
  end

  def search_apellido_like
    query.apellido(options[:apellido_like])
  end

  def search_adhesiones_observadas_eq
    if options[:adhesiones_observadas_eq] == '1'
      cargo = ListaCargo.find(options[:lista_cargo_eq]).cargo
      cargo.tipo_cargo.instance_of?(TiposCargos::TipoCargoComunero) ? query.adhesiones_observados(cargo.numero_comuna.to_s) : query.adhesiones_multiples
    else
      query
    end
  end

  def search_adhesiones_afiliadas_eq
    options[:adhesiones_afiliadas_eq] == '1' ? query.adhesiones_afiliadas(@eleccion_id, Fuerza.partidos_en_alianza_con_fuerza(@eleccion_id, @fuerza_id).map(&:numero)) : query
  end
end
