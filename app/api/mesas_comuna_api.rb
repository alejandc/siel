class MesasComunaApi < DatosExternosApi

  base_uri SETTINGS[:mesas_comuna_api][:url]

  def self.mesas_por_comuna(numero_comuna)
    params = { comuna: numero_comuna }
    JSON.parse get('/mesas-comuna', query: params).body
  end

end
