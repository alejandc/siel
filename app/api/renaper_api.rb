class RenaperApi < DatosExternosApi

  base_uri SETTINGS[:renaper_api][:url]

  def self.ultimo_ejemplar(numero_documento, sexo)
    if Configuracion.last.habilitar_renaper_api
      params = { dni: numero_documento, sexo: sexo }
      JSON.parse get('/ultimo-ejemplar', query: params).body
    else 
      {'nroError': '503', 'message': 'Renaper API deshabilitada en configuración del Sistema' }
    end
  end

end
