class PadronApi < DatosExternosApi

  base_uri SETTINGS[:padron_api][:url]

  GENEROS = { MASCULINO: 'M', FEMENINO: 'F' }

  class << self
    def padrones
      JSON.parse get('').body
    end

    def buscar_persona(tipo_documento, numero_documento, sexo, padron_id = nil)
      padron_id_aux = padron_id.present? ? padron_id : Padrones.instance.ultimo_padron['id']
      params = { tipo_documento: tipo_documento, sexo: sexo, dni: numero_documento }

      response = get("/#{padron_id_aux}", query: params)

      personas = JSON.parse response.body
      response.success? && !personas.empty? ? personas.last : (raise PersonaNoExisteEnPadronError.new(tipo_documento, numero_documento, sexo))
    end

    def antiguedad_persona(tipo_documento, numero_documento, sexo)
      params = { tipo_documento: tipo_documento, sexo: sexo, dni: numero_documento }
      response = get('', query: params)

      JSON.parse response.body
    end
  end

end
