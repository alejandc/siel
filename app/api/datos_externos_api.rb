class DatosExternosApi
  include HTTParty

  headers 'x-access-app-id' => SETTINGS[:datos_externos_api][:headers][:x_access_app_id], 
          'x-access-token'  => SETTINGS[:datos_externos_api][:headers][:x_access_token]

end
