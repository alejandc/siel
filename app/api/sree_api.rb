class SreeApi
  include HTTParty

  base_uri SETTINGS[:sree_api][:url]
  headers 'x-access-app-id' => SETTINGS[:sree_api][:headers][:x_access_app_id], 'x-access-token' => SETTINGS[:sree_api][:headers][:x_access_token], 'accept' => SETTINGS[:sree_api][:headers][:accept]

  def self.busqueda_electores(tipo_documento, numero_documento, sexo, fecha_eleccion)
    if tipo_documento == 'DNI'
      params = { documento_numero: numero_documento.gsub(/[^0-9]/, ''), offset: 0, limit: 100, orderBy: 'apellido', estado: 'ACTIVO', fechaCalculoEstado: fecha_eleccion.strftime('%Y-%m-%d') } # yyyy-mm-dd
      persona = JSON.parse(get('/busqueda/electores', query: params).body)['rows'].first
      persona if persona.try(:[], 'sexo') == sexo
    end
  end

end
