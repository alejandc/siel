class CallejeroApi < DatosExternosApi

  base_uri SETTINGS[:callejero_api][:url]

  def self.calles
    JSON.parse get('/calles').body
  end

  def self.circuitos
    JSON.parse get('/circuitos').body
  end

  def self.secciones
    JSON.parse get('/secciones').body
  end

  def self.rangos_calles(calle_id, altura)
    params = { calle_id: calle_id, altura: altura }
    JSON.parse get('/rangos-calles', query: params).body
  end

end
