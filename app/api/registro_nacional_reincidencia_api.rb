class RegistroNacionalReincidenciaApi

  def self.alta_informe_antecedentes_penales(persona)
    if persona.valida_consulta_antecedentes_penales?
      nombres = persona.nombres.split(' ')
      nom1 = nombres.first
      nom2 = nil
      nom3 = nil

      if nombres.size > 1
        nom2 = nombres.second
        if nombres.size > 2
          nom3 = nombres.third
        end
      end

      apellidos = persona.apellidos.split(' ')
      ape1 = apellidos.first
      ape2 = nil

      if apellidos.size > 1
        ape2 = apellidos.second
      end

      connection = Excon.new('https://servicios.dnrec.jus.gov.ar/AlephServices/AltaInformeJson.aspx')
      res = connection.get(:query => {:json => { "sexoID" => persona.sexo, 
                                                 "prioridadID" => "1", 
                                                 "tipoDocumentoID" => 2,
                                                 "tipoDelitoID" => 515, 
                                                 "ape1" => I18n.transliterate(ape1), 
                                                 "ape2" => (ape2.present? ? I18n.transliterate(ape2) : ''), 
                                                 "nom1" => I18n.transliterate(nom1), 
                                                 "nom2" => (nom2.present? ? I18n.transliterate(nom2) : ''), 
                                                 "nom3" => (nom3.present? ? I18n.transliterate(nom3) : ''), 
                                                 "nroDocumento" => persona.numero_documento, 
                                                 "fechaNacimiento" => persona.fecha_nacimiento.strftime('%Y-%m-%d') + 'T00:00:00', 
                                                 "organismo" => "Tribunal Superior de Justicia de la Ciudad Autónoma de Buenos Aires", 
                                                 "nroCausa" => "PASO 2017", 
                                                 "paisOrganismoID" => 12, 
                                                 "provinciaOrganismoID" => 1, 
                                                 "localidadOrganismoID" => 0, 
                                                 "emailOrganismo" => "siel@tsjbaires.gob.ar" }.to_json})

      respuesta = res.data[:body].match(/{.+}/)[0]

      #TODO falta manejar el error en la peticion por falla del servidor externo o timeout

      if respuesta.present?
        return JSON.parse(respuesta)
      end

      nil
    end    
  end

  def self.consulta_informe_antecedentes_penales_pdf(referencia_id)
    res = Excon.new("https://servicios.dnrec.jus.gov.ar/AlephServices/GetInformeJson.aspx?id=#{referencia_id}").get
    respuesta = res.data[:body].match(/{.+}/)[0]

    if respuesta.present?
      return JSON.parse(respuesta)
    end

    nil
  end
  

end
