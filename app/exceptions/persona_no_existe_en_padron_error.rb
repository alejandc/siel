class PersonaNoExisteEnPadronError < StandardError
  attr_reader :tipo_documento, :numero_documento, :genero

  def initialize(tipo_documento, numero_documento, genero, mensaje = "La persona buscada no existe en el padrón")
    @tipo_documento = tipo_documento
    @numero_documento = numero_documento
    @genero = genero
    super(mensaje)
  end

end
