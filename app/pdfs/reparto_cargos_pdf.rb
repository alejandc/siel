require 'render_anywhere'
 
class RepartoCargosPdf
  include RenderAnywhere
 
  def initialize(importacion_escrutinio, fuerza, cargo)
    @importacion_escrutinio = importacion_escrutinio
    @fuerza = fuerza
    @cargo = cargo
    @lista_sugerida_ganadora = importacion_escrutinio.eleccion_paso? ? RepartoDHondt.lista_sugerida(importacion_escrutinio, fuerza, cargo) : RepartoDHondt.lista_ganadora(importacion_escrutinio, cargo)
  end
 
  def to_pdf
    kit = PDFKit.new(as_html, page_size: 'A4')
    kit.to_file("#{Rails.root}/tmp/reparto_cargos.pdf")
  end
 
  def filename
    "Reparto Cargos #{importacion_escrutinio.eleccion.descripcion}.pdf"
  end
 
  private
 
    attr_reader :importacion_escrutinio, :fuerza, :cargo, :lista_sugerida_ganadora
 
    def as_html
      render template: "importacion_escrutinios/reparto_cargos_pdf", layout: 'print_pdf_files', locals: { importacion_escrutinio: importacion_escrutinio, fuerza: fuerza, cargo: cargo, lista_sugerida_ganadora: lista_sugerida_ganadora }
    end
end