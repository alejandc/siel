﻿
class RepartoAnexo1Pdf < Prawn::Document

  	def initialize(eleccion, comunas)
    	super(page_size: "A4")

		eleccion_cargos = eleccion.eleccion_cargos.select { |ec| ec.seleccionado? }

		bounding_box([0, cursor], width: bounds.right, height: bounds.height) do

			page_header(eleccion)
	
			move_down 15
			text "Total de votos en la Ciudad para #{eleccion_cargos.map { |ec| ec.cargo.nombre }.to_sentence}", align: :center
	
			move_down 15		
			text "Total de Mesas: #{eleccion.cantidad_mesas}"
			text_box "Total de inscriptos: #{eleccion.cantidad_inscriptos}", at: [0, cursor], width: 300
			text_box "Porcentaje de votos: ### <--- ¿cómo se calcula?", at: [300, cursor], color: 'FF0000'

			move_down 50
			text "¿QUÉ ORDEN LE PONEMOS AL LISTADO?", color: 'ff0000'
			table(tabla_totales(eleccion, eleccion_cargos), width: bounds.width, cell_style: { inline_format: true }, column_widths: { 0 => 50, 2 => 100})

			eleccion_cargos.each do |ec|
				start_new_page
				cocientes_y_cargos(ec)

				start_new_page
				distribucion_cargos(ec)
			end

			comunas.each do |comuna|
				start_new_page
				total_votos_comuna(eleccion, eleccion_cargos, comuna)
			end
		end
	end

	def total_votos_comuna(eleccion, eleccion_cargos, comuna)
		page_header(eleccion)
		move_down 15
		text "Total de votos en la #{UnicodeUtils.upcase(comuna.nombre)} para #{eleccion_cargos.map { |ec| ec.cargo.nombre }.to_sentence}", align: :center

		move_down 15
		text "Total de Mesas: ---NO POSEEMOS ESTA APERTURA---", color: 'FF0000'
		text_box "Total de inscriptos: ---NO POSEEMOS ESTA APERTURA---", at: [0, cursor], width: 300
		text_box "Porcentaje de votos: ### <--- ¿cómo se calcula?", at: [300, cursor], color: 'FF0000'

		move_down 40
		table(tabla_totales_comuna(eleccion, eleccion_cargos, comuna), width: bounds.width, cell_style: { inline_format: true, size: 10 }, column_widths: { 0 => 50, 2 => 100})
	end

	def tabla_totales_comuna(eleccion, eleccion_cargos, comuna)
		header = [{content: "<b>Partido o Alianza</b>", colspan: 2, align: :center}]
		eleccion_cargos.each { |ec| header << {content: "<b>#{ec.cargo.nombre}</b>", align: :center} }
		tabla = [header]


		eleccion.fuerzas.sort { |f1, f2| f1.numero <=> f2.numero }.each do |f|
			fila = [{content: f.numero.to_s, align: :center}, f.nombre]
			eleccion_cargos.each { |ec| fila << {content: ec.resultados_elecciones.select { |r| r.fuerza == f && r.comuna == comuna }.sum(&:cantidad_votos).to_s, align: :center} }
			tabla << fila
		end

		fila_en_blanco = ['', 'VOTOS EN BLANCO']
		eleccion_cargos.each do |ec|
			fila_en_blanco << {content: eleccion.resultados_elecciones.select { |r| r.en_blanco? && r.eleccion_cargo == ec && r.comuna == comuna }.sum(&:cantidad_votos).to_s, align: :center}
		end
		tabla << fila_en_blanco

		fila_nulos = ['', 'VOTOS NULOS']
		eleccion_cargos.each do |ec|
			fila_nulos << {content: eleccion.resultados_elecciones.select { |r| r.nulos? && r.eleccion_cargo == ec && r.comuna == comuna }.sum(&:cantidad_votos).to_s, align: :center}
		end
		tabla << fila_nulos

		fila_totales = ['', 'TOTALES']
		eleccion_cargos.each do |ec|
			fila_totales << {content: eleccion.resultados_elecciones.select { |r| r.eleccion_cargo == ec && r.comuna == comuna }.sum(&:cantidad_votos).to_s, align: :center}
		end
		tabla << fila_totales

		tabla
	end

	def distribucion_cargos(eleccion_cargo)
		page_header(eleccion_cargo.eleccion)
		move_down 15
		text "Distribución de cargos", align: :center

		move_down 15
		text "Cargo: #{UnicodeUtils.upcase(eleccion_cargo.cargo.nombre)}"
		text "Método: #{UnicodeUtils.upcase(eleccion_cargo.cargo.get_reparto.nombre)}"
		text "Electores: ### <-- ¿De dónde se toma este dato?", color: 'FF0000', style: :bold
		text "Umbral: 0 <-- ¿Es uno por cargo, porque tenemos uno solo para toda la elección?", color: 'FF0000', style: :bold
		text "Cantidad: #{eleccion_cargo.cantidad_escanios}"

		tabla = [[{content: '<b>Partido o Alianza</b>', align: :center, colspan: 2}, {content: '<b>Votos</b>', align: :center}, {content: '<b>Adjudicados</b>', align: :center}]]
		fuerzas_ganadoras = eleccion_cargo.ganadoras.map { |c| c.fuerza }.uniq.sort { |a, b| b.cantidad_votos(eleccion_cargo) <=> a.cantidad_votos(eleccion_cargo) }

		tabla += fuerzas_ganadoras.map do |f|
			[{content: f.numero.to_s, align: :center}, UnicodeUtils.upcase(f.nombre), {content: f.cantidad_votos(eleccion_cargo).to_s, align: :center}, {content: f.cantidad_escanios_adjudicados(eleccion_cargo).to_s, align: :center}]
		end

		move_down 20
		table(tabla, width: bounds.width, cell_style: { inline_format: true, size: 10 }, column_widths: {0 => 30, 2 => 50, 3 => 120})
	end

	def cocientes_y_cargos(eleccion_cargo)
		page_header(eleccion_cargo.eleccion)
		move_down 15
		text "Cocientes y cargos", align: :center

		move_down 15
		text "Cargo: #{UnicodeUtils.upcase(eleccion_cargo.cargo.nombre)}"
		text "Método: #{UnicodeUtils.upcase(eleccion_cargo.cargo.get_reparto.nombre)}"
		text "Electores: ### <-- ¿De dónde se toma este dato?", color: 'FF0000', style: :bold
		text "Umbral: 0 <-- ¿Es uno por cargo, porque tenemos uno solo para toda la elección?", color: 'FF0000', style: :bold
		text "Cantidad: #{eleccion_cargo.cantidad_escanios}"

		move_down 20
		table(tabla_cocientes(eleccion_cargo.ganadoras), width: bounds.width, cell_style: { inline_format: true, size: 10 }, column_widths: {0 => 30, 2 => 50, 3 => 60, 4 => 70, 5 => 70})
	end

	def tabla_cocientes(candidaturas)
		tabla = [[{content: '<b>PARTIDO O ALIANZA</b>', colspan: 2, align: :center}, {content: '<b>VOTOS</b>', align: :center}, {content: '<b>DIVISOR</b>', align: :center}, {content: '<b>COCIENTE</b>', align: :center}, {content: '<b>CANTIDAD</b>', align: :center}]]

		tabla += candidaturas.map do |c|
			[{content: c.fuerza.numero.to_s, align: :center}, UnicodeUtils.upcase(c.fuerza.nombre), {content: c.fuerza.cantidad_votos(c.eleccion_cargo).to_s, align: :center}, {content: c.divisor.to_s, align: :center}, {content: c.cociente.to_s, align: :center}, {content: '1', align: :center}]
		end

		tabla
	end

	def page_header(eleccion)
		move_down 30
		text "ANEXO I", align: :center, style: :bold
		text "CIUDAD AUTÓNOMA DE BUENOS AIRES", align: :center, style: :bold
		text "Elecciones del #{I18n.l(eleccion.fecha, format: :long)}", align: :center
	end

	def tabla_totales(eleccion, eleccion_cargos)
		header = [{content: "<b>Partido o Alianza</b>", colspan: 2, align: :center}]
		eleccion_cargos.each { |ec| header << {content: "<b>#{ec.cargo.nombre}</b>", align: :center} }

		tabla = [header]

		eleccion.fuerzas.sort { |f1, f2| f1.numero <=> f2.numero }.each do |f|
			fila = [{content: f.numero.to_s, align: :center}, f.nombre]
			eleccion_cargos.each { |ec| fila << {content: f.cantidad_votos(ec).to_s, align: :center} }
			tabla << fila
		end

		fila_en_blanco = ['', 'VOTOS EN BLANCO']
		eleccion_cargos.each do |ec|
			fila_en_blanco << {content: eleccion.resultados_elecciones.select { |r| r.en_blanco? && r.eleccion_cargo == ec}.sum(&:cantidad_votos).to_s, align: :center}
		end
		tabla << fila_en_blanco

		fila_nulos = ['', 'VOTOS NULOS']
		eleccion_cargos.each do |ec|
			fila_nulos << {content: eleccion.resultados_elecciones.select { |r| r.nulos? && r.eleccion_cargo == ec}.sum(&:cantidad_votos).to_s, align: :center}
		end
		tabla << fila_nulos

		fila_totales = ['', 'TOTALES']
		eleccion_cargos.each do |ec|
			fila_totales << {content: eleccion.resultados_elecciones.select { |r| r.eleccion_cargo == ec}.sum(&:cantidad_votos).to_s, align: :center}
		end
		tabla << fila_totales

		tabla
	end

end
