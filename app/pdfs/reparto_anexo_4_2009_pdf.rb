﻿
class RepartoAnexo42009Pdf < Prawn::Document

  	def initialize(eleccion)
    	super(page_size: "A4")

		eleccion_cargos = eleccion.eleccion_cargos.select { |ec| ec.seleccionado? }

		bounding_box([0, cursor], width: bounds.right, height: bounds.height) do
			eleccion_cargos.each do |ec|
				move_down 30
				text "Anexo IV - Adjudicación de bancas por lista", align: :center, style: :bold
		
				move_down 30
				table(tabla_adjudicacion(ec), width: bounds.width, cell_style: { inline_format: true, size: 9 }, column_widths: { 0 => 50, 2 => 70, 3 => 70})
				footer
			end
		end
	end

	def footer
		options = { at: [0, 0], width: bounds.width, color: "555555", size: 8, align: :center }
		number_pages '"2013 - Año del 30 aniversario de la vuelta a la democracia" Ley 4.194 / 4.453', options
	end

	def tabla_adjudicacion(eleccion_cargo)
		tabla = [[{content: '<b>Nros. De Lista</b>', align: :center}, {content: '<b>Nombres</b>', align: :center}, {content: '<b>Votos obtenidos</b>', align: :center}, {content: '<b>Bancas obtenidas</b>', align: :center}]]

		eleccion_cargo.fuerzas.select { |f| f.vigente? }.uniq.sort { |a, b| b.cantidad_votos(eleccion_cargo) <=> a.cantidad_votos(eleccion_cargo) }.each do |f|
			tabla << [{content: f.numero.to_s, align: :center}, f.nombre, {content: f.cantidad_votos(eleccion_cargo).to_s, align: :right}, {content: f.cantidad_escanios_adjudicados(eleccion_cargo).to_s, align: :right}]
		end

		tabla
	end

end
