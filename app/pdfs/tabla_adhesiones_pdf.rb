class TablaAdhesionesPdf < Prawn::Document

  def initialize(lista_cargo)
    super(page_size: "A4")

    path = lista_cargo.path_exportacion_adhesiones

    tabla_adhesiones = AdhesionDecorator.decorate_collection(lista_cargo.adhesiones).map do |adhesion|
      [Prawn::Table::Cell::Text.new(self, [0,0], content: adhesion.persona_numero_documento.to_s, align: :center, size: 9),
      Prawn::Table::Cell::Text.new(self, [0,0], content: adhesion.persona_nombres.to_s, align: :left, size: 9),
      Prawn::Table::Cell::Text.new(self, [0,0], content: adhesion.persona_apellidos.to_s, align: :left, size: 9),
      Prawn::Table::Cell::Text.new(self, [0,0], content: (adhesion.afiliado? ? 'SI' : 'NO'), align: :center, size: 9)]
    end.unshift([
                  Prawn::Table::Cell::Text.new(self, [0,0], content: 'Documento', align: :center, size: 9),
                  Prawn::Table::Cell::Text.new(self, [0,0], content: 'Nombre', align: :center, size: 9),
                  Prawn::Table::Cell::Text.new(self, [0,0], content: 'Apellido', align: :center, size: 9),
                  Prawn::Table::Cell::Text.new(self, [0,0], content: 'Afiliado', align: :center, size: 9)
                ])

    text "#{lista_cargo.fuerza_nombre} - #{lista_cargo.lista_nombre} - #{lista_cargo.cargo_nombre.gsub(Cargo::CARGOS[:diputado], 'Legisladores')}", :align => :center, :size => 18
    move_down 20
    text "Listado de Adhesiones", :align => :center, :size => 15
    move_down 20

    table(tabla_adhesiones, width: bounds.width)

    render_file(path)
  end

end