﻿
class RepartoAnexo2Pdf < Prawn::Document

  	def initialize(eleccion)
    	super(page_size: "A4")

		bounding_box([0, cursor], width: bounds.right, height: bounds.height) do

			eleccion_cargos = eleccion.eleccion_cargos.select { |ec| ec.seleccionado? }

			eleccion_cargos.each do |ec|
				move_down 30
				text "ANEXO II", align: :center, style: :bold

				move_down 20
				text "NÓMINA DE #{ec.cargo.nombre.upcase}", align: :center, style: :bold

				move_down 20
				table(tabla_candidatos(ec.ganadoras), width: bounds.width, cell_style: { inline_format: true, size: 10 }, column_widths: { 0 => 50, 4 => 50})
			end
		end

	end

	def tabla_candidatos(candidaturas)
		tabla = [[{content: '<b>Orden</b>', align: :center}, {content: '<b>Apellido</b>', align: :center}, {content: '<b>Nombre</b>', align: :center}, {content: '<b>Partido o Alianza</b>', align: :center}, {content: '<b>Lista</b>', align: :center}]]

		tabla += candidaturas.each_with_index.map do |c, i|
			[{content: (i + 1).to_s, align: :center}, UnicodeUtils.upcase(c.persona.apellidos), UnicodeUtils.upcase(c.persona.nombres), UnicodeUtils.upcase(c.fuerza.nombre), {content: c.fuerza.numero.to_s, align: :center}]
		end

		tabla
	end

end
