class TablasComparativasPdf < Prawn::Document

  def initialize(lista_cargo, originales, ordenadas)
    super(page_size: "A4", page_layout: :landscape)
    
    text "#{lista_cargo.fuerza_nombre.upcase} - #{lista_cargo.lista_nombre.upcase} - #{lista_cargo.cargo_nombre.gsub(Cargo::CARGOS[:diputado], 'Legisladores').upcase}", size: 18, align: :center
    define_grid(columns: 2, rows: 11, gutter: 10)
    
    imprimir_tabla([[1, 0], [10, 1]], "Lista Original", lista_cargo, originales, ordenadas)
    go_to_page(1)
    imprimir_tabla([[1, 1], [10, 2]], "Lista Sugerida", lista_cargo, ordenadas, originales)
  end

  def imprimir_tabla(posiciones, titulo, lista_cargo, candidaturas_render, candidaturas_comparar)
    tabla_candidaturas = []

    candidaturas_render.each_slice(lista_cargo.cargo_cantidad_minima).with_index do |listado_candidaturas, index|
      tabla_candidaturas.push((0..3).to_a.map { Prawn::Table::Cell::Text.new(self, [0,0], content: '', height: 20) }) if index == 1

      CandidaturaDecorator.decorate_collection(listado_candidaturas).each_with_index do |candidatura, i|
        pos_candidatura = (index * lista_cargo.cargo_cantidad_minima) + i
        tabla_candidaturas.push([Prawn::Table::Cell::Text.new(self, [0,0], content: candidatura.orden.to_s, background_color: (candidaturas_comparar[pos_candidatura].persona != candidaturas_render[pos_candidatura].persona ? candidatura.color_reordenado_pdf : "FFFCF8"), align: :center, size: 10),
        Prawn::Table::Cell::Text.new(self, [0,0], content: candidatura.persona_numero_documento.to_s, background_color: (candidaturas_comparar[pos_candidatura].persona != candidaturas_render[pos_candidatura].persona ? candidatura.color_reordenado_pdf : "FFFCF8"), align: :right, size: 10),
        Prawn::Table::Cell::Text.new(self, [0,0], content: candidatura.nombre_completo.to_s, background_color: (candidaturas_comparar[pos_candidatura].persona != candidaturas_render[pos_candidatura].persona ? candidatura.color_reordenado_pdf : "FFFCF8"), align: :left, size: 10),
        Prawn::Table::Cell::Text.new(self, [0,0], content: candidatura.persona_sexo.to_s, background_color: (candidaturas_comparar[pos_candidatura].persona != candidaturas_render[pos_candidatura].persona ? candidatura.color_reordenado_pdf : "FFFCF8"), align: :center, size: 10)])
      end
    end

    tabla_candidaturas.unshift([
                                 Prawn::Table::Cell::Text.new(self, [0,0], content: 'Orden', align: :center),
                                 Prawn::Table::Cell::Text.new(self, [0,0], content: 'DNI', align: :center),
                                 Prawn::Table::Cell::Text.new(self, [0,0], content: 'Precandidato/a', align: :center),
                                 Prawn::Table::Cell::Text.new(self, [0,0], content: 'Sexo', align: :center)
                               ])

    grid(posiciones.first, posiciones.second).bounding_box do
      text titulo, size: 14
      table(tabla_candidaturas, width: 370)
    end
  end

end