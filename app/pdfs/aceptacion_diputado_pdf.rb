﻿class AceptacionDiputadoPdf < Prawn::Document
  include CandidaturasHelper

  def initialize(candidatura)
    super(page_size: "A4")

	default_leading 5

    persona = candidatura.persona
    domicilio = persona.domicilio
    fuerza = candidatura.fuerza
    eleccion = candidatura.eleccion_cargo.eleccion


	candidatura_texto = (persona.sexo == "M") ? "DIPUTADO" : "DIPUTADA"

	bounding_box([0, cursor], width: bounds.right, height: bounds.height) do
		move_down 30		

		text "ACEPTACIÓN DE CANDIDATURA DE #{candidatura_texto}", align: :center
		move_down 30

		text "Tribunal Superior de Justicia:"
		move_down 30

		calle = (domicilio.nil? or domicilio.calle_referencia.nil? or domicilio.calle_referencia.empty?) ? '______________________________' : domicilio.calle
		numero = (domicilio.nil? or domicilio.numero.nil? or domicilio.numero.empty?) ? '_____________' : domicilio.numero
		piso = (domicilio.nil? or domicilio.piso.nil? or domicilio.piso.empty?) ? '' : (" piso "+domicilio.piso+" ")
		depto = (domicilio.nil? or domicilio.depto.nil? or domicilio.depto.empty?) ? '' : (" dpto. "+domicilio.depto+" ")		
		candidatura_titualaridad = (candidatura.titular?) ? "titular" : "suplente"
		fuerza_texto = (fuerza.tipo == "PARTIDO") ? "del partido" : "de la alianza"

		parrafo1 = <<EOS
#{persona.nombres} #{persona.apellidos}, DNI/LE/LC n° #{persona.numero_documento}, con domicilio real en la calle #{calle} n° 
#{numero}#{piso}#{depto}, manifiesto por la presente que acepto la candidatura a #{candidatura_texto.downcase} de la Ciudad Autónoma de Buenos Aires, 
en el lugar #{orden(candidatura)} #{candidatura_titualaridad} de la lista #{fuerza_texto} #{fuerza.nombre}, para las elecciones del día 
#{I18n.l(eleccion.fecha, format: :long)}.
EOS
		text parrafo1.gsub("\n",''), align: :justify
		move_down 10

		text "A tal fin detallo a continuación los siguientes datos:"
		move_down 10

		text "Fecha y lugar de nacimiento: #{I18n.l(persona.fecha_nacimiento) unless persona.fecha_nacimiento.nil?} #{persona.lugar_nacimiento}"
		move_down 10

		text "Nombre del padre: #{persona.nombre_padre}"
		move_down 10

		text "Nombre de la madre: #{persona.nombre_madre}"
		move_down 10

		text "Y acompaño fotocopias de mi documento cívico (DNI/LC/LE).<sup>1</sup>", inline_format: true
		move_down 10

		parrafo2 = <<EOS
Constituyo domicilio para la notificación y/o citación vinculada con todos los actos del proceso electoral en la calle
 #{calle} n° #{numero}#{piso}#{depto} de la Ciudad Autónoma de Buenos Aires.
EOS
		text parrafo2.gsub("\n",''), align: :justify
		move_down 100

		text "_______________________________", align: :right
		move_down 10
		text "Firma             ", align: :right

		parrafo_final = <<EOS
<sup>1</sup> Páginas del documento donde conste: a) Fotografía, b) datos personales (lugar y fecha de nacimiento, etc.), c) últimos dos 
domicilios (si tiene único domicilio acompañar también copia de la hoja en blanco correspondiente a "cambios de domicilio"), si 
son argentinos por opción, copia de las páginas con las anotaciones pertinentes. 
EOS

		text parrafo_final.gsub("\n", ''), align: :justify, valign: :bottom, inline_format: true
	end
  end

end