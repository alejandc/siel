require 'render_anywhere'
 
class ListaCargosGeneralPdf
  include RenderAnywhere
 
  def initialize(lista_cargo)
    @lista_cargo = lista_cargo
  end
 
  def to_pdf
    kit = PDFKit.new(as_html, page_size: 'A4')
    kit.to_file("#{Rails.root}/tmp/lista_general_#{I18n.transliterate(lista_cargo.fuerza.nombre.downcase).gsub(' ', '_')}.pdf")
  end
 
  def filename
    "Lista General #{lista_cargo.fuerza.nombre}.pdf"
  end
 
  private
 
    attr_reader :lista_cargo
 
    def as_html
      render template: "lista_cargos_generales/lista_cargos_pdf", layout: 'print_pdf_files', locals: { lista_cargo: lista_cargo }
    end
end
