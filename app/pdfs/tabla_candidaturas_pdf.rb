class TablaCandidaturasPdf < Prawn::Document

  def initialize(lista_cargo, candidaturas)
    super(page_size: "A4")

    tabla_candidaturas = []

    candidaturas.each_slice(lista_cargo.cargo_cantidad_minima).with_index do |listado_candidaturas, index|

      tabla_candidaturas.push((0..4).to_a.map { Prawn::Table::Cell::Text.new(self, [0,0], content: '', height: 20) }) if index == 1

      CandidaturaDecorator.decorate_collection(listado_candidaturas).each do |candidatura|
        tabla_candidaturas.push([Prawn::Table::Cell::Text.new(self, [0,0], content: candidatura.orden.to_s, background_color: candidatura.color_sexo_pdf, align: :center),
        Prawn::Table::Cell::Text.new(self, [0,0], content: candidatura.persona_numero_documento.to_s, background_color: candidatura.color_sexo_pdf, align: :center),
        Prawn::Table::Cell::Text.new(self, [0,0], content: candidatura.nombre_completo.to_s, background_color: candidatura.color_sexo_pdf, align: :center),
        Prawn::Table::Cell::Text.new(self, [0,0], content: candidatura.persona_denominacion.to_s, background_color: candidatura.color_sexo_pdf, align: :center),
        Prawn::Table::Cell::Text.new(self, [0,0], content: candidatura.persona_fecha_nacimiento.to_s, background_color: candidatura.color_sexo_pdf, align: :center)])
      end

    end

    tabla_candidaturas.unshift(['Posición', 'Documento', 'Nombre Completo', 'Conocido Como', 'Fecha Nacimiento'])

    table(tabla_candidaturas, width: bounds.width)
  end

end