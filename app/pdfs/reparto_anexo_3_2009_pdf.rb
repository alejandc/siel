﻿
class RepartoAnexo32009Pdf < Prawn::Document

  	def initialize(eleccion)
    	super(page_size: "A4")

		eleccion_cargos = eleccion.eleccion_cargos.select { |ec| ec.seleccionado? }

		bounding_box([0, cursor], width: bounds.right, height: bounds.height) do
			eleccion_cargos.each do |ec|
				move_down 30
				text "Anexo III - Cocientes de distribución de bancas", align: :center, style: :bold
		
				move_down 30
				table(tabla_cocientes(ec), width: bounds.width, cell_style: { inline_format: true, size: 9 }, column_widths: { 0 => 50, 2 => 80})
				footer
			end
		end
	end

	def footer
		options = { at: [0, 0], width: bounds.width, color: "555555", size: 8, align: :center }
		number_pages '"2013 - Año del 30 aniversario de la vuelta a la democracia" Ley 4.194 / 4.453', options
	end

	def tabla_cocientes(eleccion_cargo)
		tabla = [[{content: '<b>Nro. De orden</b>', align: :center}, {content: '<b>Lista</b>', align: :center}, {content: '<b>Cociente</b>', align: :center}]]

		eleccion_cargo.ganadoras.each_with_index do |c, i|
			tabla << [{content: (i + 1).to_s, align: :center}, c.fuerza.nombre, {content: formatear_cociente(c.cociente), align: :right}]
		end

		tabla
	end

	def formatear_cociente(numero)
  		partes = sprintf('%.4f', numero).split('.')
  		partes[0] = partes[0].reverse.gsub(%r{([0-9]{3}(?=([0-9])))}, "\\1.").reverse
  		partes.join(',')
	end

end
