﻿
class RepartoAnexo52009Pdf < Prawn::Document

  	def initialize(eleccion)
    	super(page_size: "A4")

		bounding_box([0, cursor], width: bounds.right, height: bounds.height) do

			eleccion_cargos = eleccion.eleccion_cargos.select { |ec| ec.seleccionado? }

			eleccion_cargos.each do |ec|
				move_down 15
				text "Anexo V - Legisladores electos", align: :center, style: :bold

				move_down 15
				table(tabla_candidatos(ec.ganadoras), width: bounds.width, cell_style: { inline_format: true, size: 9 }, column_widths: { 0 => 50, 3 => 80})

				footer
			end
		end

	end

	def footer
		options = { at: [0, 0], width: bounds.width, color: "555555", size: 8, align: :center }
		number_pages '"2013 - Año del 30 aniversario de la vuelta a la democracia" Ley 4.194 / 4.453', options
	end

	def tabla_candidatos(candidaturas)
		tabla = [[{content: '<b>Nro. De orden</b>', align: :center}, {content: '<b>Apellidos</b>', align: :center}, {content: '<b>Nombres</b>', align: :center}, {content: '<b>Nro. De documento cívico</b>', align: :center}]]

		tabla += candidaturas.each_with_index.map do |c, i|
			[{content: (i + 1).to_s, align: :center}, UnicodeUtils.upcase(c.persona.apellidos), UnicodeUtils.upcase(c.persona.nombres), {content: formatear_documento(c.persona.numero_documento), align: :right}]
		end

		tabla
	end

	def formatear_documento(numero)
  		numero.to_s.reverse.gsub(%r{([0-9]{3}(?=([0-9])))}, "\\1.").reverse
	end

end
