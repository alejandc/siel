﻿
class RepartoAnexo22009Pdf < Prawn::Document

  	def initialize(eleccion, comunas)
    	super(page_size: "A4")

		eleccion_cargos = eleccion.eleccion_cargos.select { |ec| ec.seleccionado? }

		bounding_box([0, cursor], width: bounds.right, height: bounds.height) do

			move_down 10
			text "Anexo II - Totales de votos por lista", align: :center, style: :bold
	
			move_down 30
			table(tabla_totales(eleccion, eleccion_cargos), width: bounds.width, cell_style: { inline_format: true, size: 9 }, column_widths: { 0 => 50, 2 => 60})
			footer

			comunas.each do |comuna|
				start_new_page

				move_down 10
				text "Anexo II - Totales de votos por lista en la COMUNA #{comuna.numero}", align: :center, style: :bold
				text "** excluídos los electores nacionales procesados sin condena, escrutados por la Cámara Nacional Electoral en Distrito Único", align: :center
		
				move_down 30
				table(tabla_totales_comuna(eleccion, eleccion_cargos, comuna), width: bounds.width, cell_style: { inline_format: true, size: 9 }, column_widths: { 0 => 50, 2 => 60})
				footer
			end

			start_new_page

			move_down 10
			text "Anexo II - Totales de votos informados por la Cámara Nacional Electoral de procesados sin condena, computados como Distrito Único", align: :center, style: :bold
	
			move_down 30
			table(tabla_totales_comuna(eleccion, eleccion_cargos, nil), width: bounds.width, cell_style: { inline_format: true, size: 9 }, column_widths: { 0 => 50, 2 => 60})
			footer
		end
	end

	def footer
		options = { at: [0, 0], width: bounds.width, color: "555555", size: 8, align: :center }
		number_pages '"2013 - Año del 30 aniversario de la vuelta a la democracia" Ley 4.194 / 4.453', options
	end

	def tabla_totales_comuna(eleccion, eleccion_cargos, comuna)
		header = [{content: '<b>Nros de Lista</b>', align: :center}, {content: '<b>Nombres</b>', align: :center}]
		eleccion_cargos.each { |ec| header << {content: '<b>Totales de votos por Lista</b>', align: :center} }

		tabla = [header]

		eleccion.fuerzas.select { |f| f.vigente? }.sort { |f1, f2| f1.numero <=> f2.numero }.each do |f|
			fila = [{content: f.numero.to_s, align: :center}, f.nombre]
			eleccion_cargos.each { |ec| fila << {content: ec.resultados_elecciones.select { |r| r.fuerza == f && r.comuna == comuna }.sum(&:cantidad_votos).to_s, align: :right} }
			tabla << fila
		end

		fila_totales = [{content: '<b>Total de votos positivos</b>', align: :center, colspan: 2}]
		eleccion_cargos.each do |ec|
			total = eleccion.resultados_elecciones.select { |r| r.eleccion_cargo == ec && r.comuna == comuna }.sum(&:cantidad_votos).to_s
			fila_totales << {content: "<b>#{total}</b>", align: :right}
		end
		tabla << fila_totales

		tabla
	end

	def tabla_totales(eleccion, eleccion_cargos)
		header = [{content: '<b>Nros de Lista</b>', align: :center}, {content: '<b>Nombres</b>', align: :center}]
		eleccion_cargos.each { |ec| header << {content: '<b>Totales de votos por Lista</b>', align: :center} }

		tabla = [header]

		eleccion.fuerzas.select { |f| f.vigente? }.sort { |f1, f2| f1.numero <=> f2.numero }.each do |f|
			fila = [{content: f.numero.to_s, align: :center}, f.nombre]
			eleccion_cargos.each { |ec| fila << {content: f.cantidad_votos(ec).to_s, align: :right} }
			tabla << fila
		end

		fila_totales = [{content: '<b>Total de votos positivos</b>', align: :center, colspan: 2}]
		eleccion_cargos.each do |ec|
			total = eleccion.resultados_elecciones.select { |r| r.eleccion_cargo == ec}.sum(&:cantidad_votos).to_s
			fila_totales << {content: "<b>#{total}</b>", align: :right}
		end
		tabla << fila_totales

		tabla
	end

end
