﻿class UsuarioMailer < ActionMailer::Base
  default from: DEFAULT_FROM
  layout 'siel_mail'

  def clave_blanqueada(usuario)
    @usuario = usuario

    mail(to: usuario.email, subject: "SIEL - Su contraseña se ha cambiado con éxito")
  end

  def welcome_email(usuario)
    @usuario = usuario

    mail(to: usuario.email, subject: 'SIEL - Nuevo acceso a extranet')
  end

  def updated(usuario)
    @usuario = usuario

    mail(to: usuario.email, subject: "SIEL - Información modificada")
  end

end
