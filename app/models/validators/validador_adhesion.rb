class Validators::ValidadorAdhesion
  attr_accessor :adhesion

  VALIDACIONES = {
    LISTAS_MULTIPLES: { NOMBRE: 'LISTAS_MULTIPLES', ICONO: 'fa fa-list-ul', TITULO: I18n.t('activerecord.errors.models.adhesion.observaciones.listas_multiples') },
    COMUNA: { NOMBRE: 'COMUNA', ICONO: 'fa fa-home', TITULO: I18n.t('activerecord.errors.models.adhesion.observaciones.comuna') },
    AFILIACION: { NOMBRE: 'AFILIACION', ICONO: 'fa fa-address-card', TITULO: I18n.t('activerecord.errors.models.adhesion.observaciones.afiliacion') }
  }

  def initialize(adhesion)
    self.adhesion = adhesion
  end

  def observaciones
    [
      estructura_validacion(:LISTAS_MULTIPLES, adhesion.adhesiones_multiples),
      estructura_validacion(:COMUNA, adhesion.cargo.tipo_cargo.instance_of?(TiposCargos::TipoCargoComunero) && !adhesion.pertenece_a_comuna?)
    ].compact
  end

  def validar_afiliacion
    icono, titulo = componentes_validacion(VALIDACIONES, :AFILIACION)
    [icono, titulo] if adhesion.afiliado?
  end

  private

  def componentes_validacion(tipo_validacion, nombre_validacion)
    [tipo_validacion[nombre_validacion][:ICONO], tipo_validacion[nombre_validacion][:TITULO], tipo_validacion[nombre_validacion][:NOMBRE]]
  end

  def estructura_validacion(nombre_validacion, condicion)
    icono, titulo = componentes_validacion(VALIDACIONES, nombre_validacion)
    [icono, titulo] if condicion
  end

end
