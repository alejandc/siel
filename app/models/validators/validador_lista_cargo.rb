class Validators::ValidadorListaCargo
  attr_accessor :lista_cargo, :candidatos_observados

  VALIDACIONES_CANDIDATURAS_BLOQUEANTES = {
    CUPO_CARGOS: { NOMBRE: 'CUPO_CARGOS', ICONO: 'fa fa-users', TITULO: -> (cantidad_actual, cantidad_minima) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.cupo_cargos', cantidad_actual: cantidad_actual, cantidad_minima: cantidad_minima) } },
    CUPO_GENEROS: { NOMBRE: 'CUPO_GENEROS', ICONO: 'fa fa-venus-mars', TITULO: I18n.t('activerecord.errors.models.lista_cargo.observaciones.cupo_generos') },
    AL_MENOS_UNA_CANDIDATURA: { NOMBRE: 'AL_MENOS_UNA_CANDIDATURA', ICONO: 'fa fa-user', TITULO: I18n.t('activerecord.errors.models.lista_cargo.observaciones.al_menos_una_candidatura') }
  }

  VALIDACIONES_CANDIDATURAS_NO_BLOQUEANTES = {
    SUPLENTES_FALTANTES: { NOMBRE: 'SUPLENTES_FALTANTES', ICONO: 'fa fa-users', TITULO: -> (cantidad_suplentes_faltantes, cantidad_suplentes_requeridos) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.suplentes_faltantes', cantidad_suplentes_faltantes: cantidad_suplentes_faltantes, cantidad_suplentes_requeridos: cantidad_suplentes_requeridos) } },
    FOTO_PRIMER_CANDIDATO: { NOMBRE: 'FOTO_PRIMER_CANDIDATO', ICONO: 'fa fa-hourglass', TITULO: I18n.t('activerecord.errors.models.lista_cargo.observaciones.foto_primer_candidato') },
    EDAD_MINIMA: { NOMBRE: 'EDAD_MINIMA', ICONO: 'fa fa-address-card', TITULO: -> (parametros) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.edad_minima', cantidad_candidatos_con_observacion: parametros.first, edad_minima: parametros.second) } },
    CAMPOS_FALTANTES: { NOMBRE: 'CAMPOS_FALTANTES', ICONO: 'fa fa-info-circle', TITULO: -> (parametros) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.campos_faltantes', cantidad_candidatos_con_observacion: parametros.first) } },
    CANDIDATOS_SIN_FOTOCOPIA_DNI: { NOMBRE: 'CANDIDATOS_SIN_FOTOCOPIA_DNI', ICONO: 'fa fa-file-o', TITULO: -> (parametros) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.candidatos_sin_fotocopia_dni', cantidad_candidatos_con_observacion: parametros.first) } },
    CANDIDATOS_SIN_ACEPTACION_CANDIDATURA: { NOMBRE: 'CANDIDATOS_SIN_ACEPTACION_CANDIDATURA', ICONO: 'fa fa-file-text-o', TITULO: -> (parametros) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.candidatos_sin_aceptacion_candidatura', cantidad_candidatos_con_observacion: parametros.first) } },
    CANDIDATOS_SIN_CERTIFICADO_REINCIDENCIA_CRIMINAL: { NOMBRE: 'CANDIDATOS_SIN_CERTIFICADO_REINCIDENCIA_CRIMINAL', ICONO: 'fa fa-list-alt', TITULO: -> (parametros) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.candidatos_sin_certificado_reincidencia_criminal', cantidad_candidatos_con_observacion: parametros.first) } },
    REPETIDOS_EN_LISTA: { NOMBRE: 'REPETIDOS_EN_LISTA', ICONO: 'fa fa-tags', TITULO: -> (parametros) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.repetidos_en_lista', cantidad_candidatos_con_observacion: parametros.first) } },
    INCLUIDOS_PADRON: { NOMBRE: 'INCLUIDOS_PADRON', ICONO: 'fa fa-users', TITULO: -> (parametros) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.incluidos_padron', cantidad_candidatos_con_observacion: parametros.first) } },
    CANDIDATURAS_MULTIPLES: { NOMBRE: 'CANDIDATURAS_MULTIPLES', ICONO: 'fa fa-address-book-o', TITULO: -> (parametros) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.candidaturas_multiples', cantidad_candidatos_con_observacion: parametros.first) } },
    DEUDORES_ALIMENTARIOS: { NOMBRE: 'DEUDORES_ALIMENTARIOS', ICONO: 'fa fa-cutlery', TITULO: -> (parametros) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.deudores_alimentarios', cantidad_candidatos_con_observacion: parametros.first) } },
    REINCIDENCIAS_CARGOS: { NOMBRE: 'REINCIDENCIAS_CARGOS', ICONO: 'fa fa-hand-paper-o', TITULO: -> (parametros) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.reincidencias_cargos', cantidad_candidatos_con_observacion: parametros.first) } },
    INHABILITADOS: { NOMBRE: 'INHABILITADOS', ICONO: 'fa fa-ban', TITULO: -> (parametros) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.inhabilitados', cantidad_candidatos_con_observacion: parametros.first) } },
    ANTIGUEDAD_CIUDADANIA_PERSONAS: { NOMBRE: 'ANTIGUEDAD_CIUDADANIA_PERSONAS', ICONO: 'fa fa-flag', TITULO: -> (parametros) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.antiguedad_ciudadania_personas', cantidad_candidatos_con_observacion: parametros.first, antiguedad_minima: parametros.second) } },
    ANTIGUEDAD_RESIDENCIA_PERSONAS: { NOMBRE: 'ANTIGUEDAD_RESIDENCIA_PERSONAS', ICONO: 'fa fa-home', TITULO: -> (parametros) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.antiguedad_residencia_personas', cantidad_candidatos_con_observacion: parametros.first, antiguedad_minima: parametros.second, residencia: parametros.third) } }
  }

  VALIDACIONES_ADHESIONES_BLOQUEANTES = {
    CONFORMIDAD_ADHESIONES: { NOMBRE: 'CONFORMIDAD_ADHESIONES', ICONO: 'fa fa-file-word-o', TITULO: I18n.t('activerecord.errors.models.lista_cargo.observaciones.conformidad_adhesiones') },
    MINIMO_ADHESIONES: { NOMBRE: 'MINIMO_ADHESIONES', ICONO: 'fa fa-users', TITULO: -> (faltantes) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.minimo_adhesiones', faltantes: faltantes) } },
    PORCENTAJE_AFILIADOS: { NOMBRE: 'PORCENTAJE_AFILIADOS', ICONO: 'fa fa-address-card', TITULO: -> (porcentaje_actual, porcentaje_requerido) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.porcentaje_afiliados', porcentaje_actual: porcentaje_actual, porcentaje_requerido: porcentaje_requerido) } },
    AL_MENOS_UNA_ADHESION: { NOMBRE: 'AL_MENOS_UNA_ADHESION', ICONO: 'fa fa-user', TITULO: I18n.t('activerecord.errors.models.lista_cargo.observaciones.al_menos_una_adhesion') }
  }

  VALIDACIONES_ADHESIONES_NO_BLOQUEANTES = {
    ADHERENTES_MULTIPLES: { NOMBRE: 'ADHERENTES_MULTIPLES', ICONO: 'fa fa-list-ul', TITULO: -> (cantidad) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.adherentes_multiples', cantidad: cantidad) } },
    ADHERENTES_CONFLICTO_COMUNA: { NOMBRE: 'ADHERENTES_CONFLICTO_COMUNA', ICONO: 'fa fa-home', TITULO: -> (cantidad) { I18n.t('activerecord.errors.models.lista_cargo.observaciones.adherentes_conflicto_comuna', cantidad: cantidad) } }
  }

  def initialize(lista_cargo)
    self.lista_cargo = lista_cargo
  end

  def observaciones_candidaturas_bloqueantes
    [
      validar_al_menos_una_candidatura
    ].compact
  end

  def observaciones_candidaturas_no_bloqueantes
    [
      validar_cupo_cargos,
      validar_cupo_generos,
      validar_suplentes_faltantes,
      estructura_validacion_candidaturas_no_bloqueante(:EDAD_MINIMA, /EDAD_MINIMA/, parametros: [lista_cargo.cargo_edad_requerida]),
      estructura_validacion_candidaturas_no_bloqueante(:CAMPOS_FALTANTES, /CAMPOS_FALTANTES/),
      estructura_validacion_candidaturas_no_bloqueante(:CANDIDATOS_SIN_FOTOCOPIA_DNI, /FOTOCOPIA_DNI/),
      estructura_validacion_candidaturas_no_bloqueante(:CANDIDATOS_SIN_ACEPTACION_CANDIDATURA, /ACEPTACION_CANDIDATURA/),
      estructura_validacion_candidaturas_no_bloqueante(:CANDIDATOS_SIN_CERTIFICADO_REINCIDENCIA_CRIMINAL, /CERTIFICADO_REINCIDENCIA_CRIMINAL/),
      estructura_validacion_candidaturas_no_bloqueante(:REPETIDOS_EN_LISTA, /ESTA_EN_LISTA/),
      estructura_validacion_candidaturas_no_bloqueante(:INCLUIDOS_PADRON, /INCLUIDO_PADRON/),
      estructura_validacion_candidaturas_no_bloqueante(:CANDIDATURAS_MULTIPLES, /PERTENECE_OTRA_LISTA/),
      estructura_validacion_candidaturas_no_bloqueante(:DEUDORES_ALIMENTARIOS, /DEUDOR_ALIMENTARIO/),
      estructura_validacion_candidaturas_no_bloqueante(:REINCIDENCIAS_CARGOS, /REINCIDENCIA_CARGO/),
      estructura_validacion_candidaturas_no_bloqueante(:INHABILITADOS, /INHABILITADO/),
      estructura_validacion_candidaturas_no_bloqueante(:ANTIGUEDAD_CIUDADANIA_PERSONAS, /ANTIGUEDAD_CIUDADANIA_PERSONA/, parametros: [lista_cargo.cargo_anios_residencia]),
      estructura_validacion_candidaturas_no_bloqueante(:ANTIGUEDAD_RESIDENCIA_PERSONAS, /ANTIGUEDAD_RESIDENCIA_PERSONA/, parametros: [lista_cargo.cargo_anios_residencia, (lista_cargo.cargo.tipo_cargo.instance_of?(TiposCargos::TipoCargoComunero) ? 'residencia en la comuna' : 'residencia en la Ciudad de Buenos Aires')])
    ].compact
  end

  def observaciones_adhesiones_bloqueantes
    [
      validar_al_menos_una_adhesion
    ].compact
  end

  def observaciones_adhesiones_no_bloqueantes
    [
      validar_conformidad_adhesiones,
      validar_minimo_adhesiones,
      validar_porcentaje_afiliados,
      estructura_validacion_adherentes_no_bloqueante(:ADHERENTES_MULTIPLES, lista_cargo.adhesiones_multiples, true),
      estructura_validacion_adherentes_no_bloqueante(:ADHERENTES_CONFLICTO_COMUNA, lista_cargo.adhesiones_conflicto_comuna, lista_cargo.cargo.tipo_cargo.instance_of?(TiposCargos::TipoCargoComunero))
    ].compact
  end

  #---------------------------------------------------------------------------------------#
  # CANDIDATURAS
  #---------------------------------------------------------------------------------------#
  def validar_cupo_cargos
    icono, titulo, nombre = componentes_validacion(VALIDACIONES_CANDIDATURAS_BLOQUEANTES, :CUPO_CARGOS)
    cantidad_minima_cargo = lista_cargo.cargo_cantidad_minima
    cantidad_actual_cargo = lista_cargo.candidaturas.count
    [icono, observacion_con_icono(icono, titulo.call(cantidad_actual_cargo, cantidad_minima_cargo)), nombre] if cantidad_actual_cargo < cantidad_minima_cargo
  end

  def validar_cupo_generos
    icono, titulo, nombre = componentes_validacion(VALIDACIONES_CANDIDATURAS_BLOQUEANTES, :CUPO_GENEROS)
    [icono, observacion_con_icono(icono, titulo), nombre] if !lista_cargo.cumple_ley_cupos? || lista_cargo.consecutivo_incorregible?
  end

  def validar_suplentes_faltantes
    icono, titulo, nombre = componentes_validacion(VALIDACIONES_CANDIDATURAS_NO_BLOQUEANTES, :SUPLENTES_FALTANTES)
    cantidad_suplentes_faltantes = lista_cargo.cantidad_suplentes_faltantes
    cantidad_suplentes_requeridos = lista_cargo.cargo_cantidad_suplentes_requeridos
    [icono, observacion_con_icono(icono, titulo.call(cantidad_suplentes_faltantes, cantidad_suplentes_requeridos)), nombre] if cantidad_suplentes_faltantes > 0
  end

  def validar_al_menos_una_candidatura
    icono, titulo, nombre = componentes_validacion(VALIDACIONES_CANDIDATURAS_BLOQUEANTES, :AL_MENOS_UNA_CANDIDATURA)
    [icono, observacion_con_icono(icono, titulo), nombre] if lista_cargo.candidaturas_count < 1
  end

  #---------------------------------------------------------------------------------------#
  # ADHESIONES
  #---------------------------------------------------------------------------------------#
  def validar_conformidad_adhesiones
    icono, titulo, nombre = componentes_validacion(VALIDACIONES_ADHESIONES_BLOQUEANTES, :CONFORMIDAD_ADHESIONES)
    [icono, observacion_con_icono(icono, titulo), nombre] if lista_cargo.archivo_conformidad_adhesion.blank?
  end

  def validar_minimo_adhesiones
    icono, titulo, nombre = componentes_validacion(VALIDACIONES_ADHESIONES_BLOQUEANTES, :MINIMO_ADHESIONES)
    cantidad_actual = lista_cargo.adhesiones.count
    cantidad_minima = lista_cargo.cargo_cantidad_minima_adherentes

    if lista_cargo.cargo.tipo_cargo.instance_of? TiposCargos::TipoCargoComunero
      [icono, observacion_con_icono(icono, titulo.call(cantidad_minima - cantidad_actual)), nombre] if cantidad_actual != cantidad_minima
    else
      [icono, observacion_con_icono(icono, titulo.call(cantidad_minima - cantidad_actual)), nombre] if cantidad_actual < cantidad_minima
    end
  end

  def validar_porcentaje_afiliados
    icono, titulo, nombre = componentes_validacion(VALIDACIONES_ADHESIONES_BLOQUEANTES, :PORCENTAJE_AFILIADOS)
    porcentaje_afiliados = lista_cargo.porcentaje_afiliados
    [icono, observacion_con_icono(icono, titulo.call(porcentaje_afiliados.round(2), lista_cargo.cargo_porcentaje_minimo_afiliados.round(2))), nombre] if porcentaje_afiliados < lista_cargo.cargo_porcentaje_minimo_afiliados
  end

  def validar_al_menos_una_adhesion
    icono, titulo, nombre = componentes_validacion(VALIDACIONES_ADHESIONES_BLOQUEANTES, :AL_MENOS_UNA_ADHESION)
    [icono, observacion_con_icono(icono, titulo), nombre] if lista_cargo.adhesiones_count < 1
  end

  private

  def candidatos_con_observaciones_no_bloqueantes
    if candidatos_observados.blank?
      self.candidatos_observados = lista_cargo.candidatos_observados.select do |observado|
        observado[:observaciones].detect { |observacion| Validators::ValidadorCandidatura::OBSERVACIONES_NO_BLOQUEANTES.include?(observacion.third) }
      end
    end

    candidatos_observados
  end

  def componentes_validacion(tipo_validacion, nombre_validacion)
    [tipo_validacion[nombre_validacion][:ICONO], tipo_validacion[nombre_validacion][:TITULO], tipo_validacion[nombre_validacion][:NOMBRE]]
  end

  def observacion_con_icono(icono, titulo)
    "<i class='text-danger #{icono}'></i> #{titulo}"
  end

  def estructura_validacion_candidaturas_no_bloqueante(nombre_validacion, nombre_observacion, opciones = nil)
    icono, titulo, nombre = componentes_validacion(VALIDACIONES_CANDIDATURAS_NO_BLOQUEANTES, nombre_validacion)
    candidatos_con_observacion = candidatos_con_observaciones_no_bloqueantes.select { |candidato| candidato[:observaciones].detect { |observacion| observacion.third =~ nombre_observacion } }
    parametros_aux = opciones.try(:[], :parametros) || []
    parametros_aux.unshift(candidatos_con_observacion.size)
    [icono, observacion_con_icono(icono, titulo.call(parametros_aux)), nombre] unless candidatos_con_observacion.empty?
  end

  def estructura_validacion_adherentes_no_bloqueante(nombre_validacion, adhesiones, condicion)
    icono, titulo, nombre = componentes_validacion(VALIDACIONES_ADHESIONES_NO_BLOQUEANTES, nombre_validacion)
    [icono, observacion_con_icono(icono, titulo.call(adhesiones.count)), nombre] if !adhesiones.empty? && condicion
  end

end
