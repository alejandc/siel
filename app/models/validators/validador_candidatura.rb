class Validators::ValidadorCandidatura
  attr_accessor :candidatura

  VALIDACIONES = {
    EDAD_MINIMA: {
      NOMBRE: 'EDAD_MINIMA',
      ICONO: 'fa fa-address-card',
      TITULO: -> (parametros) { I18n.t('activerecord.errors.models.candidatura.observaciones.edad_minima', edad_minima: parametros.first) },
      TITULO_VERIFICACION: -> (parametros) { I18n.t('activerecord.errors.models.candidatura.observaciones.edad_minima', edad_minima: parametros.first) },
      CONDICION_EJECUCION: -> (_) { true },
      CONDICION_VALIDACION: -> (candidatura) { candidatura.persona_fecha_nacimiento.present? && !candidatura.edad_valida? },
      PARAMETROS_TITULO: -> (candidatura) { [candidatura.cargo_edad_requerida.to_s] }
    },
    CAMPOS_FALTANTES: {
      NOMBRE: 'CAMPOS_FALTANTES',
      ICONO: 'fa fa-info-circle',
      TITULO: -> (_) { I18n.t('activerecord.errors.models.candidatura.observaciones.campos_faltantes') },
      TITULO_VERIFICACION: -> (_) { I18n.t('activerecord.errors.models.candidatura.observaciones.campos_faltantes') },
      CONDICION_EJECUCION: -> (_) { true },
      CONDICION_VALIDACION: -> (candidatura) { candidatura.persona_campos_faltantes? },
      PARAMETROS_TITULO: -> (_) { [] }
    },
    FOTOCOPIA_DNI: {
      NOMBRE: 'FOTOCOPIA_DNI',
      ICONO: 'fa fa-file-o',
      TITULO: -> (_) { I18n.t('activerecord.errors.models.candidatura.observaciones.fotocopia_dni_faltante') },
      TITULO_VERIFICACION: -> (_) { I18n.t('activerecord.errors.models.candidatura.observaciones.fotocopia_dni_verificacion') },
      CONDICION_EJECUCION: -> (_) { true },
      CONDICION_VALIDACION: -> (_) { true },
      PARAMETROS_TITULO: -> (_) { [] }
    },
    ACEPTACION_CANDIDATURA: {
      NOMBRE: 'ACEPTACION_CANDIDATURA',
      ICONO: 'fa fa-file-text-o',
      TITULO: -> (_) { I18n.t('activerecord.errors.models.candidatura.observaciones.aceptacion_candidatura_faltante') },
      TITULO_VERIFICACION: -> (_) { I18n.t('activerecord.errors.models.candidatura.observaciones.aceptacion_candidatura_verificacion') },
      CONDICION_EJECUCION: -> (_) { true },
      CONDICION_VALIDACION: -> (_) { true },
      PARAMETROS_TITULO: -> (_) { [] }
    },
    CERTIFICADO_REINCIDENCIA_CRIMINAL: {
      NOMBRE: 'CERTIFICADO_REINCIDENCIA_CRIMINAL',
      ICONO: 'fa fa-list-alt',
      TITULO: -> (_) { I18n.t('activerecord.errors.models.candidatura.observaciones.reincidencia_criminal_faltante') },
      TITULO_VERIFICACION: -> (_) { I18n.t('activerecord.errors.models.candidatura.observaciones.reincidencia_criminal_verificacion') },
      CONDICION_EJECUCION: -> (_) { true },
      CONDICION_VALIDACION: -> (_) { true },
      PARAMETROS_TITULO: -> (_) { [] }
    },
    ESTA_EN_LISTA: {
      NOMBRE: 'ESTA_EN_LISTA',
      ICONO: 'fa fa-tags',
      TITULO: -> (parametros) { I18n.t('activerecord.errors.models.candidatura.observaciones.esta_en_lista') + parametros.first.cargos_en_lista.map(&:nombre).join(', ') },
      TITULO_VERIFICACION: -> (parametros) { I18n.t('activerecord.errors.models.candidatura.observaciones.esta_en_lista') + parametros.first.cargos_en_lista.map(&:nombre).join(', ') },
      CONDICION_EJECUCION: -> (_) { true },
      CONDICION_VALIDACION: -> (candidatura) { candidatura.cargos_en_lista.count > 1 },
      PARAMETROS_TITULO: -> (candidatura) { [candidatura] }
    },
    INCLUIDO_PADRON: {
      NOMBRE: 'INCLUIDO_PADRON',
      ICONO: 'fa fa-users',
      TITULO: -> (_) { I18n.t('activerecord.errors.models.candidatura.observaciones.incluido_padron') },
      TITULO_VERIFICACION: -> (_) { I18n.t('activerecord.errors.models.candidatura.observaciones.incluido_padron') },
      CONDICION_EJECUCION: -> (_) { true },
      CONDICION_VALIDACION: -> (candidatura) { !candidatura.persona_en_padron },
      PARAMETROS_TITULO: -> (_) { [] }
    },
    CANDIDATURAS_MULTIPLES: {
      NOMBRE: 'PERTENECE_OTRA_LISTA',
      ICONO: 'fa fa-address-book-o',
      TITULO: -> (_) { I18n.t('activerecord.errors.models.candidatura.observaciones.candidaturas_multiples') },
      TITULO_VERIFICACION: -> (_) { I18n.t('activerecord.errors.models.candidatura.observaciones.candidaturas_multiples') },
      CONDICION_EJECUCION: -> (_) { true },
      CONDICION_VALIDACION: -> (candidatura) { candidatura.candidaturas_multiples_en_otras_listas? },
      PARAMETROS_TITULO: -> (_) { [] }
    },
    DEUDOR_ALIMENTARIO: {
      NOMBRE: 'DEUDOR_ALIMENTARIO',
      ICONO: 'fa fa-cutlery',
      TITULO: -> (_) { I18n.t('activerecord.errors.models.candidatura.observaciones.deudor_alimentario') },
      TITULO_VERIFICACION: -> (_) { I18n.t('activerecord.errors.models.candidatura.observaciones.deudor_alimentario') },
      CONDICION_EJECUCION: -> (_) { true },
      CONDICION_VALIDACION: -> (candidatura) { candidatura.persona_deudor_alimentario?(candidatura) },
      PARAMETROS_TITULO: -> (_) { [] }
    },
    REINCIDENCIA_CARGO: {
      NOMBRE: 'REINCIDENCIA_CARGO',
      ICONO: 'fa fa-hand-paper-o',
      TITULO: -> (parametros) { I18n.t('activerecord.errors.models.candidatura.observaciones.reincidencia_cargo', nombre_cargo: parametros.first) },
      TITULO_VERIFICACION: -> (parametros) { I18n.t('activerecord.errors.models.candidatura.observaciones.reincidencia_cargo', nombre_cargo: parametros.first) },
      CONDICION_EJECUCION: -> (_) { true },
      CONDICION_VALIDACION: -> (candidatura) { candidatura.persona_reincidencia_cargo?(candidatura) },
      PARAMETROS_TITULO: -> (candidatura) { [candidatura.cargo_nombre] }
    },
    INHABILITADO: {
      NOMBRE: 'INHABILITADO',
      ICONO: 'fa fa-ban',
      TITULO: -> (_) { I18n.t('activerecord.errors.models.candidatura.observaciones.inhabilitado') },
      TITULO_VERIFICACION: -> (_) { I18n.t('activerecord.errors.models.candidatura.observaciones.inhabilitado') },
      CONDICION_EJECUCION: -> (_) { true },
      CONDICION_VALIDACION: -> (candidatura) { candidatura.persona_inhabilitado?(candidatura) },
      PARAMETROS_TITULO: -> (_) { [] }
    },
    ANTIGUEDAD_CIUDADANIA_PERSONA: {
      NOMBRE: 'ANTIGUEDAD_CIUDADANIA_PERSONA',
      ICONO: 'fa fa-flag',
      TITULO: -> (parametros) { I18n.t('activerecord.errors.models.candidatura.observaciones.antiguedad_minima_ciudadania', antiguedad_minima: parametros.first) },
      TITULO_VERIFICACION: -> (parametros) { I18n.t('activerecord.errors.models.candidatura.observaciones.antiguedad_minima_ciudadania', antiguedad_minima: parametros.first) },
      CONDICION_EJECUCION: -> (candidatura) { !candidatura.existe_registro_candidatura? },
      CONDICION_VALIDACION: -> (candidatura) { !candidatura.cumple_ciudadania? && candidatura.persona_nacionalidad_argentina.blank? },
      # CONDICION_VALIDACION: -> (candidatura) { (candidatura.persona.en_padron && candidatura.antiguedad_persona < candidatura.cargo_anios_ciudadania && (!candidatura.persona.nativo? && !candidatura.persona.por_opcion?)) },
      PARAMETROS_TITULO: -> (candidatura) { [candidatura.cargo_anios_ciudadania] }
    },
    ANTIGUEDAD_RESIDENCIA_PERSONA: {
      NOMBRE: 'ANTIGUEDAD_RESIDENCIA_PERSONA',
      ICONO: 'fa fa-home',
      TITULO: -> (parametros) { I18n.t('activerecord.errors.models.candidatura.observaciones.antiguedad_minima_residencia', antiguedad_minima: parametros.first, residencia: parametros.second) },
      TITULO_VERIFICACION: -> (parametros) { I18n.t('activerecord.errors.models.candidatura.observaciones.antiguedad_minima_residencia', antiguedad_minima: parametros.first, residencia: parametros.second) },
      CONDICION_EJECUCION: -> (candidatura) { !candidatura.cargo.tipo_cargo.instance_of?(TiposCargos::TipoCargoComunero) && !candidatura.existe_registro_candidatura? },
      CONDICION_VALIDACION: -> (candidatura) { !candidatura.cumple_residencia? && candidatura.persona_residencia.blank? },
      # CONDICION_VALIDACION: -> (candidatura) { candidatura.persona.en_padron && candidatura.antiguedad_persona < candidatura.cargo_anios_residencia && !candidatura.persona.nativo_comuna? },
      PARAMETROS_TITULO: -> (candidatura) { [candidatura.cargo_anios_residencia, (candidatura.cargo.tipo_cargo.instance_of?(TiposCargos::TipoCargoComunero) ? 'residencia en la comuna' : 'residencia en la Ciudad de Buenos Aires')] }
    },
    ANTIGUEDAD_RESIDENCIA_PERSONA_COMUNA: {
      NOMBRE: 'ANTIGUEDAD_RESIDENCIA_PERSONA_COMUNA',
      ICONO: 'fa fa-home',
      TITULO: -> (parametros) { I18n.t('activerecord.errors.models.candidatura.observaciones.antiguedad_minima_residencia', antiguedad_minima: parametros.first, residencia: parametros.second) },
      TITULO_VERIFICACION: -> (parametros) { I18n.t('activerecord.errors.models.candidatura.observaciones.antiguedad_minima_residencia', antiguedad_minima: parametros.first, residencia: parametros.second) },
      CONDICION_EJECUCION: -> (candidatura) { candidatura.cargo.tipo_cargo.instance_of?(TiposCargos::TipoCargoComunero) },
      CONDICION_VALIDACION: -> (candidatura) { !candidatura.cumple_residencia_comuna? },
      # CONDICION_VALIDACION: -> (candidatura) { (candidatura.persona_domicilio_comuna.blank? || candidatura.cargo_numero_comuna != candidatura.persona_domicilio_comuna.to_i || candidatura.antiguedad_persona < candidatura.cargo.anios_residencia) },
      PARAMETROS_TITULO: -> (candidatura) { [candidatura.cargo_anios_residencia, 'residencia en la comuna'] }
    }
  }

  # Ciudadanía:                antiguedad de persona es menor al requerido por el cargo && check de nacionalidad vacío
  # Residencia diputados/jefe: antiguedad de persona es menor al requerido por el cargo && check de residencia vacío
  # Residencia comunero:       antiguedad de persona es menor al requerido por el cargo

  #                CASO                                     APARECE ÍCONO
  #
  # cumple con años && nacionalidad vacía                          NO
  # cumple con años && nacionalidad completada                     NO
  # no cumple con años && nacionalidad vacía                       SI
  # no cumple con años && nacionalidad completada                  NO

  OBSERVACIONES_NO_BLOQUEANTES = %w(INCLUIDO_PADRON CANDIDATURAS_MULTIPLES ESTA_EN_LISTA FOTOCOPIA_DNI ACEPTACION_CANDIDATURA CERTIFICADO_REINCIDENCIA_CRIMINAL DEUDOR_ALIMENTARIO REINCIDENCIA_CARGO EDAD_MINIMA INHABILITADO CAMPOS_FALTANTES ANTIGUEDAD_CIUDADANIA_PERSONA ANTIGUEDAD_RESIDENCIA_PERSONA ANTIGUEDAD_RESIDENCIA_PERSONA_COMUNA)
  VALIDAN_SIEMPRE_BORRABLES = %w(CANDIDATURAS_MULTIPLES EDAD_MINIMA)
  VALIDAN_SIEMPRE_NO_BORRABLES = %w(INCLUIDO_PADRON DEUDOR_ALIMENTARIO REINCIDENCIA_CARGO INHABILITADO)
  VALIDAN_CREACION = %w(FOTOCOPIA_DNI ACEPTACION_CANDIDATURA CERTIFICADO_REINCIDENCIA_CRIMINAL ANTIGUEDAD_CIUDADANIA_PERSONA ANTIGUEDAD_RESIDENCIA_PERSONA)

  VALIDACIONES_OBSERVABLES = %w(EDAD_MINIMA FOTOCOPIA_DNI ACEPTACION_CANDIDATURA CERTIFICADO_REINCIDENCIA_CRIMINAL INCLUIDO_PADRON CANDIDATURAS_MULTIPLES DEUDOR_ALIMENTARIO REINCIDENCIA_CARGO INHABILITADO ANTIGUEDAD_CIUDADANIA_PERSONA ANTIGUEDAD_RESIDENCIA_PERSONA)

  def initialize(candidatura)
    self.candidatura = candidatura
  end

  def observaciones
    [
      estructura_validacion(:EDAD_MINIMA),
      estructura_validacion(:CAMPOS_FALTANTES),
      estructura_validacion(:FOTOCOPIA_DNI),
      estructura_validacion(:ACEPTACION_CANDIDATURA),
      estructura_validacion(:CERTIFICADO_REINCIDENCIA_CRIMINAL),
      estructura_validacion(:INCLUIDO_PADRON),
      estructura_validacion(:ESTA_EN_LISTA),
      estructura_validacion(:CANDIDATURAS_MULTIPLES),
      estructura_validacion(:DEUDOR_ALIMENTARIO),
      estructura_validacion(:REINCIDENCIA_CARGO),
      estructura_validacion(:INHABILITADO),
      estructura_validacion(:ANTIGUEDAD_CIUDADANIA_PERSONA),
      estructura_validacion(:ANTIGUEDAD_RESIDENCIA_PERSONA),
      estructura_validacion(:ANTIGUEDAD_RESIDENCIA_PERSONA_COMUNA)
    ].compact
  end

  private

  def estructura_validacion(nombre_validacion)
    icono, titulo, nombre, condicion_ejecucion, condicion_validacion, parametros_titulo = componentes_validacion(nombre_validacion)

    if condicion_ejecucion.call(candidatura) && !candidatura.persona_contiene_archivo_tipo?(candidatura.lista_cargo, nombre.downcase) && condicion_validacion.call(candidatura)
      [icono, titulo.call(parametros_titulo.call(candidatura)), nombre]
    else
      return nil
    end
  end

  def componentes_validacion(nombre_validacion)
    [VALIDACIONES[nombre_validacion][:ICONO], VALIDACIONES[nombre_validacion][:TITULO], VALIDACIONES[nombre_validacion][:NOMBRE], VALIDACIONES[nombre_validacion][:CONDICION_EJECUCION], VALIDACIONES[nombre_validacion][:CONDICION_VALIDACION], VALIDACIONES[nombre_validacion][:PARAMETROS_TITULO]]
  end

end
