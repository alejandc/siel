class Validators::ValidadorLista
  attr_accessor :lista

  VALIDACIONES = {
    CANDIDATOS_OBSERVADOS: { NOMBRE: 'CANDIDATOS_OBSERVADOS', ICONO: 'fa fa-list-ul', TITULO: -> (cantidad) { I18n.t('activerecord.errors.models.lista.observaciones.candidatos_observados', cantidad_candidatos_observados: cantidad) } },
    CUPO_CARGOS: { NOMBRE: 'CUPO_CARGOS', ICONO: 'fa fa-list-ul', TITULO: -> (cantidad_actual, cantidad_minima) { I18n.t('activerecord.errors.models.lista.observaciones.cupo_cargos', cantidad_actual: cantidad_actual, cantidad_minima: cantidad_minima) } },
    CUPO_GENEROS: { NOMBRE: 'CUPO_GENEROS', ICONO: 'fa fa-list-ul', TITULO: I18n.t('activerecord.errors.models.lista.observaciones.cupo_generos') }
  }

  def initialize(lista)
    self.lista = lista
  end

  def observaciones(cargo)
    [
      validar_candidatos_observados(cargo),
      validar_cupo_cargos(cargo),
      validar_cupo_generos(cargo)
    ].compact
  end

  def validar_candidatos_observados(cargo)
    icono = VALIDACIONES[:CANDIDATOS_OBSERVADOS][:ICONO]
    titulo = VALIDACIONES[:CANDIDATOS_OBSERVADOS][:TITULO]
    identificador = VALIDACIONES[:CANDIDATOS_OBSERVADOS][:NOMBRE]
    candidatos_observados = lista.candidatos_observados(cargo)
    [icono, titulo.call(candidatos_observados.size), identificador] unless candidatos_observados.empty?
  end

  def validar_cupo_cargos(cargo)
    icono = VALIDACIONES[:CUPO_CARGOS][:ICONO]
    titulo = VALIDACIONES[:CUPO_CARGOS][:TITULO]
    identificador = VALIDACIONES[:CUPO_CARGOS][:NOMBRE]
    cantidad_minima_cargo = cargo.cantidad_minima
    cantidad_actual_cargo = lista.candidaturas.cargo(cargo).size
    [icono, titulo.call(cantidad_actual_cargo, cantidad_minima_cargo), identificador] if cantidad_actual_cargo < cantidad_minima_cargo
  end

  def validar_cupo_generos(cargo)
    icono = VALIDACIONES[:CUPO_GENEROS][:ICONO]
    titulo = VALIDACIONES[:CUPO_GENEROS][:TITULO]
    identificador = VALIDACIONES[:CUPO_GENEROS][:NOMBRE]
    [icono, titulo, identificador] if !lista.cumple_ley_cupos?(cargo) || Candidatura.mismo_sexo_consecutivo(lista.candidaturas.cargo(cargo)).present?
  end

end
