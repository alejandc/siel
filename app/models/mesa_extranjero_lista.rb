# == Schema Information
#
# Table name: mesa_extranjero_litas
#
#  id                       :integer          not null, primary key
#  cantidad_votos_jefe      :integer
#  cantidad_votos_diputados :integer
#  cantidad_votos_comuna    :integer
#  lista_id                 :integer
#  mesa_extranjero_id       :integer
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

class MesaExtranjeroLista < ActiveRecord::Base

  belongs_to :lista
  belongs_to :mesa_extranjero

  validates :lista_id, :mesa_extranjero_id, presence: true

end
