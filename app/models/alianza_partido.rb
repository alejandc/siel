# == Schema Information
#
# Table name: alianza_partidos
#
#  id         :integer          not null, primary key
#  alianza_id :integer
#  partido_id :integer
#  created_at :datetime
#  updated_at :datetime
#

class AlianzaPartido < ActiveRecord::Base
  has_paper_trail

  belongs_to :alianza, class_name: 'Fuerza', foreign_key: 'alianza_id'
  belongs_to :partido, class_name: 'Fuerza', foreign_key: 'partido_id'
end
