# == Schema Information
#
# Table name: verificaciones_firmas
#
#  id                         :integer          not null, primary key
#  descripcion                :string(255)
#  cantidad_firmas_total      :integer
#  confianza                  :float(24)
#  error                      :float(24)
#  observaciones              :text(16777215)
#  cantidad_firmas_muestra    :integer
#  cantidad_hojas             :integer
#  cantidad_firmas_hoja       :integer
#  created_at                 :datetime
#  updated_at                 :datetime
#  variable_p                 :float(24)
#  cantidad_firmas_requeridas :integer
#

class VerificacionFirmas < ActiveRecord::Base
  has_paper_trail

  has_many :casos_verificacion, autosave: true

  attr_accessible :descripcion, :confianza, :variable_p, :error, :cantidad_firmas_total, :observaciones, :cantidad_firmas_muestra, :cantidad_hojas, :cantidad_firmas_hoja, :cantidad_firmas_requeridas

  ERRORES_RELATIVOS = [5.0, 2.5]
  VALORES_P = [0.8, 0.9]
  COEF_CONFIABILIDAD = { 90.0 => 1.645, 95.0 => 1.96, 99.0 => 2.58 }

  validates :descripcion, :cantidad_firmas_total, :confianza, :variable_p, :error, :cantidad_firmas_muestra, :cantidad_hojas, :cantidad_firmas_hoja, presence: true
  validates :descripcion, length: { maximum: 255 }
  validates :confianza, inclusion: { in: COEF_CONFIABILIDAD.keys }
  validates :error, inclusion: { in: ERRORES_RELATIVOS }
  validates :cantidad_firmas_total, :cantidad_firmas_muestra, :cantidad_firmas_hoja, :cantidad_hojas, numericality: { only_integer: true, greater_than: 0 }

  def rango_confianza
    total = cantidad_firmas_total
    p = proporcion_firmas_validas
    t = COEF_CONFIABILIDAD[confianza]
    error = error_de_estimacion

    [(total * p - t * error).round, (total * p + t * error).round]
  end

  def error_de_estimacion
    poblacion = cantidad_firmas_total
    muestra = cantidad_firmas_muestra
    p = proporcion_firmas_validas

    Math.sqrt(poblacion * (poblacion - muestra) * p * (1 - p) / (muestra - 1))
  end

  def proporcion_firmas_validas
    casos_validos.size.to_f / cantidad_firmas_muestra
  end

  def casos_validos
    casos_verificacion.select { |c| !c.invalido? && !c.fila_vacia }
  end

  def casos_cargados
    casos_verificacion.select(&:cargado?)
  end

  def casos_a_firmar
    casos_verificacion.select { |c| !c.fila_vacia && !c.invalido? && !c.firmado }
  end

  def casos_a_cargar
    casos_verificacion.select { |c| !c.cargado? }
  end

  def proximo_caso_a_cargar
    casos_a_cargar.sort.first
  end

  def generar_caso_adicional
    coordenadas = generar_coordenadas_poblacion - casos_verificacion.map { |caso| [caso.hoja, caso.fila] }
    rand_coord = coordenadas.sample
    casos_verificacion.new(hoja: rand_coord[0], fila: rand_coord[1])
  end

  def generar_casos
    generar_coordenadas_muestra.each { |coord| casos_verificacion.new(hoja: coord[0], fila: coord[1]) }
  end

  def generar_coordenadas_muestra
    # Array con coordenadas
    coord = generar_coordenadas_poblacion
    # Índice alteatorio
    i = rand(coord.size)
    # Distancia de los elementos de la muestra
    h = (coord.size / cantidad_firmas_muestra).floor

    coord.rotate(i).each_slice(h).map(&:first).first(cantidad_firmas_muestra)
  end

  def generar_coordenadas_poblacion
    (1..cantidad_hojas).map { |hoja| (1..cantidad_firmas_hoja).map { |fila| [hoja, fila] } }.flatten(1)
  end

  def variable_t
    confianza.nil? ? nil : COEF_CONFIABILIDAD[confianza]
  end

  def variable_e
    error.nil? ? nil : (error / 100)
  end

  # Se calcula el tamaño de la muestra aplicando la fórmula:
  #
  #     n = ( t² * (1 - p) ) / ( e² * p )
  #
  def calcular_tamanio_muestra
    if confianza.nil? || error.nil?
      nil
    else
      t = variable_t
      e = variable_e
      p = variable_p

      (((t * t) * (1 - p)) / ((e * e) * p)).to_i
    end
  end

  # Calcula el tamaño de la muestra, ajustado por el tamaño
  # de la población finita (cantidad de firmas total)
  #
  #     m = n / (1 + n / N)
  #
  # Donde N es la cantidad total de firmas, y n el tamaño
  # de la muestra
  def calcular_cantidad_firmas_muestra
    if calcular_tamanio_muestra.nil? || cantidad_firmas_total.nil?
      nil
    else
      n = calcular_tamanio_muestra.to_f
      cant_firmas = cantidad_firmas_total.to_f

      self.cantidad_firmas_muestra = (n / (1 + n / cant_firmas)).to_i
    end
  end

end
