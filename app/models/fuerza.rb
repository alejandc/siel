# == Schema Information
#
# Table name: fuerzas
#
#  id                       :integer          not null, primary key
#  numero                   :integer
#  nombre                   :string(255)
#  expediente               :string(255)
#  tipo                     :string(255)
#  observaciones            :text(65535)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  estado                   :string(255)
#  domicilio_constituido_id :integer
#  domicilio_real_id        :integer
#  fecha_baja               :date
#  eleccion_id              :integer
#  escudo_file_name         :string(255)
#  escudo_content_type      :string(255)
#  escudo_file_size         :integer
#  escudo_updated_at        :datetime
#  web                      :string(255)
#

class Fuerza < ActiveRecord::Base
  scope :partidos, -> { where(tipo: 'PARTIDO') }
  scope :alianzas, -> { where(tipo: 'ALIANZA') }

  # Filter scopes
  scope :vigentes, -> { where(estado: 'VIGENTE') }
  scope :activos, -> { where('fecha_baja IS NULL') }
  scope :numero, -> (numero) { where(numero: numero) }
  scope :nombre, -> (nombre) { where('nombre LIKE ?', "%#{nombre}%") }
  scope :estado, -> (estado) { where(estado: estado) }
  scope :tipo, -> (tipo) { where(tipo: tipo) }
  scope :alianzas_eleccion, -> (eleccion_id) { where('tipo = ? AND eleccion_id = ?', 'ALIANZA', eleccion_id) }
  scope :expediente, -> (expediente) { where('expediente LIKE ?', "%#{expediente}%") }
  scope :observaciones, -> (obs) { where('observaciones LIKE ?', "%#{obs}%") }
  scope :participo_de_eleccion?, -> (eleccion_id) { where('eleccion_id = ? OR eleccion_id = ?', Eleccion.find(eleccion_id).general.id, Eleccion.find(eleccion_id).paso.id) }
  scope :apoderado, -> (apoderado) { joins(:apoderados).where('apoderados.numero_documento LIKE ? OR apoderados.nombres LIKE ? OR apoderados.apellidos LIKE ? OR concat(apoderados.nombres, " ", apoderados.apellidos) LIKE ?', "%#{apoderado}%", "%#{apoderado}%", "%#{apoderado}%", "%#{apoderado}%") }
  scope :con_infracciones, -> (_) { joins(:infracciones) }
  scope :eleccion_id, -> (eleccion_id) { where('(tipo = "PARTIDO" AND eleccion_id IS NULL) OR (tipo = "ALIANZA" AND eleccion_id = ?)', eleccion_id) }

  attr_accessible :acta_constitucion_id, :carta_organica_id, :domicilio_real,
                  :domicilio_real_attributes, :domicilio_constituido,
                  :domicilio_constituido_attributes, :expediente, :nombre, :numero,
                  :observaciones, :plataforma_id, :tipo, :estado, :ids_partidos,
                  :apoderados_attributes, :boletas, :boletas_attributes,
                  :documentos_fuerzas_attributes, :documentos_fuerzas, 
                  :fecha_baja, :eleccion_id, :escudo, :web, :fuerza_elecciones_attributes

  validates :numero, :nombre, :expediente, :tipo, presence: true
  validates :eleccion_id, presence: true, if: :es_alianza?
  validates :numero, numericality: true
  validate  :sin_partidos, if: :es_alianza?

  before_save :quitar_partidos, unless: :es_alianza?

  # A hack to combine the has_many with the through for a reflexive association
  has_many :partidos_de_alianza, foreign_key: :alianza_id, class_name: 'AlianzaPartido', dependent:  :destroy
  has_many :partidos, through: :partidos_de_alianza

  # A hack to combine the has_many with the through for a reflexive association
  has_many :alianzas_de_partido, foreign_key: :partido_id, class_name: 'AlianzaPartido'
  has_many :alianzas, through: :alianzas_de_partido

  has_many :fuerza_elecciones
  has_many :apoderados, dependent: :destroy
  has_many :boletas, dependent: :destroy
  has_many :documentos_fuerzas, dependent: :destroy, class_name: 'DocumentoFuerza'
  has_many :infracciones
  has_many :candidaturas
  has_many :resultados_elecciones
  has_many :listas
  has_many :afiliaciones

  has_attached_file :escudo, styles: { medium: '300x300>', thumb: '100x100>' }, default_url: '/empty_badge.jpg'
  validates_attachment_content_type :escudo, content_type: %r{\Aimage\/.*\z}

  belongs_to :domicilio_constituido, foreign_key: :domicilio_constituido_id, class_name: 'Domicilio'
  belongs_to :domicilio_real, foreign_key: :domicilio_real_id, class_name: 'Domicilio'
  belongs_to :eleccion

  accepts_nested_attributes_for :apoderados, :domicilio_real, :domicilio_constituido, :boletas, :documentos_fuerzas, :fuerza_elecciones, allow_destroy: true

  has_paper_trail

  delegate :calle_nombre, :numero, :piso, :depto, :comuna, :codigo_postal, to: :domicilio_constituido, prefix: true, allow_nil: true

  TIPOS = %w(PARTIDO ALIANZA)
  ESTADOS = ['VIGENTE', 'NO VIGENTE']

  attr_accessor :cociente
  attr_accessor :divisor

  def vigente?
    estado == 'VIGENTE'
  end

  def calcular_cociente(eleccion_cargo)
    self.cociente = cantidad_votos(eleccion_cargo).to_f / divisor.to_f
  end

  def posee_candidaturas_para_asignar?(eleccion_cargo)
    candidaturas_cargo(eleccion_cargo.eleccion, eleccion_cargo.cargo).any? { |c| !c.tiene_escanio? }
  end

  def cantidad_escanios_adjudicados(eleccion_cargo)
    candidaturas_cargo(eleccion_cargo.eleccion, eleccion_cargo.cargo).select(&:tiene_escanio).size
  end

  def asignar_escanio(eleccion_cargo)
    candidaturas = candidaturas_cargo(eleccion_cargo.eleccion, eleccion_cargo.cargo).reject(&:tiene_escanio)
    ganadora = candidaturas.sort[0]

    ganadora.tiene_escanio = true
    ganadora.divisor = divisor
    ganadora.cociente = cociente

    ganadora
  end

  def cantidad_votos(eleccion_cargo)
    ResultadoEleccion.por_fuerza(self).por_eleccion_cargo(eleccion_cargo).sum(:cantidad_votos)
  end

  def posee_candidaturas_invalidas?(eleccion)
    invalidas = candidaturas_eleccion(eleccion).select { |c| !c.valida? || !c.documentacion_completa? }

    invalidas.present? and !invalidas.empty?
  end

  def cumple_ley_cupos?(eleccion)
    cumple = true

    Cargo::CARGOS.each do |nombre_cargo|
      cargo = Cargo.find_by(nombre: nombre_cargo)
      cumple &&= cumple_ley_cupos_cargo?(eleccion, cargo)
    end

    cumple
  end

  def cumple_ley_cupos_cargo?(eleccion, cargo)
    candidaturas = candidaturas_cargo(eleccion, cargo)

    candidaturas.nil? or candidaturas.empty? or Candidatura.cumple_ley_cupos?(candidaturas.sort)
  end

  def miembro_alianza?(eleccion_id)
    !alianzas.alianzas_eleccion(eleccion_id).empty?
  end

  def nombres_archivos_expedientes
    nombres = []

    unless expediente.nil?
      expediente.split(',').each do |num_expe|
        partes = num_expe.split('/')
        nombres << "Expe-#{partes[0].to_i}-#{partes[1]}.pdf"
      end
    end

    nombres
  end

  def oficializar_lista(eleccion)
    candidaturas = candidaturas_eleccion(eleccion)

    candidaturas.each do |c|
      c.fecha_oficializacion = DateTime.now
      c.save
    end

    candidaturas
  end

  def desoficializar_lista(eleccion)
    candidaturas = candidaturas_eleccion(eleccion)

    candidaturas.each do |c|
      c.fecha_oficializacion = nil
      c.save
    end

    candidaturas
  end

  def oficializada?(eleccion)
    Candidatura.joins(:eleccion_cargo).where('fecha_renuncia IS NULL AND fuerza_id = ? AND eleccion_cargos.eleccion_id = ? AND fecha_oficializacion IS NOT NULL', id, eleccion.id).exists?
  end

  def init_associations
    fuerza_elecciones.build(eleccion_id: Eleccion.actual.id)
    build_domicilio_constituido
    build_domicilio_real
  end

  def add_boleta(eleccion, archivo_subido)
    boleta = Boleta.new
    boleta.fuerza = self
    boleta.eleccion = eleccion
    boleta.archivo = archivo_subido
    boleta.save!

    boletas << boleta
  end

  def add_documento(descripcion, archivo_subido)
    documento = DocumentoFuerza.new
    documento.descripcion = descripcion
    documento.fuerza = self
    documento.archivo = archivo_subido
    documento.save!

    documentos_fuerzas << documento
  end

  def cargos_postulados(eleccion)
    cargos = Set.new
    candidaturas_eleccion(eleccion).each { |c| cargos.add(c.eleccion_cargo.cargo) }
    cargos
  end

  def cumple_minimos?(eleccion)
    cargos = cargos_postulados(eleccion)

    cargos.nil? or cargos.empty? or cargos.inject(true) { |cumple, cargo| cumple && cumple_minimo?(eleccion, cargo) }
  end

  def cumple_minimo?(eleccion, cargo)
    maximo_orden(eleccion, cargo) >= cargo.cantidad_minima
  end

  # def cumple_maximo?(eleccion, cargo)
  #   maximo_orden(eleccion, cargo) >= cargo.cantidad_maxima
  # end

  # def maximo_orden(eleccion, cargo)
  #   Candidatura.joins(:eleccion_cargo).where('eleccion_cargos.eleccion_id = ? AND eleccion_cargos.cargo_id = ? AND fuerza_id = ? AND fecha_renuncia IS NULL', eleccion.id, cargo.id, id).maximum(:orden) || 0
  # end

  # def agregar_candidatura(eleccion, cargo, numero_documento, nombres, apellidos, sexo)
  #   candidatura = nil

  #   unless cumple_maximo?(eleccion, cargo)
  #     eleccion_cargo = EleccionCargo.where(eleccion_id: eleccion.id, cargo_id: cargo.id, seleccionado: true).first

  #     if eleccion_cargo
  #       persona = Persona.where(numero_documento: numero_documento).first_or_create(nombres: nombres, apellidos: apellidos, sexo: sexo)
  #       maximo_orden = maximo_orden(eleccion, cargo)

  #       candidatura = Candidatura.create!(persona: persona, eleccion_cargo: eleccion_cargo, fuerza: self, orden: (maximo_orden + 1)) unless eleccion_cargo.nil?
  #     end
  #   end

  #   candidatura
  # end

  def ids_partidos=(lista)
    nuevos_ids = lista.split(',').map(&:to_i)
    ids = partidos.empty? ? [] : partidos.map(&:id)

    ids.each do |id|
      partidos.delete(Fuerza.find(id)) unless nuevos_ids.include?(id)
    end

    nuevos_ids.each do |id|
      partidos << Fuerza.find(id) unless id.nil? or id == 0 or ids.include?(id)
    end
  end

  def participo_de?(eleccion)
    Candidatura.exists?(['eleccion_id = ? AND fuerza_id = ?', eleccion.id, id])
  end

  def ids_partidos
    partidos.map(&:id).join(',')
  end

  def es_alianza?
    tipo == 'ALIANZA'
  end

  def tiene_personeria_juridica?
    !expediente.blank?
  end

  def candidaturas_cargo(eleccion, cargo)
    ec = EleccionCargo.where(eleccion_id: eleccion.id, cargo_id: cargo.id).first
    Candidatura.por_fuerza(self).select { |c| c.eleccion_cargo == ec && c.fecha_renuncia.nil? }
    # candidaturas.select { |c| c.eleccion_cargo == ec && c.fecha_renuncia.nil? }
  end

  def resultado(eleccion_cargo, comuna = nil)
    ResultadoEleccion.por_eleccion_cargo(eleccion_cargo).por_fuerza(self).por_comuna(comuna).first || ResultadoEleccion.new(fuerza: self, eleccion_cargo: eleccion_cargo, comuna: comuna)
  end

  def self.partidos_y_alianzas(eleccion)
    alianzas = alianzas_eleccion(eleccion).vigentes.activos
    partidos_de_alianzas = alianzas.flat_map(&:partidos)
    partidos = Fuerza.partidos.vigentes.activos - partidos_de_alianzas

    (alianzas + partidos).sort_by { |partido| partido.nombre.downcase }
  end

  def self.filtrar(attributes)
    supported_filters = [:numero, :nombre, :expediente, :estado, :tipo, :observaciones, :apoderado, :con_infracciones, :eleccion_id]
    attributes.slice(*supported_filters).inject(send('activos')) do |scope, (key, value)|
      value.present? ? scope.send(key, value) : scope
    end
  end

  def self.partidos_en_alianza_con_fuerza(eleccion_id, fuerza_id)
    fuerza = Fuerza.find(fuerza_id)
    fuerzas = fuerza.es_alianza? ? fuerza.partidos : fuerza.alianzas.alianzas_eleccion(eleccion_id).first.try(:partidos)
    fuerzas.to_a.empty? ? [fuerza] : fuerzas
  end

  # def as_json(_)
  #   super(methods: 'es_alianza?', include: {
  #     eleccion: {},
  #     partidos: {},
  #     documentos_fuerzas: { include: :archivo },
  #     apoderados: {},
  #     boletas: { include: [:eleccion, :archivo] },
  #     domicilio_constituido: { methods: ['descripcion'] },
  #     domicilio_real: { methods: ['descripcion'] }
  #   })
  # end

  def <=>(other)
    nombre <=> other.nombre
  end

  private

  def quitar_partidos
    partidos_de_alianza.clear
  end

  def sin_partidos
    errors.add(:sin_partidos, 'Partidos no puede quedar vacío.') if partidos.empty?
  end

end
