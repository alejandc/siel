# == Schema Information
#
# Table name: listas
#
#  id          :integer          not null, primary key
#  nombre      :string(255)
#  estado      :string(255)
#  fuerza_id   :integer
#  eleccion_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  data_info   :text(16777215)
#  errores     :text(16777215)
#  numero      :integer
#

class Lista < ActiveRecord::Base
  paginates_per 50
  has_paper_trail

  serialize :data_info, Hash
  serialize :errores, Hash

  before_create :setear_estado_inicial
  before_validation :formatear_atributos

  ESTADOS = { ABIERTA: 'ABIERTA', CERRADA: 'CERRADA', REABIERTA: 'REABIERTA', OFICIALIZADA_JUNTA: 'OFICIALIZADA_JUNTA', OFICIALIZADA_TSJ: 'OFICIALIZADA_TSJ' }

  attr_accessible :nombre, :estado, :fuerza, :eleccion, :eleccion_id, :fuerza_id,
                  :data_info, :lista_cargos, :errores, :numero

  scope :numero, -> (numero) { where(numero: numero) }
  scope :por_nombres, -> (nombres) { where('nombre in (?)', nombres.map(&:upcase)) }
  scope :por_eleccion, -> (eleccion) { where('eleccion_id = ?', eleccion.id) if eleccion }
  scope :por_fuerza, -> (fuerza) { where('fuerza_id = ?', fuerza.id) if fuerza }

  has_many :candidaturas, -> { order(orden: :asc) }
  has_many :adhesiones
  has_many :lista_cargos, dependent: :destroy
  has_many :usuarios, dependent: :destroy

  belongs_to :fuerza
  belongs_to :eleccion

  validates :nombre, :fuerza_id, :eleccion_id, presence: true
  validates_uniqueness_of :nombre, case_sensitive: false, scope: [:eleccion_id, :fuerza_id]
  validates_uniqueness_of :numero, scope: :eleccion_id, if: Proc.new { |lista| lista.numero.present? }
  validate :validar_listas_cargo_requeridas

  delegate :descripcion, :paso?, to: :eleccion, prefix: true
  delegate :nombre, :numero, to: :fuerza, prefix: true

  def validar_listas_cargo_requeridas
    errors.add(:lista_cargos, 'Debe seleccion por lo menos un cargo') if data_info.empty?
  end

  def build_lista_cargos
    eleccion.cargos.each do |cargo|
      lista_cargos << ListaCargo.new(lista: self, cargo: cargo,
                                     estado_candidaturas: :sin_validar,
                                     estado_adhesiones: :sin_validar)
    end
  end

  def candidaturas_eleccion
    candidaturas.where('eleccion_id = ? AND fecha_renuncia IS NULL', eleccion_id)
  end

  def asignar_cargos_habilitados(cargos)
    unless data_info.keys.size == cargos.size && data_info.keys.all? { |cargo| cargos.include?(cargo.to_s) }
      data_info_dup = data_info.dup

      Cargo::CARGOS.keys.each do |cargo|
        data_info_dup = cargos.include?(cargo.to_s) ? data_info_dup.merge(cargo => true).dup : data_info_dup.except(cargo).dup
      end

      # Hack para que actualice los datos en la base
      assign_attributes(data_info: {})
      assign_attributes(data_info: data_info_dup)
    end
  end

  def cargos_habilitados
    Cargo::CARGOS.keys.select { |cargo| data_info[cargo].present? }
  end

  def cargo_habilitado?(nombre_cargo)
    data_info.key?(Cargo::CARGOS.key(nombre_cargo))
  end

  def lista_cargos_habilitados
    lista_cargos.cargos(cargos_habilitados.map { |clave_cargo| Cargo::CARGOS[clave_cargo] })
  end

  def valida?
    cerrada? || lista_cargos_habilitados.no_validas.empty?
  end

  def oficializada?
    oficializada_tsj? || oficializada_junta?
  end

  def oficializada_tsj?
    estado.eql?(ESTADOS[:OFICIALIZADA_TSJ])
  end

  def oficializada_junta?
    estado.eql?(ESTADOS[:OFICIALIZADA_JUNTA])
  end

  def validar!
    lista_cargos_habilitados.no_cerradas.each do |lista_cargo|
      lista_cargo.validar_candidaturas! unless lista_cargo.estado_candidaturas_cerrada?
      lista_cargo.validar_adhesiones! unless lista_cargo.estado_adhesiones_cerrada?
    end
  end

  def oficializar!(usuario)
    if usuario.lista_partidaria?
      raise CanCan::AccessDenied.new("No esta autorizado a realizar esta operación!", :update, Lista)
    end

    if usuario.apoderado?
      update_attribute(:estado, ESTADOS[:OFICIALIZADA_JUNTA])
    elsif usuario.apoderado? && estado.eql?(ESTADOS[:OFICIALIZADA_TSJ])
      raise CanCan::AccessDenied.new("No esta autorizado a realizar esta operación!", :update, Lista)
    elsif usuario.interno?
      update_attribute(:estado, ESTADOS[:OFICIALIZADA_TSJ])
    end
  end

  def desoficializar_general!
    desoficializar!(Lista::ESTADOS[:REABIERTA])
    lista_cargos_habilitados.each do |lc|
      lc.abrir_candidaturas!(Lista::ESTADOS[:REABIERTA])
      lc.abrir_adhesiones!(Lista::ESTADOS[:REABIERTA])
    end
  end

  def desoficializar!(estado_apertura)
    update_attribute(:estado, estado_apertura)
  end

  def cerrar!
    lista_cargos_habilitados.each do |lc|
      lc.cerrar_candidaturas!
      lc.cerrar_adhesiones!
    end
  end

  def abierta?
    estado == Lista::ESTADOS[:ABIERTA]
  end

  def reabierta?
    estado == Lista::ESTADOS[:REABIERTA]
  end

  def cerrada?
    estado == Lista::ESTADOS[:CERRADA]
  end

  def cargos_cerrados?
    lista_cargos_habilitados.where('estado_candidaturas_cd <> ? OR estado_adhesiones_cd <> ?', ListaCargo.estado_candidaturas[:cerrada], ListaCargo.estado_adhesiones[:cerrada]).empty?
  end

  private

  def setear_estado_inicial
    self.estado = Lista::ESTADOS[:ABIERTA]
  end

  def formatear_atributos
    self.nombre = nombre.upcase
  end

end
