# == Schema Information
#
# Table name: casos_verificacion
#
#  id                     :integer          not null, primary key
#  hoja                   :integer
#  fila                   :integer
#  numero_documento       :integer
#  nombre                 :string(255)
#  apellido               :string(255)
#  figura_en_padron       :boolean
#  fila_vacia             :boolean
#  verificacion_firmas_id :integer
#  created_at             :datetime
#  updated_at             :datetime
#  ilegible               :boolean
#  sexo                   :string(255)
#  duplicado              :boolean
#  firmado                :boolean
#

class CasoVerificacion < ActiveRecord::Base

  belongs_to :verificacion_firmas

  attr_accessible :hoja, :fila, :firmado, :numero_documento, :nombre, :apellido, :duplicado, :figura_en_padron, :fila_vacia, :ilegible, :verificacion_firmas

  after_update :reemplazar_por_fila_vacia

  def borrar_datos_cargados
    self.numero_documento = nil
    self.nombre = nil
    self.apellido = nil
    self.figura_en_padron = nil
    self.fila_vacia = nil
    self.ilegible = nil
    self.sexo = nil
    self.duplicado = nil
    self.firmado = nil
  end

  def invalido?
    (!figura_en_padron.nil? && !figura_en_padron) || ilegible || duplicado || (!firmado.nil? && !firmado)
  end

  def quitar_marca_duplicados
    unless duplicado
      casos = CasoVerificacion.where('verificacion_firmas_id = ? AND numero_documento = ? AND id <> ? AND duplicado = ?', verificacion_firmas_id, numero_documento, id, true)

      unless casos.nil? || casos.empty?
        casos[0].duplicado = false
        casos[0].save
        casos[0]
      end
    end
  end

  def marcar_duplicado
    casos = CasoVerificacion.where('verificacion_firmas_id = ? AND numero_documento = ? AND id <> ?', verificacion_firmas_id, numero_documento, id)

    self.duplicado = !casos.empty?
    save

    self
  end

  def buscar_en_padron
    renglon = RenglonPadron.find_by(matricula: numero_documento)
    self.figura_en_padron = !renglon.nil?

    unless renglon.nil?
      self.nombre = renglon.nombre
      self.apellido = renglon.apellido
      self.sexo = renglon.sexo
    end

    self
  end

  def cargado?
    !numero_documento.nil? || !fila_vacia.nil? || !ilegible.nil?
  end

  def <=>(other)
    (hoja <=> other.hoja).nonzero? || (fila <=> other.fila)
  end

  protected

  def reemplazar_por_fila_vacia
    if fila_vacia_changed? && fila_vacia
      verificacion_firmas.generar_caso_adicional
      verificacion_firmas.save
    end
  end
end
