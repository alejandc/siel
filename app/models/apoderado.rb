# == Schema Information
#
# Table name: apoderados
#
#  id               :integer          not null, primary key
#  fuerza_id        :integer
#  created_at       :datetime
#  updated_at       :datetime
#  persona_id       :integer
#  nombres          :string(255)
#  apellidos        :string(255)
#  telefono         :string(255)
#  email            :string(255)
#  numero_documento :integer
#  calle            :string(255)
#  numero           :string(255)
#  piso             :string(255)
#  depto            :string(255)
#  comuna           :string(255)
#  codigo_postal    :string(255)
#

class Apoderado < ActiveRecord::Base

  has_paper_trail

  belongs_to :fuerza

  attr_accessible :fuerza, :nombres, :apellidos, :telefono, :email, :numero_documento, :calle, :numero, :piso, :depto, :comuna, :codigo_postal
  validates :nombres, :apellidos, presence: true

end
