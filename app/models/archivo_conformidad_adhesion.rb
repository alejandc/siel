# == Schema Information
#
# Table name: archivo_conformidad_adhesiones
#
#  id             :integer          not null, primary key
#  lista_cargo_id :integer
#  archivo_id     :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class ArchivoConformidadAdhesion < ActiveRecord::Base
  belongs_to :lista_cargo
  belongs_to :archivo, dependent: :destroy

  attr_accessible :archivo, :tipo_archivo, :lista_cargo_id

  has_paper_trail on: [:create]

  def archivo=(archivo_subido)
    if archivo.nil?
      nuevo_archivo = Archivo.new
      nuevo_archivo.nombre = File.basename(archivo_subido.original_filename)
      nuevo_archivo.tipo_contenido = archivo_subido.content_type

      nuevo_archivo.paper_trail.without_versioning do
        nuevo_archivo.save!
      end

      nuevo_archivo.contenido = archivo_subido.read

      self.archivo_id = nuevo_archivo.id
    else
      archivo.contenido_archivos.destroy_all

      archivo.nombre = File.basename(archivo_subido.original_filename)
      archivo.tipo_contenido = archivo_subido.content_type

      archivo.paper_trail.without_versioning do
        archivo.save!
      end

      archivo.contenido = archivo_subido.read
    end
  end
end
