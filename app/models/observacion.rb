# == Schema Information
#
# Table name: observaciones
#
#  id                     :integer          not null, primary key
#  lista_cargo_id         :integer
#  observable_id          :integer
#  nombre_candidato       :string(255)
#  observable_type        :string(255)
#  referencia_observacion :string(255)
#  estado_cd              :integer
#  comentarios            :text(16777215)
#

class Observacion < ActiveRecord::Base
  has_paper_trail meta: { lista_cargo_id: :lista_cargo_id }, on: [:update]
  attr_accessible :lista_cargo_id, :nombre_candidato, :observable_id, :observable_type, :referencia_observacion, :estado_cd, :comentarios
  before_destroy :eliminar_versiones

  belongs_to :lista_cargo
  belongs_to :observable, polymorphic: true

  validates :lista_cargo_id, :observable_id, :observable_type, :referencia_observacion, :estado_cd, presence: true

  as_enum :estado, %w(pendiente subsanada no_subsanada)

  scope :join_candidaturas, -> { joins('INNER JOIN candidaturas ON candidaturas.id = observaciones.observable_id AND observaciones.observable_type = "Candidatura"') }
  scope :includes_archivos_persona, -> { includes(observable: { persona: { archivos_persona: :archivo } }) }
  scope :lista_cargo_id, -> (lista_cargo_id) { where(lista_cargo_id: lista_cargo_id) }
  scope :observables_ids, -> (ids_observables) { where('observable_id IN (?)', ids_observables) }
  scope :orden_posicion, -> { order('candidaturas.orden ASC, observaciones.id ASC') }
  scope :nombre_candidato, -> (nombre_candidato) { where('LOWER(nombre_candidato) LIKE LOWER(?)', "%#{nombre_candidato}%") }
  scope :referencia_observacion, -> (referencia_observacion) { where(referencia_observacion: referencia_observacion) }
  scope :subsanado, -> (subsanado) { where(estado_cd: subsanado) }
  scope :candidatura, -> (candidatura) { where(observable_id: candidatura.id, observable_type: 'Candidatura') }
  scope :candidaturas, -> (candidaturas) { where('observable_type = "Candidatura" AND observable_id IN (?)', candidaturas.pluck(&:id)) }
  scope :no_candidatura, -> (candidatura) { where('observable_id != ?', candidatura.id) }

  def self.crear_observacion(candidatura, referencia_observacion, con_version)
    unless candidatura.observaciones.exists?(referencia_observacion: referencia_observacion)
      observacion = new(lista_cargo_id: candidatura.lista_cargo_id, nombre_candidato: candidatura.decorate.nombre_completo, observable_id: candidatura.id, observable_type: 'Candidatura', referencia_observacion: referencia_observacion, estado_cd: Observacion.estados[:pendiente])
      con_version ? observacion.save : observacion.paper_trail.without_versioning { observacion.save }
    end
  end

  def self.eliminar_observacion(candidatura, referencia_observacion)
    if candidatura.observaciones.exists?(referencia_observacion: referencia_observacion)
      observaciones = candidatura(candidatura).referencia_observacion(referencia_observacion)
      PaperTrail::Version.where('item_type = "Observacion" AND item_id IN (?)', observaciones.select(:id)).delete_all
      observaciones.delete_all
    end
  end

  def cambiar_estado!(estado)
    assign_attributes(estado_cd: Observacion.estados[estado.to_sym])
    save
  end

  def persona
    observable.persona
  end

  def archivos_persona
    persona.archivos_persona.lista_cargo(lista_cargo)
  end

  private

  def eliminar_versiones
    PaperTrail::Version.where(item_type: 'Observacion', item_id: id).delete_all
  end

end
