# == Schema Information
#
# Table name: archivos_persona
#
#  id             :integer          not null, primary key
#  persona_id     :integer
#  archivo_id     :integer
#  created_at     :datetime
#  updated_at     :datetime
#  descripcion    :string(255)
#  tipo_archivo   :string(255)
#  lista_cargo_id :integer
#

class ArchivoPersona < ActiveRecord::Base
  has_paper_trail
  after_create  :actualizar_estado_observacion, if: -> (archivo_persona) { archivo_persona.lista_cargo_id.present? }
  after_update  :actualizar_estado_observacion, if: -> (archivo_persona) { archivo_persona.lista_cargo_id.present? }
  after_destroy :actualizar_estado_observacion, if: -> (archivo_persona) { archivo_persona.lista_cargo_id.present? }

  TIPO_ARCHIVO = { 'DOCUMENTOS OBLIGATORIOS' =>
                    { 'Aceptación Candidatura' => 'aceptacion_candidatura',
                      'Copia DNI' => 'fotocopia_dni',
                      'Certificado Reincidencia Criminal' => 'certificado_reincidencia_criminal' },
                   'SUBSANAR OBSERVACIONES' =>
                    { 'Deudor Alimentario' => 'deudor_alimentario',
                      'Inhabilitación' => 'inhabilitado',
                      'Residencia en la Ciudad' => 'antiguedad_residencia_persona',
                      'Residencia en la Comuna' => 'antiguedad_residencia_persona_comuna' },
                    'REINCIDENCIA' =>
                    { 'Antecedentes Penales' => 'antecedentes_penales'} }

  ARCHIVOS_OBLIGATORIOS = %w(fotocopia_dni aceptacion_candidatura certificado_reincidencia_criminal)

  attr_accessible :archivo, :descripcion, :tipo_archivo, :persona_id, :lista_cargo_id
  attr_accessor :tipo_archivo_descripcion

  belongs_to :persona
  belongs_to :lista_cargo
  belongs_to :archivo, dependent: :destroy
  has_many :candidaturas, through: :persona

  scope :lista_cargo, -> (lc) { where(lista_cargo: lc) }

  validates :tipo_archivo, :persona, :archivo, :lista_cargo_id, presence: true
  validates :tipo_archivo, inclusion: { in: TIPO_ARCHIVO.values.collect(&:values).flatten }

  def self.archivos_subsanar_cargo(cargo)
    if cargo.tipo_cargo.instance_of?(TiposCargos::TipoCargoComunero)
      TIPO_ARCHIVO['SUBSANAR OBSERVACIONES'].except('Residencia en la Ciudad')
    else
      TIPO_ARCHIVO['SUBSANAR OBSERVACIONES'].except('Residencia en la Comuna')
    end
  end

  def archivo=(archivo_subido)
    if archivo.nil?
      nuevo_archivo = Archivo.new
      nuevo_archivo.nombre = File.basename(archivo_subido.original_filename)
      nuevo_archivo.tipo_contenido = archivo_subido.content_type

      nuevo_archivo.paper_trail.without_versioning do
        nuevo_archivo.save!
      end

      nuevo_archivo.contenido = archivo_subido.read

      self.archivo_id = nuevo_archivo.id
    else
      archivo.contenido_archivos.destroy_all

      archivo.nombre = File.basename(archivo_subido.original_filename)
      archivo.tipo_contenido = archivo_subido.content_type

      archivo.paper_trail.without_versioning do
        archivo.save!
      end

      archivo.contenido = archivo_subido.read
    end
  end

  def as_json(_)
    super(include: { archivo: {} }, methods: [:tipo_archivo_descripcion])
  end

  def tipo_archivo_descripcion
    TIPO_ARCHIVO.values.reduce({}, :merge).key(tipo_archivo)
  end

  def subsana_observacion?
    TIPO_ARCHIVO['SUBSANAR OBSERVACIONES'].values.include? tipo_archivo
  end

  private

  def actualizar_estado_observacion
    if ArchivoPersona::ARCHIVOS_OBLIGATORIOS.include?(tipo_archivo)
      candidatura = candidaturas.lista_cargo(lista_cargo).first
      if candidatura
        observacion = candidatura.observaciones.referencia_observacion(tipo_archivo.upcase).first
        observacion.pendiente!
        observacion.save!
      end
    end
  end

end
