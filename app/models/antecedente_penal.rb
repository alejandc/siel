# == Schema Information
#
# Table name: antecedente_penal
#
#  id                     :integer          not null, primary key
#  estado_cd              :integer
#  fecha_ultima_consulta  :datetime
#  referencia_id          :string(255)
#  persona_id             :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  resultado              :text(65535)
#  fecha_inicio_solicitud :datetime
#  archivo_pdf            :binary(16777215)
#

class AntecedentePenal < ActiveRecord::Base
  has_paper_trail

  belongs_to :persona

  attr_accessible :estado, :estado_cd, :persona, :persona_id, :archivo,
                  :fecha_ultima_consulta, :referencia_id, :fecha_inicio_solicitud, 
                  :resultado, :archivo_pdf

  as_enum :estado, %w(no_solicitado inicio_solicitud en_proceso finalizado error_solicitud), prefix: true

  def contiene_certificado_valido?
    !archivo_pdf.blank? && !fecha_inicio_solicitud.blank? && ((DateTime.now - ap.fecha_inicio_solicitud.to_datetime).ceil <= 5)
  end
  
end
