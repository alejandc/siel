# == Schema Information
#
# Table name: domicilios
#
#  id               :integer          not null, primary key
#  calle_referencia :string(255)
#  numero           :string(255)
#  piso             :string(255)
#  depto            :string(255)
#  comuna           :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#  fecha            :date
#  codigo_postal    :string(255)
#  calle_nombre     :string(255)
#  origen_cd        :integer
#

class Domicilio < ActiveRecord::Base
  before_save :calcular_comuna
  before_save :calcular_calle_nombre, if: :usuario?

  has_paper_trail

  attr_accessible :calle_referencia, :calle_nombre, :comuna, :depto, :numero, :piso,
                  :fecha, :codigo_postal, :origen, :origen_cd

  has_one :persona

  as_enum :origen, padron: 0, renaper: 1, usuario: 2

  def completo?
    !calle_referencia.blank? && !numero.nil? && !numero.empty?
  end

  def descripcion
    "#{calle_nombre} #{numero} #{piso} #{depto}".gsub(/\s{2,}/, '')
  end

  def as_json(_)
    super(methods: ['descripcion'])
  end

  private

  def calcular_comuna
    if persona.present? && (calle_referencia.present? && numero.present?) && (calle_referencia_changed? || numero_changed?)
      circuito_id = CallejeroApi.rangos_calles(calle_referencia, numero).try(:first).try(:[], 'circuito_id')
      self.comuna = Callejero.instance.comuna_en_circuito(circuito_id) if circuito_id.present?
    end
  end

  def calcular_calle_nombre
    assign_attributes(calle_nombre: "#{Callejero.instance.calle(calle_referencia.to_i)['nombre']} #{numero}") if calle_referencia.present? && (calle_referencia_changed? || numero_changed?)
  end

end
