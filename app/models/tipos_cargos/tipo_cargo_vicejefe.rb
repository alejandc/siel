class TiposCargos::TipoCargoVicejefe

  def initialize(cargo)
    @cargo = cargo
  end

  def edad_requerida
    30
  end

  def cantidad_minima_adherentes
    0
  end

  def cantidad_maxima_adherentes
    0
  end

  def cantidad_minima_adherentes_afiliados
    0
  end

  def genero_consecutivo
    0
  end

  def get_reparto
  end

  def cumple_ley_cupos?(_candidaturas)
    true
  end

  def consecutivo_incorregible?(_candidaturas)
    false
  end

end
