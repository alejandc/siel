class TiposCargos::TipoCargoComunero

  def initialize(cargo)
    @cargo = cargo
  end

  def edad_requerida
    18
  end

  def leyenda_ciudadania
    'Nativo Comuna'
  end

  def anios_ciudadania
    2
  end

  def anios_residencia
    2
  end

  def cantidad_minima_adherentes
    200
  end

  def cantidad_maxima_adherentes
    200
  end

  def cantidad_minima_adherentes_afiliados
    (cantidad_minima_adherentes * 0.1).to_i
  end

  def genero_consecutivo
    1
  end

  def get_reparto
  end

  def cumple_ley_cupos?(candidaturas)
    candidaturas_cargo = candidaturas
    (candidaturas_cargo.hombres.size <= candidaturas_cargo.mujeres.size + 1) && (candidaturas_cargo.mujeres.size <= candidaturas_cargo.hombres.size + 1)
  end

  def consecutivo_incorregible?(candidaturas)
    Candidatura.mismo_sexo_consecutivo(candidaturas).present?
  end

  def numero_comuna
    @cargo.nombre.scan(/\d+/).join.to_i
  end

end
