class TiposCargos::TipoCargoJefe

  def initialize(cargo)
    @cargo = cargo
  end

  def edad_requerida
    30
  end

  def leyenda_ciudadania
    'Nativo Ciudad de Buenos Aires'
  end

  def anios_ciudadania
    5
  end

  def anios_residencia
    5
  end

  def cantidad_minima_adherentes
    1000
  end

  def cantidad_maxima_adherentes
    4000
  end

  def cantidad_minima_adherentes_afiliados
    (cantidad_minima_adherentes * 0.1).to_i
  end

  def genero_consecutivo
    0
  end

  def get_reparto
  end

  def cumple_ley_cupos?(_candidaturas)
    true
  end

  def consecutivo_incorregible?(_candidaturas)
    false
  end

  def numero_comuna
    nil
  end

end
