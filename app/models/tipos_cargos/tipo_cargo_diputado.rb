class TiposCargos::TipoCargoDiputado

  def initialize(cargo)
    @cargo = cargo
  end

  def edad_requerida
    18
  end

  def leyenda_ciudadania
    'Nativo Ciudad de Buenos Aires'
  end

  def anios_ciudadania
    4
  end

  def anios_residencia
    4
  end

  def cantidad_minima_adherentes
    1000
  end

  def cantidad_maxima_adherentes
    4000
  end

  def cantidad_minima_adherentes_afiliados
    (cantidad_minima_adherentes * 0.1).to_i
  end

  def genero_consecutivo
    2
  end

  def get_reparto
    RepartoDHondt
  end

  def cumple_ley_cupos?(candidaturas)
    unless candidaturas.empty?
      candidaturas_cargo = candidaturas
      porcentaje_hombres = (candidaturas_cargo.hombres.size * 100 / candidaturas_cargo.size)
      porcentaje_mujeres = (candidaturas_cargo.mujeres.size * 100 / candidaturas_cargo.size)
      porcentaje_hombres <= 65 && porcentaje_mujeres <= 65
    end
  end

  def consecutivo_incorregible?(candidaturas)
    Candidatura.mismo_sexo_consecutivo(candidaturas).present?
  end

  def numero_comuna
    nil
  end

end
