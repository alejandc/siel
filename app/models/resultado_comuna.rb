# == Schema Information
#
# Table name: resultados_comunas
#
#  id                  :integer          not null, primary key
#  eleccion_id         :integer
#  comuna_id           :integer
#  cantidad_mesas      :integer
#  cantidad_inscriptos :integer
#  created_at          :datetime
#  updated_at          :datetime
#

class ResultadoComuna < ActiveRecord::Base

  belongs_to :eleccion
  belongs_to :comuna

  attr_accessible :cantidad_inscriptos, :cantidad_mesas, :eleccion, :comuna

end
