# == Schema Information
#
# Table name: documentos_fuerzas
#
#  id          :integer          not null, primary key
#  fuerza_id   :integer
#  archivo_id  :integer
#  descripcion :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class DocumentoFuerza < ActiveRecord::Base
  belongs_to :fuerza
  belongs_to :archivo, dependent: :destroy

  attr_accessible :descripcion, :archivo

  has_paper_trail

  delegate :nombre, to: :archivo, prefix: true

  def archivo=(archivo_subido)
    if archivo.nil?
      nuevo_archivo = Archivo.new
      nuevo_archivo.nombre = File.basename(archivo_subido.original_filename)
      nuevo_archivo.tipo_contenido = archivo_subido.content_type
      nuevo_archivo.save!

      nuevo_archivo.contenido = archivo_subido.read

      self.archivo_id = nuevo_archivo.id
    else
      archivo.contenido_archivos.destroy_all

      archivo.nombre = File.basename(archivo_subido.original_filename)
      archivo.tipo_contenido = archivo_subido.content_type
      archivo.save!

      archivo.contenido = archivo_subido.read
    end
  end

end
