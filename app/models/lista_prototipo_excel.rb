class ListaPrototipoExcel

  def initialize(lista, lista_cargo = nil)
    @lista_cargos = lista_cargo.present? ? [lista_cargo] : lista.lista_cargos_habilitados
    @excel = Spreadsheet::Workbook.new

    @negrita = Spreadsheet::Format.new(weight: :bold)
    @bordes = Spreadsheet::Format.new(border: :medium)
    @bordes_centrado = Spreadsheet::Format.new(border: :medium, horizontal_align: :center)
    @bordes_negrita = Spreadsheet::Format.new(weight: :bold, border: :medium, horizontal_align: :center)
  end

  def imprimir
    @lista_cargos.each do |lista_cargo|
      imprimir_hoja_cargo(lista_cargo)
    end

    @excel
  end

  private

  def imprimir_hoja_cargo(lista_cargo)
    sheet = @excel.create_worksheet(name: lista_cargo.cargo_nombre.gsub('/a', '-a').gsub('/os', '-os'))

    imprimir_encabezados(sheet, lista_cargo)
    imprimir_tabla_candidaturas(sheet, lista_cargo)
    aplicar_estilos_columnas(sheet)
  end

  def imprimir_encabezados(sheet, lista_cargo)
    sheet.row(0).set_format(0, @negrita)
    sheet.row(0)[0] = 'Eleccion'
    sheet.row(0)[1] = lista_cargo.eleccion_descripcion

    sheet.row(1).set_format(0, @negrita)
    sheet.row(1)[0] = 'Nombre de la Lista'
    sheet.row(1)[1] = lista_cargo.lista_nombre

    ['Categoría', 'Cargo', 'Orden', 'Nombre / "Conocido Como"', 'DNI', 'Sexo', 'Observaciones'].each_with_index do |titulo, index|
      sheet.row(4).set_format(index, @bordes_negrita)
      sheet.row(4)[index] = titulo
    end
  end

  def imprimir_tabla_candidaturas(sheet, lista_cargo)
    fila_actual = 5

    lista_cargo.candidaturas.each do |candidatura|
      observaciones_candidatura = candidatura.validador_observaciones

      sheet.row(fila_actual).concat([candidatura.cargo_nombre.gsub(Cargo::CARGOS[:diputado], 'Legisladores').upcase,
                                     candidatura.titular? ? 'Titular' : 'Suplente',
                                     candidatura.decorate.orden,
                                     candidatura.decorate.nombre_completo,
                                     candidatura.persona_numero_documento,
                                     candidatura.persona_sexo])

      if observaciones_candidatura.empty?
        sheet.row(fila_actual).set_format(6, @bordes)
      else
        observaciones_candidatura.each_with_index do |observacion, i|
          sheet.row(fila_actual + i)[6] = observacion.second
          sheet.row(fila_actual + i).set_format(6, @bordes)
        end
      end

      6.times { |n| sheet.merge_cells(fila_actual, n, fila_actual + (observaciones_candidatura.empty? ? 0 : observaciones_candidatura.size - 1), n) }

      sheet.row(fila_actual).set_format(0, @bordes)
      sheet.row(fila_actual).set_format(1, @bordes)
      sheet.row(fila_actual).set_format(2, @bordes_centrado)
      sheet.row(fila_actual).set_format(3, @bordes)
      sheet.row(fila_actual).set_format(4, @bordes_centrado)
      sheet.row(fila_actual).set_format(5, @bordes_centrado)
      sheet.row(fila_actual).set_format(6, @bordes)

      fila_actual += (observaciones_candidatura.empty? ? 1 : observaciones_candidatura.size)
    end

    6.times { |n| sheet.row(fila_actual - 1).set_format(n, @bordes) }
  end

  def aplicar_estilos_columnas(sheet)
    sheet.column(0).width = 15
    sheet.column(1).width = 10
    sheet.column(2).width = 8
    sheet.column(3).width = 35
    sheet.column(4).width = 10
    sheet.column(5).width = 8
    sheet.column(6).width = 120
  end

end
