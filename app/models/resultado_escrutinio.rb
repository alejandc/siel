# == Schema Information
#
# Table name: resultado_escrutinios
#
#  id                        :integer          not null, primary key
#  eleccion_id               :integer
#  importacion_escrutinio_id :integer
#  numero_partido            :integer
#  lista_id                  :integer
#  total_votos               :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  codigo_cargo              :string(255)
#

class ResultadoEscrutinio < ActiveRecord::Base
  self.table_name = 'resultado_escrutinios'

  belongs_to :eleccion
  belongs_to :lista
  belongs_to :importacion_escrutinio

  validates :eleccion_id, :importacion_escrutinio_id, :numero_partido, :lista_id, :total_votos, presence: true

  scope :fuerza, -> (fuerza) { where(numero_partido: fuerza.numero) }
  scope :agrupados_partido, -> { group(:numero_partido) }
  scope :agrupados_cargo, -> { group(:codigo_cargo) }
  scope :agrupados_partido_lista, ->(numero_partido) { where(numero_partido: numero_partido).group(:lista_id) }
  scope :total_votos, -> { sum(:total_votos) }
  scope :total_votos_partido_cargo, -> (numero_partido, codigo_cargo) { where(numero_partido: numero_partido, codigo_cargo: codigo_cargo).total_votos }
  scope :total_votos_cargo, -> (codigo_cargo) { where(codigo_cargo: codigo_cargo).group(:numero_partido).total_votos }
  scope :total_votos_lista_cargo, -> (lista_id, codigo_cargo) { where(lista_id: lista_id, codigo_cargo: codigo_cargo).total_votos }

end
