# == Schema Information
#
# Table name: fiscales
#
#  id                     :integer          not null, primary key
#  tipo_documento         :string(255)
#  numero_documento       :integer
#  sexo                   :string(255)
#  fuerza_id              :integer
#  eleccion_id            :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  tipo_cd                :integer
#  codigo_establecimiento :integer
#  comuna                 :integer
#

class Fiscal < ActiveRecord::Base
  paginates_per 50

  attr_accessible :tipo_documento, :numero_documento, :sexo, :fuerza_id, :fuerza,
                  :eleccion_id, :eleccion, :tipo_cd, :codigo_establecimiento, :comuna

  belongs_to :fuerza
  belongs_to :eleccion

  scope :eleccion, -> (eleccion) { where(eleccion: eleccion) }
  scope :fuerza, -> (fuerza) { where(fuerza: fuerza) }
  scope :persona, -> (tipo_documento, numero_documento, sexo) { where(tipo_documento: tipo_documento, numero_documento: numero_documento, sexo: sexo) }

  as_enum :tipo, mesa: 0, general_comuna: 1, general_establecimiento: 2

  delegate :nombre, to: :fuerza, prefix: true

  validates :tipo_documento, :numero_documento, :sexo, :eleccion, :fuerza, :comuna, :tipo_cd, presence: true
  validate :repetido_eleccion
  validate :verificar_maximo_fiscal_general_establecimiento
  validate :verificar_maximo_fiscal_general_comuna
  validate :verificar_maximo_fiscal_mesa
  validate :verificar_codigo_establecimiento

  def self.search_by_filter(filtro_fiscal)
    fiscales = self

    if filtro_fiscal.numero_documento.present?
      fiscales = fiscales.where(numero_documento: filtro_fiscal.numero_documento)
    end

    if filtro_fiscal.eleccion_id.present?
      fiscales = fiscales.where(eleccion_id: filtro_fiscal.eleccion_id)
    end

    if filtro_fiscal.fuerza_id.present?
      fiscales = fiscales.where(fuerza_id: filtro_fiscal.fuerza_id)
    end

    if filtro_fiscal.tipo_cd.present?
      fiscales = fiscales.where(tipo_cd: filtro_fiscal.tipo_cd)
    end

    if filtro_fiscal.comuna.present?
      fiscales = fiscales.where(comuna: filtro_fiscal.comuna)
    end

    if filtro_fiscal.codigo_establecimiento.present?
      fiscales = fiscales.where(codigo_establecimiento: filtro_fiscal.codigo_establecimiento)
    end

    fiscales.order('created_at DESC')
  end

  def persona
    Persona.find_by(tipo_documento: tipo_documento, numero_documento: numero_documento, sexo: sexo)
  end

  def nombre_completo
    "#{persona.nombres} #{persona.apellidos}"
  end

  private

  def repetido_eleccion
    if Fiscal.exists?(eleccion: eleccion, tipo_documento: tipo_documento, numero_documento: numero_documento, sexo: sexo)
      errors.add(:repetido_eleccion, 'El Fiscal que se desea agregar ya esta en la lista o esta asociado a otra Agrupación Política en la misma elección.')
    end
  end

  def verificar_codigo_establecimiento
    if codigo_establecimiento.nil? && (mesa? || general_establecimiento?)
      errors.add(:codigo_establecimiento, 'Codigo de Establecimiento requerido para Fiscal tipo Mesa o Establecimiento.')
    end
  end

  def verificar_maximo_fiscal_general_comuna
    if tipo_cd == 1 && Fiscal.where(tipo_cd: 1, comuna: self.comuna).count > 1
      errors.add(:tipo_cd, 'Ya se alcanzó la cantidad maxima de Fiscales Generales por la Comuna')
    end
  end

  def verificar_maximo_fiscal_general_establecimiento
    if tipo_cd == 2 && Fiscal.where(tipo_cd: 2, codigo_establecimiento: self.codigo_establecimiento).count > 1
      errors.add(:tipo_cd, 'Ya se alcanzó la cantidad maxima de Fiscales Generales por Establecimiento')
    end
  end

  def verificar_maximo_fiscal_mesa
    if tipo_cd == 0 && !self.codigo_establecimiento.nil? && Fiscal.where(tipo_cd: 0).count >= MesasComuna.instance.mesas_por_establecimiento(self.comuna, self.codigo_establecimiento).count
      errors.add(:tipo_cd, 'Ya se alcanzó la cantidad maxima de Fiscales por Mesas del Establecimiento')
    end
  end
end
