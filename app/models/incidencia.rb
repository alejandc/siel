# == Schema Information
#
# Table name: incidencias
#
#  id               :integer          not null, primary key
#  tipo_documento   :string(255)
#  numero_documento :integer
#  sexo             :string(255)
#  tipo_incidencia  :string(255)
#  eleccion_id      :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  descripcion      :string(255)
#  cargo_id         :integer
#

class Incidencia < ActiveRecord::Base

  TIPOS_INCIDENCIAS = { 'Deudor Alimentario' => 'DEUDOR_ALIMENTARIO',
                        'Inhabilitado' => 'INHABILITADO',
                        'Reelección Inhabilitada' => 'REINCIDENCIA_CARGO' }

  attr_accessible :tipo_documento, :numero_documento, :sexo, :tipo_incidencia, :eleccion,
                  :eleccion_id, :descripcion, :cargo_id, :cargo

  belongs_to :eleccion
  belongs_to :cargo

  validates :numero_documento, :sexo, :tipo_incidencia, :eleccion_id, presence: true
  validates :cargo_id, presence: true, if: "tipo_incidencia == Incidencia::TIPOS_INCIDENCIAS['Reelección Inhabilitada']"
  validates :tipo_incidencia, inclusion: { in: TIPOS_INCIDENCIAS.values }

  # Metodo para procesar y generar las incidencias en la base de datos
  # @params incidencia [Incidencia] modelo que contiene la info generico (eleccion y tipo incidencia)
  # @params datos [String] con formato NUMERO_DOCUMENTO, TIPO_DOCUMENTO, SEXO concatenados para generar multiples incidencias
  # @return [Array] que contiene las incidencias que no se puede crear en base a las validaciones
  def self.procesar_incidencias(incidencia, datos)
    lista_documentos = datos.strip.split(/[\n\r]/).delete_if(&:blank?)

    incidencias_invalidas = []
    incidencias_validas = []
    lista_documentos.each do |info|
      numero_documento, tipo_documento, sexo, descripcion = info.gsub(/\s+/, '').split(',').map(&:upcase)
      sexo = 'NA' if sexo.blank?
      tipo_documento = tipo_documento.include?('DNI') ? 'DNI' : 'L'

      if numero_documento.try(:match, /^(\d)+$/) && sexo.try(:match, /M|F|NA/) && Persona::DOCUMENTO_TIPOS.values.include?(tipo_documento)
        unless Incidencia.where(tipo_incidencia: incidencia[:tipo_incidencia], eleccion_id: incidencia[:eleccion_id],
                                numero_documento: numero_documento, sexo: sexo, tipo_documento: tipo_documento).exists?

          incidencias_validas << Incidencia.new(tipo_incidencia: incidencia[:tipo_incidencia], eleccion_id: incidencia[:eleccion_id], cargo_id: incidencia[:cargo_id],
                             numero_documento: numero_documento, sexo: sexo, tipo_documento: tipo_documento, descripcion: descripcion)
        end
      else
        incidencias_invalidas << "#{numero_documento}, #{tipo_documento}, #{sexo}"
      end
    end

    Incidencia.import incidencias_validas, batch_size: 5000

    incidencias_invalidas
  end

  #--------------------------------------------------------------------------------#
  # Class Methods
  #--------------------------------------------------------------------------------#
  def self.search_by_filter(filtro_incidencia)
    incidencias = self

    if filtro_incidencia.tipo_incidencia.present?
      incidencias = incidencias.where(tipo_incidencia: filtro_incidencia.tipo_incidencia)
    end

    if filtro_incidencia.numero_documento.present?
      incidencias = incidencias.where(numero_documento: filtro_incidencia.numero_documento)
    end

    if filtro_incidencia.eleccion_id.present?
      incidencias = incidencias.where(eleccion_id: filtro_incidencia.eleccion_id)
    end

    incidencias.order('created_at DESC')
  end
end
