# == Schema Information
#
# Table name: personas
#
#  id                     :integer          not null, primary key
#  tipo_documento         :string(255)
#  numero_documento       :integer
#  nombres                :string(255)
#  apellidos              :string(255)
#  sexo                   :string(255)
#  fecha_nacimiento       :date
#  lugar_nacimiento       :string(255)
#  nombre_padre           :string(255)
#  nombre_madre           :string(255)
#  estado                 :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  domicilio_id           :integer
#  telefono               :string(255)
#  email                  :string(255)
#  nativo_caba            :boolean
#  nacionalidad_argentina :string(255)
#  denominacion           :string(255)
#  residencia             :string(255)
#  domicilio_completo     :string(255)
#  avatar_file_name       :string(255)
#  avatar_content_type    :string(255)
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  en_padron              :boolean          default(FALSE)
#

class Persona < ActiveRecord::Base
  has_paper_trail

  before_save :normalizar_atributos
  after_create :generar_antecedente_penal

  attr_accessor :obtener_info_renaper_attr

  NACIONALIDADES = { NATIVO: 'NATIVO', POR_OPCION: 'POR_OPCION', NATURALIZADO: 'NATURALIZADO' }
  RESIDENCIAS = { NATIVO_COMUNA: 'NATIVO_COMUNA', RESIDENCIA_COMUNA: 'RESIDENCIA_COMUNA' }
  DOCUMENTO_TIPOS = { DNI: 'DNI', LC: 'LC', LE: 'LE', L: 'L' }

  NACIONALIDADES.values.each do |nacionalidad|
    define_method("#{nacionalidad.downcase}?") do
      nacionalidad_argentina == nacionalidad
    end
  end

  RESIDENCIAS.values.each do |residencia_aux|
    define_method("#{residencia_aux.downcase}?") do
      residencia == residencia_aux
    end
  end

  { deudor_alimentario: 'Deudor Alimentario', reincidencia_cargo: 'Reelección Inhabilitada', inhabilitado: 'Inhabilitado' }.each do |metodo, incidencia|
    define_method("#{metodo}?") do |candidatura|
      query = Incidencia.where(eleccion_id: candidatura.eleccion.id, numero_documento: numero_documento, sexo: sexo, tipo_incidencia: Incidencia::TIPOS_INCIDENCIAS[incidencia])

      if incidencia == Incidencia::TIPOS_INCIDENCIAS.key('REINCIDENCIA_CARGO')
        query = query.where(cargo: candidatura.cargo)
      end

      query.exists?
    end
  end

  attr_accessible :apellidos, :estado, :fecha_nacimiento, :lugar_nacimiento,
                  :nombre_madre, :nombre_padre, :nombres, :numero_documento,
                  :sexo, :denominacion, :tipo_documento, :domicilio, :domicilio_completo,
                  :domicilio_attributes, :telefono, :email, :nativo_caba, :denominacion,
                  :nacionalidad_argentina, :residencia, :avatar, :en_padron,
                  :archivos_persona_attributes, :archivos_persona, :archivos_aceptacion_candidatura,
                  :archivos_fotocopia_dni, :archivos_certificado_reincidencia_criminal

  has_one :antecedente_penal
  has_many :candidaturas
  has_many :fuerzas, through: :candidaturas
  has_many :archivos_persona, class_name: 'ArchivoPersona', dependent: :destroy
  has_many :archivos, through: :archivos_persona
  has_many :archivos_aceptacion_candidatura, -> { where(tipo_archivo: 'aceptacion_candidatura') }, class_name: 'ArchivoPersona', dependent: :destroy
  has_many :archivos_fotocopia_dni, -> { where(tipo_archivo: 'fotocopia_dni') }, class_name: 'ArchivoPersona', dependent: :destroy
  has_many :archivos_certificado_reincidencia_criminal, -> { where(tipo_archivo: 'certificado_reincidencia_criminal') }, class_name: 'ArchivoPersona', dependent: :destroy
  has_many :adhesion
  has_attached_file :avatar, styles: { medium: '200x200>', thumb: '100x100>' }, default_url: '/empty_user.png'

  belongs_to :domicilio, dependent: :destroy

  accepts_nested_attributes_for :domicilio, allow_destroy: true
  accepts_nested_attributes_for :archivos_persona, allow_destroy: true, reject_if: proc { |attributes| attributes['archivo'].blank? }

  scope :dni_sexo, -> (tipo_documento, numero_documento, sexo) { where(tipo_documento: tipo_documento, numero_documento: numero_documento, sexo: sexo) }

  # validates :numero_documento, uniqueness: { unless: proc { |a| a.numero_documento.blank? }, message: 'Ya existe otra persona con ese número de documento' }
  validates_uniqueness_of :numero_documento, scope: [:sexo, :tipo_documento], presence: true
  validates :numero_documento, :sexo, presence: true
  validates :nombres, :apellidos, presence: true, on: :update
  # validates :nombres, :apellidos, :fecha_nacimiento, presence: true, on: :update
  # validates :domicilio_completo, :nacionalidad_argentina, :residencia, presence: true, unless: :en_padron, on: :update
  validates_attachment_content_type :avatar, content_type: %r{\Aimage\/.*\z}

  delegate :calle_referencia, :calle_nombre, :numero, :comuna, to: :domicilio, prefix: true

  def extranjero?
    numero_documento.between?(15000000, 16000000) || numero_documento >= 90000000
  end

  def cargo_en_lista(lista)
    lista.candidaturas.persona(self).first.cargo_nombre
  end

  def candidaturas_multiples(eleccion)
    Candidatura.candidaturas_eleccion(self, eleccion)
  end

  def candidaturas_multiples_en_otras_listas(eleccion, lista_cargo)
    Candidatura.joins(:lista_cargo).where('fecha_renuncia IS NULL AND persona_id = ? AND eleccion_id = ? AND lista_cargos.lista_id != ?', id, eleccion.id, lista_cargo.lista_id)
  end

  def candidaturas_multiples_en_otras_listas?(eleccion, lista_cargo)
    candidaturas_multiples_en_otras_listas(eleccion, lista_cargo).count >= 1
  end

  def adhesiones_multiples_en_otras_listas?(lista_cargo)
    Adhesion.joins(lista_cargo: :lista).where('persona_id = ? AND listas.eleccion_id = ? AND lista_cargos.lista_id != ?', id, lista_cargo.eleccion.id, lista_cargo.lista_id).count >= 1
  end

  def es_mayor_a?(edad, fecha)
    if fecha_nacimiento.present? && fecha.present?
      edad(fecha) >= edad
    else
      false
    end
  end

  def edad(fecha)
    if fecha_nacimiento.present? && fecha.present?
      fecha.year - fecha_nacimiento.year - ((fecha.month > fecha_nacimiento.month || (fecha.month == fecha_nacimiento.month && fecha.day >= fecha_nacimiento.day)) ? 0 : 1)
    else
      -1
    end
  end

  def adjuntar_archivo(subido, descripcion)
    archivo = Archivo.new
    archivo.nombre = File.basename(subido.original_filename)
    archivo.descripcion = descripcion
    archivo.tipo_contenido = subido.content_type
    archivo.save!

    archivos << archivo
    save

    # Se asigna por separado para que ya tenga un ID para la fk
    archivo.contenido = subido.read
  end

  def as_json(_)
    super(methods: [], include: { domicilio: {} })
  end

  def contiene_archivo_tipo?(lista_cargo, tipo_archivo)
    !archivos_persona.select { |archivo| archivo.lista_cargo_id == lista_cargo.id && archivo.tipo_archivo == tipo_archivo }.empty?
  end

  def campos_faltantes?
    fecha_nacimiento.blank? || (domicilio_calle_nombre.blank? && (domicilio_calle_referencia.blank? || domicilio_numero.blank?))
  end

  def archivo_requerido_por_tipo_archivo(tipo_archivo, lista_cargo)
    if tipo_archivo == ArchivoPersona::TIPO_ARCHIVO['DOCUMENTOS OBLIGATORIOS']['Aceptación Candidatura']
      archivos_aceptacion_candidatura.find_by_lista_cargo_id(lista_cargo.id)
    elsif tipo_archivo == ArchivoPersona::TIPO_ARCHIVO['DOCUMENTOS OBLIGATORIOS']['Copia DNI']
      archivos_fotocopia_dni.find_by_lista_cargo_id(lista_cargo.id)
    elsif tipo_archivo == ArchivoPersona::TIPO_ARCHIVO['DOCUMENTOS OBLIGATORIOS']['Certificado Reincidencia Criminal']
      archivos_certificado_reincidencia_criminal.find_by_lista_cargo_id(lista_cargo.id)
    end
  end

  def nombre_completo
    "#{nombres} #{apellidos}"
  end

  def self.buscar_o_crear(renglon_padron, opciones)
    # Tuvimos que optar por elegir entre LE y LC, ya que no conocemos las abreviaturas de estas variantes
    renglon_padron['tipo_documento'] = renglon_padron['tipo_documento'] =~ /L/ ? 'L' : 'DNI'
    persona = find_by(tipo_documento: renglon_padron['tipo_documento'], numero_documento: renglon_padron['matricula'], sexo: renglon_padron['sexo']) || Persona.new

    persona.paper_trail.without_versioning do
      domicilio = if persona.domicilio.blank?
                    persona.build_domicilio(calle_nombre: renglon_padron['domicilio'], numero: '', piso: '', depto: '', codigo_postal: '', comuna: renglon_padron['seccion'].try(:to_i), origen: (renglon_padron['domicilio'].present? ? :padron : :usuario))
                  else
                    persona.domicilio
                  end

      domicilio.paper_trail.without_versioning do
        persona.update_attributes!(tipo_documento: renglon_padron['tipo_documento'],
                                   numero_documento: renglon_padron['matricula'],
                                   sexo: renglon_padron['sexo'],
                                   nombres: renglon_padron['nombre'],
                                   apellidos: renglon_padron['apellido'],
                                   domicilio_completo: renglon_padron['domicilio'],
                                   denominacion: '',
                                   en_padron: renglon_padron['en_padron'] == false ? false : true)
      end
    end

    if opciones[:obtener_info_renaper] && (persona.fecha_nacimiento.blank? || persona.domicilio_calle_nombre.blank?)
      persona.obtener_info_renaper
      persona.save!
    end

    persona
  end

  def self.buscar_o_crear_extranjero(campos_extranjero, opciones)
    persona = find_by(numero_documento: campos_extranjero['documento_numero']) || Persona.new(numero_documento: campos_extranjero['documento_numero'], sexo: campos_extranjero['sexo'], tipo_documento: campos_extranjero['documento_tipo'], nombres: campos_extranjero['nombre'], apellidos: campos_extranjero['apellido'])
    persona.save unless persona.persisted?
    persona.create_domicilio(comuna: campos_extranjero['numeroseccion']) unless persona.domicilio.present?

    if opciones[:obtener_info_renaper] && (persona.fecha_nacimiento.blank? || persona.domicilio_calle_nombre.blank?)
      persona.obtener_info_renaper
      persona.save!
    end

    persona
  end

  def obtener_info_renaper
    if tipo_documento.upcase == 'DNI'
      info_renaper = RenaperApi.ultimo_ejemplar(numero_documento, sexo)

      if info_renaper['nroError'] == '0'
        self.fecha_nacimiento = Date.parse(info_renaper['fechaNacimiento'])
        self.nombres = info_renaper['nombres'].try(:upcase)
        self.apellidos = info_renaper['apellido'].try(:upcase)

        if domicilio.blank?
          domicilio_aux = Domicilio.new(calle_referencia: '',
                                        calle_nombre: info_renaper['calle'],
                                        numero: info_renaper['numero'],
                                        piso: info_renaper['piso'],
                                        depto: info_renaper['departamento'],
                                        codigo_postal: info_renaper['cpostal'],
                                        origen: :renaper)

          domicilio_aux.paper_trail.without_versioning { domicilio_aux.save! }
          self.domicilio = domicilio_aux
        elsif domicilio.calle_nombre.blank?
          domicilio.calle_nombre = "#{info_renaper['calle']} #{info_renaper['numero']} - #{info_renaper['cpostal']}".gsub(/\s{2,}/, '')
        end
      end
    end
  end

  def puede_editar_domicilio?
    domicilio.blank? || domicilio.persisted? && domicilio.calle_nombre.blank? || domicilio.usuario?
  end

  def valida_consulta_antecedentes_penales?
    !nombres.blank? && !apellidos.blank? && !numero_documento.blank? && !sexo.blank? && !fecha_nacimiento.blank?
  end

  private

  def normalizar_atributos
    self.nombres = nombres.try(:upcase)
    self.apellidos = apellidos.try(:upcase)
    self.sexo = sexo.try(:upcase)
  end

  def generar_antecedente_penal
    AntecedentePenal.create(estado_cd: 0, persona_id: id)
  end

end
