# == Schema Information
#
# Table name: usuarios
#
#  id              :integer          not null, primary key
#  email           :string(255)
#  nombre_usuario  :string(255)
#  password_digest :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  perfiles        :string(255)
#  lista_id        :integer
#  eleccion_id     :integer
#  habilitado      :boolean          default(TRUE)
#


class Usuario < ActiveRecord::Base
  include PerfilUsuario # Todo lo referente al uso de perfiles
  has_paper_trail

  after_initialize :cargar_perfiles, if: :es_usuario_interno? # Callback en modulo PerfilUsuario

  attr_accessor :fuerza_id, :perfil

  attr_accessible :email, :nombre_usuario, :password, :password_confirmation,
                  :usuarios_fuerzas_attributes, :password_digest, :perfiles, :id,
                  :lista_id, :eleccion_id, :lista, :fuerza_id, :perfil, :habilitado

  has_many :usuarios_fuerzas, dependent: :destroy, class_name: 'UsuarioFuerza'
  belongs_to :lista
  belongs_to :eleccion

  accepts_nested_attributes_for :usuarios_fuerzas

  has_secure_password

  validates_presence_of :password, on: :create
  validates :email, :nombre_usuario, presence: true, uniqueness: true
  validates :perfiles, presence: true, inclusion: { in: Usuario.perfiles_permitidos.map(&:last) }
  validates :password, :password_confirmation, length: { in: 6..25 }, if: :cambia_contrasenia?
  validate :existencia_active_directory

  scope :nombre_usuario, -> (nombre_usuario) { where('LOWER(nombre_usuario) LIKE LOWER(?)', "%#{nombre_usuario}%") }
  scope :de_lista, -> { where(perfiles: 'lista_partidaria') }
  scope :eleccion, -> (eleccion_id) { where(eleccion_id: eleccion_id) }
  scope :fuerza, -> (fuerza) { joins(:usuarios_fuerzas).where('usuarios_fuerzas.fuerza_id = ?', fuerza.id) }
  scope :perfil, -> (perfil) { where(perfiles: perfil) }

  def fuerza
    unless usuarios_fuerzas.nil? || usuarios_fuerzas.empty?
      usuarios_fuerzas.first.fuerza
    end
  end

  def pertenece_a_fuerza?(fuerza)
    usuarios_fuerzas.detect { |usuario_fuerza| usuario_fuerza.fuerza_id == fuerza.id }
  end

  def limpiar_filtros
    self.eleccion_id = Eleccion.actual.paso.id
    self.fuerza_id = nil
    self.perfil = nil
    self.nombre_usuario = nil
  end

  def blanquear_clave
    nueva_clave = SecureRandom.urlsafe_base64

    self.password = nueva_clave
    self.password_confirmation = nueva_clave
  end

  def cambiar_contrasenia!(vieja_contrasenia, nueva_contrasenia)
    if authenticate(vieja_contrasenia)
      self.password = nueva_contrasenia
      self.password_confirmation = nueva_contrasenia
      validar_contrasenia
    else
      errors.add(:wrong_password, 'La contraseña actual es incorrecta')
      false
    end
  end

  def self.search_by_filter(filtro_usuario)
    usuarios = self

    if filtro_usuario.nombre_usuario.present?
      usuarios = usuarios.where(nombre_usuario: filtro_usuario.nombre_usuario)
    end

    if filtro_usuario.eleccion_id.present?
      usuarios = usuarios.where(eleccion_id: filtro_usuario.eleccion_id)
    end

    if filtro_usuario.fuerza_id.present?
      usuarios = usuarios.fuerza(Fuerza.find(filtro_usuario.fuerza_id))
    end

    if filtro_usuario.perfil.present?
      usuarios = usuarios.perfil(filtro_usuario.perfil)
    end

    usuarios.order('created_at DESC')
  end

  private

  def existencia_active_directory
    errors.add(:nombre_usuario, 'Usuario existente en Active Directory') if !nombre_usuario.empty? && TSJ_SECURITY.validate(nombre_usuario)
  end

  def validar_contrasenia
    if password.size >= 6
      save!
    else
      errors.add(:password_validations, 'La contraseña es muy débil')
      false
    end
  end

  def cambia_contrasenia?
    password.present? && password_confirmation.present?
  end

end
