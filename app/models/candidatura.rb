﻿
# == Schema Information
#
# Table name: candidaturas
#
#  id                   :integer          not null, primary key
#  persona_id           :integer
#  created_at           :datetime
#  updated_at           :datetime
#  orden                :integer
#  fotocopia_dni        :boolean
#  constancia_domicilio :boolean
#  tipo_nacionalidad    :string(255)
#  cargo_dos_periodos   :boolean
#  aceptacion_firmada   :boolean
#  deudor_alimentario   :boolean
#  inhabilitado         :boolean
#  fecha_oficializacion :datetime
#  reside_en_comuna     :boolean
#  fecha_renuncia       :datetime
#  dni_tarjeta          :boolean
#  divisor              :integer
#  cociente             :decimal(12, 4)
#  tiene_escanio        :boolean
#  eleccion_id          :integer
#  cargo_id             :integer
#  validaciones         :text(16777215)
#  lista_cargo_id       :integer
#  antiguedad_persona   :integer
#  antiguedad_comuna    :integer
#  lista_cargo_paso_id  :integer
#

class Candidatura < ActiveRecord::Base
  has_paper_trail meta: { lista_cargo_id: :lista_cargo_id }
  before_validation :asignar_orden
  after_initialize :crear_validador
  before_create :calcular_antiguedad_persona

  serialize :validaciones, Hash

  belongs_to :persona
  belongs_to :eleccion
  belongs_to :cargo
  belongs_to :lista_cargo, counter_cache: true
  belongs_to :lista_cargo_paso, class_name: 'ListaCargo', foreign_key: 'lista_cargo_paso_id'
  has_one :lista, through: :lista_cargo
  has_one :fuerza, through: :lista_cargo
  has_many :observaciones, as: :observable, dependent: :delete_all

  attr_accessor :incumple_cupo, :validador

  attr_accessible :lista_cargo_id, :persona_id, :persona, :lista_cargo, :orden,
                  :fotocopia_dni, :dni_tarjeta, :constancia_domicilio,
                  :tipo_nacionalidad, :cargo_dos_periodos, :aceptacion_firmada,
                  :deudor_alimentario, :inhabilitado, :persona_attributes,
                  :reside_en_comuna, :fecha_renuncia, :cociente, :tiene_escanio,
                  :eleccion, :eleccion_id, :cargo, :cargo_id, :validaciones,
                  :antiguedad_persona, :antiguedad_comuna, :updated_at, :lista_cargo_paso_id

  accepts_nested_attributes_for :persona

  TIPOS_NACIONALIDAD = %w(NATIVO POR_OPCION NATURALIZADO)

  # FIXME: verificar donde se usa, parece que es viejo y ya no sirve mas (borrarlo...)
  VALIDACIONES = { constancia_domicilio: 'Acreditación de residencia en CABA',
                   fotocopia_dni: 'Presenta fotocopia de DNI',
                   aceptacion_firmada: 'Formulario de aceptación de candidatura firmada',
                   cargo_dos_periodos: 'Posee cargos en los últimos dos períodos',
                   deudor_alimentario: 'Es deudor alimentario',
                   inhabilitado: 'INHABILITADO',
                   dni_tarjeta: 'Presenta DNI tarjeta' }

  # Filter scopes
  scope :lista_cargo, -> (lista_cargo) { where(lista_cargo: lista_cargo) }
  scope :no_renunciadas, -> { where('fecha_renuncia IS NULL') }
  scope :tipo_fuerza, -> (tf) { joins(lista_cargo: :fuerza).where('fuerzas.tipo = ?', tf.upcase) }
  scope :eleccion, -> (e) { where('eleccion_id = ?', e) }
  scope :cargo, -> (c) { where('cargo_id = ?', c) }
  scope :persona, -> (persona) { where(persona: persona) }
  scope :numero_documento, -> (num) { joins(:persona).where('personas.numero_documento = ?', num) }
  scope :apellido, -> (ape) { joins(:persona).where('personas.apellidos like ?', "%#{ape}%") }
  scope :figura_en_padron, -> (figura) { joins(:persona).joins('LEFT JOIN renglones_padron ON personas.numero_documento = renglones_padron.matricula').where("renglones_padron.id IS #{(figura == 'true') ? 'NOT' : ''} NULL") }
  scope :nativo_caba, -> (nativo) { joins(:persona).where("personas.nativo_caba = ? #{'OR personas.nativo_caba IS NULL' if nativo == 'false'}", nativo == 'true') }
  scope :cargo_dos_periodos, -> (repite) { where("cargo_dos_periodos = ? #{'OR cargo_dos_periodos IS NULL' if repite == 'false'}", repite == 'true') }
  scope :constancia_domicilio, -> (presento) { where("constancia_domicilio = ? #{'OR constancia_domicilio IS NULL' if presento == 'false'}", presento == 'true') }
  scope :dni_tarjeta, -> (tarjeta) { where("dni_tarjeta = ? #{'OR dni_tarjeta IS NULL' if tarjeta == 'false'} ", tarjeta == 'true') }
  scope :por_eleccion_y_cargo, -> (eleccion, cargo) { where(eleccion: eleccion, cargo: cargo) }
  scope :por_fuerza, -> (fuerza) { joins(:lista_cargo).where('fuerza_id = ?', fuerza.id) }
  scope :hombres, -> { joins(:persona).where('personas.sexo = ?', 'M') }
  scope :mujeres, -> { joins(:persona).where('personas.sexo = ?', 'F') }
  scope :candidaturas_eleccion, -> (persona, eleccion) { persona(persona).eleccion(eleccion).no_renunciadas }
  scope :de_listas_no_cerradas, -> { joins(:lista_cargo).where('lista_cargos.estado_candidaturas_cd <> ? AND lista_cargos.estado_adhesiones_cd <> ?', ListaCargo.estado_candidaturas[:cerrada], ListaCargo.estado_adhesiones[:cerrada]).select('DISTINCT lista_cargo_id') }
  scope :join_personas, -> { joins('INNER JOIN personas ON personas.id = candidaturas.persona_id ') }

  delegate :tipo_documento, :numero_documento, :nombres, :apellidos, :denominacion, :fecha_nacimiento, :sexo, :avatar, :en_padron,
           :domicilio_completo, :domicilio_comuna, :nacionalidad_argentina, :residencia, :contiene_archivo_tipo?,
           :deudor_alimentario?, :reincidencia_cargo?, :inhabilitado?, :campos_faltantes?, :domicilio_calle_nombre, to: :persona, prefix: true
  delegate :tipo, :nombre, to: :fuerza, prefix: true
  delegate :fecha, :anio, :paso?, :general?, :habilitar_carga_candidaturas, to: :eleccion, prefix: true
  delegate :observaciones, to: :validador, prefix: true
  delegate :nombre, :edad_requerida, :anios_ciudadania, :anios_residencia, :numero_comuna, :cantidad_minima, to: :cargo, prefix: true
  delegate :estado_candidaturas_sin_validar!, :cambiar_estado_candidaturas_sin_validar!, :estado_adhesiones_sin_validar!, :cambiar_estado_adhesiones_sin_validar!, to: :lista_cargo, prefix: true
  delegate :reabierta?, to: :lista, prefix: true
  delegate :lista_numero, :lista_nombre, to: :lista_cargo_paso, prefix: true

  validate  :cumple_maximo, on: :create
  validate  :eleccion_cargo_seleccionado, :esta_en_cargo, :validar_numero_documento_extranjero
  validates :lista_cargo_paso_id, presence: true, if: -> (candidatura) { candidatura.eleccion_general? }

  # Metodo valido solo para validar que un candidato no sea extranjero.
  def validar_numero_documento_extranjero
    if persona.extranjero?
      errors.add(:persona_numero_documento, 'Número documento no admitido: El DNI ingresado pertenece a una persona extranjera. Los candidatos deben ser argentinos nativos, naturalizados o por opción.')
    end
  end

  def self.find_by_eleccion_documentos(eleccion, numeros_documento)
    Candidatura.eleccion(eleccion).joins(:persona).where('personas.numero_documento in (?)', numeros_documento)
  end

  def candidaturas_en_lista
    Candidatura.joins(lista_cargo: :cargo).where('lista_cargos.id IN (?) AND persona_id = ?', lista_cargo.lista.lista_cargos_habilitados.select('id'), persona_id)
  end

  def cargos_en_lista
    candidaturas_en_lista.select('cargos.nombre')
  end

  def nativo?
    !tipo_nacionalidad.nil? && tipo_nacionalidad == 'NATIVO'
  end

  def cumple_minimo?
    fuerza.cumple_minimo?(eleccion, cargo)
  end

  def cumple_ciudadania?
    antiguedad_persona >= cargo.anios_ciudadania
  end

  def cumple_residencia?
    antiguedad_persona >= cargo.anios_residencia
  end

  def cumple_residencia_comuna?
    antiguedad_comuna >= cargo.anios_residencia
  end

  def existe_registro_candidatura?
    RegistroCandidatura.candidatura(self).afectadas_por_anio(eleccion_anio).exists?
  end

  def borrar
    transaction do
      destroy
      ActiveRecord::Base.connection.execute(query_actualizar_orden('-', orden, lista_cargo.maximo_orden))

      # Para ejecutar callback de observer y actualizar observación de foto
      lista_cargo.candidaturas.first.try(:save)
    end
  end

  def renunciar(fecha)
    Candidatura.mover_posiciones(proximas, -1)
    self.fecha_renuncia = fecha
    save
  end

  def proximas
    Candidatura.where('eleccion_id = ? AND cargo_id = ? AND lista_cargo_id = ? AND orden > ? AND fecha_renuncia IS NULL', eleccion_id, cargo_id, lista_cargo_id, orden).order(:orden)
  end

  def query_actualizar_orden(operador, minimo_orden, maximo_orden)
    %(
      UPDATE candidaturas
      SET orden = orden #{operador} 1
      WHERE eleccion_id = #{eleccion_id}
        AND cargo_id = #{cargo_id}
        AND lista_cargo_id = #{lista_cargo_id}
        AND orden >= #{minimo_orden} AND orden <= #{maximo_orden}
        AND fecha_renuncia IS NULL
        AND id != #{id};
    ).gsub(/\s+/, ' ').strip
  end

  def disminuir_anteriores(nuevo_orden)
    ActiveRecord::Base.connection.execute(query_actualizar_orden('-', orden, nuevo_orden))
  end

  def incrementar_proximas(nuevo_orden)
    ActiveRecord::Base.connection.execute(query_actualizar_orden('+', nuevo_orden, orden))
  end

  def titular?
    orden <= cargo.cantidad_minima
  end

  def documentacion_completa?
    (persona.nativo_caba || constancia_domicilio) && fotocopia_dni && aceptacion_firmada && !persona.fecha_nacimiento.nil?
  end

  def valida?
    (persona.fecha_nacimiento.nil? || edad_valida?) and !persona_deudor_alimentario? and !inhabilitado? and !cargo_dos_periodos
  end

  def edad_valida?
    edad_requerida = cargo_edad_requerida
    persona.es_mayor_a?(edad_requerida, fecha_validacion_edad)
  end

  def fecha_validacion_edad
    case cargo.nombre
    when Cargo::CARGOS[:jefe] then eleccion.fecha
    when Cargo::CARGOS[:vicejefe] then eleccion.fecha
    when Cargo::CARGOS[:diputado] then eleccion.fecha_posesion
    when /Comuna/ then eleccion.fecha
    end
  end

  def figura_en_padron?
    RenglonPadron.where(matricula: persona.numero_documento).exists?
  end

  def renglon_padron
    RenglonPadron.find_by(matricula: persona.numero_documento)
  end

  def oficializada?
    !fecha_oficializacion.nil?
  end

  def constancia_aceptacion
    case cargo.nombre
    when Cargo::CARGOS[:jefe] then AceptacionJefePdf.new(self)
    when Cargo::CARGOS[:vicejefe] then AceptacionVicejefePdf.new(self)
    when Cargo::CARGOS[:diputado] then AceptacionDiputadoPdf.new(self)
    when /Comuna/ then AceptacionComuneroPdf.new(self)
    end
  end

  def self.filtrar(attributes)
    supported_filters = [:tipo_fuerza, :eleccion, :cargo, :numero_documento,
                         :apellido, :figura_en_padron, :nativo_caba, :cargo_dos_periodos,
                         :constancia_domicilio, :dni_tarjeta]

    attributes.slice(*supported_filters).inject(send('no_renunciadas')) do |scope, (key, value)|
      value.present? ? scope.send(key, value) : scope
    end
  end

  def primero?
    orden == 1
  end

  def ultimo?
    maximo = lista_cargo.candidaturas_eleccion.select { |c| c.cargo == cargo }.map(&:orden).max
    orden == maximo
  end

  def cambiar_orden!(nuevo_orden)
    Candidatura.transaction do
      if nuevo_orden > orden
        disminuir_anteriores(nuevo_orden)
      elsif nuevo_orden < orden
        incrementar_proximas(nuevo_orden)
      end

      update_attributes!(orden: nuevo_orden)

      # Para ejecutar callback de observer y actualizar observación de foto
      lista_cargo.candidaturas.first.try(:save) if nuevo_orden != 1
    end
  end

  def subir
    ant = anterior

    self.orden -= 1
    ant.orden += 1

    save
    ant.save
  end

  def bajar
    sig = siguiente

    self.orden += 1
    sig.orden -= 1

    save
    sig.save
  end

  def resultado
    eleccion_cargo.resultado(fuerza)
  end

  def anterior
    lista_cargo.candidaturas_eleccion.detect { |c| c.cargo_id == cargo_id && c.orden == (orden - 1) }
  end

  def siguiente
    lista_cargo.candidaturas_eleccion.detect { |c| c.cargo_id == cargo_id && c.orden == (orden + 1) }
  end

  def <=>(other)
    if cargo_id == other.cargo_id
      orden <=> other.orden
    else
      cargo <=> other.cargo
    end
  end

  def self.destroyed(eleccion, fuerza)
    versions = PaperTrail::Version.where(item_type: name, event: 'destroy')

    versions.select do |v|
      c = v.reify
      c.eleccion_id == eleccion.id && c.lista_cargo.fuerza_id == fuerza.id
    end
  end

  def self.cumple_ley_cupos?(candidaturas)
    Candidatura.tercero_mismo_sexo(candidaturas).nil?
  end

  def self.corregir_cupo(candidaturas)
    cupo_corregido = candidaturas.dup
    sexo_consecutivo = nil

    while cantidad_mismo_sexo_consecutivo = Candidatura.mismo_sexo_consecutivo(cupo_corregido)
      sexo_consecutivo = cantidad_mismo_sexo_consecutivo.persona.sexo
      cupo_corregido.delete(cantidad_mismo_sexo_consecutivo)
      Candidatura.reordenar_cargo(cupo_corregido)
    end

    contador_candidaturas_para_borrar = candidaturas.size - cupo_corregido.size
    
    candidaturas = candidaturas.reverse.delete_if do |c|
      if c.persona.sexo == sexo_consecutivo && contador_candidaturas_para_borrar >= 0
        contador_candidaturas_para_borrar -= 1
        true
      else
        false
      end
    end.reverse

    Candidatura.reordenar_cargo(candidaturas)

    candidaturas.each_with_index { |c, i| c.orden = i + 1 }
  end

  # FIXME: Chequear si se va a seguir usando, ya que los ordenamientos siempre se hacen por cargo
  def self.auto_ordenamiento(candidaturas)
    reordenadas = []

    Cargo.all.sort.each do |cargo|
      candidaturas_cargo = candidaturas.select { |c| c.cargo_id == cargo.id }
      reordenadas += Candidatura.auto_ordenamiento_cargo(candidaturas_cargo) unless candidaturas_cargo.empty?
    end

    reordenadas
  end

  # FIXME: Chequear si se va a seguir usando, ya que los ordenamientos siempre se hacen por cargo
  def self.sugerir_orden(candidaturas)
    reordenadas = []

    Cargo.all.sort.each do |cargo|
      candidaturas_cargo = candidaturas.select { |c| c.cargo_id == cargo.id }

      unless candidaturas_cargo.empty?
        Candidatura.sugerir_orden_cargo(candidaturas_cargo)
        reordenadas += candidaturas_cargo
      end
    end

    reordenadas
  end

  def self.reordenar_cargo(candidaturas)
    cantidad_mismo_sexo_consecutivo = Candidatura.mismo_sexo_consecutivo(candidaturas)

    if cantidad_mismo_sexo_consecutivo.present?
      sexo_repetido = cantidad_mismo_sexo_consecutivo.persona.sexo.upcase
      orden_primer_candidato_en_conflicto  = cantidad_mismo_sexo_consecutivo.orden - 3
      
      if sexo_repetido == 'F' && orden_primer_candidato_en_conflicto > 1
        Candidatura.mover_item(candidaturas, candidaturas[orden_primer_candidato_en_conflicto], orden_primer_candidato_en_conflicto)
        Candidatura.reordenar_cargo(candidaturas)
      else
        candidaturas_restantes = candidaturas[(cantidad_mismo_sexo_consecutivo.orden - 1)..candidaturas.length]
        proximo_sexo = Candidatura.primero_sexo(candidaturas_restantes, (sexo_repetido == 'M') ? 'F' : 'M')

        # ¿Puede corregirse la lista_cargo?
        if proximo_sexo.present?
          Candidatura.mover_item(candidaturas, proximo_sexo, cantidad_mismo_sexo_consecutivo.orden)
          Candidatura.reordenar_cargo(candidaturas)
        end
      end
    end
  end

  def self.sugerir_orden_cargo(candidaturas)
    candidaturas.sort!

    reordenar_cargo(candidaturas)

    if Candidatura.mismo_sexo_consecutivo(candidaturas)
      candidaturas.reverse!
      candidaturas.each_with_index { |c, i| c.orden = i + 1 }
      reordenar_cargo(candidaturas)
      candidaturas.reverse!
      candidaturas.each_with_index { |c, i| c.orden = i + 1 }
    end
  end

  def self.auto_ordenamiento_cargo(candidaturas)
    sugerir_orden_cargo(candidaturas)
    candidaturas.each(&:save)
  end

  def self.mover_item(candidaturas, item, nueva_posicion)
    candidaturas.delete_at(item.orden - 1)
    candidaturas.insert(nueva_posicion - 1, item)
    candidaturas.each_with_index { |c, i| c.orden = i + 1 }
  end

  # FIXME: Al parecer no se está utilizando más
  def self.mover_item!(candidaturas, item, nueva_posicion)
    Candidatura.mover_item(candidaturas, item, nueva_posicion)
    candidaturas.each(&:save)
  end

  def self.primero_sexo(candidaturas, sexo)
    candidaturas.detect { |c| c.persona.sexo == sexo } if candidaturas
  end

  # FIXME: Chequear para que se usa
  def self.marcar_incumplimientos_cupo!(candidaturas)
    candidaturas.each_cons(3) do |a|
      a[2].incumple_cupo = (a[0].persona.sexo == a[1].persona.sexo && a[1].persona.sexo == a[2].persona.sexo && a[0].cargo == a[1].cargo && a[1].cargo == a[2].cargo)
    end
  end

  def self.mismo_sexo_consecutivo(candidaturas)
    unless candidaturas.empty?
      consecutividad = candidaturas.first.cargo.genero_consecutivo + 1
      candidaturas.each_cons(consecutividad) do |a|
        if a.all? { |candidatura| candidatura.persona_sexo == a.first.persona_sexo }
          return a.last
        end
      end
    end
  end

  # FIXME: Cuando se tenga que importar una candidatura, chequear el método "agregar_candidatura", que debería estar en la lista_cargo y no en la fuerza
  # def self.importar(eleccion, fuerza, rows)
  #   rows.each do |r|
  #     cargo = Cargo.find_by(nombre: r['Cargo'])
  #     fuerza.crear_candidatura(eleccion, cargo, r['DNI'].to_i, r['Nombres'], r['Apellidos'], r['Sexo']) unless cargo.nil?
  #   end
  # end

  def self.mover_posiciones(candidaturas, posiciones)
    candidaturas.each do |c|
      c.orden += posiciones
      c.save
    end
  end

  def self.referencia_job_importacion
    'importar_precandidatos_job_id'
  end

  # FIXME: ver donde se usa este metodo ya que al final esta incluyendo eleccion_cargo para llegar
  # a la eleccion y el cargo que ahora lo tenemos directamente en la candidatura.
  def as_json(_)
    super(methods: ['incumple_cupo', 'fotocopia_dni?', 'inhabilitado?', 'reside_en_comuna?', 'constancia_domicilio?', 'cargo_dos_periodos?', 'aceptacion_firmada?', 'deudor_alimentario?', 'edad_valida?', 'documentacion_completa?', 'valida?', 'titular?', 'primero?', 'ultimo?', 'oficializada?'], include: { persona: { include: { domicilio: { methods: ['descripcion'] } } }, fuerza: {}, eleccion_cargo: { include: [:cargo, :eleccion] } })
  end

  def eleccion_cargo
    @eleccion_cargo ||= EleccionCargo.find_by!(eleccion: eleccion, cargo: cargo)
  end

  def candidaturas_multiples_en_otras_listas
    persona.candidaturas_multiples_en_otras_listas(eleccion, lista_cargo)
  end

  def candidaturas_multiples_en_otras_listas?
    persona.candidaturas_multiples_en_otras_listas?(eleccion, lista_cargo)
  end

  private

  def asignar_orden
    self.orden = lista_cargo.candidaturas.where(eleccion: eleccion, cargo: cargo, fecha_renuncia: nil).count + 1 unless persisted?
  end

  def cumple_maximo
    errors.add(:cumple_maximo, 'El cupo de candidatos ya esta completo') unless lista_cargo.cumple_maximo?
  end

  def eleccion_cargo_seleccionado
    errors.add(:cargo_no_seleccionado, 'La elección no cumple con el cargo') unless EleccionCargo.find_by(eleccion: eleccion, cargo: cargo).seleccionado
  end

  def esta_en_cargo
    errors.add(:esta_en_cargo, 'Ya está agregado en el cargo') if !lista_cargo.candidaturas.persona(persona).empty? && !persisted?
  end

  def crear_validador
    self.validador = Validators::ValidadorCandidatura.new(self)
  end

  def calcular_antiguedad_persona
    self.antiguedad_persona = (persona.en_padron && antiguedad_persona.nil?) ? Padrones.instance.antiguedad_persona(persona.tipo_documento, persona.numero_documento, persona.sexo) : 0
    self.antiguedad_comuna = (persona.en_padron && antiguedad_comuna.nil?) ? Padrones.instance.antiguedad_comuna(persona.tipo_documento, persona.numero_documento, persona.sexo, cargo) : 0
    true # Para que no falle al guardar si el if da false
  end

end
