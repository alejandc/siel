# == Schema Information
#
# Table name: padrones
#
#  id                   :integer          not null, primary key
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  fecha                :datetime
#  cantidad_renglones   :integer
#  importacion_completa :boolean
#  created_by           :string(255)
#

class Padron < ActiveRecord::Base
  attr_accessible :fecha

  has_many :renglones_padron, dependent: :destroy, class_name: 'RenglonPadron'

  def cantidad_real_renglones
    RenglonPadron.where(padron_id: id).count
  end

  def self.importar(filename, username, bulk_size = BULK_INSERT_SIZE)
    lineas = File.foreach(filename).count
    p "cantidad de lineas: #{lineas}"

    padron = Padron.new
    padron.fecha = Time.now.to_s(:db)
    # padron.cantidad_renglones = File.size(filename) / BYTES_RENGLON_PADRON
    padron.cantidad_renglones = lineas - 1
    padron.importacion_completa = false
    padron.created_by = username
    padron.save

    Thread.new do
      ActiveRecord::Base.connection_pool.with_connection do |conn|
        prefix = 'INSERT INTO renglones_padron (padron_id, matricula, clase, apellido, nombre, profesion, domicilio, analfabeto, tipo_documento, seccion, circuito, mesa, sexo, desfor, created_at, updated_at) VALUES '

        real_level = Rails.logger.level
        Rails.logger.level = Logger::WARN # Lo apagamos para que no loguee el contenido binario

        values = StringIO.new
        values << prefix
        cantidad = 0

        File.open(filename, 'rb').each_line do |line|
          unless line.nil? or line.strip.empty?
            transcoded = line.ensure_encoding('UTF-8',
                                              external_encoding: [::Encoding::UTF_8, ::Encoding::ISO_8859_1],
                                              invalid_characters: :transcode)

            attrs = RenglonPadron.parse_to_hash(transcoded)
            values << "#{cantidad == 0 ? '' : ', '} (#{padron.id}, #{attrs[:matricula]}, #{build_values(attrs)} , '#{Time.now.to_s(:db)}', '#{Time.now.to_s(:db)}')"
            cantidad += 1

            if (cantidad % bulk_size) == 0
              conn.execute(values.string)
              values = StringIO.new
              values << prefix
              cantidad = 0
            end
          end
        end

        conn.execute(values.string) if cantidad > 0

        Rails.logger.level = real_level

        padron.cantidad_renglones = padron.cantidad_real_renglones
        padron.importacion_completa = true
        padron.save

        File.delete(filename)
      end
    end

    padron
  end

  def self.build_values(attrs)
    values = [attrs[:clase], attrs[:apellido].gsub("'", "''"), attrs[:nombre].gsub("'", "''"), attrs[:profesion],
              attrs[:domicilio].gsub("'", "''"), attrs[:analfabeto], attrs[:tipo_documento], attrs[:seccion],
              attrs[:circuito], attrs[:mesa], attrs[:sexo], attrs[:desfor].gsub("'", "''")]

    "'#{values.join("', '")}'"
  end

end
