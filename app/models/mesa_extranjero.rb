# == Schema Information
#
# Table name: mesa_extranjeros
#
#  id               :integer          not null, primary key
#  numero_mesa      :integer
#  comuna           :integer
#  total_inscriptos :integer
#  eleccion_id      :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class MesaExtranjero < ActiveRecord::Base
  has_paper_trail

  belongs_to :eleccion
  has_many :mesa_extrajero_listas
  has_many :listas, through: :mesa_extrajero_listas

  attr_accessible :numero_mesa, :comuna, :total_inscriptos, :eleccion_id, :eleccion

  validates :numero_mesa, :comuna, :eleccion_id, presence: true
end
