# == Schema Information
#
# Table name: eleccion_cargos
#
#  id                :integer          not null, primary key
#  eleccion_id       :integer
#  cargo_id          :integer
#  seleccionado      :boolean
#  created_at        :datetime
#  updated_at        :datetime
#  cantidad_escanios :integer
#  comuna_id         :integer
#

class EleccionCargo < ActiveRecord::Base

  has_paper_trail

  belongs_to :eleccion
  belongs_to :cargo
  belongs_to :comuna

  has_many :resultados_elecciones, class_name: 'ResultadoEleccion', foreign_key: 'eleccion_cargo_id'
  has_many :candidaturas

  attr_accessible :eleccion, :cargo, :seleccionado, :cargo_id, :cantidad_escanios

  validates :cantidad_escanios, numericality: true, allow_nil: true

  delegate :cantidad_maxima, to: :cargo, prefix: true

  def fuerzas
    Fuerza.where('id in (?)', candidaturas.joins(:lista).select('DISTINCT fuerza_id as id'))
  end

  def candidaturas
    Candidatura.por_eleccion_y_cargo(eleccion, cargo)
  end

  def ganadoras
    candidaturas.select(&:tiene_escanio?).sort { |a, b| b.cociente <=> a.cociente }
  end

  def resultados_completos?
    cantidad_escanios.present? && resultados_elecciones.all?(&:votos_cargados?)
  end

  def resultado(fuerza)
    ResultadoEleccion.por_eleccion_cargo(self).por_fuerza(fuerza).first
  end

  def desasignar_escanios
    candidaturas.each do |c|
      c.divisor = nil
      c.cociente = nil
      c.tiene_escanio = nil
      c.save
    end
  end

  def asignar_escanios
    reparto = cargo.get_reparto
    reparto.asignar_escanios(self)
  end

  def <=>(other)
    cargo <=> other.cargo
  end

  def seleccionar
    self.seleccionado = true
  end
end
