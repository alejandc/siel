# == Schema Information
#
# Table name: lista_cargos
#
#  id                     :integer          not null, primary key
#  estado_candidaturas_cd :integer
#  data_info              :text(16777215)
#  cargo_id               :integer
#  lista_id               :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  estado_adhesiones_cd   :integer
#  errores                :text(16777215)
#  candidaturas_count     :integer          default(0)
#  adhesiones_count       :integer          default(0)
#  alertas                :text(16777215)
#

class ListaCargo < ActiveRecord::Base
  has_paper_trail only: [:estado_adhesiones_cd, :estado_candidaturas_cd], on: [:update], meta: { lista_cargo_id: :id }
  serialize :data_info, Hash
  serialize :errores, Hash
  serialize :alertas, Hash

  after_update :calcular_estado_lista
  before_create :setear_estado_inicial
  after_initialize :crear_validador

  attr_accessible :estado_candidaturas_cd, :estado_candidaturas, :estado_adhesiones_cd, :estado_adhesiones, :cargo, :data_info, :lista, :errores, :alertas

  attr_accessor :validador

  belongs_to :cargo
  belongs_to :lista

  has_one :fuerza, through: :lista
  has_one :eleccion, through: :lista
  has_one :archivo_conformidad_adhesion, dependent: :destroy

  has_many :candidaturas, -> { order(orden: :asc) }, dependent: :destroy
  has_many :adhesiones, dependent: :delete_all

  as_enum :estado_candidaturas, %w(sin_validar valida incompleta con_errores cerrada), prefix: true
  as_enum :estado_adhesiones, %w(sin_validar valida incompleta con_errores cerrada), prefix: true

  validates :cargo, :estado_candidaturas, :estado_adhesiones, presence: true

  delegate :numero, :nombre, :eleccion_id, :fuerza_id, :abierta?, :cerrada?, :reabierta?, :oficializada?, to: :lista, prefix: true
  delegate :observaciones_candidaturas_bloqueantes, :observaciones_candidaturas_no_bloqueantes, :observaciones_adhesiones_bloqueantes, :observaciones_adhesiones_no_bloqueantes, to: :validador
  delegate :anios_ciudadania, :anios_residencia, :cantidad_minima, :cantidad_minima_adherentes, :cantidad_maxima_adherentes, :cantidad_maxima, :nombre, :edad_requerida, :porcentaje_minimo_afiliados, :cantidad_suplentes_requeridos, to: :cargo, prefix: true
  delegate :nombre, to: :fuerza, prefix: true
  delegate :paso?, :general?, :descripcion, :fecha, :habilitar_carga_candidaturas, to: :eleccion, prefix: true

  scope :cargos, -> (nombres_cargos) { joins(:cargo).where('cargos.nombre IN (?)', nombres_cargos) }
  scope :no_validas, -> { where('estado_candidaturas_cd NOT IN (?) OR estado_adhesiones_cd NOT IN (?)', [estado_candidaturas[:cerrada], estado_candidaturas[:valida]], [estado_adhesiones[:cerrada], estado_adhesiones[:valida]]) }
  scope :no_cerradas, -> { where('estado_candidaturas_cd != ? OR estado_adhesiones_cd != ?', estado_candidaturas[:cerrada], estado_adhesiones[:cerrada]) }
  scope :con_alertas_en_candidaturas, -> { where('alertas NOT LIKE ?', '%candidaturas: []%') }
  scope :con_alertas_en_adhesiones, -> { where('alertas NOT LIKE ?', '%adhesiones: []%') }

  def importacion_job(modelo)
    data_info[clave_importacion(modelo)]
  end

  def setear_importacion(modelo, job_id)
    data_info_importacion = data_info.merge(clave_importacion(modelo) => job_id)
    update_attribute(:data_info, data_info_importacion)
  end

  def actualizar_importacion(modelo)
    clave_importacion = clave_importacion(modelo)
    data_info_dup = data_info.merge(clave_importacion => nil).dup

    # Hack para que actualice los datos en la base
    update_attributes!(data_info: {})
    update_attributes!(data_info: data_info_dup)
  end

  def path_archivo_importar(modelo)
    nombre_archivo = if modelo.eql?('Adhesion')
                       "importar_adherentes_lista_#{lista.nombre.downcase.gsub(/\s+/, '_')}.xls"
                     elsif modelo.eql?('Candidatura')
                       "importar_precandidatos_lista_#{lista.nombre.downcase.gsub(/\s+/, '_')}.xls"
                     else
                       raise "Valor invalido -> #{modelo}"
                     end

    Rails.root.join('tmp', 'import_files', nombre_archivo).to_s
  end

  def path_archivo_con_errores_importacion(modelo)
    nombre_archivo = if modelo.eql?('Adhesion')
                       "errores_importacion_adherentes_lista_#{lista.nombre.downcase.gsub(/\s+/, '_')}_cargo_#{cargo.nombre_snake_case}.xls"
                     elsif modelo.eql?('Candidatura')
                       "errores_importacion_precandidatos_lista_#{lista.nombre.downcase.gsub(/\s+/, '_')}_cargo_#{cargo.nombre_snake_case}.xls"
                     else
                       raise "Valor invalido -> #{modelo}"
                     end

    Rails.root.join('tmp', 'import_files_error', nombre_archivo).to_s
  end

  def path_exportacion_adhesiones
    Rails.root.join('tmp', 'import_files', "exportar adhesiones #{id}.pdf").to_s
  end

  def borrar_candidaturas
    transaction do
      candidaturas.each do |candidatura|
        candidatura.paper_trail.without_versioning { candidatura.destroy }
      end
      errores_importacion = path_archivo_con_errores_importacion('Candidatura')
      File.delete(errores_importacion) if File.exist?(errores_importacion)
      PaperTrail::Version.create(whodunnit: PaperTrail.whodunnit, item_type: 'Candidatura', item_id: id, event: 'destroy_all', lista_cargo_id: id)
    end
  end

  def borrar_adhesiones
    transaction do
      cambiar_estado_adhesiones_sin_validar!
      errores_importacion = path_archivo_con_errores_importacion('Adhesion')
      File.delete(errores_importacion) if File.exist?(errores_importacion)
      adhesiones.delete_all
      ListaCargo.reset_counters(id, :adhesiones)
      PaperTrail::Version.create(whodunnit: PaperTrail.whodunnit, item_type: 'Adhesion', item_id: id, event: 'destroy_all', lista_cargo_id: id)
    end
  end

  def crear_candidatura(persona)
    Candidatura.new(persona: persona, lista_cargo: self, eleccion: eleccion, cargo: cargo)
  end

  def agregar_adhesion(persona)
    Adhesion.new(persona: persona, lista_cargo: self)
  end

  def maximo_orden
    candidaturas.where(eleccion: eleccion, fecha_renuncia: nil).maximum(:orden) || 0
  end

  def posee_candidato?(persona)
    candidaturas.find_by(persona_id: persona.id).present?
  end

  def posee_adherente?(persona)
    adhesiones.find_by(persona_id: persona.id).present?
  end

  def cumple_maximo?
    candidaturas_count < cargo.cantidad_maxima
  end

  def cantidad_suplentes_faltantes
    cargo_cantidad_suplentes_requeridos - cargo.cantidad_suplentes(candidaturas_count)
  end

  def cumple_maximo_adherentes?
    adhesiones_count < cargo.cantidad_maxima_adherentes
  end

  def cumple_ley_cupos?
    cargo.cumple_ley_cupos?(candidaturas)
  end

  def consecutivo_incorregible?
    cargo.consecutivo_incorregible?(candidaturas)
  end

  def candidatos_observados
    candidaturas.map { |candidatura| { candidatura: candidatura, observaciones: candidatura.validador_observaciones } }.reject { |candidatura| candidatura[:observaciones].empty? }
  end

  def cerrada?
    estado_candidaturas_cerrada? && estado_adhesiones_cerrada?
  end

  def candidaturas_faltantes
    cargo_cantidad_minima - candidaturas_count
  end

  def adhesiones_faltantes
    cargo_cantidad_minima_adherentes - adhesiones_count
  end

  def adhesiones_observados
    adhesiones.join_personas.adhesiones_observados(cargo.numero_comuna.to_s)
  end

  def adhesiones_multiples
    adhesiones.adhesiones_multiples
  end

  def adhesiones_conflicto_comuna
    adhesiones.join_personas.adhesiones_conflicto_comuna(cargo.numero_comuna.to_s)
  end

  def cantidad_adherentes_afiliados
    adhesiones.join_personas.adhesiones_afiliadas(lista.eleccion_id, Fuerza.partidos_en_alianza_con_fuerza(lista.eleccion_id, lista.fuerza_id).map(&:numero)).count
  end

  def porcentaje_afiliados
    (adhesiones_count > 0) ? ((adhesiones.join_personas.adhesiones_afiliadas(lista.eleccion_id, Fuerza.partidos_en_alianza_con_fuerza(lista.eleccion_id, lista.fuerza_id).map(&:numero)).count * 100) / adhesiones_count).to_i : 0
  end

  def validar_candidaturas!
    observaciones_pendientes_candidaturas_bloqueantes = observaciones_candidaturas_bloqueantes
    observaciones_pendientes_candidaturas_no_bloqueantes = observaciones_candidaturas_no_bloqueantes
    observaciones_pendientes_candidaturas_bloqueantes = validar_candidaturas(observaciones_pendientes_candidaturas_bloqueantes)

    errores_adhesiones = errores[:adhesiones]
    observaciones_adhesiones = alertas[:adhesiones]

    save!
    update_attributes!(errores: {}, alertas: {})
    update_attributes!(errores: { candidaturas: observaciones_pendientes_candidaturas_bloqueantes.map(&:second), adhesiones: errores_adhesiones },
                       alertas: { candidaturas: observaciones_pendientes_candidaturas_no_bloqueantes.map(&:second), adhesiones: observaciones_adhesiones })

    [observaciones_pendientes_candidaturas_bloqueantes, observaciones_pendientes_candidaturas_no_bloqueantes]
  end

  def validar_adhesiones!
    observaciones_pendientes_adhesiones_bloqueantes = observaciones_adhesiones_bloqueantes
    observaciones_pendientes_adhesiones_no_bloqueantes = observaciones_adhesiones_no_bloqueantes
    observaciones_pendientes_adhesiones_bloqueantes = validar_adhesiones(observaciones_pendientes_adhesiones_bloqueantes)

    errores_candidaturas = errores[:candidaturas]
    observaciones_candidaturas = alertas[:candidaturas]

    save!
    update_attributes!(errores: {}, alertas: {})
    update_attributes!(errores: { candidaturas: errores_candidaturas, adhesiones: observaciones_pendientes_adhesiones_bloqueantes.map(&:second) },
                       alertas: { candidaturas: observaciones_candidaturas, adhesiones: observaciones_pendientes_adhesiones_no_bloqueantes.map(&:second) })

    [observaciones_pendientes_adhesiones_bloqueantes, observaciones_pendientes_adhesiones_no_bloqueantes]
  end

  def cerrar_candidaturas!
    cerrar_asociacion!('candidaturas')
  end

  def cerrar_adhesiones!
    cerrar_asociacion!('adhesiones')
  end

  def abrir_candidaturas!(estado_apertura)
    abrir_asociacion!('candidaturas')
    lista.desoficializar!(estado_apertura)
  end

  def abrir_adhesiones!(estado_apertura)
    abrir_asociacion!('adhesiones')
    lista.desoficializar!(estado_apertura)
  end

  def cambiar_estado_candidaturas_sin_validar!
    update_attributes!(estado_candidaturas: ListaCargo.estado_candidaturas[:sin_validar], errores: { candidaturas: [], adhesiones: errores[:adhesiones] }, alertas: { candidaturas: [], adhesiones: alertas[:adhesiones] })
  end

  def cambiar_estado_adhesiones_sin_validar!
    update_attributes!(estado_adhesiones: ListaCargo.estado_adhesiones[:sin_validar], errores: { candidaturas: errores[:candidaturas], adhesiones: [] }, alertas: { candidaturas: alertas[:candidaturas], adhesiones: [] })
  end

  private

  # FIXME: Por ahora no se utiliza
  # def borrar_asociaciones(asociacion)
  #   nombre_asociacion = asociacion.klass.name.pluralize.underscore
  #   transaction do
  #     update_attribute("estado_#{nombre_asociacion}_cd".to_sym, ListaCargo.estado_adhesiones[:sin_validar])
  #     errores_importacion = path_archivo_con_errores_importacion(asociacion.klass.name)
  #     ActiveRecord::Base.connection.execute("DELETE FROM #{nombre_asociacion} WHERE lista_cargo_id = #{id}")
  #     File.delete(errores_importacion) if File.exist?(errores_importacion)
  #     ListaCargo.reset_counters(id, nombre_asociacion.to_sym)
  #     PaperTrail::Version.create(whodunnit: PaperTrail.whodunnit, item_type: asociacion.klass.name, item_id: id, event: 'destroy_all', lista_cargo_id: id)
  #     ActiveRecord::Base.connection.execute("DELETE FROM observaciones WHERE lista_cargo_id = #{id}")
  #     ActiveRecord::Base.connection.execute("DELETE FROM versions WHERE lista_cargo_id = #{id} AND item_type = 'Observacion'")
  #   end
  # end

  def clave_importacion(modelo)
    if modelo.eql? 'Adhesion'
      "importar_adherentes_cargo_#{cargo_id}"
    elsif modelo.eql? 'Candidatura'
      "importar_precandidatos_cargo_#{cargo_id}"
    end
  end

  def calcular_estado_lista
    if lista.cargos_cerrados?
      lista.update_attributes!(estado: Lista::ESTADOS[:CERRADA]) if !lista.cerrada? && !lista.oficializada?
    # else
    #   if !lista.abierta? && !lista.reabierta? && !lista.oficializada?
    #     lista.update_attributes!(estado: Lista::ESTADOS[:ABIERTA])
    #   else
    #     lista.update_attributes!(estado: Lista::ESTADOS[:REABIERTA])
    #   end
    end
  end

  def setear_estado_inicial
    estado_candidaturas_sin_validar!
    estado_adhesiones_sin_validar!
    self.errores = { candidaturas: [], adhesiones: [] }
    self.alertas = { candidaturas: [], adhesiones: [] }
  end

  def crear_validador
    self.validador = Validators::ValidadorListaCargo.new(self)
  end

  def validar_candidaturas(observaciones_pendientes_bloqueantes)
    observaciones_pendientes = observaciones_pendientes_bloqueantes

    if observaciones_pendientes_bloqueantes.empty?
      estado_candidaturas_valida!
    elsif observaciones_pendientes_bloqueantes.detect { |observacion| observacion.third == Validators::ValidadorListaCargo::VALIDACIONES_CANDIDATURAS_BLOQUEANTES[:CUPO_CARGOS][:NOMBRE] }
      estado_candidaturas_incompleta!
      observaciones_pendientes = [observaciones_pendientes_bloqueantes.detect { |observacion| observacion.third == Validators::ValidadorListaCargo::VALIDACIONES_CANDIDATURAS_BLOQUEANTES[:CUPO_CARGOS][:NOMBRE] }]
    else
      estado_candidaturas_con_errores!
    end

    observaciones_pendientes
  end

  def validar_adhesiones(observaciones_pendientes_bloqueantes)
    observaciones_pendientes = observaciones_pendientes_bloqueantes

    if observaciones_pendientes_bloqueantes.empty?
      estado_adhesiones_valida!
    elsif observaciones_pendientes_bloqueantes.detect { |observacion| observacion.third == Validators::ValidadorListaCargo::VALIDACIONES_ADHESIONES_BLOQUEANTES[:MINIMO_ADHESIONES][:NOMBRE] }
      estado_adhesiones_incompleta!
      observaciones_pendientes = [observaciones_pendientes_bloqueantes.detect { |observacion| observacion.third == Validators::ValidadorListaCargo::VALIDACIONES_ADHESIONES_BLOQUEANTES[:MINIMO_ADHESIONES][:NOMBRE] }]
    else
      estado_adhesiones_con_errores!
    end

    observaciones_pendientes
  end

  def cerrar_asociacion!(asociacion)
    send("validar_#{asociacion}!") #if send("estado_#{asociacion}_sin_validar?")
    send("estado_#{asociacion}_valida?") ? update_attributes!("estado_#{asociacion}".to_sym => ListaCargo.send("estado_#{asociacion}")[:cerrada]) : errors.add(:cerrar_lista, "No es posible cerrar las #{asociacion}.")
  end

  def abrir_asociacion!(asociacion)
    send("cambiar_estado_#{asociacion}_sin_validar!")
    save!
  end
end
