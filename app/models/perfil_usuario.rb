module PerfilUsuario
  PERFILES = { 1 => 'administrador', 2 => 'tsj', 3 => 'apoderado', 4 => 'lista_partidaria' }

  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def perfiles_permitidos
      PERFILES.select { |clave, _| clave == 3 || clave == 4 }
    end
  end

  PERFILES.values.each do |perfil|
    define_method("#{perfil}?") do
      perfiles.split(',').detect { |perfil_usuario| perfil == perfil_usuario }.present?
    end
  end

  def es_del_grupo?(grupo)
    perfiles.split(',').detect { |perfil| perfil == grupo }
  end

  def interno?
    administrador? || tsj?
  end

  def externo?
    apoderado? || lista_partidaria?
  end

  private

  def es_usuario_interno?
    !persisted? && perfiles.blank? && nombre_usuario.present?
  end

  def cargar_perfiles
    grupos = TSJ_SECURITY.groups(nombre_usuario)
    perfil_usuario = MAPA_GRUPOS_PERFILES[grupos.split(',').detect { |grupo| grupo.match(/(GGINFORMATICA|GGSAO)/) }]
    self.perfiles = PERFILES.values.detect { |perfil| perfil == perfil_usuario }
    self
  end
end
