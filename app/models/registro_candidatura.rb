# == Schema Information
#
# Table name: registro_candidaturas
#
#  id               :integer          not null, primary key
#  numero_documento :integer
#  sexo             :string(255)
#  tipo_documento   :string(255)
#  anio_candidatura :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class RegistroCandidatura < ActiveRecord::Base
  paginates_per 50

  after_create :eliminar_observacion_residencia_nacionalidad
  after_destroy :crear_observacion_residencia_nacionalidad

  attr_accessible :numero_documento, :tipo_documento, :sexo, :anio_candidatura

  validates :numero_documento, :sexo, :tipo_documento, :anio_candidatura, presence: true

  scope :candidatura, -> (candidatura) { where(tipo_documento: candidatura.persona_tipo_documento, numero_documento: candidatura.persona_numero_documento, sexo: candidatura.persona_sexo) }
  scope :afectadas_por_anio, -> (anio) { where('anio_candidatura >= ?', anio - Configuracion.last.antiguedad_registro_candidaturas) }

  # Metodo para procesar y generar los registros de candidaturas en la base de datos
  # @params datos [String] con formato NUMERO_DOCUMENTO, TIPO_DOCUMENTO, SEXO, AÑO_CANDIDATURA concatenados para generar los registros
  # @return [Array] que contiene los registros de candidaturas que no se puede crear en base a las validaciones
  def self.procesar_registro_candidaturas(datos)
    datos_registro_candidaturas = datos.strip.split(/[\n\r]/).delete_if(&:blank?)

    registro_candidaturas_invalidas = []
    datos_registro_candidaturas.each do |info|
      numero_documento, tipo_documento, sexo, anio_candidatura = info.gsub(/\s+/, '').split(',').map(&:upcase)

      if numero_documento.try(:match, /^(\d)+$/) && anio_candidatura.try(:match, /^(\d)+$/) && (sexo.eql?("M") || sexo.eql?("F")) && Persona::DOCUMENTO_TIPOS.values.include?(tipo_documento)
        unless RegistroCandidatura.where(numero_documento: numero_documento, sexo: sexo, tipo_documento: tipo_documento, anio_candidatura: anio_candidatura).exists?

          RegistroCandidatura.create!(numero_documento: numero_documento, sexo: sexo, tipo_documento: tipo_documento, anio_candidatura: anio_candidatura)
        else
          registro_candidaturas_invalidas << "#{numero_documento}, #{tipo_documento}, #{sexo}, #{anio_candidatura} (Registro de candidatura existente)"
        end
      else
        registro_candidaturas_invalidas << "#{numero_documento}, #{tipo_documento}, #{sexo}, #{anio_candidatura} (formato inválido)"
      end
    end

    registro_candidaturas_invalidas
  end

  #--------------------------------------------------------------------------------#
  # Class Methods
  #--------------------------------------------------------------------------------#
  def self.search_by_filter(filtro_registro_candidatura)
    registro_cadidaturas = self

    if filtro_registro_candidatura.numero_documento.present?
      registro_cadidaturas = registro_cadidaturas.where(numero_documento: filtro_registro_candidatura.numero_documento)
    end

    if filtro_registro_candidatura.anio_candidatura.present?
      registro_cadidaturas = registro_cadidaturas.where(anio_candidatura: filtro_registro_candidatura.anio_candidatura)
    end

    registro_cadidaturas.order('created_at DESC')
  end

  private

  def eliminar_observacion_residencia_nacionalidad
    eleccion_actual = Eleccion.actual

    if eleccion_actual.present?
      if anio_candidatura >= (eleccion_actual.anio - Configuracion.last.antiguedad_registro_candidaturas)
        persona = Persona.find_by(tipo_documento: tipo_documento, numero_documento: numero_documento, sexo: sexo)
        if persona.present?
          persona.candidaturas.eleccion(eleccion_actual).each do |candidatura|
            Observacion.eliminar_observacion(candidatura, 'ANTIGUEDAD_CIUDADANIA_PERSONA')
            Observacion.eliminar_observacion(candidatura, 'ANTIGUEDAD_RESIDENCIA_PERSONA')
          end
        end
      end
    end
  end

  def crear_observacion_residencia_nacionalidad
    eleccion_actual = Eleccion.actual

    if eleccion_actual.present?
      if anio_candidatura >= (eleccion_actual.anio - Configuracion.last.antiguedad_registro_candidaturas)
        persona = Persona.find_by(tipo_documento: tipo_documento, numero_documento: numero_documento, sexo: sexo)
        if persona.present?
          persona.candidaturas.eleccion(eleccion_actual).each do |candidatura|
            Observacion.crear_observacion(candidatura, 'ANTIGUEDAD_CIUDADANIA_PERSONA', false) unless candidatura.cumple_ciudadania?
            Observacion.crear_observacion(candidatura, 'ANTIGUEDAD_RESIDENCIA_PERSONA', false) unless candidatura.cumple_residencia?
          end
        end
      end
    end
  end
end
