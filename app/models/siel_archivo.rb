# == Schema Information
#
# Table name: siel_archivos
#
#  id              :integer          not null, primary key
#  activo          :boolean
#  archivo_type_cd :integer
#  archivo_id      :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class SielArchivo < ActiveRecord::Base
  
  belongs_to :archivo, dependent: :destroy
  
  attr_accessible :archivo, :activo, :archivo_type_cd

  has_paper_trail

  as_enum :archivo_type, conformidad_adhesiones: 0, aceptacion_candidaturas: 1, acordada_electoral: 2

  validates :archivo_id, :archivo_type, presence: true

  scope :activos, -> { where(activo: true) }
  scope :no_activos, -> { where(activo: false) }
  scope :conformidad_adhesion, -> { where(archivo_type_cd: 0) }
  scope :aceptacion_candidatura, -> { where(archivo_type_cd: 1) }
  scope :recientes, -> { order('created_at DESC') }


  def archivo=(archivo_subido)
    if archivo.nil?
      nuevo_archivo = Archivo.new
      nuevo_archivo.nombre = File.basename(archivo_subido.original_filename)
      nuevo_archivo.tipo_contenido = archivo_subido.content_type
      nuevo_archivo.save!

      nuevo_archivo.contenido = archivo_subido.read

      self.archivo_id = nuevo_archivo.id
    else
      archivo.contenido_archivos.destroy_all

      archivo.nombre = File.basename(archivo_subido.original_filename)
      archivo.tipo_contenido = archivo_subido.content_type
      archivo.save!

      archivo.contenido = archivo_subido.read
    end
  end

  def self.desactivar_archivos(siel_archivo)
    where.not(id: siel_archivo.id).where(archivo_type_cd: siel_archivo.archivo_type_cd).update_all(activo: false)
  end

end
