# == Schema Information
#
# Table name: infracciones
#
#  id            :integer          not null, primary key
#  fuerza_id     :integer
#  fecha         :date
#  expediente    :string(255)
#  observaciones :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#

class Infraccion < ActiveRecord::Base

  belongs_to :fuerza

  attr_accessible :expediente, :fecha, :observaciones, :fuerza_id

  has_paper_trail

  validates :expediente, :fecha, presence: true

end
