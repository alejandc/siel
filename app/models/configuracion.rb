# == Schema Information
#
# Table name: configuraciones
#
#  id                               :integer          not null, primary key
#  habilitar_renaper_api            :boolean
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  antiguedad_registro_candidaturas :integer
#

class Configuracion < ActiveRecord::Base
  has_paper_trail

  attr_accessible :habilitar_renaper_api, :antiguedad_registro_candidaturas
  
  validates :habilitar_renaper_api, inclusion: { in: [true, false] }
  validates :antiguedad_registro_candidaturas, numericality: true, presence: true

  def self.verificar_configuracion_inicial
    Configuracion.create!(habilitar_renaper_api: true) if Configuracion.count == 0
  end
end
