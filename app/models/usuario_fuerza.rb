# == Schema Information
#
# Table name: usuarios_fuerzas
#
#  id         :integer          not null, primary key
#  fuerza_id  :integer
#  usuario_id :integer
#  created_at :datetime
#  updated_at :datetime
#

class UsuarioFuerza < ActiveRecord::Base
  belongs_to :fuerza
  belongs_to :usuario

  attr_accessible :usuario, :fuerza, :fuerza_id, :usuario_id
end
