# == Schema Information
#
# Table name: cargos
#
#  id              :integer          not null, primary key
#  nombre          :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#  orden           :integer
#  cantidad_minima :integer
#  cantidad_maxima :integer
#

class Cargo < ActiveRecord::Base
  attr_accessible :nombre, :orden, :cantidad_maxima, :cantidad_minima
  has_many :eleccion_cargos

  attr_accessor :tipo_cargo
  after_initialize :crear_tipo_cargo

  delegate :edad_requerida, :leyenda_ciudadania, :anios_ciudadania, :cantidad_minima_adherentes, :cantidad_maxima_adherentes, :anios_residencia,
           :cantidad_minima_adherentes_afiliados, :genero_consecutivo, :get_reparto, :numero_comuna, to: :tipo_cargo

  scope :con_nombres, -> (nombres) { where('nombre IN (?)', nombres) }

  has_paper_trail

  CARGOS = { jefe: 'Jefe/a de Gobierno', vicejefe: 'Vicejefe', diputado: 'Diputados/as',
             comuna_1: 'Junta Comunal 1', comuna_2: 'Junta Comunal 2', comuna_3: 'Junta Comunal 3',
             comuna_4: 'Junta Comunal 4', comuna_5: 'Junta Comunal 5', comuna_6: 'Junta Comunal 6',
             comuna_7: 'Junta Comunal 7', comuna_8: 'Junta Comunal 8', comuna_9: 'Junta Comunal 9',
             comuna_10: 'Junta Comunal 10', comuna_11: 'Junta Comunal 11', comuna_12: 'Junta Comunal 12',
             comuna_13: 'Junta Comunal 13', comuna_14: 'Junta Comunal 14', comuna_15: 'Junta Comunal 15' }

  CARGOS_ESCRUTINIO = { jefe: 'JV', diputado: 'DP', 
                        comuna_1: 'JC1', comuna_2: 'JC2', comuna_3: 'JC3',
                        comuna_4: 'JC4', comuna_5: 'JC5', comuna_6: 'JC6',
                        comuna_7: 'JC7', comuna_8: 'JC8', comuna_9: 'JC9',
                        comuna_10: 'JC10', comuna_11: 'JC11', comuna_12: 'JC12',
                        comuna_13: 'JC13', comuna_14: 'JC14', comuna_15: 'JC15' }

  def porcentaje_minimo_afiliados
    10
  end

  def cantidad_titulares(cantidad)
    (cantidad >= cantidad_minima) ? cantidad_minima : cantidad
  end

  def cantidad_suplentes(cantidad)
    (cantidad > cantidad_minima) ? (cantidad - cantidad_minima) : 0
  end

  def cantidad_suplentes_requeridos
    cantidad_maxima - cantidad_minima
  end

  def nombre_snake_case
    nombre.downcase.gsub(%r{\s+|\/}, '_')
  end

  def cumple_ley_cupos?(candidaturas)
    tipo_cargo.cumple_ley_cupos?(candidaturas)
  end

  def consecutivo_incorregible?(candidaturas)
    tipo_cargo.consecutivo_incorregible?(candidaturas)
  end

  def cargo_escrutinio
    CARGOS_ESCRUTINIO[CARGOS.key(nombre)]
  end

  def <=>(other)
    orden <=> other.orden
  end

  private

  def crear_tipo_cargo
    nombre_tipo_cargo = (nombre =~ /Comuna/i ? 'Comunero' : CARGOS.key(nombre).to_s).camelize
    self.tipo_cargo = "TiposCargos::TipoCargo#{nombre_tipo_cargo}".constantize.new(self)
  end

end
