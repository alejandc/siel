# == Schema Information
#
# Table name: afiliaciones
#
#  id               :integer          not null, primary key
#  numero_documento :integer
#  sexo             :string(255)
#  tipo_documento   :string(255)
#  fuerza_id        :integer
#  eleccion_id      :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  fuerza_codigo    :integer
#

class Afiliacion < ActiveRecord::Base
  paginates_per 50

  attr_accessible :numero_documento, :tipo_documento, :sexo, :fuerza_id, :fuerza,
                  :eleccion_id, :eleccion, :fuerza_codigo

  belongs_to :fuerza
  belongs_to :eleccion

  validates :numero_documento, :sexo, :tipo_documento, :eleccion, :fuerza_codigo, presence: true

  # Metodo para procesar y generar las afiliaciones en la base de datos
  # @params afiliacion [Afiliacion] modelo que contiene la info generico (eleccion y fuerza)
  # @params datos [String] con formato NUMERO_DOCUMENTO, TIPO_DOCUMENTO, SEXO concatenados para generar multiples afiliaciones
  # @return [Array] que contiene las afiliaciones que no se puede crear en base a las validaciones
  def self.procesar_afiliaciones(afiliacion, datos)
    datos_afiliaciones = datos.strip.split(/[\n\r]/).delete_if(&:blank?)

    afiliaciones_invalidas = []
    afiliaciones_validas = []
    datos_afiliaciones.each do |info|
      numero_documento, tipo_documento, sexo, fuerza_codigo = info.gsub(/\s+/, '').split(',').map(&:upcase)
      tipo_documento = tipo_documento.include?('DNI') ? 'DNI' : 'L'

      if numero_documento.try(:match, /^(\d)+$/) && (sexo.eql?("M") || sexo.eql?("F")) && Persona::DOCUMENTO_TIPOS.values.include?(tipo_documento)
        unless Afiliacion.where(eleccion_id: afiliacion[:eleccion_id], numero_documento: numero_documento,
                                 sexo: sexo, tipo_documento: tipo_documento).exists?

          afiliaciones_validas << Afiliacion.new(fuerza_codigo: fuerza_codigo, eleccion_id: afiliacion[:eleccion_id],
                             numero_documento: numero_documento, sexo: sexo, tipo_documento: tipo_documento)

        else
          afiliaciones_invalidas << "#{numero_documento}, #{tipo_documento}, #{sexo}, #{fuerza_codigo} (afiliación existente)"
        end
      else
        afiliaciones_invalidas << "#{numero_documento}, #{tipo_documento}, #{sexo}, #{fuerza_codigo} (formato inválido)"
      end
    end

    Afiliacion.import afiliaciones_validas, batch_size: 5000

    afiliaciones_invalidas
  end

  def limpiar_filtros
    self.numero_documento = nil
    self.fuerza_codigo = nil
  end
  

  #--------------------------------------------------------------------------------#
  # Class Methods
  #--------------------------------------------------------------------------------#
  def self.search_by_filter(filtro_afiliacion)
    afiliaciones = self

    if filtro_afiliacion.numero_documento.present?
      afiliaciones = afiliaciones.where(numero_documento: filtro_afiliacion.numero_documento)
    end

    if filtro_afiliacion.eleccion_id.present?
      afiliaciones = afiliaciones.where(eleccion_id: filtro_afiliacion.eleccion_id)
    end

    if filtro_afiliacion.fuerza_codigo.present?
      alianza = Fuerza.alianzas.where(numero: filtro_afiliacion.fuerza_codigo).last
      afiliaciones = if alianza
        fuerzas = Fuerza.partidos_en_alianza_con_fuerza(filtro_afiliacion.eleccion_id, alianza.id)
        afiliaciones.where(fuerza_codigo: fuerzas.map(&:numero))
      else
        afiliaciones.where(fuerza_codigo: filtro_afiliacion.fuerza_codigo)
      end
    end

    afiliaciones.order('created_at DESC')
  end
end
