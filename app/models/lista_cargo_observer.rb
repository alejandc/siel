class ListaCargoObserver < ActiveRecord::Observer
  observe :candidatura, :adhesion, :persona, :archivo_persona

  def after_create(record)
    case record.class.name
    when 'Candidatura'
      candidaturas_multiples = record.persona.candidaturas_multiples(record.eleccion)

      if candidaturas_multiples.count > 1
        candidaturas_multiples.de_listas_no_cerradas.each do |candidatura|
          candidatura.lista_cargo.abrir_candidaturas!(candidatura.lista_cargo.lista.estado)
          candidatura.lista_cargo.abrir_adhesiones!(candidatura.lista_cargo.lista.estado)
        end
      end

      record.lista_cargo_cambiar_estado_candidaturas_sin_validar!
      record.lista_cargo.save!
    when 'Adhesion'
      record.lista_cargo_cambiar_estado_adhesiones_sin_validar!
      record.lista_cargo.save!
    end
  end

  def before_update(record)
    case record.class.name
    when 'Persona'
      if record.changed?
        record.candidaturas.each do |candidatura|
          candidatura.lista_cargo_cambiar_estado_candidaturas_sin_validar!
          candidatura.lista_cargo.save!
        end
      end
    when 'Adhesion'
      record.lista_cargo_cambiar_estado_adhesiones_sin_validar!
      record.lista_cargo.save!
    end
  end

  def before_destroy(record)
    case record.class.name
    when 'Candidatura'
      record.lista_cargo_cambiar_estado_candidaturas_sin_validar!
      record.lista_cargo.save!
    when 'Adhesion'
      record.lista_cargo_cambiar_estado_adhesiones_sin_validar!
      record.lista_cargo.save!
    when 'ArchivoPersona'
      record.candidaturas.each do |candidatura|
        candidatura.lista_cargo_cambiar_estado_candidaturas_sin_validar!
        candidatura.lista_cargo.save!
      end
    end
  end

end
