class CandidaturaObserver < ActiveRecord::Observer
  observe :candidatura, :incidencia

  def after_save(record)
    case record.class.name
    when 'Candidatura'
      candidatura = record
      crear_observaciones_validan_siempre_borrables(candidatura)
      crear_observaciones_validan_siempre_no_borrables(candidatura)
    when 'Incidencia'
    end
  end

  def after_create(record)
    case record.class.name
    when 'Candidatura'
      candidatura = record
      Validators::ValidadorCandidatura::VALIDAN_CREACION.each do |nombre_observacion|
        if Validators::ValidadorCandidatura::VALIDACIONES[nombre_observacion.to_sym][:CONDICION_EJECUCION].call(candidatura)
          case nombre_observacion
          when 'ANTIGUEDAD_CIUDADANIA_PERSONA'
            Observacion.crear_observacion(candidatura, nombre_observacion, true) if !candidatura.cumple_ciudadania? && !RegistroCandidatura.candidatura(candidatura).afectadas_por_anio(candidatura.eleccion_anio).exists?
          when 'ANTIGUEDAD_RESIDENCIA_PERSONA'
            Observacion.crear_observacion(candidatura, nombre_observacion, true) if !candidatura.cumple_residencia? && !RegistroCandidatura.candidatura(candidatura).afectadas_por_anio(candidatura.eleccion_anio).exists?
          when 'ANTIGUEDAD_RESIDENCIA_PERSONA_COMUNA'
            Observacion.crear_observacion(candidatura, nombre_observacion, true) unless candidatura.cumple_residencia_comuna?
          else
            Observacion.crear_observacion(candidatura, nombre_observacion, true)
          end
        end
      end
    when 'Incidencia'
      incidencia = record
      persona = Persona.dni_sexo(incidencia.tipo_documento, incidencia.numero_documento, incidencia.sexo).first
      if persona.present?
        persona.candidaturas.each { |candidatura| Observacion.crear_observacion(candidatura, incidencia.tipo_incidencia, true) if candidatura.eleccion_id == incidencia.eleccion_id }
      end
    end
  end

  def before_destroy(record)
    case record.class.name
    when 'Candidatura'
      candidatura = record
      observaciones = Observacion.candidatura(candidatura)
      PaperTrail::Version.where('item_type = "Observacion" AND item_id IN (?)', observaciones.select(:id)).delete_all
      observaciones.delete_all
      eliminar_observaciones_con_candidatos_afectados(candidatura.candidaturas_en_lista, 'ESTA_EN_LISTA')
      eliminar_observaciones_con_candidatos_afectados(candidatura.candidaturas_multiples_en_otras_listas, 'CANDIDATURAS_MULTIPLES')
    when 'Incidencia'
      incidencia = record
      persona = Persona.dni_sexo(incidencia.tipo_documento, incidencia.numero_documento, incidencia.sexo).first
      if persona.present?
        persona.candidaturas.each { |candidatura| Observacion.eliminar_observacion(candidatura, incidencia.tipo_incidencia) if candidatura.eleccion_id == incidencia.eleccion_id }
      end
    end
  end

  private

  def crear_observaciones_validan_siempre_borrables(candidatura)
    Validators::ValidadorCandidatura::VALIDAN_SIEMPRE_BORRABLES.each do |nombre_observacion|
      observacion = Validators::ValidadorCandidatura::VALIDACIONES[nombre_observacion.to_sym]
      cumple_condicion = observacion[:CONDICION_EJECUCION].call(candidatura) && observacion[:CONDICION_VALIDACION].call(candidatura)
      cumple_condicion ? Observacion.crear_observacion(candidatura, nombre_observacion, true) : Observacion.eliminar_observacion(candidatura, nombre_observacion)

      # Crea observaciones de candidatos afectados por estas validaciones
      crear_observaciones_con_candidatos_afectados(candidatura, candidatura.candidaturas_en_lista, nombre_observacion, 'ESTA_EN_LISTA')
      crear_observaciones_con_candidatos_afectados(candidatura, candidatura.candidaturas_multiples_en_otras_listas, nombre_observacion, 'CANDIDATURAS_MULTIPLES')
    end
  end

  def crear_observaciones_validan_siempre_no_borrables(candidatura)
    Validators::ValidadorCandidatura::VALIDAN_SIEMPRE_NO_BORRABLES.each do |nombre_observacion|
      observacion = Validators::ValidadorCandidatura::VALIDACIONES[nombre_observacion.to_sym]
      Observacion.crear_observacion(candidatura, nombre_observacion, true) if observacion[:CONDICION_EJECUCION].call(candidatura) && observacion[:CONDICION_VALIDACION].call(candidatura)
    end
  end

  def crear_observaciones_con_candidatos_afectados(candidatura, candidaturas, nombre_observacion_iterada, nombre_observacion)
    (candidaturas - [candidatura]).each { |candidatura_aux| Observacion.crear_observacion(candidatura_aux, nombre_observacion_iterada, false) } if nombre_observacion_iterada == nombre_observacion
  end

  def eliminar_observaciones_con_candidatos_afectados(candidaturas, nombre_observacion)
    candidaturas.each { |candidatura_aux| Observacion.eliminar_observacion(candidatura_aux, nombre_observacion) } if candidaturas.count < 3
  end

end
