class Ability
  include CanCan::Ability

  def initialize(usuario)
    @usuario = usuario
    @usuario ||= Usuario.new

    cargar_permisos_base

    if @usuario.administrador?
      cargar_permisos_administrador
    elsif @usuario.tsj?
      cargar_permisos_tsj
    elsif @usuario.apoderado?
      cargar_permisos_apoderado
    elsif @usuario.lista_partidaria?
      cargar_permisos_lista_partidaria
    end
  end

  def cargar_permisos_administrador
    can :manage, :all
    can :asignar_eleccion, Usuario
    can :asignar_rol, Usuario
    can :asignar_fuerza, Usuario
    can :manage, MesaExtranjero
    can :manage, MesaExtranjeroLista
    can :manage, RenglonEscrutinio
    can :manage, Configuracion
    can :manage, ImportacionEscrutinio
    can :index, :lista_general

    cannot :cambiar_contrasenia, Usuario
  end

  def cargar_permisos_tsj
    can :manage, Eleccion
    can :manage, Fuerza
    can :manage, Usuario
    can :manage, UsuarioFuerza
    can :manage, Padron
    can :manage, RenglonPadron
    can :manage, Boleta
    can :manage, Cargo
    can :manage, EleccionCargo
    can :manage, Comuna
    can :manage, ResultadoComuna
    can :manage, ResultadoEleccion
    can :manage, Lista
    can :manage, ListaCargo
    can :manage, Afiliacion
    can :manage, Incidencia
    can :asignar_eleccion, Usuario
    can :asignar_rol, Usuario
    can :asignar_fuerza, Usuario
    can :index, :lista_general

    cannot :cambiar_contrasenia, Usuario
  end

  def cargar_permisos_apoderado
    can :read, Usuario
    can :manage, Lista

    can :manage, Fiscal

    can :manage, Fuerza do |fuerza|
      @usuario.pertenece_a_fuerza?(fuerza)
    end

    can :manage, Boleta do |boleta|
      @usuario.pertenece_a_fuerza?(boleta.fuerza)
    end

    can :manage, Observacion do |observacion|
      @usuario.lista_id == observacion.lista_cargo.lista_id
    end

    cannot :descargar_excel_lista, Lista do |lista|
      !((lista.cerrada? || lista.reabierta? || lista.oficializada?) && lista.eleccion_paso?)
    end

    cannot :asignar_eleccion, Usuario
    cannot :asignar_rol, Usuario
    cannot :asignar_fuerza, Usuario
    cannot :manage, Cargo
    cannot :manage, Incidencia
    cannot :manage, SielArchivo
    cannot :manejar_fiscales_otros_partidos, Fiscal
    cannot :edit, Lista do |lista|
      lista.oficializada?
    end

    can :manage, ListaCargo do |lista_cargo|
      lista_cargo.eleccion_habilitar_carga_candidaturas && (lista_cargo.lista_cerrada? || lista_cargo.lista_reabierta? || lista_cargo.lista_oficializada?) && lista_cargo.eleccion_paso?
    end

    can :manage, Candidatura do |candidatura|
      candidatura.eleccion_habilitar_carga_candidaturas && candidatura.lista_reabierta? && candidatura.eleccion_paso?
    end

    can :manage, Adhesion do |adhesion|
      adhesion.eleccion_habilitar_carga_candidaturas && adhesion.lista_reabierta? && adhesion.eleccion_paso?
    end
  end

  def cargar_permisos_lista_partidaria
    can :manage, Lista, id: @usuario.lista_id
    cannot :index, Lista
    cannot :manage, Cargo
    cannot :manage, Incidencia
    cannot :manage, SielArchivo
    cannot :manejar_fiscales_otros_partidos, Fiscal
    cannot :manage, Observacion
    cannot :edit, Lista
    cannot :descargar_excel_lista, Lista

    can :manage, ListaCargo do |lista_cargo|
      lista_cargo.lista_id == @usuario.lista_id
    end
  end

  def cargar_permisos_base
    can :manage, Candidatura do |candidatura|
      @usuario.interno? || @usuario.pertenece_a_fuerza?(candidatura.fuerza)
    end

    cannot :index, :lista_general
    cannot :manage, Usuario
    can :cambiar_contrasenia, Usuario
    can :manage, Persona
    can :manage, ArchivoPersona
    can :manage, VerificacionFirmas
    can :manage, CasoVerificacion
    can :manage, AlianzaPartido
    can :manage, Domicilio
    can :manage, SielArchivo
    can :edit, Lista
    can :manage, Observacion
    can :manage, Fiscal do |fiscal|
      @usuario.pertenece_a_fuerza?(fiscal.fuerza)
    end

    can :manejar_fiscales_otros_partidos, Fiscal
    can :descargar_excel_lista, Lista
  end

end
