# == Schema Information
#
# Table name: importacion_escrutinios
#
#  id                 :integer          not null, primary key
#  cantidad_registros :integer
#  eleccion_id        :integer
#  data_info          :text(65535)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  job_id             :string(255)
#

class ImportacionEscrutinio < ActiveRecord::Base

  has_many :resultados_escrutinio, dependent: :delete_all
  has_many :renglones_escrutinio, dependent: :delete_all
  belongs_to :eleccion

  scope :importacion_activa, -> { where('job_id IS NOT NULL') }

  delegate :paso?, :general?, :descripcion, to: :eleccion, prefix: true
  delegate :total_votos, :votos_positivos, :total_votos_codigo, :sin_partidos_asociados, to: :renglones_escrutinio

  def path_archivo_importar
    Rails.root.join('tmp', 'import_files', 'importacion_escrutinio.csv').to_s
  end

  def actualizar_importacion(job_id)
    update_attributes!(job_id: job_id, cantidad_registros: renglones_escrutinio.count)
  end

  def path_archivo_con_errores_importacion
    Rails.root.join('tmp', 'import_files_error', 'importacion_escrutinio.csv').to_s
  end

  def partidos_con_totales
    cargos_seleccionados = cargos_seleccionados_eleccion

    total_votos = cargos_seleccionados.inject({}) { |hash, cargo| hash.merge(cargo.downcase.to_sym => renglones_escrutinio.por_cargo(cargo).votos_positivos.total_votos) }
    total_votos_partido_y_cargo = resultados_escrutinio.agrupados_partido.agrupados_cargo.total_votos.inject({}) { |hash, votos_cargo| hash.merge([votos_cargo.first.first, votos_cargo.first.last.gsub(/JC\d+/, 'JC')] => hash[[votos_cargo.first.first, votos_cargo.first.last.gsub(/JC\d+/, 'JC')]].to_i + votos_cargo.last) }
    total_votos_blanco = cargos_seleccionados.inject({}) { |hash, cargo| hash.merge(cargo.downcase.to_sym => renglones_escrutinio.por_cargo(cargo).con_codigo(:blanco).total_votos) }

    Fuerza.partidos_y_alianzas(eleccion).map do |partido|
      totales_por_cargo = cargos_seleccionados.inject({}) do |hash, cargo|
        total_votos_partido_con_cargo = total_votos_partido_y_cargo[[partido.numero, cargo]]

        hash.merge({
                     cargo.downcase.to_sym => {
                                                total_votos: total_votos_partido_con_cargo.to_i,
                                                porcentaje_positivos: total_votos_partido_con_cargo.to_f * 100.0 / total_votos[cargo.downcase.to_sym].to_f,
                                                porcentaje_validos: total_votos_partido_con_cargo.to_f * 100.0 / (total_votos[cargo.downcase.to_sym].to_f + total_votos_blanco[cargo.downcase.to_sym].to_f)
                                              }
                   })
      end

      {
        partido: partido,
        votos_categoria: totales_por_cargo,
        total_votos: totales_por_cargo.values.inject(0) { |suma, hash| suma + hash[:total_votos] }
      }
    end.sort { |primer_partido, segundo_partido| segundo_partido[:total_votos] <=> primer_partido[:total_votos] }
  end

  def listas_con_totales(fuerza, cargo)
    total_votos_partido = resultados_escrutinio.total_votos_partido_cargo(fuerza.numero, cargo.cargo_escrutinio) || 0

    # FIXME: agregar condicion de listas oficilizadas para hacer el calculo...
    Lista.por_eleccion(eleccion).por_fuerza(fuerza).includes(lista_cargos: { candidaturas: :persona }).select { |lista| lista.cargo_habilitado?(cargo.nombre) }.map do |lista|
      total_votos_lista = ResultadoEscrutinio.total_votos_lista_cargo(lista.id, cargo.cargo_escrutinio) || 0

      {
        lista: lista,
        candidaturas: lista.lista_cargos.detect { |lc| lc.cargo_id == cargo.id }.candidaturas.to_a,
        total_votos: total_votos_lista,
        resto_total_votos: total_votos_lista,
        porcentaje: (total_votos_partido > 0) ? ((total_votos_lista * 100.0) / total_votos_partido) : 0,
        divisor: 1
      }
    end.sort { |primer_lista, segunda_lista| segunda_lista[:total_votos] <=> primer_lista[:total_votos] }
  end

  def lista_ganadora(cargo)
    total_votos_partido_cargo = resultados_escrutinio.total_votos_cargo(cargo.cargo_escrutinio) || 0

    Lista.por_eleccion(eleccion).includes(lista_cargos: { candidaturas: :persona }).map do |lista|
      total_votos_lista = total_votos_partido_cargo[lista.fuerza_numero] || 0

      {
        lista: lista,
        candidaturas: lista.lista_cargos.detect { |lc| lc.cargo_id == cargo.id }.candidaturas.to_a,
        total_votos: total_votos_lista,
        resto_total_votos: total_votos_lista,
        porcentaje: (total_votos_partido > 0) ? ((total_votos_lista * 100.0) / total_votos_partido) : 0,
        divisor: 1
      }
    end.sort { |primer_lista, segunda_lista| segunda_lista[:total_votos] <=> primer_lista[:total_votos] }
  end

  def calcular_resultados!
    ResultadoEscrutinio.delete_all(importacion_escrutinio_id: id)

    listas_con_totales_jefe = renglones_escrutinio.agrupados_partido_lista_cargo(Cargo::CARGOS_ESCRUTINIO[:jefe]).total_votos
    listas = if eleccion.paso?
      Lista.por_eleccion(eleccion).numero(listas_con_totales_jefe.keys).includes(:fuerza)
    else
      Lista.includes(:fuerza).joins(:fuerza).where('listas.eleccion_id = ? and fuerzas.numero in (?)', eleccion.id, listas_con_totales_jefe.keys)
    end

    listas.each do |lista|
      ResultadoEscrutinio.create!(importacion_escrutinio_id: id, eleccion_id: eleccion_id, codigo_cargo: Cargo::CARGOS_ESCRUTINIO[:jefe],
                                  numero_partido: lista.fuerza_numero, lista_id: lista.id, total_votos: listas_con_totales_jefe[lista.numero].to_i)
    end

    listas_con_totales_diputados = renglones_escrutinio.agrupados_partido_lista_cargo(Cargo::CARGOS_ESCRUTINIO[:diputado]).total_votos
    listas = if eleccion.paso?
      Lista.por_eleccion(eleccion).numero(listas_con_totales_diputados.keys).includes(:fuerza)
    else
      Lista.includes(:fuerza).joins(:fuerza).where('listas.eleccion_id = ? and fuerzas.numero in (?)', eleccion.id, listas_con_totales_diputados.keys)
    end

    listas.each do |lista|
      ResultadoEscrutinio.create!(importacion_escrutinio_id: id, eleccion_id: eleccion_id, codigo_cargo: Cargo::CARGOS_ESCRUTINIO[:diputado],
                                  numero_partido: lista.fuerza_numero, lista_id: lista.id, total_votos: listas_con_totales_diputados[lista.numero].to_i)
    end

    listas_con_totales_comunas = renglones_escrutinio.agrupados_comuna.total_votos
    listas = if eleccion.paso?
      Lista.por_eleccion(eleccion).numero(listas_con_totales_comunas.keys.map(&:first)).includes(:fuerza)
    else
      Lista.includes(:fuerza).joins(:fuerza).where('listas.eleccion_id = ? and fuerzas.numero in (?)', eleccion.id, listas_con_totales_comunas.keys.map(&:first))
    end

    listas.each do |lista|
      (1..15).each do |numero_comuna|
        if listas_con_totales_comunas[[lista.numero, numero_comuna]]
          ResultadoEscrutinio.create!(importacion_escrutinio_id: id, eleccion_id: eleccion_id, codigo_cargo: Cargo::CARGOS_ESCRUTINIO["comuna_#{numero_comuna}".to_sym],
                                      numero_partido: lista.fuerza_numero, lista_id: lista.id, total_votos: listas_con_totales_comunas[[lista.numero, numero_comuna]].to_i)
        end
      end
    end
  end

  def cargos_seleccionados_eleccion
    cargos_seleccionados = eleccion.cargos_seleccionados.map { |cargo| Cargo::CARGOS_ESCRUTINIO[Cargo::CARGOS.key(cargo.nombre)] }.compact

    if cargos_seleccionados.detect { |cargo| cargo.match(/JC/) }
      cargos_seleccionados.reject! { |cargo| cargo.match(/JC/) }
      cargos_seleccionados.push('JC')
    else
      cargos_seleccionados
    end
  end

  def cargos_ganadores_fuerza(fuerza)
    cargos_seleccionados = cargos_seleccionados_eleccion

    total_votos = cargos_seleccionados.inject({}) { |hash, cargo| hash.merge(cargo.downcase.to_sym => renglones_escrutinio.por_cargo(cargo).votos_positivos.total_votos) }
    total_votos_blanco = cargos_seleccionados.inject({}) { |hash, cargo| hash.merge(cargo.downcase.to_sym => renglones_escrutinio.por_cargo(cargo).con_codigo(:blanco).total_votos) }
    votos_por_cargo = resultados_escrutinio.fuerza(fuerza).agrupados_cargo.total_votos.inject({}) { |hash, votos_cargo| hash.merge(votos_cargo.first.gsub(/JC\d+/, 'JC') => hash[votos_cargo.first.gsub(/JC\d+/, 'JC')].to_i + votos_cargo.last) }
    cargos_ganadores = cargos_seleccionados.select { |cargo| (votos_por_cargo[cargo].to_f * 100.0 / (total_votos[cargo.downcase.to_sym].to_f + total_votos_blanco[cargo.downcase.to_sym].to_f)) >= 1.5 }

    if cargos_ganadores.include?('JC')
      cargos_ganadores.delete('JC')
      15.times { |numero_comuna| cargos_ganadores.push("JC#{numero_comuna + 1}") }
    end

    nombres_cargos_ganadores = cargos_ganadores.map { |cargo| Cargo::CARGOS[Cargo::CARGOS_ESCRUTINIO.key(cargo)] }

    Cargo.where(nombre: nombres_cargos_ganadores)
  end

end
