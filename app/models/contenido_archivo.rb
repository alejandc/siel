# == Schema Information
#
# Table name: contenido_archivos
#
#  id         :integer          not null, primary key
#  archivo_id :integer
#  contenido  :binary(16777215)
#  created_at :datetime
#  updated_at :datetime
#

class ContenidoArchivo < ActiveRecord::Base
  belongs_to :archivo
  attr_accessible :contenido, :archivo
end
