class FiscalesExcel

  def self.importar(eleccion, fuerza, archivo, limpiar_fiscales)

    Fiscal.eleccion(eleccion).fuerza(fuerza).destroy_all if limpiar_fiscales
    worksheet = Spreadsheet.open(archivo.path).worksheet(0)
    total_fiscales = (worksheet.count - 1)
    objetos_con_error = []

    (1..total_fiscales).each do |index|
      row = worksheet.row(index)
      if row[0].present? && row[1].present? && row[2].present? && row[3].present? && row[4].present?
        begin
          renglon_padron = PadronApi.buscar_persona(row[0].to_s.strip, row[1].to_s.strip, row[2].to_s.strip)
          Persona.buscar_o_crear(renglon_padron, obtener_info_renaper: true)

          esta_en_lista = @esta_en_lista = !Fiscal.eleccion(eleccion).fuerza(fuerza).persona(row[0], row[1], row[2]).empty?

          if esta_en_lista
            objetos_con_error << { tipo_documento: row[0], dni: row[1], sexo: row[2], tipo_fiscal: row[3], comuna: row[4], codigo_establecimiento: row[5], 
                                   error: 'Ya esta incluido en la lista' }
          else
            fiscal = Fiscal.new(eleccion: eleccion, fuerza: fuerza, 
                                tipo_documento: row[0], 
                                numero_documento: row[1], 
                                sexo: row[2], 
                                tipo_cd: Fiscal.tipos[row[3].downcase],
                                comuna: row[4],
                                codigo_establecimiento: row[5])
            unless fiscal.save
              objetos_con_error << { tipo_documento: row[0], dni: row[1], sexo: row[2], tipo_fiscal: row[3], comuna: row[4], codigo_establecimiento: row[5], 
                                     error: fiscal.errors.full_messages.join(', ') }
            end
          end

        rescue PersonaNoExisteEnPadronError => error
          Rails.logger.info "La persona (#{row[1]}) que se desea importar como adhesion no existe en el padron."
          objetos_con_error << { tipo_documento: row[0], dni: row[1], sexo: row[2], tipo_fiscal: row[3], comuna: row[4], codigo_establecimiento: row[5], 
                                 error: error.message }
        end
      else
        objetos_con_error << { tipo_documento: row[0], dni: row[1], sexo: row[2], tipo_fiscal: row[3], comuna: row[4], codigo_establecimiento: row[5], 
                               error: 'Todos los campos son obligatorios (Tipo Documento, DNI, Sexo, Tipo Fiscal y Comuna)' }
      end
    end

    objetos_con_error
  end

  def self.excel_con_errores(objetos_con_error, fuerza)
    Spreadsheet.client_encoding = 'UTF-8'
    errores_xls = Spreadsheet::Workbook.new
    path = Rails.root.join('tmp', 'import_files_error', "errores_fiscales_#{fuerza.id}_#{DateTime.now.strftime('%d-%m_%H-%M')}.xls").to_s

    errores_sheet = errores_xls.create_worksheet name: 'Errores'
    errores_sheet.row(0).concat(['Tipo Documento', 'Numero Documento', 'Sexo', 'Tipo Fiscal', 'Comuna', 'Codigo Establecimiento', 'Errores'])

    objetos_con_error.each_with_index do |error, index|
      errores_sheet[(index + 1), 0] = error[:tipo_documento]
      errores_sheet[(index + 1), 1] = error[:dni]
      errores_sheet[(index + 1), 2] = error[:sexo]
      errores_sheet[(index + 1), 3] = error[:tipo_fiscal]
      errores_sheet[(index + 1), 4] = error[:comuna]
      errores_sheet[(index + 1), 5] = error[:codigo_establecimiento]
      errores_sheet[(index + 1), 6] = error[:error]
    end

    errores_xls.write(path)
    path
  end

  def self.exportar(eleccion, fuerza)
    Spreadsheet.client_encoding = 'UTF-8'
    archivo = Spreadsheet::Workbook.new

    hoja = archivo.create_worksheet name: 'Fiscales'
    hoja.row(0).concat(['Tipo Documento', 'Numero Documento', 'Sexo', 'Tipo Fiscal', 'Comuna', 'Codigo Establecimiento'])

    fiscales = Fiscal.eleccion(eleccion)
    fiscales = fiscales.fuerza(fuerza) if fuerza.present?

    fiscales.each_with_index do |fiscal, index|
      hoja[(index + 1), 0] = fiscal.tipo_documento
      hoja[(index + 1), 1] = fiscal.numero_documento
      hoja[(index + 1), 2] = fiscal.sexo
      hoja[(index + 1), 3] = fiscal.tipo.to_s.upcase
      hoja[(index + 1), 4] = fiscal.comuna
      hoja[(index + 1), 5] = fiscal.codigo_establecimiento
    end

    archivo
  end
end
