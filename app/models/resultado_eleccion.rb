# == Schema Information
#
# Table name: resultados_elecciones
#
#  id                :integer          not null, primary key
#  eleccion_cargo_id :integer
#  fuerza_id         :integer
#  cantidad_votos    :integer
#  created_at        :datetime
#  updated_at        :datetime
#  comuna_id         :integer
#  nulos             :boolean
#  en_blanco         :boolean
#

class ResultadoEleccion < ActiveRecord::Base

  has_paper_trail

  belongs_to :eleccion_cargo
  belongs_to :fuerza
  belongs_to :comuna

  has_many :eleccion_cargos
  # has_many :candidaturas, -> (record) { where('candidaturas.eleccion_id = ? AND candidaturas.cargo_id = ?', record.eleccion_cargo.eleccion_id, record.eleccion_cargo.cargo_id) }, through: :fuerza

  attr_accessible :cantidad_votos, :fuerza, :eleccion_cargo, :comuna, :nulos, :en_blanco

  validates :cantidad_votos, numericality: true, allow_nil: true

  scope :por_eleccion_cargo, -> (eleccion_cargo) { where(eleccion_cargo_id: eleccion_cargo.id) }
  scope :por_fuerza, -> (fuerza) { where(fuerza_id: fuerza.id) }
  scope :por_comuna, -> (comuna) { (comuna.nil? ? where('comuna_id IS NULL') : where(comuna_id: comuna.id)) }

  def candidaturas
    Candidatura.por_fuerza(fuerza).where('candidaturas.eleccion_id = ? AND candidaturas.cargo_id = ?', eleccion_cargo.eleccion_id, eleccion_cargo.cargo_id)
  end

  def votos_cargados?
    !cantidad_votos.nil?
  end

end
