# == Schema Information
#
# Table name: elecciones
#
#  id                              :integer          not null, primary key
#  descripcion                     :string(255)
#  fecha                           :date
#  notas                           :text(16777215)
#  observaciones                   :text(16777215)
#  created_at                      :datetime
#  updated_at                      :datetime
#  archivo_id                      :integer
#  expediente                      :string(255)
#  fecha_posesion                  :date
#  porcentaje_exclusion            :decimal(10, )
#  tipo                            :string(255)
#  eleccion_relacionada_id         :integer
#  fecha_limite_carga_candidaturas :datetime
#  habilitar_carga_candidaturas    :boolean
#

class Eleccion < ActiveRecord::Base
  after_create :asignar_paso_creando_general

  attr_accessible :descripcion, :fecha, :fecha_posesion, :notas, :observaciones,
                  :archivo, :eleccion_cargos_attributes, :expediente,
                  :cantidad_mesas, :cantidad_inscriptos, :porcentaje_exclusion,
                  :resultados_elecciones_attributes, :resultados_comunas_attributes,
                  :tipo, :eleccion_relacionada_id, :archivo_id, :fecha_limite_carga_candidaturas,
                  :habilitar_carga_candidaturas

  TIPOS = { paso: 'paso', general: 'general' }

  TIPOS.values.each do |tipo|
    define_method("#{tipo}?") do
      self.tipo == tipo
    end

    define_method(tipo) do
      self.tipo == tipo ? self : eleccion_relacionada
    end
  end

  has_many :fuerza_elecciones
  has_many :cargos, through: :eleccion_cargos
  has_many :eleccion_cargos
  has_many :resultados_elecciones, through: :eleccion_cargos
  has_many :resultados_comunas, class_name: 'ResultadoComuna', foreign_key: 'eleccion_id'
  has_many :listas
  has_many :candidaturas, through: :listas
  has_many :afiliaciones

  belongs_to :archivo
  has_one :eleccion, class_name: 'Eleccion', foreign_key: :eleccion_relacionada_id
  belongs_to :eleccion_relacionada, class_name: 'Eleccion'

  has_paper_trail

  validates :descripcion, :fecha_posesion, :fecha, presence: true
  validates :porcentaje_exclusion, :cantidad_inscriptos, :cantidad_mesas, numericality: true, allow_blank: true
  validate  :verificar_fecha_limite_carga_candidatura

  accepts_nested_attributes_for :eleccion_cargos, allow_destroy: true
  accepts_nested_attributes_for :resultados_elecciones
  accepts_nested_attributes_for :resultados_comunas

  # Filter scopes
  scope :con_resultados, -> { joins('INNER JOIN eleccion_cargos ON eleccion_cargos.eleccion_id = elecciones.id').joins('INNER JOIN resultados_elecciones ON resultados_elecciones.eleccion_cargo_id = eleccion_cargos.id').uniq }
  scope :por_tipo, -> (tipo) { where(tipo: tipo) }

  def verificar_fecha_limite_carga_candidatura
    
    if (tipo.blank? || paso?) && (fecha_limite_carga_candidaturas.blank? || fecha <= fecha_limite_carga_candidaturas)
      errors.add(:fecha_limite_carga_candidaturas, 'No debe estar en Blanco y no puede ser mayor a la fecha de la elección') 
    end
  end

  def self.actual
    # FIXME: ver como poder identificar eleccion actual en base a un estado mas que por
    # la primera odenando por fecha
    Eleccion.por_tipo('paso').order('fecha DESC').first
  end

  def anio
    fecha.year
  end

  def cantidad_mesas
    resultados_comunas.to_a.sum { |rc| rc.cantidad_mesas.nil? ? 0 : rc.cantidad_mesas }
  end

  def cantidad_inscriptos
    resultados_comunas.to_a.sum { |rc| rc.cantidad_inscriptos.nil? ? 0 : rc.cantidad_inscriptos }
  end

  def resultados_completos?
    porcentaje_exclusion.present? && cantidad_mesas.present? && cantidad_inscriptos.present? && eleccion_cargos.select(&:seleccionado?).all?(&:resultados_completos?)
  end

  def borrar_reparto
    resultados_elecciones.each(&:destroy)
    resultados_comunas.delete_all
    eleccion_cargos.each do |ec|
      ec.cantidad_escanios = nil
      ec.save

      ec.desasignar_escanios
    end
    self.porcentaje_exclusion = nil
    save
  end

  def primer_eleccion_cargo
    eleccion_cargos.select(&:seleccionado?).first
  end

  def asignar_escanios
    candidaturas_ganadoras = []
    eleccion_cargos.each { |ec| candidaturas_ganadoras += ec.asignar_escanios if ec.seleccionado? }
    candidaturas_ganadoras
  end

  def inicializar_resultados
    self.porcentaje_exclusion = 0
    Comuna.all.each do |comuna|
      inicializar_resultados_votos(comuna)
      resultados_comunas.create(eleccion: self, comuna: comuna, cantidad_mesas: 0, cantidad_inscriptos: 0)
    end
    inicializar_resultados_votos
  end

  def inicializar_resultados_votos(comuna = nil)
    eleccion_cargos.select(&:seleccionado?).each do |ec|
      fuerzas.each do |f|
        r = f.resultado(ec, comuna)
        r.save if r.new_record?
      end
      # Un resultado más para los votos nulos, y otro en blanco
      ResultadoEleccion.create(eleccion_cargo: ec, comuna: comuna, en_blanco: true)
      ResultadoEleccion.create(eleccion_cargo: ec, comuna: comuna, nulos: true)
    end
  end

  def nombres_archivos_expedientes
    nombres = []

    unless expediente.nil?
      expediente.split(',').each do |num_expe|
        partes = num_expe.split('/')
        nombres << "Expe-#{partes[0].to_i}-#{partes[1]}.pdf"
      end
    end

    nombres
  end

  def posee_candidatura?(persona)
    candidaturas.find_by(persona: persona, fecha_renuncia: nil).present?
  end

  def fuerzas
    fuerzas_eleccion = []
    fuerzas_eleccion += Fuerza.where(tipo: 'ALIANZA', eleccion_id: id)
    # fuerzas_eleccion += Fuerza.joins(listas: :candidaturas).where("fuerzas.tipo = 'PARTIDO' AND candidaturas.eleccion_id = ?", id).uniq
    general_id = general.id
    paso_id = paso.id
    fuerzas_eleccion += Fuerza.joins(:listas).where("fuerzas.tipo = 'PARTIDO' AND (listas.eleccion_id = ? OR listas.eleccion_id = ?)", general_id, paso_id).uniq
    fuerzas_eleccion
  end

  def vigente?
    fecha >= Date.today
  end

  def tiempo_pendiente(usuario_junta_partidaria)
    if paso?
      if usuario_junta_partidaria
        (((fecha_limite_carga_candidaturas + SETTINGS[:eleccion][:adicional_limite_carga_candidaturas].hours )- DateTime.now) / 3600).to_i
      else
        ((fecha_limite_carga_candidaturas - DateTime.now) / 3600).to_i
      end
    end
  end

  def calcular_fecha_limite(usuario_junta_partidaria)
    if paso?
      if usuario_junta_partidaria
        fecha_limite_carga_candidaturas + SETTINGS[:eleccion][:adicional_limite_carga_candidaturas].hours
      else
        fecha_limite_carga_candidaturas
      end
    end
  end

  def cargos_seleccionados
    Cargo.joins(:eleccion_cargos).where('eleccion_cargos.seleccionado = ? AND eleccion_cargos.eleccion_id = ?', true, id)
  end

  def tiene_resolucion?
    archivo.present?
  end

  def adjuntar_archivo(subido)
    archivo = Archivo.new
    archivo.nombre = File.basename(subido.original_filename)
    archivo.tipo_contenido = subido.content_type
    archivo.save!

    self.archivo = archivo
    save

    # Se asigna por separado para que ya tenga un ID para la fk
    archivo.contenido = subido.read
  end

  def asignar_cargos(cargos)
    eleccion_cargos << cargos.map { |c| EleccionCargo.new(cargo: c, eleccion: self) }
  end

  private

  def asignar_paso_creando_general
    if tipo.blank? || paso?
      self.tipo = TIPOS[:paso]
      build_eleccion(attributes.except('id', 'created_at', 'updated_at').merge(tipo: 'general', fecha: fecha.try(:+, 30.day), fecha_limite_carga_candidaturas: nil))
      eleccion_cargos.each { |cargo| eleccion.eleccion_cargos << cargo.dup }
      eleccion.save!
      self.eleccion_relacionada_id = eleccion.id
      save!
    end
  end
end
