# encoding: utf-8
# == Schema Information
#
# Table name: renglones_padron
#
#  id             :integer          not null, primary key
#  padron_id      :integer
#  clase          :string(255)
#  apellido       :string(255)
#  nombre         :string(255)
#  profesion      :string(255)
#  domicilio      :string(255)
#  analfabeto     :string(255)
#  tipo_documento :string(255)
#  seccion        :string(255)
#  circuito       :string(255)
#  mesa           :string(255)
#  sexo           :string(255)
#  desfor         :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  matricula      :integer
#

class RenglonPadron < ActiveRecord::Base
  belongs_to :padron
  attr_accessible :analfabeto, :apellido, :circuito, :clase, :desfor, :domicilio, :matricula, :mesa, :nombre, :profesion, :seccion, :sexo, :tipo_documento, :padron

  # Filter scopes
  scope :matricula, -> (num) { where('matricula = ?', num) }
  scope :nombre, -> (nom) { where('nombre like ?', "%#{nom}%") }
  scope :apellido, -> (ape) { where('apellido like ?', "%#{ape}%") }

  MATRICULA = (0..7)
  CLASE = (8..11)
  APELLIDO = (12..51)
  NOMBRE = (52..91)
  PROFESION = (92..98)
  DOMICILIO = (99..132)
  ANALFABETO = (133..133)
  TIPO_DOCUMENTO = (134..140)
  SECCION = (141..145)
  CIRCUITO = (146..150)
  MESA = (151..154)
  SEXO = (155..155)
  DESFOR = (156..156)

  def self.filtrar(attributes)
    supported_filters = [:matricula, :nombre, :apellido]
    attributes.slice(*supported_filters).inject(self) do |scope, (key, value)|
      value.present? ? scope.send(key, value) : scope
    end
  end

  def self.parse_to_hash(line)
    if line.present?
      linea = line.split('|')

      [:matricula, :clase, :apellido, :nombre, :profesion, :domicilio, :analfabeto, :tipo_documento, :seccion, :circuito, :mesa, :sexo, :desfor].each_with_index.inject({}) do |hash, pair|
        attr, index = pair
        hash[attr] = (attr == :matricula ? linea[index].to_i : linea[index].strip) if linea[index].present?
        hash
      end
    end
  end

end
