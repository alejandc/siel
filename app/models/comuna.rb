# == Schema Information
#
# Table name: comunas
#
#  id          :integer          not null, primary key
#  numero      :integer
#  nombre      :string(255)
#  descripcion :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class Comuna < ActiveRecord::Base

  has_paper_trail

  has_many :eleccion_cargos
  has_many :resultados_elecciones

  attr_accessible :descripcion, :nombre, :numero

end
