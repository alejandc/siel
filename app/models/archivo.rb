# == Schema Information
#
# Table name: archivos
#
#  id             :integer          not null, primary key
#  nombre         :string(255)
#  tipo_contenido :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  descripcion    :string(255)
#

class Archivo < ActiveRecord::Base
  has_paper_trail

  attr_accessible :nombre, :tipo_contenido
  has_many :contenido_archivos, dependent: :destroy

  def contenido=(datos)
    real_level = Rails.logger.level
    Rails.logger.level = Logger::ERROR # Lo apagamos para que no loguee el contenido binario

    # Dividir sus bytes en partes de acuerdo al parámetro
    datos.bytes.each_slice(BYTES_SEGMENTO) do |slice|
      # Pero guardarlo como string, porque así lo usa Ruby
      contenido = ContenidoArchivo.new(archivo: self, contenido: slice.pack('c*'))
      contenido.save!
    end

    Rails.logger.level = real_level
  end

  def contenido
    contenido_archivos.map(&:contenido).join
  end

end
