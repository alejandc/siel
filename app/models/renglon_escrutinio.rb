# == Schema Information
#
# Table name: renglon_escrutinios
#
#  id                        :integer          not null, primary key
#  cantidad_votos            :integer
#  codigo_cargo              :string(255)
#  codigo_partido            :integer
#  descripcion_partido       :string(255)
#  descripcion_cargo         :string(255)
#  codigo_seccion            :integer
#  codigo_circuito           :integer
#  numero_mesa               :integer
#  sexo_mesa                 :string(255)
#  importacion_escrutinio_id :integer
#  cargo_id                  :integer
#  contable_type             :string(255)
#  contable_id               :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  codigo_cargo_eleccion     :integer
#

class RenglonEscrutinio < ActiveRecord::Base
  self.table_name = 'renglon_escrutinios'

  CODIGOS_VOTOS = { blanco: 990, nulo: 991, recurrido: 992, impugnado: 993 }

  belongs_to :importacion_escrutinio

  validates :cantidad_votos, :codigo_cargo, :codigo_partido, :descripcion_partido, :descripcion_cargo, :codigo_seccion, :numero_mesa, :sexo_mesa, :importacion_escrutinio_id, :codigo_cargo_eleccion, presence: true

  scope :importacion_escrutinio, -> (importacion_escrutinio) { where(importacion_escrutinio: importacion_escrutinio) }
  scope :por_listas, -> (listas_ids) { where('codigo_partido IN (?)', listas_ids) }
  scope :con_codigo, -> (codigo_voto) { where(codigo_partido: RenglonEscrutinio::CODIGOS_VOTOS[codigo_voto]) }
  scope :sin_codigo, -> (codigo_voto) { where.not('codigo_partido = ?', RenglonEscrutinio::CODIGOS_VOTOS[codigo_voto]) }
  scope :total_votos, -> { sum(:cantidad_votos) }
  scope :total_votos_codigo, -> (codigo_voto) { con_codigo(codigo_voto).total_votos }
  scope :votos_positivos, -> { sin_codigo(:blanco).sin_codigo(:nulo).sin_codigo(:recurrido).sin_codigo(:impugnado) }
  scope :sin_partidos_asociados, -> { joins('LEFT OUTER JOIN listas ON renglon_escrutinios.codigo_partido = listas.numero').votos_positivos.where('listas.numero IS NULL') }
  scope :select_partido, -> { select('DISTINCT codigo_partido, descripcion_partido') }
  scope :agrupados_partido_lista_cargo, -> (cargo) { where(codigo_cargo: cargo).group(:codigo_partido) }
  scope :agrupados_partido_y_cargo, -> { group(:codigo_partido, :codigo_cargo) }
  scope :agrupados_comuna, -> { where(codigo_cargo: 'JC').group(:codigo_partido, :codigo_seccion) }
  scope :por_cargo, -> (cargo) { where(codigo_cargo: cargo) }

end
