# == Schema Information
#
# Table name: adhesiones
#
#  id                   :integer          not null, primary key
#  persona_id           :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  validaciones         :text(16777215)
#  lista_cargo_id       :integer
#  adhesiones_multiples :boolean          default(FALSE)
#

class Adhesion < ActiveRecord::Base
  has_paper_trail meta: { lista_cargo_id: :lista_cargo_id }, on: [:create, :destroy]
  paginates_per 30

  after_initialize :crear_validador

  serialize :validaciones, Hash

  attr_accessor :validador

  attr_accessible :lista_cargo, :lista_cargo_id, :persona, :persona_id, :validaciones, :adhesiones_multiples

  belongs_to :lista_cargo, counter_cache: true
  belongs_to :persona
  has_one :lista, through: :lista_cargo
  has_one :eleccion, through: :lista
  has_one :cargo, through: :lista_cargo
  has_one :domicilio, through: :persona

  scope :lista_cargo, -> (lista_cargo) { where(lista_cargo: lista_cargo) }
  scope :cargo, -> (c) { joins(:lista_cargo).where('lista_cargos.cargo_id = ?', c) }
  scope :orden_creacion, -> { order('adhesiones.created_at DESC') }
  scope :adhesiones_multiples, -> { where(adhesiones_multiples: true) }
  scope :en_listas_abiertas, -> { joins(:lista_cargo).where('lista_cargos.estado_adhesiones_cd <> ?', ListaCargo.estado_adhesiones[:cerrada]) }
  scope :en_otra_lista_mismo_cargo, -> (lista_cargo, persona) { joins(lista_cargo: :lista).where('eleccion_id = ? AND lista_cargos.id <> ? AND lista_cargos.cargo_id = ? AND persona_id = ?', lista_cargo.lista_eleccion_id, lista_cargo.id, lista_cargo.cargo_id, persona.id) }
  scope :join_personas, -> { joins('INNER JOIN personas ON personas.id = adhesiones.persona_id ') }

  # Estos scopes deberán ser llamados como precondición de hacer el join con personas
  scope :numero_documento, -> (numero_documento) { where('personas.numero_documento LIKE ?', "%#{numero_documento}%") }
  scope :nombre, -> (nombre) { where('lower(personas.nombres) LIKE lower(?)', "%#{nombre}%") }
  scope :apellido, -> (apellido) { where('lower(personas.apellidos) LIKE lower(?)', "%#{apellido}%") }
  scope :adhesiones_afiliadas, -> (eleccion_id, fuerzas_codigos) { joins('INNER JOIN afiliaciones ON afiliaciones.tipo_documento = personas.tipo_documento AND afiliaciones.numero_documento = personas.numero_documento AND afiliaciones.sexo = personas.sexo').where('afiliaciones.eleccion_id = ? AND afiliaciones.fuerza_codigo IN (?)', eleccion_id, fuerzas_codigos) }
  scope :adhesiones_conflicto_comuna, -> (comuna) { joins(:cargo).joins('INNER JOIN domicilios ON domicilios.id = personas.domicilio_id').where('comuna <> ?', comuna) }
  scope :adhesiones_observados, -> (comuna) { joins(:cargo).joins('INNER JOIN domicilios ON domicilios.id = personas.domicilio_id').where('comuna <> ? OR adhesiones_multiples = ?', comuna, true) }

  validates :lista_cargo_id, :persona_id, presence: true

  validate :cumple_maximo, on: :create

  accepts_nested_attributes_for :persona

  delegate :tipo_documento, :numero_documento, :nombres, :apellidos, :sexo, :domicilio_comuna, :domicilio_completo, :domicilio_calle_nombre, to: :persona, prefix: true
  delegate :eleccion_id, :fuerza_id, to: :lista_cargo
  delegate :estado_candidaturas_sin_validar!, :cambiar_estado_candidaturas_sin_validar!, :estado_adhesiones_sin_validar!, :cambiar_estado_adhesiones_sin_validar!, to: :lista_cargo, prefix: true
  delegate :observaciones, :validar_afiliacion, to: :validador
  delegate :numero_comuna, to: :cargo, prefix: true
  delegate :reabierta?, to: :lista, prefix: true
  delegate :habilitar_carga_candidaturas, to: :eleccion, prefix: true

  def cumple_maximo
    errors.add(:cumple_maximo, "El cupo máximo de #{cargo.cantidad_maxima_adherentes} adherentes ya esta completo") unless lista_cargo.cumple_maximo_adherentes?
  end

  def crear_validador
    self.validador = Validators::ValidadorAdhesion.new(self)
  end

  def asignar_adhesiones_multiples!(opciones)
    adhesiones = Adhesion.en_otra_lista_mismo_cargo(lista_cargo, persona)
    multiple = opciones[:al] == :crear ? !adhesiones.empty? : adhesiones.count > 1
    update_attribute(:adhesiones_multiples, multiple) if opciones[:al] != :eliminar
    adhesiones.en_listas_abiertas.each { |adhesion| adhesion.update_attributes!(adhesiones_multiples: multiple) }
  end

  def afiliado?
    !Afiliacion.where('tipo_documento = ? AND numero_documento = ? AND sexo = ? AND fuerza_codigo IN (?) AND eleccion_id = ?', persona_tipo_documento, persona_numero_documento, persona_sexo, Fuerza.partidos_en_alianza_con_fuerza(lista.eleccion_id, lista.fuerza_id).map(&:numero), lista_cargo.lista_eleccion_id).empty?
  end

  def pertenece_a_comuna?
    cargo_numero_comuna == persona_domicilio_comuna.to_i
  end

  class << self
    def referencia_job_importacion
      'importar_adherentes_job_id'
    end

    def calculo_incremento_adhesiones_multiples!
      ActiveRecord::Base.connection.execute(%(
                                              UPDATE adhesiones a1
                                              JOIN   lista_cargos lc1   ON a1.lista_cargo_id = lc1.id
                                              JOIN   listas l1          ON lc1.lista_id = l1.id
                                              JOIN   adhesiones a2      ON a1.persona_id = a2.persona_id
                                              JOIN   lista_cargos lc2   ON a2.lista_cargo_id = lc2.id
                                              JOIN   listas l2          ON lc2.lista_id = l2.id
                                              SET    a1.adhesiones_multiples = 1, lc1.estado_adhesiones_cd = #{ListaCargo.estado_adhesiones[:sin_validar]}
                                              WHERE  l1.eleccion_id = l2.eleccion_id
                                              AND    lc1.cargo_id = lc2.cargo_id
                                              AND    lc1.lista_id <> lc2.lista_id
                                              AND    lc1.estado_adhesiones_cd <> #{ListaCargo.estado_adhesiones[:cerrada]};
                                            ).gsub(/\s+/, ' ').strip)
    end

    def asignar_adhesiones_multiples_default!
      ActiveRecord::Base.connection.execute(%(
                                              UPDATE adhesiones a1
                                              JOIN   lista_cargos lc1 ON a1.lista_cargo_id = lc1.id
                                              SET    a1.adhesiones_multiples = 0
                                              WHERE  lc1.estado_adhesiones_cd <> #{ListaCargo.estado_adhesiones[:cerrada]};
                                            ).gsub(/\s+/, ' ').strip)
    end
  end

end
