class BitacoraExcel
  def self.exportar(lista_cargo, versiones)
    Spreadsheet.client_encoding = 'UTF-8'
    archivo = Spreadsheet::Workbook.new
    negrita = Spreadsheet::Format.new(weight: :bold)
    path = Rails.root.join('tmp', 'import_files_error', "bitacora_#{lista_cargo.id}_#{DateTime.now.strftime('%d-%m_%H-%M')}.xls").to_s

    hoja = archivo.create_worksheet name: 'Bitácora'
    hoja.row(0).concat(['Autor', 'Fecha', 'Acción', 'Cambios'])

    (0..3).each { |index| hoja.row(0).set_format(index, negrita) }

    VersionListaCargoDecorator.decorate_collection(versiones).each_with_index do |version, index|
      hoja[(index + 1), 0] = version.whodunnit
      hoja[(index + 1), 1] = version.created_at.strftime('%d/%m/%Y %H:%M')
      hoja[(index + 1), 2] = version.accion
      hoja[(index + 1), 3] = version.cambios.gsub(/<((\/?)strong|(\/?)em|br)>/, '')
    end

    archivo.write(path)
    path
  end
end
