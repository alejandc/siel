# == Schema Information
#
# Table name: fuerza_elecciones
#
#  id          :integer          not null, primary key
#  umbral      :float(24)
#  fuerza_id   :integer
#  eleccion_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class FuerzaEleccion < ActiveRecord::Base
  attr_accessible :eleccion_id, :fuerza_id, :umbral

  belongs_to :eleccion
  belongs_to :fuerza

  delegate :descripcion, to: :eleccion, prefix: true

  scope :eleccion, -> (eleccion) { where(eleccion: eleccion) }
end
