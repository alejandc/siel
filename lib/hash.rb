class Hash
  def valores_hash_multidimencional(key)
    valores = []
    valores << self[key]

    values.each do |valor_hash|
      unless valor_hash.is_a? Array
        valores_hash = [valor_hash]
        valores_hash.each do |valor|
          valores += valor.valores_hash_multidimencional(key) if valor.is_a? Hash
        end
      end
    end
    valores.compact
  end

  def except_nested_key(key)
    copy = Marshal.load(Marshal.dump(self))

    copy.each { |(k, v)| v.delete(key) if v.is_a? Hash }
  end
end
