﻿module ControllerAuthorizer

  @@base_access = []
  @@controller_access = {}

  def self.included(base)
    # base.before_filter :check_groups

    def base.allow_base_access_to(groups)
      @@base_access = groups
    end

    def base.allow_access_to(groups)
      @@controller_access[to_s] = groups
    end
  end

  private

  def check_groups
    controller_access = @@controller_access[self.class.to_s] || @@base_access

    unless user_groups.nil? || user_groups.empty? || controller_access.empty?
      controller_access.each do |group|
        return true unless user_groups[group].nil?
      end

      render file: 'public/401', status: :unauthorized, layout: false
    end
  end

end
