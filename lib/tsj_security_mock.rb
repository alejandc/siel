class TsjSecurityMock

  USUARIOS = [
    { username: 'usuario_mock', password: '123siel', groups: %w(grupo1 grupo2) }
  ]

  def self.obtener_usuario(username)
    USUARIOS.detect { |usuario| usuario[:username] == username }
  end

  def self.authenticate(username, password)
    usuario = obtener_usuario(username)
    (usuario.present? && usuario[:password] == password).to_s
  end

  def self.groups(username)
    obtener_usuario(username).try(:[], :groups).try(:join, ', ')
  end

  def self.validate(username)
    obtener_usuario(username)
  end
end
