require 'tsj_security_error'

class TsjSecurity
  require 'rest_client'
  require 'httparty'
  require 'crack'
  require 'crack/xml'

  def self.validate(username)
    ws_url = TSJ_AUTH_WS['URL'] + "/xml/#{username}"

    begin
      response = RestClient.get(ws_url)
    rescue Exception => e
      display_error = "Error de comunicación con el web service de identidad (#{e.message})"
      raise TsjSecurityError.new(display_error), display_error
    end

    response_tag = TSJ_AUTH_WS['exists_response_tag']
    result_tag = TSJ_AUTH_WS['exists_result_tag']

    (Crack::XML.parse(response)[response_tag][result_tag] == 'true')
  end

  # This methods params (TSJ_AUTH_WS) can be found in each environment config files (development.rb, etc.)
  def self.authenticate(username, password)
    ws_url = TSJ_AUTH_WS['URL'] + "/xml/#{username}/#{password}"

    begin
      response = RestClient.get(ws_url)
    rescue Exception => e
      display_error = "Error de comunicación con el web service de identidad (#{e.message})"
      raise TsjSecurityError.new(display_error), display_error
    end

    response_tag = TSJ_AUTH_WS['auth_response_tag']
    result_tag = TSJ_AUTH_WS['auth_result_tag']

    Crack::XML.parse(response)[response_tag][result_tag]
  end

  def self.groups(username)
    ws_url = TSJ_AUTH_WS['URL']

    begin
      response = RestClient.get(ws_url + "/groups/#{username}")
    rescue Exception => e
      display_error = "Error de comunicación con el web service de identidad (#{e.message})"
      raise TsjSecurityError.new(display_error), display_error
    end

    response_tag = TSJ_AUTH_WS['groups_response_tag']
    result_tag = TSJ_AUTH_WS['groups_result_tag']

    Crack::XML.parse(response)[response_tag][result_tag]
  end

end
