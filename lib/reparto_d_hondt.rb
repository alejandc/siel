﻿class RepartoDHondt
  class << self

    def generar_listas_eleccion_general(importacion_escrutinio, fuerza, cargo)
      exitoso = true

      ActiveRecord::Base.transaction do
        begin
          if fuerza.present? && cargo.present?
            lista_sugerida = RepartoDHondt.lista_sugerida(importacion_escrutinio, fuerza, cargo)
            eleccion_general = importacion_escrutinio.eleccion.general

            # Se genera la lista si no existe
            lista = fuerza.listas.por_eleccion(eleccion_general).empty? ? fuerza.listas.new(nombre: fuerza.nombre, eleccion: eleccion_general) : fuerza.listas.por_eleccion(eleccion_general).first

            # Se le agregan los cargos habilitados si ya están asignados algunos cargos
            lista.asignar_cargos_habilitados(lista.data_info.keys.map(&:to_s) | [Cargo::CARGOS.key(cargo.nombre).to_s])
            lista.build_lista_cargos unless lista.persisted?
            lista.save

            lista_cargo = lista.lista_cargos.cargos([cargo.nombre]).first
            lista_cargo.candidaturas.destroy_all
            ListaCargo.reset_counters(lista_cargo.id, :candidaturas)
            lista_cargo.reload

            lista_sugerida.each do |candidatura|
              candidatura_a_agregar = lista_cargo.crear_candidatura(candidatura[:candidatura].persona)
              candidatura_a_agregar.assign_attributes(lista_cargo_paso_id: candidatura[:candidatura].lista_cargo.id)
              candidatura_a_agregar.save
            end
          end
        rescue => e
          exitoso = false
          raise ActiveRecord::Rollback
        end
      end

      exitoso
    end

    def lista_sugerida(importacion_escrutinio, fuerza, cargo)
      eleccion = importacion_escrutinio.eleccion
      umbral = fuerza.fuerza_elecciones.eleccion(eleccion).first.umbral.to_f

      listas_con_totales = importacion_escrutinio.listas_con_totales(fuerza, cargo).select { |lista| lista[:porcentaje].to_f >= umbral.to_f }

      if !listas_con_totales.empty?
        cargo.cantidad_maxima.times.inject([]) do |lista_sugerida, posicion|
          break lista_sugerida if listas_con_totales.empty?
          posicion += 1
          candidatura_con_totales = obtener_candidato(posicion, lista_sugerida, listas_con_totales)

          lista_sugerida.push(candidatura_con_totales)
        end
      else
        []
      end
    end
    
    def lista_ganadora(importacion_escrutinio, cargo)
      eleccion = importacion_escrutinio.eleccion
      listas_con_totales = importacion_escrutinio.lista_ganadora(cargo)

      if !listas_con_totales.empty?
        cargo.cantidad_maxima.times.inject([]) do |lista_sugerida, posicion|
          break lista_sugerida if listas_con_totales.empty?
          posicion += 1
          candidatura_con_totales = obtener_candidato(posicion, lista_sugerida, listas_con_totales)

          lista_sugerida.push(candidatura_con_totales)
        end
      else
        []
      end
    end

    def obtener_candidato(posicion, lista_sugerida, listas_con_totales)
      lista_con_totales = listas_con_totales.first

      # obtener primer candidatura que cumpla con el sexo
      candidatura_seleccionada = lista_con_totales[:candidaturas].detect { |candidatura| sexo_requerido(lista_sugerida).include?(candidatura.persona_sexo) }

      # elimino al candidatura seleccionado de la lista de candidaturas
      lista_con_totales[:candidaturas].delete(candidatura_seleccionada)

      # generamos el hash con el candidatura, cociente y divisor
      candidatura_sugerida_con_totales = {
        posicion: posicion,
        candidatura: candidatura_seleccionada,
        lista: lista_con_totales[:lista],
        divisor: lista_con_totales[:divisor],
        cociente: lista_con_totales[:resto_total_votos],
        total_votos: lista_con_totales[:total_votos]
      }

      # incrementamos el divisor
      lista_con_totales[:divisor] += 1

      # aplicar divisor a la cantidad de votos de la lista con totales
      lista_con_totales[:resto_total_votos] = lista_con_totales[:total_votos].to_f / lista_con_totales[:divisor].to_f

      # quitar las listas que no tienen más candidaturas y reordenar listas con totales por cantidad de votos
      listas_con_totales.reject! { |lct| lct[:candidaturas].empty? }
      listas_con_totales.sort! { |primer_lista, segunda_lista| segunda_lista[:resto_total_votos] <=> primer_lista[:resto_total_votos] }

      candidatura_sugerida_con_totales
    end

    def sexo_requerido(lista_sugerida)
      if lista_sugerida.size >= 2 && lista_sugerida[-2][:candidatura].persona_sexo == lista_sugerida[-1][:candidatura].persona_sexo
        lista_sugerida[-1][:candidatura].persona_sexo == 'M' ? 'F' : 'M'
      else
        %w(M F)
      end
    end

  end

end
