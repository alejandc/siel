source 'http://rubygems.org'

gem 'rails', '~> 4.2.5'

group :development do
  gem 'rails-erd'
  gem 'pry'
  gem 'pry-byebug'
  gem 'rubocop'
  gem 'overcommit'
  gem 'annotate'
  gem 'capistrano', '2.15.5'
  gem 'capistrano-sidekiq'
  gem 'rack-mini-profiler'
end

group :test do
  gem 'simplecov', require: false
  gem 'minitest-reporters'
end

gem 'mysql2'
gem 'sqlite3'
gem 'ensure-encoding'
gem 'roo'
gem 'spreadsheet'
gem 'ntlm-sso'
gem 'paper_trail'
gem 'consulta_expedientes', '1.0.1'
gem 'remotipart'
gem 'momentjs-rails', '>= 2.9.0'
gem 'bootstrap3-datetimepicker-rails', '~> 4.17.43'
gem 'prawn', '~> 2.1.0'
gem 'prawn-table'
gem 'factory_girl_rails', '~> 4.0'
gem 'validates_timeliness', '~> 3.0'
gem 'bcrypt-ruby', '~> 3.1.2'
gem 'unicode_utils'
gem 'net-ssh', '< 2.8.0' # La versión 2.8.0 hace que no funcione la autenticación con Capistrano por SSH
gem 'mocha'
gem 'cancancan'
gem 'sidekiq'
gem 'sidekiq-status'
gem 'sinatra', require: false
gem 'redis-namespace'
gem 'activerecord-session_store', github: 'rails/activerecord-session_store'
gem 'simple_enum'
gem 'httparty'
gem 'rest_client'
gem 'crack'
gem 'htmltoword'
gem 'best_in_place', '~> 3.0.1'
gem 'pdfkit'
gem 'render_anywhere'
gem 'wkhtmltopdf-binary'
gem 'excon'

# Graylog
gem 'gelf'
gem 'lograge'

# Email Error Notifications
gem 'exception_notification'

# Paginacion
gem 'kaminari'

# Decorator
gem 'draper'

# Paperclip
gem 'paperclip', '~> 5.0.0'

# Observers
gem 'rails-observers'

# Searches
gem 'searchlight'

# Batch insert
gem 'activerecord-import'

gem 'sass-rails',   '~> 4.0.0'
gem 'uglifier', '>= 1.3.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', :platforms => :ruby

gem 'jquery-rails'

# Gemas temporales por compatibilidad para upgrade a Rails 4.0.0
gem 'protected_attributes'

gem 'bootstrap-glyphicons'
gem 'font-awesome-rails'
